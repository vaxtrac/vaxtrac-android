package com.vaxtrac.android.vaxtracapp.biometrics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;

import bmtafis.simple.*;

public class Engine {

	private float threshold;
	private Context mContext;
	
	private final String TAG = "AFISEngine";
	private AfisEngine mEngine;
	private double MATCH_THRESOLD = 70.0; //This is a little low if you want FMR 0 [78 or so]. Great for minimal FRR
	private double VERIFY_THRESHOLD = 20.0;

	public static boolean is_ready = true;

    public Engine(Context mContext){
        this(mContext, 25.0f);
    }

	public Engine(Context mContext, float threshold) {
		this.threshold = threshold;
		this.mContext = mContext;
        mEngine = new AfisEngine(mContext);
		mEngine.setThreshold(threshold);
        Log.i(TAG,"Engine Started");
	}

	public double verify(String hex_template_1, Person p2){
		Person p1 = new Person();
		final Fingerprint f1 = new Fingerprint();
		f1.setFinger(Finger.ANY);
		f1.setIsoTemplate(hexStringToByteArray(hex_template_1));
		return verify(p1,p2);
	}

    //Get a score for two hex encoded string iso templates
	public double verify(String hex_template_1, String hex_template_2){
		return verify(hexStringToByteArray(hex_template_1), hexStringToByteArray(hex_template_2));
	}

    public boolean verifyBoolean(String hex_template_1, String hex_template_2){
        double score;
        try {
            score = verify(hex_template_1, hex_template_2);
        }catch(Exception e){
            Log.e(TAG, String.format("Error matching templates:\nT1:%s\nT2:%s\nE:%s", hex_template_1, hex_template_2, e.toString()));
            return false;
        }
        Log.d(TAG, String.format("VerifyBoolean Score:%s",score));
        if (score>=VERIFY_THRESHOLD){
            return true;
        }
        return false;
    }
	
	public double verify(byte[] template_1, byte[] template_2){
		Person p1 = new Person();
		Person p2 = new Person();
		final Fingerprint f1 = new Fingerprint();
		final Fingerprint f2 = new Fingerprint();
		f1.setIsoTemplate(template_1);
		f2.setIsoTemplate(template_2);
		p1.setFingerprints(new ArrayList<Fingerprint>(){{add(f1);}});
		p2.setFingerprints(new ArrayList<Fingerprint>(){{add(f2);}});
		mEngine.setThreshold(0.0f);
        double score = mEngine.verify(p1, p2);
        mEngine.setThreshold(threshold);
        return score;
	}
	
	public double verify(Person p1, Person p2){
		return mEngine.verify(p1, p2);
	}


    public List<Person> identify(Person person, List<Person> gallery){
        if (gallery.size()>0){
            return (ArrayList<Person>) mEngine.identify(person, gallery.toArray(new Person[0]));
        }
        return new ArrayList<Person>();
    }

    /*
    Performs matching against the cache. Returns a list of the cache_ids in order of score
     */
	public List<Match> getBestMatches(Map<String, String> templates, SearchCache cache){
		is_ready = false;
        Log.i(TAG, String.format("Starting Search on cache of size %s",cache.candidates.size()));
		Set<Integer> hits = new HashSet<Integer>();
		Map<Integer,Double> scores = new HashMap<Integer, Double>();
		Map<Integer,Double> v_scores = new HashMap<Integer, Double>();
		List<String> fingers = new ArrayList<String>(){{
			add("left_thumb");
			add("right_thumb");
			add("left_index");
			add("right_index");
		}}; 
		Person whole_person = mapToPerson(999999999, templates);
		Map<String, Person> p_hold= new HashMap<String, Person>();
		for (String f: fingers){
			Map<String,String> f_hold = new HashMap<String,String>();
			f_hold.put(f,templates.get(f));
			p_hold.put(f, mapToPerson(999999999, f_hold));
		}

        Log.d(TAG, "Starting First ID Pass");
        List<Person> spikes = identify(whole_person, cache.candidates);
        Log.d(TAG, String.format("Finished First ID Pass with %s matches",spikes.size()));
        if (spikes.size()<1){
            return new ArrayList<Match>();
        }
        Log.d(TAG, "Sending Update Broadcast");
        BiometricsManager.updateStatus(String.format(AppController.getStringResource(R.string.limited_search), spikes.size()));

		Log.i(TAG, "Found " + Integer.toString(spikes.size())+ " candidates for inspection.");
		for (Person p: spikes){
			hits.add(p.getId());
			scores.put(p.getId(), 0.0);
			v_scores.put(p.getId(), 0.0);
		}

		this.mEngine.setThreshold(0.0f);
		List<Integer>super_threshold = new ArrayList<Integer>();
		for (int id: hits){
			//Log.i(TAG, "Verify whole for ID: " + Integer.toString(id) + " : " + Double.toString(whole_score));
			double score = 0.0;
			for (String f: fingers){
                Person verifyPerson = cache.candidates_map.get(id);
                // TODO get verify score for _id
				//double f_score = verify_in_cache(p_hold.get(f), id);
                double f_score = 0.0;
                try {
                    f_score = verify(p_hold.get(f), verifyPerson);
                }catch (Exception e){
                    Log.e(TAG, String.format("Couldn't verify %s | %s",f, e.toString()));
                }
				Log.i(TAG, "Verify finger " + "for id: " + Integer.toString(id) + " : " + Double.toString(f_score));
				score += f_score;
			}
			if (score >=MATCH_THRESOLD){
				super_threshold.add(id);
				scores.put(id, score);	
			}
		}
		mEngine.setThreshold(threshold);
		List<Engine.Match> matches = new ArrayList<Engine.Match>();
		
		for(int id: super_threshold){
			double score = scores.get(id);
			matches.add(new Match(cache.string_alias.get(id), score));
		}
		Collections.sort(matches);
		Log.i(TAG, "Returning " + Integer.toString(matches.size()) + " matches.");
		is_ready = true;
		return matches;
	}

    public static Person mapToPerson(Integer uuid, Map<String, String> templates){
        Person p = new Person();
        p.setId(uuid);
        List prints = new ArrayList<Fingerprint>();
        List<String> fingers = new ArrayList<String>(){{
            add("left_thumb");
            add("right_thumb");
            add("left_index");
            add("right_index");
        }};
        if (templates != null){
            for (String finger:fingers){
                String temp = templates.get(finger);
                if (temp != null){
                    Fingerprint f = new Fingerprint();
                    f.setIsoTemplate(hexStringToByteArray(temp));
                    f.setFinger(Finger.valueOf(finger.toUpperCase()));
                    prints.add(f);
                }
            }
            p.setFingerprints(prints);
            return p;
        }
        return null;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
	
	public class Match implements Comparable<Match>{
		
		public double score;
		public String uuid;
		
		public Match(String uuid, Double score){
			this.score = score;
			this.uuid = uuid;
		}

		public double getScore(){
			return score;
		}
		
		@Override
		public int compareTo(Match o) {
			return Double.compare(o.getScore(),getScore());
		}
	}
}

