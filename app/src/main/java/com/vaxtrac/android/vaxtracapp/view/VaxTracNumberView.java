package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FilterQueryProvider;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.data.TranslationCursor;

public class VaxTracNumberView extends LinearLayout {

    Context mContext;
    Cursor c = null;
    Cursor c2 = null;
    CommCareDataManager ccDM;

    ListView list;
    ColoredCursorAdapter adapter;
    AutoCompleteTextView autocomplete;

    private String TAG = "VTNumberView";

    public VaxTracNumberView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mContext = getContext();
        ccDM = AppController.mCommCareDataManager;
        list = (ListView) findViewById(R.id.vtnumber_listView);
        autocomplete = (AutoCompleteTextView) findViewById(R.id.vtnumber_autocomplete);
        PopulateList async = new PopulateList();
        async.execute();
        autocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null){
                    try {
                        Log.i(TAG, "ApplyFilter onTextChange");
                        adapter.getFilter().filter(s.toString());
                        adapter.notifyDataSetChanged();
                    }catch (NullPointerException e){/*Rotation Change triggers...*/}
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Action for clicking a listItem from the patientList
                Cursor selected = (Cursor) adapterView.getItemAtPosition(i);
                String selectedID = selected.getString(selected.getColumnIndex("docid"));
                Log.i(TAG, String.format("Selected DocID %s", selectedID));

                Intent commCareSessionAction = new Intent();
                commCareSessionAction.setAction(CommCareAPI.SESSION_ACTION);
                commCareSessionAction.putExtra(CommCareAPI.SESSION_ACTION_EXTRA,
                        String.format("COMMAND_ID %1$s CASE_ID %3$s %4$s COMMAND_ID %2$s",
                                CommCareAPI.SEARCH_MODULE, CommCareAPI.VAXTRAC_NUMBER_SEARCH_MENU,
                                "patient_case_id", selectedID));
                mContext.startActivity(commCareSessionAction);
            }
        });

    }

    public class PopulateList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "Getting cursors...");
            c = new TranslationCursor(ccDM.patientDB.allItems(),new String[]{"sex"});
            return null;

        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if (isCancelled()) {
                Log.i(TAG, "Caught Canceled Signal");
                Toast.makeText(mContext, "CommCare must be running!", Toast.LENGTH_LONG).show();
                super.onPostExecute(aVoid);
                return;
            }

            adapter = new ColoredCursorAdapter(mContext,
                    R.layout.row_vtnumber_searchview,
                    c,
                    new String[]{"name","dob","sex","village","vtnumber"},
                    new int[]{R.id.list_item_name, R.id.list_item_dob, R.id.list_item_sex, R.id
                            .list_item_village,R.id.list_item_vtnum});

            adapter.setFilterQueryProvider(new FilterQueryProvider() {
                @Override
                public Cursor runQuery(CharSequence constraint) {
                    if(constraint == null) {
                        return c;
                    } else {
                        return new TranslationCursor(ccDM.patientDB.allRowsItemsLike("vtnumber", constraint
                                .toString()),new String[]{"sex"});
                    }
                }
            });

            list.setAdapter(adapter);
            super.onPostExecute(aVoid);
        }
    }
}