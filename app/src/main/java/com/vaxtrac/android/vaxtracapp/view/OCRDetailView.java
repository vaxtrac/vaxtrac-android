package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.OCRDetailPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.Presenter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class OCRDetailView extends LinearLayout {

    private static final String TAG = "OCRDetailView";

    LayoutInflater inflater;
    LinearLayout container;

    Presenter presenter;
    ImageView vialImage;
    Spinner vaccineType;
    NumberPicker maximumDoses;
    EditText lotNumber;
    EditText batchNumber;
    DatePicker manufactureDate;
    DatePicker expirationDate;

    ArrayList<ViewGroup> vialLayouts;

    public OCRDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "Constructor");
        inflater = LayoutInflater.from(context);
        presenter = new OCRDetailPresenter(context, this, inflater);
        vialLayouts = new ArrayList<>();
        AppController.mScreen.inject(presenter);
    }

    public void onFinishInflate() {
        container = (LinearLayout) findViewById(R.id.ocr_detail_container);
        ((OCRDetailPresenter)presenter).loadVials();
        Log.d(TAG, "onFinishInflate");
        super.onFinishInflate();
        Log.d(TAG, "Finished Super Inflation");
    }

    public Presenter getPresenter() {
        return presenter;
    }

    public void addToContainer(View view) {
        container.addView(view);
    }

    public void addVialLayout(ViewGroup layout, int position) {
        vialLayouts.add(position, layout);
    }

    public ViewGroup getVialLayout(int position) {
        return vialLayouts.get(position);
    }

    public String getVaccineType(ViewGroup vialLayout) {
        Spinner vaccineSpinner = (Spinner) vialLayout.findViewById(R.id.vaccine_type);
        String[] vaccineArrayEnglish = getResources().getStringArray(R.array.antigen_array_en);
        return vaccineArrayEnglish[Long.valueOf(vaccineSpinner.getSelectedItemId()).intValue()];
    }

    public int getMaximumDoses(ViewGroup vialLayout) {
        EditText maximumDoses = (EditText) vialLayout.findViewById(R.id.max_doses);
        return Integer.parseInt(maximumDoses.getText().toString());
    }

    public String getLotNumber(ViewGroup vialLayout) {
        EditText lotNumber = (EditText) vialLayout.findViewById(R.id.lot_number);
        return lotNumber.getText().toString();
    }

    public String getBatchNumber(ViewGroup vialLayout) {
        EditText batchNumber = (EditText) vialLayout.findViewById(R.id.batch_number);
        return batchNumber.getText().toString();
    }

    public Date getManufactureDate(ViewGroup vialLayout) {
        DatePicker manufactureDate = (DatePicker) vialLayout.findViewById(R.id.manufacture_date);
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.clear();
        cal.set(manufactureDate.getYear(), manufactureDate.getMonth(), manufactureDate.getDayOfMonth());
        Log.d(TAG, "mfg " + cal.toString());
        return cal.getTime();
    }

    public Date getExpirationDate(ViewGroup vialLayout) {
        DatePicker expirationDate = (DatePicker) vialLayout.findViewById(R.id.expiration_date);
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.clear();
        cal.set(expirationDate.getYear(), expirationDate.getMonth(), expirationDate.getDayOfMonth());
        Log.d(TAG, "exp " + cal.toString());
        return cal.getTime();
    }
}
