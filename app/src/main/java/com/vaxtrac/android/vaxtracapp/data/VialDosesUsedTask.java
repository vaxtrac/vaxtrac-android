package com.vaxtrac.android.vaxtracapp.data;

import android.os.AsyncTask;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

public class VialDosesUsedTask extends AsyncTask<Void, Void, Map<String, Integer>> {

    private static final String TAG = "VialDosesUsedTask";

    public interface TaskListener {
        void onFinished(Map<String, Integer> vialDoses);
    }

    private TaskListener listener;

    private Calendar startDate;
    private Calendar endDate;
    private SimpleDateFormat sqlDateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
    private static final DateFormat isoDateFormat =
            new UnlocalizedDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");

    public VialDosesUsedTask(Calendar startDate, Calendar endDate) {
        this.startDate = startDate;
        if(endDate != null) {
            this.endDate = endDate;
        } else {
            this.endDate = startDate;
        }
    }

    public void setTaskListener(TaskListener listener) {
        this.listener = listener;
    }

    public String getStartDate() {
        return sqlDateFormat.format(startDate.getTime());
    }

    public String getEndDate() {
        return sqlDateFormat.format(endDate.getTime());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Map<String, Integer> doInBackground(Void[] params) {
        return AppController.mOCRManager.getVialDosesUsed(getStartDate(), getEndDate());
    }

    @Override
    protected void onPostExecute(Map<String, Integer> visits) {
        super.onPostExecute(visits);
        listener.onFinished(visits);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}