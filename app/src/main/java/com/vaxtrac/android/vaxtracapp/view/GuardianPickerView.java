package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.data.TestCursor;
import com.vaxtrac.android.vaxtracapp.data.TranslationCursor;
import com.vaxtrac.android.vaxtracapp.presenter.GuardianPickerPresenter;

public class GuardianPickerView extends LinearLayout {

    private static final String TAG = "GuardianPickerView";

    Context context;
    GuardianPickerPresenter presenter;
    ColoredCursorAdapter adapter;
    Cursor lastCursor;
    ListView table;


    public GuardianPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        table = (ListView) findViewById(R.id.guardian_picker_listview);
    }

    public void populateTable(Cursor cursor){
        Cursor translationCursor = new TranslationCursor(cursor, new String[]{"relation"});

        //Make a test cursor to show if a row has biometrics attached

        lastCursor = new TestCursor(translationCursor, "hasbiometrics") {
            int testColumnIndex = super.getColumnIndex("leftthumb");

            @Override
            public boolean applyLogic() {
                if(super.getString(testColumnIndex).length() <= 0){
                    return false;
                }
                return true;
            }
        };

        Log.i(TAG, String.format("Cursor size | %s",cursor.getCount()));
        adapter = new ColoredCursorAdapter(this.context,
                R.layout.row_guardian_picker,
                lastCursor,
                new String[]{"name", "relation", "hasbiometrics"},
                new int[]{R.id.list_item_text1, R.id.list_item_text2, R.id.list_item_text3});
        table.setAdapter(adapter);
    }

}
