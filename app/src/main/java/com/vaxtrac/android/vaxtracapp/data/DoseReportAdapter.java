package com.vaxtrac.android.vaxtracapp.data;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.models.DoseReport;

public class DoseReportAdapter extends BaseAdapter {
    private Context context;
    private DoseReport report;

    public DoseReportAdapter(Context context, DoseReport doseReport) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    private static class ViewHolder {
        public TextView textView;
    }
}
