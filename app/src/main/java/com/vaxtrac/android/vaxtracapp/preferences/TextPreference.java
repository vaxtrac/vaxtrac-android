package com.vaxtrac.android.vaxtracapp.preferences;


import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

public class TextPreference extends EditTextPreference{

    public TextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        Preferences.addPreference(this);
    }
}
