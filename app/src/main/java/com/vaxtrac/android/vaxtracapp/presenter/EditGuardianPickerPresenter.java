package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.view.EditGuardianPickerView;
import com.vaxtrac.android.vaxtracapp.view.GuardianPickerView;

public class EditGuardianPickerPresenter extends GuardianPickerPresenter{

    protected GuardianPickerView view;
    private String TAG = "GuardianPickerPresenter";
    private String guardianId;

    private static final String GUARDIAN_CASE_ID = "guardian_case_id";

    public EditGuardianPickerPresenter(final Context context, EditGuardianPickerView view) {
        super(context, view);
        setView(view);
    }

    @Override
    public void setView(View view) {this.view = (EditGuardianPickerView) view;}

    public void chooseGuardian(String guardianId) {
        Log.d(TAG, "Guardian ID " + guardianId);
        context.startActivity(CommCareAPI.getGuardianEditForm(guardianId));
    }

    @Override
    protected void noGuardianFound(){
        Log.e(TAG, String.format("No Guardian Found for childID: %s", childID));
    }

    @Override
    protected void guardiansReady(){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
            try {
                view.populateTable(getGuardians());
            }catch (NullPointerException e){
                Log.e(TAG, "Couldn't populate GuardianPicker Table");
                e.printStackTrace();
            }
            }
        });
    }
}
