package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.view.HomeView;

public class HomePresenter extends Presenter implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "HomePresenter";

    private Context context;
    private MainActivity activity;
    private HomeView view;
    private CommCareAPI commCareAPI;

    private static final int DEVELOPER_THRESHOLD = 10;
    private int unlockDeveloperCount = 0;

    public HomePresenter(Context context, View view) {
        this.context = context;
        this.view = (HomeView) view;
        activity = (MainActivity) context;
        MainActivity.resumeState = MainActivity.ResumeState.START;
        commCareAPI = AppController.commCareAPI;
        commCareAPI.setHomePresenter(this);
        if(isAuthenticated()) {
            activity.startScheduleLoader();
            AppController.mCommCareDataManager.loadVillages();
        }
    }

    public void setView(View view) {
        this.view = (HomeView) view;
    }

    public boolean isAuthenticated() {
        return commCareAPI.isAuthenticated();
    }

    public void launchRegistrationForm() {
        context.startActivity(CommCareAPI.getPatientRegistrationForm(
                Preferences.getSessionLocationType()));
    }

    public void logout() {
        commCareAPI.logout();
        revokePermissions();
    }

    public void revokePermissions() {
        Log.d(TAG, "disabling all");
        invalidate();
    }

    public void invalidate() {
        Log.d(TAG, "invalidating view");
        //view.invalidate();
    }

    public boolean isDeveloper() {
        return PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext())
                .getBoolean(Preferences.PREF_DEVELOPER, true);
    }

    public void logoClick() {
        if(unlockDeveloperCount < DEVELOPER_THRESHOLD && !isDeveloper()) {
            unlockDeveloperCount++;
        } else {
            unlockDeveloperCount = 0;
            if(!isDeveloper()) {
                unlockDeveloperMode();
                view.unlockPreferencesMenu();
            }
        }
    }

    public boolean siteSelectionEnabled() {
        return Preferences.sessionLocationEnabled();
    }

    public void toggleSiteSelection() {
        String vaccinationSite = Preferences.getSessionLocationType();
        String[] vaccinationSites = context.getResources().getStringArray(R.array
                .vaccination_session_locations);
        for(int i = 0, sitesLength = vaccinationSites.length; i < sitesLength; i++) {
            if(vaccinationSite.equals(vaccinationSites[i])) {
                int nextSite;
                if(i != sitesLength - 1) {
                    nextSite = i+1;
                } else {
                    nextSite = 0;
                }
                changeSiteSelection(vaccinationSites[nextSite]);
            }
        }
    }

    public void changeSiteSelection(String siteName) {
        Preferences.saveSessionLocationType(siteName);
        view.updateSiteSelection(siteName);
    }

    public String getSelectedSite() {
        return Preferences.getSessionLocationType();
    }

    public void unlockDeveloperMode() {
        PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext()).edit().putBoolean
                (Preferences.PREF_DEVELOPER, true).commit();
    }

    public void prefChanged(String key) {
        if(key.equals(Preferences.PREF_DEVELOPER)) {
            if(!PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext())
                    .getBoolean(key, false)) {
                view.closePreferencesDrawer();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        prefChanged(key);
    }
}
