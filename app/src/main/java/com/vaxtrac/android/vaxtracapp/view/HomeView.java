package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.AppVersion;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.preferences.PreferencesAdapter;
import com.vaxtrac.android.vaxtracapp.presenter.HomePresenter;

public class HomeView extends DrawerLayout {

    private static final String TAG = "HomeView";

    LinearLayout siteSelection;
    TextView siteSelectionLabel;
    LinearLayout registrationButton;
    LinearLayout searchButton;
    LinearLayout reportsButton;
    LinearLayout statisticsButton;
    LinearLayout callbacksButton;
    LinearLayout ocrButton;
    ListView settingsDrawer;
    ImageView vaxtracLogo;
    Button syncButton;
    Button vtNumber;

    HomePresenter presenter;

    Typeface typeface;

    Context context;

    public HomeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        presenter = new HomePresenter(context, this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            Log.d(TAG, "Back Pressed");
        }
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            Log.d(TAG, "Back Pressed");
        }
        return true;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        registrationButton = (LinearLayout) findViewById(R.id.register);
        searchButton = (LinearLayout) findViewById(R.id.search);
        reportsButton = (LinearLayout) findViewById(R.id.reports);
        statisticsButton = (LinearLayout) findViewById(R.id.summary_stats);
        callbacksButton = (LinearLayout) findViewById(R.id.callbacks);
        ocrButton = (LinearLayout) findViewById(R.id.vial_ocr);
        settingsDrawer = (ListView) findViewById(R.id.settings_drawer);
        vaxtracLogo = (ImageView) findViewById(R.id.vaxtrac_logo);
        siteSelection = (LinearLayout) findViewById(R.id.outreach_session_selector);
        siteSelectionLabel = (TextView) findViewById(R.id.session_type_text);

        registrationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.launchRegistrationForm();
            }
        });

        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.mCommCareDataManager.getAllVillages() != null)
                    AppController.getFlow().goTo(new Screens.SearchScreen());
                else
                    Toast.makeText(context, getResources().getString(R.string.villages_loading), Toast.LENGTH_LONG).show();

            }
        });


        reportsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goTo(new Screens.LocalReportScreen());
            }
        });

        statisticsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goTo(new Screens.SummaryReportScreen());
            }
        });


        callbacksButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.mCommCareDataManager.getAllVillages() != null)
                    AppController.getFlow().goTo(new Screens.CallbacksScreen());
                else
                    Toast.makeText(context, getResources().getString(R.string.villages_loading), Toast.LENGTH_LONG).show();
            }
        });

        if(AppVersion.isEnabled("ocr")){
            ocrButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppController.getFlow().goTo(new Screens.OCRImagesScreen());
                }
            });
        }else{
            ocrButton.setVisibility(View.GONE);
        }

        if(presenter.siteSelectionEnabled()) {
            siteSelection.setVisibility(VISIBLE);
            siteSelectionLabel.setText(presenter.getSelectedSite());
            siteSelection.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.toggleSiteSelection();
                }
            });
        }

/*
        syncButton = (Button) findViewById(R.id.sync);
        syncButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.syncCommCare();

            }
        });


        vtNumber = (Button) findViewById(R.id.vtnumber);
        vtNumber.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goTo(new VTNumberScreen());
            }
        });
        */

        settingsDrawer.setAdapter(new PreferencesAdapter(context));
        settingsDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "Item Clicked " + position);
            }
        });

        if(!presenter.isDeveloper()) {
            Log.d(TAG, "Developer Disabled");
            this.setDrawerLockMode(LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
        }

        vaxtracLogo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.logoClick();
            }
        });
    }

    public void updateSiteSelection(String siteName) {
        siteSelectionLabel.setText(siteName);
    }

    public void unlockPreferencesMenu() {
        this.setDrawerLockMode(LOCK_MODE_UNLOCKED, GravityCompat.END);
        Toast.makeText(context, "Developer options on", Toast.LENGTH_SHORT).show();
    }

    public void closePreferencesDrawer() {
        closeDrawer(settingsDrawer);
        this.setDrawerLockMode(LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
    }

    public Typeface loadTypeFace(String font) {
        return Typeface.createFromAsset(AppController.getAppContext().getAssets(),
                font);
    }
}
