package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.R;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class VideoAdapter extends BaseAdapter {

    private static final String TAG = "VideoAdapter";
    private static final String VIDEO_ASSET_FOLDER = "videos";
    private static final String VIDEO_EXTERNAL_FILES_FOLDER = "videos";

    Context context;
    LayoutInflater layoutInflater;
    Map<Integer, String> videos = new HashMap<>();

    public VideoAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        String[] filenames = loadVideosFromFolder(VIDEO_EXTERNAL_FILES_FOLDER);
        Log.d(TAG, filenames.length + " video files");
        for(int i = 0, length = filenames.length; i < length; i++) {
            if(filenames[i].contains(".mp4") || filenames[i].contains(".avi")) {
                Log.d(TAG, "Video filename : " + filenames[i]);
                videos.put(i, filenames[i]);
            }
        }
    }

    public String[] loadVideosFromFolder(String folder) {
        return new File(context.getExternalFilesDir(null) + "/" + folder).list();
    }

    public void addVideo(String filename) {
        if(!videos.values().contains(filename)) {
            videos.put(getCount(), filename);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return videos.size();
    }

    @Override
    public Object getItem(int position) {
        return videos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        String filename = (String)getItem(position);
        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.video_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.video_image);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.video_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String videoPath = context.getExternalFilesDir(null).toString() + "/" + VIDEO_EXTERNAL_FILES_FOLDER
                + "/" + filename;
        Log.d(TAG, "Bitmap path " + videoPath);

        BitmapWorkerTask bitmapWorkerTask = new BitmapWorkerTask(viewHolder.imageView);
        bitmapWorkerTask.execute(videoPath);

        /*
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(context.getExternalFilesDir
                (null) + "/" + filename, MediaStore.Video.Thumbnails
                .MINI_KIND);
        */

        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setDataSource(videoPath);
        } catch (Exception exception) {
            Log.e(TAG, "Couldn't set data source for metadata");
        }
        String title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever
                .METADATA_KEY_TITLE);

        viewHolder.textView.setText(title != null ? title : filename);

        return convertView;
    }

    private static class ViewHolder {
        public ImageView imageView;
        public TextView textView;
    }

    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<>(imageView);
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(String... filepath) {
            String path = filepath[0];
            return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }
}
