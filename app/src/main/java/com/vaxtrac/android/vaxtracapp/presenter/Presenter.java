package com.vaxtrac.android.vaxtracapp.presenter;

import android.os.Bundle;

public abstract class Presenter {
    public static Bundle bundle = new Bundle();

    public static void setBundle(Bundle restoredBundle) {
        bundle = restoredBundle;
    }

    public static Bundle getBundle() {
        return bundle;
    }

    public Presenter() {

    }
}
