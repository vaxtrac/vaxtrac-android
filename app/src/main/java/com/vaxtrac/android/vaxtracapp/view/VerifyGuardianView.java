package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.data.TestCursor;
import com.vaxtrac.android.vaxtracapp.data.TranslationCursor;
import com.vaxtrac.android.vaxtracapp.presenter.VerifyGuardianPresenter;

public class VerifyGuardianView extends GuardianPickerView {

    VerifyGuardianPresenter presenter;

    Button registerGuardian;
    private static final String TAG = "VerifyGuardianView";
    private boolean guardianAdded = false;

    public VerifyGuardianView(Context context, AttributeSet attrs) {
        super(context, attrs);
        presenter = new VerifyGuardianPresenter(context, this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        presenter.setView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        presenter.setView(null);
        presenter.unregisterReceivers();
        presenter.destroyBroadcastManager();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        registerGuardian = (Button) findViewById(R.id.verify_guardian_new_btn);
        registerGuardian.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.registerGuardian();
            }
        });
        table.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Action for clicking a listItem from the patientList
                Cursor selected = (Cursor) adapterView.getItemAtPosition(i);
                String selectedID = selected.getString(selected.getColumnIndex("docid"));
                Log.i(TAG, String.format("Selected DocID %s", selectedID));
                presenter.StartScan(selectedID);
            }
        });

    }

    public void verifyFailed() {
        Toast.makeText(context, AppController.getStringResource(R.string.verify_failed), Toast.LENGTH_LONG).show();
    }

    public void scanFailed() {
        Toast.makeText(context, AppController.getStringResource(R.string.verify_failed), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void populateTable(Cursor cursor) {
        Cursor translationCursor = new TranslationCursor(cursor, new String[]{"relation"});

        //Make a test cursor to show if a row has biometrics attached

        lastCursor = new TestCursor(translationCursor, "hasbiometrics") {
            int testColumnIndex = super.getColumnIndex("leftthumb");

            @Override
            public boolean applyLogic() {
                if(super.getString(testColumnIndex).length()<=0){
                    return false;
                }
                return true;
            }
        };
        Log.i(TAG, String.format("Cursor size | %s",cursor.getCount()));
        adapter = new ColoredCursorAdapter(this.context,
                R.layout.row_guardian_picker,
                lastCursor,
                new String[]{"name","relation","hasbiometrics"},
                new int[]{R.id.list_item_text1, R.id.list_item_text2,R.id.list_item_text3});
        table.setAdapter(adapter);
    }
}
