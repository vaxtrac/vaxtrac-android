package com.vaxtrac.android.vaxtracapp.presenter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.view.CallbacksView;
import com.vaxtrac.nepalidatepickerlib.NepaliDateAlertBuilder;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import dateconverter.NepaliDateConverter;


public class CallbacksPresenter extends Presenter {

    private static final String TAG = "CallbacksPresenter";

    private SimpleDateFormat df;
    protected Calendar calendar;
    protected Date selectedDate = null;
    private int selectedDuration = -1;
    private String selectedVillage;
    private DatePickerDialog.OnDateSetListener date_listener;

    protected Context context;
    private CallbacksView view;

    private Locale locale;
    private boolean isNepaliLocale;
    private boolean isNepaliFlavor;

    private final dateconverter.Date nepaliDate;
    private NepaliDateConverter converter;
    private DialogInterface.OnClickListener nepali_date_listener;

    public CallbacksPresenter(Context context, View view) {
        this.context = context;
        this.view = (CallbacksView) view;

        ((CallbacksView) view).setVillages(AppController.mCommCareDataManager.getCurrentUserVillages());

        calendar = Calendar.getInstance();
        df = new SimpleDateFormat("yyyy-MM-dd");

        nepaliDate = new dateconverter.Date(0,0,0);

        if (isNepaliLocale) {
            converter = new NepaliDateConverter();
            nepali_date_listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dateconverter.Date gregorianDate = converter.toGregorianDate(nepaliDate.toString());
                    calendar.set(Calendar.YEAR, gregorianDate.getYear());
                    calendar.set(Calendar.MONTH, gregorianDate.getMonth()-1);
                    calendar.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
                    setDate(calendar.getTime());
                    dialog.dismiss();
                }
            };
        }

        else {
            date_listener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    setDate(calendar.getTime());
                }
            };
        }


    }

    protected void setDate(Date date) {
        Log.d(TAG, "Setting Start Date " + date.toString());
        this.selectedDate = date;
        final String dateString;

        if (isNepaliLocale) {
            dateString = GlobalTranslations.dateToPrettyString(date);
        }
        else {
            dateString = df.format(date);
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    view.updateText(dateString);
                }catch (NullPointerException e){
                    Log.e(TAG, "Couldn't set date!");
                    e.printStackTrace();
                }
            }
        });

    }

    public void getStartingDate() {
        if(selectedDate == null) {
            setDate(calendar.getTime());
        } else {
            setDate(selectedDate);
        }
    }

    public View.OnClickListener getCalendarListener(){
        View.OnClickListener listener;

        if (isNepaliLocale) {
            NepaliDateAlertBuilder builder = new NepaliDateAlertBuilder(context, nepaliDate, nepali_date_listener);
            final AlertDialog dialog = builder.create();

            listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.show();
                }
            };
        }
        else {
            listener = new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    DatePickerDialog picker = new DatePickerDialog(context, date_listener, calendar
                            .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH));
                    picker.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
                    picker.show();
                }
            };
        }
        return listener;
    }

    public List<String> getDurations(){
        return Arrays.asList(context.getResources().getStringArray(R.array.callback_durations));
    }

    public void hideKeyboard(){
        /*
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override public void run() {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }, 100);
        */
    }

    public void setResult(boolean overdue, String village, int duration) {
        Date date = this.selectedDate == null ? Calendar.getInstance().getTime() : this.selectedDate;

        Log.d(TAG, String.format("Callback query: %s -> %s -> %s", village, new SimpleDateFormat
                ("yyyy-MM-dd", Locale.US).format(date), duration));

        Screens.CallbackListScreen callbackList = new Screens.CallbackListScreen(overdue, village, date, duration);
        AppController.mScreen = callbackList;
        AppController.getFlow().goTo(callbackList);
    }

}
