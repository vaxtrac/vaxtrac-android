package com.vaxtrac.android.vaxtracapp.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by sarwar on 2/10/15.
 */
public class UnlocalizedDateFormat extends SimpleDateFormat {
    /*
    Forces use of Locale.ENGLISH so we don't lose our encodings for non-userfacing dates.
     */
    public UnlocalizedDateFormat(String format){
        super(format, Locale.ENGLISH);

    }
}
