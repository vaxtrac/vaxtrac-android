package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.BuildConfig;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import dateconverter.NepaliDateConverter;

public class GlobalTranslations {

    /*
    Translates automatically generated words like words from databases or vaccine schedules.
    Grabs translations from the R...string.xml
    Keys are their machine representation (i.e. column name in database)
    Values are local translations of the keys. (male -> Male [english] or girl -> Femme [French])
    If a key should have a space, it needs a _ in .string.xml
    Each Language gets it's own string.xml file like string_enUs.xml for USA. Follow convention
        for Localization abbreviations.

    */

    //Must be populated by AppController onCreate : null pointer
    public static Resources resources = null;
    public static String packageName = null;

    public static final String TAG = "GlobalTranslations";

    public static Map<String,String> reverse = new HashMap<String,String>();

    public static final String DATE_REGEX = "[0-9]{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[012])";

    private static NepaliDateConverter converter = new NepaliDateConverter();
    private static DateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");

    public static String translate(String text, Context context){
        if (context == null) {
            return translate(text);
        }

        Locale current = context.getResources().getConfiguration().locale;
        boolean isNepaliLocale = current.getISO3Language().equals("nep");
        boolean isNepaliFlavor = BuildConfig.FLAVOR.equals("nepal");
        int parseNumber;

        try {
            parseNumber = Integer.parseInt(text);
        } catch (NumberFormatException exception) {
            parseNumber = -1;
        }

        if (isNepaliFlavor && Pattern.matches(DATE_REGEX, text)) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date d = new Date();
            try {
                d = df.parse(text);
            } catch (ParseException e) {
                Log.e(TAG, "Date could not be parsed", e);
            }
            return dateToPrettyString(d, isNepaliLocale);
        } else if (isNepaliFlavor && parseNumber > 0 && parseNumber < 100) {
            /* Other Village */
            if (parseNumber != 99) {
                return NumberFormat.getInstance().format(parseNumber);
            } else {
                return translate("other");
            }
        }
        else {
            return translate(text);
        }
    }

    public static String translate(String text){
        try{
            String value = text.toLowerCase();
            String key = resources.getString(resources.getIdentifier(text.toLowerCase().replace(" ", "_"), "string", packageName));
            reverse.put(key, value);
            return key;

        }catch (Exception e){
            if(text!= null){
                Log.i(TAG, String.format("Couldn't translate string '%s' | %s", text.toLowerCase().replace(" ", "_") ,e.toString()));
                return text;
            }else {
                Log.e(TAG, "NULL text can't be translated, returning empty string");
                return "";
            }
        }
    }


    public static String unTranslate(String text){
        try{
            return reverse.get(text);
        }catch(Exception e){
            Log.i(TAG, text + "wasn't found to untranslate");
            return text;
        }
    }

    public static String getString(int id){
        return resources.getString(id);
    }

    public static String dateToPrettyString(Date d){
        return dateToPrettyString(d, false);
    }

    public static String dateToPrettyString(Date date, boolean isNepaliLocale) {
        dateconverter.Date nepDate = converter.fromGregorianDate(nepaliDateFormat.format(date));
        if(isNepaliLocale) {
            return converter.toNepaliString(nepDate, true);
        }
        else {
            return nepDate.toString();
        }
    }
}


