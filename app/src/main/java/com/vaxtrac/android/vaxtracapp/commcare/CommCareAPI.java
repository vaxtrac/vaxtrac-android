package com.vaxtrac.android.vaxtracapp.commcare;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.util.ArrayMap;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.presenter.HomePresenter;
import com.vaxtrac.android.vaxtracapp.presenter.LoginPresenter;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class CommCareAPI {
    private static final String TAG = "CommCareAPI";

    public static final String COMMCAREHQ_URL = "http://commcarehq.org";

    public static final Uri CONTENT_PROVIDER_URI = Uri.parse("content://org.commcare.dalvik/");
    public static final String CASE_CONTENT_PROVIDER =
            "content://org.commcare.dalvik.case/casedb/case/";

    public static final String SESSION_ACTION = "org.commcare.dalvik.action.CommCareSession";
    public static final String SESSION_ACTION_EXTRA = "ccodk_session_request";

    public static final String PATIENT_MODULE = "m0";
    public static final String REGISTRATION_FORM = "f0";
    public static final String PATIENT_REGISTRATION_MENU = PATIENT_MODULE + "-" + REGISTRATION_FORM;
    
    public static final String PATIENT_MODIFY_FORM = "f3";
    public static final String PATIENT_MODIFY_MENU = PATIENT_MODULE + "-" + PATIENT_MODIFY_FORM;

    public static final String SEARCH_MODULE = "m3";

    public static final String QR_SEARCH_FORM = "f0";
    public static final String DEMOGRAPHIC_SEARCH_FORM = "f2";
    public static final String QR_SEARCH_MENU = SEARCH_MODULE + "-" + QR_SEARCH_FORM;
    public static final String DEMOGRAPHIC_SEARCH_MENU = SEARCH_MODULE + "-" + DEMOGRAPHIC_SEARCH_FORM;

    public static final String VAXTRAC_NUMBER_SEARCH_FORM = "f3";
    public static final String VAXTRAC_NUMBER_SEARCH_MENU = SEARCH_MODULE + "-" + VAXTRAC_NUMBER_SEARCH_FORM;

    public static final String VACCINE_CARD_NUMBER_SEARCH_FORM = "f4";
    public static final String VACCINE_CARD_NUMBER_SEARCH_MENU = SEARCH_MODULE + "-" +
            VACCINE_CARD_NUMBER_SEARCH_FORM;

    public static final String PHONE_SEARCH_FORM = "f4";
    public static final String PHONE_SEARCH_MENU = SEARCH_MODULE + "-" + PHONE_SEARCH_FORM;

    public static final String NAME_SEARCH_FORM = "f5";
    public static final String NAME_SEARCH_MENU = SEARCH_MODULE + "-" + NAME_SEARCH_FORM;

    public static final String GUARDIAN_MODULE = "m1";
    public static final String GUARDIAN_MODIFY_FORM = "f2";
    public static final String GUARDIAN_MODIFY_MENU = GUARDIAN_MODULE + "-" + GUARDIAN_MODIFY_FORM;

    public static final String GUARDIAN_ADD_FORM = "f0";
    public static final String GUARDIAN_ADD_MENU = GUARDIAN_MODULE + "-" + GUARDIAN_ADD_FORM;

    public static final String FINGERPRINT_SEARCH_FORM = "f1";
    public static final String FINGERPRINT_SEARCH_MENU = GUARDIAN_MODULE+ "-" +
            FINGERPRINT_SEARCH_FORM;

    public static final String CALLBACK_STATUS_FORM = "f1";
    public static final String CALLBACK_STATUS_MENU = PATIENT_MODULE + "-" +
            CALLBACK_STATUS_FORM;

    public static final String QR_ADD_FORM = "f2";
    public static final String QR_ADD_MENU = PATIENT_MODULE + "-" + QR_ADD_FORM;

    public static final int KEY_REQUEST_CODE = 1;

    private static final String KEY_REQUEST_ACTION = "org.commcare.dalvik.action" +
            ".CommCareKeyAccessRequest";
    private static final String EXTERNAL_ACTION = "org.commcare.dalvik.api.action.ExternalAction";
    private static final String UPDATE_ACTION = "org.commcare.dalvik.api.action.data.update";
    private static final String LOGIN_ACTION = "org.commcare.dalvik.api.action.session.login";
    private static final String LOGOUT_ACTION = "org.commcare.dalvik.api.action.session.logout";

    private static final String SHARING_KEY_ID = "commcare_sharing_key_id";
    private static final String SHARING_KEY_PAYLOAD = "commcare_sharing_key_payload";

    private static final String SHARING_KEY_SYMETRIC = "commcare_sharing_key_symetric";
    private static final String SHARING_KEY_CALLOUT = "commcare_sharing_key_callout";

    private static final String ACTION = "commcareaction";
    private static final String LOGIN_INTENT = "login";
    private static final String LOGOUT_INTENT = "logout";
    private static final String SYNC_INTENT = "sync";
    public static final String USERNAME_KEY = "username";
    public static final String USERID_KEY = "userid";
    private static final String PASSWORD_KEY = "password";

    private static final String COMMCARE_SHARED_PREFS = "CommCare";
    private static final String PUBLIC_KEY = "publicKey";
    private static final String KEY_ID = "keyId";
    private static final String LAST_WORKER = "lastWorker";

    private static final String PATIENT_CASE_ID = "patient_case_id";
    private static final String GUARDIAN_CASE_ID = "guardian_case_id";
    private static final String SESSION_TYPE = "session_type";

    private Context context;
    private MainActivity activity;
    private WeakReference<LoginPresenter> loginPresenter;
    private WeakReference<HomePresenter> homePresenter;

    private String keyId;
    private byte[] publicKey;

    private String currentUser;
    private String currentPassword;

    private static final int MAX_RETRYS = 10;
    private static final long LOGIN_TIMEOUT_DURATION = 10000;

    private Timer loginTimer;

    private Handler timeoutHandler;

    private boolean isAuthenticated = false;
    private boolean requestedKeys = false;

    private OkHttpClient httpClient;

    private static Map<String, String> params = new ArrayMap<>();

    public CommCareAPI() {
    }

    public void setMainActivity(MainActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    public void login(String username, String password) {
        Log.d(TAG, "login");
        Intent intent = new Intent(EXTERNAL_ACTION);
        intent.putExtra(SHARING_KEY_ID, keyId);
        Bundle action = new Bundle();
        action.putString(ACTION, LOGIN_INTENT);
        action.putString(USERNAME_KEY, username);
        action.putString(PASSWORD_KEY, password);
        currentUser = username;
        currentPassword = password;
        Pair<byte[], byte[]> serializedBundle;

        int retryCount = 0;

        do {
            Log.d(TAG, "serializing bundle " + retryCount);
            serializedBundle = serializeBundle(action);
            if (serializedBundle != null) { break; }
            retryCount++;
        } while (retryCount < MAX_RETRYS);


        if(serializedBundle != null) {
            intent.putExtra(SHARING_KEY_SYMETRIC, serializedBundle.first);
            intent.putExtra(SHARING_KEY_CALLOUT, serializedBundle.second);

            activity.sendBroadcast(intent);

            timeoutHandler = new Handler() {
                public void handleMessage(Message msg) {
                    loginTimeOut();
                }
            };

            loginTimer = new Timer();
            loginTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    timeoutHandler.obtainMessage(1).sendToTarget();
                }
            }, LOGIN_TIMEOUT_DURATION);
        }
    }

    public void loginTimeOut() {
        if(loginPresenter != null) {
            loginPresenter.get().timeout();
        }
    }

    public void cancelTimer() {
        if(loginTimer != null) {
            loginTimer.cancel();
            loginTimer = null;
        }
    }

    public void finishLogin(Intent intent) {
        if(!isAuthenticated) {
            Log.d(TAG, "finish login " + intent.getDataString() + " " + intent.getData() + " " +
                    (intent.getExtras() == null) + " " + intent.getAction());
            isAuthenticated = true;
            persist(LAST_WORKER, currentUser);
            cancelTimer();
            if (loginPresenter != null && loginPresenter.get() != null) {
                loginPresenter.get().loginFinished();
            }
        } else {
            Log.d(TAG, "Ignoring Duplicate Login Broadcast");
        }
    }

    public void setLoginPresenter(LoginPresenter presenter) {
        this.loginPresenter = new
                WeakReference<LoginPresenter>(presenter);
    }

    public void setHomePresenter(HomePresenter presenter) {
        this.homePresenter = new WeakReference<HomePresenter>(presenter);
    }

    public void logout() {
        Log.d(TAG, "logging out");
        Intent intent = new Intent(EXTERNAL_ACTION);
        intent.putExtra(SHARING_KEY_ID, keyId);
        Bundle action = new Bundle();
        action.putString(ACTION, LOGOUT_INTENT);
        action.putString(USERNAME_KEY, currentUser);
        action.putString(PASSWORD_KEY, currentPassword);
        Pair<byte[], byte[]> serializedBundle;

        int retryCount = 0;
        do {
            serializedBundle = serializeBundle(action);
            if (serializedBundle != null) { break; }
            retryCount++;
        } while (retryCount < MAX_RETRYS);

        if(serializedBundle != null) {
            intent.putExtra(SHARING_KEY_SYMETRIC, serializedBundle.first);
            intent.putExtra(SHARING_KEY_CALLOUT, serializedBundle.second);

            activity.sendBroadcast(intent);
        }
    }


    public void sync() {
        if(isCommCareRunning()) {
            testConnection(COMMCAREHQ_URL);
            if (hasKeys()) {
                Intent intent = new Intent(EXTERNAL_ACTION);
                intent.putExtra(SHARING_KEY_ID, keyId);
                Bundle action = new Bundle();
                action.putString(ACTION, SYNC_INTENT);
                Pair<byte[], byte[]> serializedBundle;

                int retryCount = 0;

                do {
                    serializedBundle = serializeBundle(action);
                    if (serializedBundle != null) {
                        break;
                    }
                    retryCount++;
                } while (retryCount < MAX_RETRYS);

                if (serializedBundle != null) {

                    intent.putExtra(SHARING_KEY_SYMETRIC, serializedBundle.first);
                    intent.putExtra(SHARING_KEY_CALLOUT, serializedBundle.second);

                    activity.sendBroadcast(intent);
                    Log.d(TAG, "Sent Sync Broadcast");
                }
            } else {
                Log.d(TAG, "Missing keys");
            }
        }
    }

    public boolean isCommCareRunning() {
        if(context != null) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningAppProcessInfo processInfo : activityManager
                    .getRunningAppProcesses()) {
                if (processInfo.processName.equals("org.commcare.dalvik")) {
                    return true;
                }
            }
        }
        return false;
    }

    public void testConnection(final String url) {
        if(httpClient == null) {
            httpClient = new OkHttpClient();
        }

        Request request = new Request.Builder()
                .url(url)
                .build();

        Call call = httpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.d(TAG, "Failed to execute " + request, e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "Ping failed unexpected code " + response);
                } else {
                    Log.d(TAG, "Pinged " + url + " successfully with response code " + response
                            .code());
                }
            }
        });
    }

    public static Intent getPatientRegistrationForm() {
        return new CommCareFormSession(PATIENT_MODULE, REGISTRATION_FORM).getIntent();
    }

    public static Intent getPatientRegistrationForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(PATIENT_MODULE, REGISTRATION_FORM, params).getIntent();
    }

    public static Intent getPatientEditForm(String patientCaseId) {
        clearParams();
        params.put(PATIENT_CASE_ID, patientCaseId);
        return new CommCareFormSession(PATIENT_MODULE, PATIENT_MODIFY_FORM, params).getIntent();
    }

    public static Intent getQRSearchForm() {
        return new CommCareFormSession(SEARCH_MODULE, QR_SEARCH_FORM).getIntent();
    }


    public static Intent getQRSearchForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(SEARCH_MODULE, QR_SEARCH_FORM, params).getIntent();
    }

    public static Intent getFingerprintSearchForm() {
        return new CommCareFormSession(GUARDIAN_MODULE, FINGERPRINT_SEARCH_FORM).getIntent();
    }

    public static Intent getFingerprintSearchForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(GUARDIAN_MODULE, FINGERPRINT_SEARCH_FORM, params).getIntent();
    }

    public static Intent getDemographicSearchForm() {
        return new CommCareFormSession(SEARCH_MODULE, DEMOGRAPHIC_SEARCH_FORM).getIntent();
    }


    public static Intent getDemographicSearchForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(SEARCH_MODULE, DEMOGRAPHIC_SEARCH_FORM, params).getIntent();
    }

    public static Intent getVTNumberSearchForm() {
        return new CommCareFormSession(SEARCH_MODULE, VAXTRAC_NUMBER_SEARCH_FORM).getIntent();
    }

    public static Intent getVTNumberSearchForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(SEARCH_MODULE, VAXTRAC_NUMBER_SEARCH_FORM, params).getIntent();
    }

    public static Intent getCardNumberSearchForm() {
        return new CommCareFormSession(SEARCH_MODULE, VACCINE_CARD_NUMBER_SEARCH_FORM).getIntent();
    }

    public static Intent getCardNumberSearchForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(SEARCH_MODULE, VACCINE_CARD_NUMBER_SEARCH_FORM, params).getIntent();
    }

    public static Intent getPhoneSearchForm() {
        return new CommCareFormSession(SEARCH_MODULE, PHONE_SEARCH_FORM).getIntent();
    }

    public static Intent getPhoneSearchForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(SEARCH_MODULE, PHONE_SEARCH_FORM, params).getIntent();
    }

    public static Intent getNameSearchForm() {
        return new CommCareFormSession(SEARCH_MODULE, NAME_SEARCH_FORM).getIntent();
    }

    public static Intent getNameSearchForm(String sessionType) {
        clearParams();
        params.put(SESSION_TYPE, sessionType);
        return new CommCareFormSession(SEARCH_MODULE, NAME_SEARCH_FORM, params).getIntent();
    }

    public static Intent getGuardianEditForm(String guardianId) {
        clearParams();
        params.put(GUARDIAN_CASE_ID, guardianId);
        return new CommCareFormSession(GUARDIAN_MODULE, GUARDIAN_MODIFY_FORM, params).getIntent();
    }

    public static Intent getAddGuardianForm(String patientCaseId) {
        clearParams();
        params.put(PATIENT_CASE_ID, patientCaseId);
        return new CommCareFormSession(GUARDIAN_MODULE, GUARDIAN_ADD_FORM, params).getIntent();
    }

    public static Intent getAddQRForm(String patientCaseId) {
        clearParams();
        params.put(PATIENT_CASE_ID, patientCaseId);
        return new CommCareFormSession(PATIENT_MODULE, QR_ADD_FORM, params).getIntent();
    }

    public static void clearParams() {
        params.clear();
    }

    public void logoutBroadcast(Intent intent) {
        Log.d(TAG, "Logout Broadcast");
        isAuthenticated = false;
        if(homePresenter != null && homePresenter.get() != null) {
            homePresenter.get().revokePermissions();
            invalidateViews();
        }
    }

    public void invalidateViews() {
        homePresenter.get().invalidate();
    }

    protected Pair<byte[], byte[]> serializeBundle(Bundle b) {
        Parcel p = Parcel.obtain();
        p.setDataPosition(0);
        p.writeBundle(b);
        return encrypt(p.marshall());
    }

    protected Pair<byte[], byte[]> encrypt(byte[] input) {
        try {
            KeyGenerator generator = KeyGenerator.getInstance("AES");
            generator.init(256, new SecureRandom());
            SecretKey aesKey = generator.generateKey();

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            KeySpec ks = new X509EncodedKeySpec(publicKey);
            RSAPublicKey pubKey = (RSAPublicKey) keyFactory.generatePublic(ks);

            Cipher keyCipher = Cipher.getInstance("RSA");
            keyCipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] encryptedAesKey = keyCipher.doFinal(aesKey.getEncoded());

            Cipher dataCipher = Cipher.getInstance("AES");
            dataCipher.init(Cipher.ENCRYPT_MODE, aesKey);

            return new Pair<byte[], byte[]>(encryptedAesKey, dataCipher.doFinal(input));
        } catch(GeneralSecurityException gse ){
            gse.printStackTrace();
            return null;
        } catch(Exception dataLengthException) {
            Log.e(TAG, "DataLengthException input too large for RSA Cipher");
            return null;
        }
    }

    /* CommCare causes a crash if we try to check for a content provider before it has launched */
    public boolean hasContentProvider() {
        /*
        if(isAuthenticated) {
            if (context != null) {
                ContentResolver contentResolver = context.getContentResolver();
                if (contentResolver != null) {
                    Uri caseUri = Uri.parse(CASE_CONTENT_PROVIDER);
                    if (caseUri != null) {
                        Cursor cursor = contentResolver.query(caseUri, null, null, null, null);
                        if (cursor != null) {
                            cursor.close();
                            return true;
                        } else {
                            Log.d(TAG, "Cursor is null");
                            return false;
                        }
                    } else {
                        Log.d(TAG, "Content provider uri is null");
                        return false;
                    }
                } else {
                    Log.d(TAG, "Content Resolver is null");
                    return false;
                }
            } else {
                Log.d(TAG, "Context is null");
                return false;
            }
        } else {
            return false;
        }*/
        return false;
    }

    public void handleResult(int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            requestedKeys = false;
            this.keyId = data.getStringExtra(SHARING_KEY_ID);
            this.publicKey = data.getByteArrayExtra(SHARING_KEY_PAYLOAD);
            persistKeys(keyId, publicKey);
        }
    }

    public boolean hasKeys() {
        if(this.keyId != null && this.publicKey != null) {
            return true;
        } else {
            SharedPreferences sharedPreferences = getCommCareSharedPrefs();
            String savedKeyId = sharedPreferences.getString(KEY_ID, null);
            String savedPublicKey = sharedPreferences.getString(PUBLIC_KEY, null);
            if(savedKeyId != null && savedPublicKey != null) {
                this.keyId = savedKeyId;
                this.publicKey = Base64.decode(savedPublicKey, Base64.DEFAULT);
                return true;
            }
        }
        return false;
    }

    public void requestKeys() {
        if(!requestedKeys) {
            requestedKeys = true;
            activity.startActivityForResult(new Intent(KEY_REQUEST_ACTION),
                    KEY_REQUEST_CODE);
        }
    }

    public void refreshKeys() {
       destroyKeys();
       requestKeys();
    }

    public boolean isAuthenticated() {
        Log.d(TAG, "Authenticated? " + (isAuthenticated || hasContentProvider()));
        Log.d(TAG, "Has Content Provider " + hasContentProvider());
        return isAuthenticated || hasContentProvider();
    }

    public void persistKeys(String keyId, byte[] publicKey) {
        SharedPreferences.Editor editor = getCommCarePrefsEditor();
        editor.putString(KEY_ID, keyId);
        editor.putString(PUBLIC_KEY, Base64.encodeToString(publicKey, Base64.DEFAULT));
        editor.apply();
    }

    public void persist(String key, String value) {
        SharedPreferences.Editor editor = getCommCarePrefsEditor();
        editor.putString(key, value);
        editor.apply();
    }

    public void destroyKeys() {
        keyId = null;
        publicKey = null;
        SharedPreferences.Editor editor = getCommCarePrefsEditor();
        editor.remove(KEY_ID);
        editor.remove(PUBLIC_KEY);
        editor.apply();
    }

    public SharedPreferences getCommCareSharedPrefs() {
        return AppController.getAppContext().getSharedPreferences(COMMCARE_SHARED_PREFS, 0);
    }

    public SharedPreferences.Editor getCommCarePrefsEditor() {
        return getCommCareSharedPrefs().edit();
    }

    public boolean hasLastUser() {
        return getLastUser() != null;
    }

    public String getLastUser() {
        return getCommCareSharedPrefs().getString(LAST_WORKER, null);
    }

    public String getUserId() {
        if(AppController.mCommCareDataManager != null) {
            return AppController.mCommCareDataManager.getUserId(currentUser);
        }
        return null;
    }
}
