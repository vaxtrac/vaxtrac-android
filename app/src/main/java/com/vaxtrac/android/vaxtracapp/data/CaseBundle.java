package com.vaxtrac.android.vaxtracapp.data;

import java.util.Map;

public class CaseBundle {

    private String caseId;
    private String userId;
    private String dateModified;

    private String caseName;
    private String ownerId;
    private String caseType;

    private Map<String, String> caseData;
}
