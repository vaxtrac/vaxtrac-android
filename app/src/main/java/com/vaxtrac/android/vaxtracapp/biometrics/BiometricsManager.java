package com.vaxtrac.android.vaxtracapp.biometrics;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.ArrayMap;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by sarwar on 1/6/15.
 */
public class BiometricsManager {

    private static final String TAG = "BiometricsManager";
    private static Context mContext;
    private static Engine mEngine = null;
    private static SearchCache mSearchCache = null;
    private static boolean cacheBuilt = false;
    private static boolean matchRunning = false;
    public static List<Engine.Match> matches = null;

    public static final String SEARCH_DONE = "com.vaxtrac.android.SEARCHDONE";
    public static final String SEARCH_NO_MATCH= "com.vaxtrac.android.SEARCHNOMATCH";
    public static final String SCAN_FAILED = "com.vaxtrac.android.SCANFAILED";
    public static final String UPDATE_BIOMETRIC_STATUS= "com.vaxtrac.android.UPDATEBIOMETRICSTATUS";
    public static final String VERIFY_OK= "com.vaxtrac.android.VERIFYOK";
    public static final String VERIFY_FAILED= "com.vaxtrac.android.VERIFYFAILED";

    public static final int BIOMETRAC_SCAN_REQUEST_CODE = 89986;

    public BiometricsManager(Context context){
        mContext = context;
        mEngine = new Engine(mContext);
        mSearchCache = new SearchCache();
    }

    public void loadCache(final Cursor cursor, final String uuidColumnName, final Map<String,String> cursorMap){
        matches = null;
        cacheBuilt = false;
        mSearchCache.clearCache();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                mSearchCache.loadCache(cursor, uuidColumnName, cursorMap);
                while(!mSearchCache.cacheBuilt){
                    try {
                        Log.d(TAG, "Waiting for cache to complete");
                        Thread.sleep(250);
                    } catch (InterruptedException e) {e.printStackTrace();}
                }
                Log.d(TAG, "Caught cache Complete, sending signal");
                cacheBuilt = true;
                Log.i(TAG, "Send Broadcast for Finished CacheBuilt");
                updateStatus(String.format(AppController.getStringResource(R.string.searching_potential_matches), mSearchCache.candidates.size()));

            }
        });
        t.start();
    }

    public void findMatches(final Map<String,String> templates){
        if (matchRunning){
            Log.d(TAG, "Match Running. Killing new Process");
            return;
        }
        matchRunning = true;
        Log.d(TAG, "Starting Match Process");
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!cacheBuilt){
                    try {
                        Log.d(TAG, "Waiting for SearchCache to be Built");
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Couldn't wait for SearchCache");
                    }
                }
                matches = mEngine.getBestMatches(templates, mSearchCache);
                matchRunning = false;
                if(matches.size()<1){
                    noMatchFound();
                    return;
                }
                //sending final status
                updateStatus(String.format(AppController.getStringResource(R.string.found_high_probability_match), matches.size()));
                //Send searchdone broadcast to local listeners
                Log.i(TAG, "Send Broadcast for SearchDone");
                Intent i = new Intent(SEARCH_DONE);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
            }
        });
        t.run();
    }

    //gets scans with a tag for the caller to get the intent back from MainActivity onresult
    public static void getScans(MainActivity application, int callbackID){
        Log.i(TAG, "Starting Scan Intent");
        MainActivity.resumeState = MainActivity.ResumeState.GETTING_SCANS;
        //MainActivity.gettingScans = true;
        Bundle b = new Bundle();
        b.putString("left_finger_assignment_0","left_index");
        b.putString("right_finger_assignment_0","left_thumb");
        b.putString("right_finger_assignment_1","right_thumb");
        b.putString("left_finger_assignment_1","right_index");
        b.putString("sessionID", UUID.randomUUID().toString());
        Intent intent = new Intent("com.biometrac.core.SCAN");
        intent.putExtras(b);
        application.startActivityForResult(intent, callbackID);
    }

    public static void getVerifyScans(MainActivity application, int callbackID){
        Log.i(TAG, "Starting Scan Intent");
        Bundle b = new Bundle();
        b.putString("sessionID", UUID.randomUUID().toString());
        b.putString("left_finger_assignment_0","left_thumb");
        b.putString("right_finger_assignment_0","right_thumb");
        Intent intent = new Intent("com.biometrac.core.SCAN");
        intent.putExtras(b);
        application.startActivityForResult(intent, callbackID);
    }

    public static void scanFailed(){
        Log.i(TAG, "Send Broadcast for ScanFailed");
        Intent i = new Intent(SCAN_FAILED);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
    }

    public static void noMatchFound(){
        Log.i(TAG, "Send Broadcast for ScanFailed");
        Intent i = new Intent(SEARCH_NO_MATCH);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
    }

    public static void updateStatus(String status){
        if (status==null){return;}
        Log.i(TAG, "Send Broadcast for UpdateStatus");
        Intent i = new Intent(UPDATE_BIOMETRIC_STATUS);
        i.putExtra("STATUS",status);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
    }

    public static void verifyOk(){
        Log.i(TAG, "Send Broadcast for VerifyOk");
        Intent i = new Intent(VERIFY_OK);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
    }

    public static void verifyFailed(){
        Log.i(TAG, "Send Broadcast for VerifyFailed");
        Intent i = new Intent(VERIFY_FAILED);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
    }

    public void verifyGuardianIdentity(final String guardianID, final Map<String, String> templates) {
        Thread t= new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor cursor = AppController.mCommCareDataManager.guardianDB.allRowsColumnEquals("docid", guardianID);
                Map<String, Integer>  fingerMap = new ArrayMap<>();
                for(String fingerName: templates.keySet()){
                    String dbString = fingerName.replace("_", "");
                    int dbPos = cursor.getColumnIndex(dbString);
                    Log.d(TAG, String.format("%s found at %s", fingerName, dbPos));
                    fingerMap.put(fingerName, dbPos);
                }
                boolean matches;
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    for(String fingerName: fingerMap.keySet()){
                        int dbPos = fingerMap.get(fingerName);
                        String gallery = cursor.getString(dbPos);
                        String probe = templates.get(fingerName);
                        Log.d(TAG, String.format("gallery print for finger:%s | %s", fingerName, gallery));
                        if (gallery!=null && probe!=null){
                            matches = mEngine.verifyBoolean(probe, gallery);
                            if(matches){
                                verifyOk();
                                return;
                            }
                        }
                    }
                }
                verifyFailed();
                return;
            }
        });
        t.start();
    }
}
