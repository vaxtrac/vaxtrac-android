package com.vaxtrac.android.vaxtracapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import com.squareup.okhttp.OkHttpClient;
import com.vaxtrac.android.vaxtracapp.biometrics.BiometricsManager;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.commcare.FormSessionManager;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.models.Patient;
import com.vaxtrac.android.vaxtracapp.models.SearchObject;
import com.vaxtrac.android.vaxtracapp.ocr.OCRManager;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.reports.ReportsManager;
import com.vaxtrac.android.vaxtracapp.service.CommCareSyncService;
import com.vaxtrac.android.vaxtracapp.service.NotificationReceiver;
import com.vaxtrac.android.vaxtracapp.service.PersistenceService;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

import java.util.Locale;

import flow.Backstack;
import flow.Flow;

@ReportsCrashes(
        httpMethod = HttpSender.Method.PUT,
        reportType = HttpSender.Type.JSON,
        formUri = BuildConfig.ACRA_URI,
        formUriBasicAuthLogin = BuildConfig.ACRA_LOGIN,
        formUriBasicAuthPassword = BuildConfig.ACRA_PASS,
        customReportContent = {})
public class AppController extends Application {

    private static final String TAG = "AppController";
    //Don't use this context outside of this scope.
    private static Context mContext = null;
    private static Flow sflow;
    public static CommCareDataManager mCommCareDataManager = null;
    public static FormSessionManager mFormSessionManager = null;
    public static BiometricsManager mBiometricsManager = null;
    public static OCRManager mOCRManager = null;
    public static ReportsManager mReportsManager = null;

    public static Patient mPatient = null;
    public static SearchObject mSearchObject = null;
    public static CommCareAPI commCareAPI;
    private static Activity currentActivity;
    public static Screen mScreen;

    public static long lastSyncStamp = 0;

    public static OkHttpClient client;

    public SharedPreferences.OnSharedPreferenceChangeListener listener = new
    SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            prefChanged(key);
        }
    };

    @Override
    public void onCreate() {
        Log.i(TAG, "AppController Started");

        if(BuildConfig.DEBUG) {
            Log.d(TAG, "Debuggable Build");
        } else {
            Log.d(TAG, "Release Build");
        }

        mContext = getApplicationContext();

        PreferenceManager.setDefaultValues(this, R.xml.preferences, true);
        Preferences preferences = new Preferences();

        setupPreferencesListener();

        //loadLanguage();

        GlobalTranslations.packageName = getPackageName();
        GlobalTranslations.resources = mContext.getResources();
        //must start before CCSyncService
        mCommCareDataManager = new CommCareDataManager(mContext, true);
        mFormSessionManager = new FormSessionManager();
        mBiometricsManager = new BiometricsManager(mContext);
        mOCRManager = new OCRManager(mContext);
        mOCRManager.init();
        mReportsManager = new ReportsManager(mContext);
        startService(new Intent(mContext, PersistenceService.class));
        startService(new Intent(mContext, CommCareSyncService.class));

        commCareAPI = new CommCareAPI();
        lastSyncStamp = System.currentTimeMillis();

        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        Log.d(TAG, "Has Connectivity " + isConnected + " on network " + activeNetwork);

        super.onCreate();

        if(Preferences.isCrashReporting()) {
            ACRA.init(this);
        }

        client = new OkHttpClient();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "appplication configuration changed");
    }

    public void setupPreferencesListener() {
        PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext())
                .registerOnSharedPreferenceChangeListener(listener);
    }

    public void loadLanguage() {
        Locale defaultLocale = mContext.getResources().getConfiguration().locale;
        String localeLang = Preferences.getLocale();
        if(defaultLocale.getISO3Language().equals(localeLang)) {
            Log.d(TAG, "default and preference language are the same");
        } else {
            Log.d(TAG, "updating locale from " + defaultLocale.getISO3Language() + " to " +
                    localeLang);
            Locale locale = new Locale(localeLang);
            Locale.setDefault(locale);
            Resources resources = mContext.getResources();
            Configuration config = resources.getConfiguration();
            config.locale = locale;
            resources.updateConfiguration(config, null);
        }
    }

    public void prefChanged(String key) {
        if(key.equals(Preferences.PREF_LANGUAGE)) {
            String newLocale = PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext())
                    .getString(key, "");
            Log.d(TAG, "original locale: " + mContext.getResources().getConfiguration().locale.getISO3Language() +
                    " new locale: " + newLocale);
            if(!newLocale.equals("")) {
                if (!newLocale.equals(mContext.getResources().getConfiguration().locale.getISO3Language())){
                    Locale locale = new Locale(newLocale);
                    Locale.setDefault(locale);
                    Resources resources = currentActivity.getResources();
                    Configuration config = resources.getConfiguration();
                    config.locale = locale;
                    resources.updateConfiguration(config, null);
                    Log.d(TAG, "updated Locale " + mContext.getResources().getConfiguration()
                            .locale.getISO3Language());
                    currentActivity.recreate();
                } else {
                    Log.d(TAG, "Locale didn't change");
                }
            } else {
                Log.d(TAG, "Locale preference unset");
            }
        }
    }

    public static void notify(String msg){
        //Show a message in the Tray. Service must be running.
        PersistenceService.showMessage(msg);
    }

    public static void syncCommCare() {
        if (CommCareSyncService.sync()) {
            PersistenceService.showMessage("CommCare Sync Started");
        } else {
            PersistenceService.showMessage("CommCare Sync Was Already Started or Failed.");
        }

        /* Wait until authenticated otherwise CC Content Provider query will fail */
        if(!mReportsManager.wasReportTaskStarted()) {
            mReportsManager.startReportTask();
        } else {
            AsyncTask.Status status = mReportsManager.reportTaskStatus();
            if(status.equals(AsyncTask.Status.FINISHED)) {
                if (mReportsManager.reportTaskFailure()) {
                    mReportsManager.startReportTask();
                }
            }
        }
    }

    public static void notifyLoginBroadcast(Intent intent) {
        commCareAPI.finishLogin(intent);
    }

    public static void notifyLogoutBroadcast(Intent intent) {
        commCareAPI.logoutBroadcast(intent);
    }

    public static void killAll(){
        //Allows for killing App from MessageTray
        Log.i(TAG, "Caught shutdown signal...");
        mContext.stopService(new Intent(mContext, PersistenceService.class));
        mContext.stopService(new Intent(mContext, NotificationReceiver.class));
        mContext.stopService(new Intent(mContext, CommCareSyncService.class));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {e.printStackTrace();}
        System.exit(0);
    }

    public static void saveFlow(Flow flow) {
        sflow = flow;
    }

    public static Flow getFlow() {
        if(sflow == null) {
            Log.d(TAG, "AppController Flow is null");
            Backstack backstack = Backstack.fromUpChain(new Screens.HomeScreen());
            Log.d(TAG, "get flow activity " + currentActivity);
            sflow = new Flow(backstack, (MainActivity) currentActivity);
            if(sflow == null) {
                Log.d(TAG, "Flow is still null");
            }
        }
        return sflow;
    }

    public static void goToScreen(Screen screen) {
        sflow.goTo(screen);
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static void setCurrentActivity(Activity activity) {
        Log.d (TAG, "Current activity " + currentActivity + ", New activity " + activity);
        currentActivity = activity;
    }

    public static Activity getMainActivity() {
        return currentActivity;
    }

    public static String getStringResource(int id){
        return GlobalTranslations.getString(id);
    }
}
