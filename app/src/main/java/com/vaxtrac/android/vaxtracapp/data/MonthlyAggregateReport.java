package com.vaxtrac.android.vaxtracapp.data;


import com.vaxtrac.android.vaxtracapp.models.DoseReport;

public class MonthlyAggregateReport {

    public boolean hasMonthlyCase(int month, int year) {
        return false;
    }

    public DoseReport getMonthlyReportCase(int month, int year) {
        return null;
    }

    public boolean generateMonthlyReportCase(int month, int year) {
        return false;
    }

    public boolean updateExistingReport(DoseReport reportCase) {
        return false;
    }

}
