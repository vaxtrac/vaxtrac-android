package com.vaxtrac.android.vaxtracapp.data;

import android.database.AbstractCursor;
import android.database.Cursor;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sam on 10/24/16.
 */

/*Cursor wrapper that presents a sorted view of the underlying cursor.
* Assumes the values of the column to sort by are strings.*/

public class SortCursor extends AbstractCursor {
    String TAG = "SortCursor";

    final String ASC = "ASC";
    final String DESC = "DESC";
    private List<MyEntry> entries;
    private Cursor cursor;
    private String sortOrder;

    public SortCursor(Cursor cursor, String sortingColumn, String sortOrder) {
        super();
        this.cursor = cursor;
        this.sortOrder = sortOrder;
        entries = new ArrayList<>();

        int pos = 0;
        while (cursor.moveToNext()) {
            entries.add(new MyEntry(pos, cursor.getString(cursor.getColumnIndex(sortingColumn))));
            pos += 1;
        }
        Collections.sort(entries);
    }

    @Override
    public Bundle getExtras() {
        return cursor.getExtras();
    }
    @Override
    public void close() {
        super.close();
        cursor.close();
    }
    @Override
    public boolean onMove(int oldPosition, int newPosition) {
        return cursor.moveToPosition(entries.get(newPosition).getPosition());
    }
    @Override
    public String[] getColumnNames() {
        return cursor.getColumnNames();
    }
    @Override
    public int getCount() {
        return cursor.getCount();
    }
    @Override
    public double getDouble(int column) {
        return cursor.getDouble(column);
    }
    @Override
    public float getFloat(int column) {
        return cursor.getFloat(column);
    }
    @Override
    public int getInt(int column) {
        return cursor.getInt(column);
    }
    @Override
    public long getLong(int column) {
        return cursor.getLong(column);
    }
    @Override
    public short getShort(int column) {
        return cursor.getShort(column);
    }
    @Override
    public String getString(int column) {
        return cursor.getString(column);
    }
    @Override
    public int getType(int column) {
        return cursor.getType(column);
    }
    @Override
    public boolean isNull(int column) {
        return cursor.isNull(column);
    }

    private class MyEntry implements Comparable<MyEntry> {
        int position;
        String value;

        MyEntry(int position, String value) {
            this.position = position;
            this.value = value;
        }

        public int getPosition() {
            return position;
        }

        public String getValue() {
            return value;
        }

        @Override
        public int compareTo(MyEntry entry) {
            if (sortOrder.equals(DESC))
                return -1 * this.value.compareTo(entry.value);
            else
                return this.value.compareTo(entry.value);
        }
    }
}
