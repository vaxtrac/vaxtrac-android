package com.vaxtrac.android.vaxtracapp.data;

import android.database.Cursor;
import android.database.CursorJoiner;
import android.database.MatrixCursor;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class UnionCursor extends MatrixCursor {
    /*
        This creates a new cursor from the union of two cursors with homogeneous columns and overlapping
        (docid default) rows. Columns pushed from old cursor into the new are specified.
     */

    String TAG = "UnionCursor";


    CursorJoiner cursorJoiner;
    Cursor cursorLeft;
    Cursor cursorRight;

    public UnionCursor(String[] columnNames, Cursor c1, Cursor c2){
        this(columnNames, c1, c2, "docid", "docid");
    }

    public UnionCursor(String[] columnNames, Cursor c1, Cursor c2, String columnParse1, String columnParse2) {
        super(columnNames);
        cursorLeft = c1;
        cursorRight = c2;
        cursorJoiner = new CursorJoiner(cursorLeft, new String[]{columnParse1}, cursorRight, new String[]{columnParse2});
        Map<String, Integer> keyPositions = new HashMap<String, Integer>();
        for (String columnName : columnNames) {
            keyPositions.put(columnName, cursorLeft.getColumnIndex(columnName));
        }
        int counter = 0;
        while (cursorJoiner.hasNext()) {
            CursorJoiner.Result result = cursorJoiner.next();
            switch (result) {
                case LEFT:
                    break;
                case RIGHT:
                    break;
                case BOTH:
                    Object[] row = new Object[columnNames.length];
                    int i = 0;
                    //Pull items by original order
                    for (String column_name : columnNames) {
                        row[i] = cursorRight.getString(keyPositions.get(column_name));
                        i += 1;
                    }
                    addRow(row);
                    counter += 1;
                    break;
                default:
                    //should never be used...
                    break;
            }

        }
        Log.i(TAG, String.format("UnionCursor Loaded with %s Elements", counter));
    }


}
