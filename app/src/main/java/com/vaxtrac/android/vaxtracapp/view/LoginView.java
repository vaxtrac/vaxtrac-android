package com.vaxtrac.android.vaxtracapp.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.LoginPresenter;

import java.lang.ref.WeakReference;

public class LoginView extends LinearLayout {

    private static final String TAG = "LoginView";

    WeakReference<Context> context;
    LoginPresenter presenter;

    EditText workerField;
    EditText passwordField;
    Button loginButton;
    ProgressBar progressBar;

    Typeface typeface;

    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Log.d(TAG, "LoginView Constructor");

        this.context = new WeakReference<>(context);

        presenter = new LoginPresenter(this.context.get());

        //prevent keyboard from popping up
        ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(TAG, "LoginView onFinishInflate");

        presenter.setView(this);

        workerField = (EditText) findViewById(R.id.workerUser);
        passwordField = (EditText) findViewById(R.id.workerPassword);

        loginButton = (Button) findViewById(R.id.workerSignIn);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard();
                presenter.loginClicked();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.loginProgress);

        workerField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                workerField.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if(presenter.hasLastUser()) {
            String lastUser = presenter.getLastUser();
            workerField.setText(lastUser);
            passwordField.setFocusableInTouchMode(true);
            passwordField.setFocusable(true);
            passwordField.requestFocus();
        }

        passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordField.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    presenter.loginClicked();
                }
                return false;
            }
        });
    }

    public void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) context.get().
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(passwordField.getWindowToken(), 0);
    }

    public void showKeyboard() {
        ((Activity)context.get()).getWindow()
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public Typeface loadTypeFace(String font) {
        return Typeface.createFromAsset(AppController.getAppContext().getAssets(),
                font);
    }

    public String getUsername() {
        return workerField.getText().toString();
    }

    public String getPassword() {
        return passwordField.getText().toString();
    }

    public void focusUsername(String warning) {
        workerField.setError(warning);
    }

    public void focusPassword(String warning) {
        passwordField.setError(warning);
    }

    public void setProgressVisibility(int state) { progressBar.setVisibility(state); }

    public void displayToast(String message) {
        Toast.makeText(context.get(), message, Toast.LENGTH_LONG).show();
    }
}
