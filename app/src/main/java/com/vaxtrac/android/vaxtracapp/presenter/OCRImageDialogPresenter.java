package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;

import com.etsy.android.grid.StaggeredGridView;
import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.models.Vial;
import com.vaxtrac.android.vaxtracapp.ocr.VialAdapter;
import com.vaxtrac.android.vaxtracapp.view.OCRImageDialogView;

public class OCRImageDialogPresenter extends Presenter {

    static final String TAG ="OCRImageDialogPresenter";

    Context context;
    OCRImageDialogView view;

    VialAdapter vialAdapter;
    static Vial selectedVial;

    public OCRImageDialogPresenter(Context context, OCRImageDialogView view) {
        this.context = context;
        this.view = view;
        vialAdapter = new VialAdapter(context, AppController.mOCRManager.getAllVials(),
                LayoutInflater.from(context));
    }

    public void populateImages(StaggeredGridView grid) {
        grid.setAdapter(vialAdapter);
    }

    public void setSelectedVial(Object item) {
        selectedVial = (Vial) item;
    }

    public static Vial getVial() {
        return selectedVial;
    }

    public void filter(String constraint) {
       Log.d(TAG, "Filter " + constraint);
       vialAdapter.getFilter().filter(constraint);
    }
}
