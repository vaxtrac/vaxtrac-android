package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.vaxtrac.android.vaxtracapp.presenter.HistoryPresenter;

public class HistoryView extends LinearLayout {

    HistoryPresenter presenter;

    public HistoryView(Context context, AttributeSet attrs) {
        super(context, attrs);

        presenter = new HistoryPresenter(context, this);
    }

    protected void onFinishInflate() {

    }
}
