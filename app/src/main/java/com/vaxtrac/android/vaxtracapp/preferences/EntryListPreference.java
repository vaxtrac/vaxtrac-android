package com.vaxtrac.android.vaxtracapp.preferences;

import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class EntryListPreference extends ListPreference {

    public EntryListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        Preferences.addPreference(this);
    }
}
