package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.PreferencesPresenter;

public class PreferencesView extends LinearLayout{

    private static final String TAG = "PreferencesView";

    private Context context;
    private PreferencesPresenter presenter;
    private ListView preferencesList;

    public PreferencesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        presenter = new PreferencesPresenter(context, this);
    }

    @Override protected void onFinishInflate() {
        preferencesList = (ListView) findViewById(R.id.preferences_list);
        preferencesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, " Position " + position + " clicked");
                presenter.handleClick(parent, view, position, id);
            }
        });
    }
}
