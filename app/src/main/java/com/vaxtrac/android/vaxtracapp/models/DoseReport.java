package com.vaxtrac.android.vaxtracapp.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.BuildConfig;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager.DoseDBHandler;
import com.vaxtrac.android.vaxtracapp.data.ReportTypeAdapter;
import com.vaxtrac.android.vaxtracapp.data.ReportsDBHelper;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import dateconverter.NepaliDateConverter;

public class DoseReport {

    private static final String TAG = DoseReport.class.getName();

    private static final int IN_PROGRESS_STATUS = 0;

    List<int[]> categories;
    Map<String, Map<Integer, int[]>> data;

    String caseId;
    Date lastModified;
    String user;

    Integer month;
    Integer year;

    Calendar calendar;
    Calendar startDate;
    Calendar endDate;

    boolean archived;
    Integer status;
    Date lastDoseTimestamp;
    Date lastSent;
    Integer revisions;
    String period;

    String categoriesJson;
    String dataJson;

    private static NepaliDateConverter nepaliDateConverter = new NepaliDateConverter();
    private static SimpleDateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");

    public static final String MONTHLY_CASE_TYPE = "monthly-report";
    private static final String TIMESTAMP_COLUMN = "timestamp";
    private static final String VACCINE_COLUMN = "vaccine_name";
    private static final String AGE_COLUMN = "patient_age";

    private static DateFormat dateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
    private static DateFormat timestampFormat =
            new UnlocalizedDateFormat("yyyy-MM-dd HH:mm:ss.SSSS");

    private static Cursor reportCases;

    public DoseReport() {

    }

    public static DoseReport loadFromCommCare(int month, int year) {
        findReportCases();
        return new DoseReport();
    }

    public static DoseReport fromFormData(List<Dose> doses, int patientAge) {
        Calendar now = Calendar.getInstance();

        DoseReport newReport = new DoseReport.Builder()
                .caseId(UUID.randomUUID().toString())
                .archived(false)
                .status(IN_PROGRESS_STATUS)
                .data(buildDoseData(doses, patientAge))
                .build();

        return newReport;
    }

    public static DoseReport loadFromCommCare(ContentValues contentValues) {
        Log.d(TAG, "loadFromCommCare" + contentValues.getAsString(ReportsDBHelper
                .REPORT_DATA_COLUMN));
        DoseReport.Builder builder = new DoseReport.Builder();
        builder.date(contentValues.getAsInteger(ReportsDBHelper.MONTH_COLUMN), contentValues
                .getAsInteger(ReportsDBHelper.YEAR_COLUMN));
        builder.caseId(contentValues.getAsString(ReportsDBHelper.CASE_ID_COLUMN));
        builder.lastModified(contentValues.getAsString(ReportsDBHelper.LAST_MODIFIED_COULMN));
        builder.user(contentValues.getAsString(ReportsDBHelper.USER_ID_COLUMN));
        builder.status(contentValues.getAsInteger(ReportsDBHelper.REPORT_STATUS_COLUMN));
        builder.archived(contentValues.getAsBoolean(ReportsDBHelper.ARCHIVED_COLUMN));
        builder.startDate(contentValues.getAsString(ReportsDBHelper.MONTH_START_COLUMN));
        builder.endDate(contentValues.getAsString(ReportsDBHelper.MONTH_END_COLUMN));
        builder.categories(contentValues.getAsString(ReportsDBHelper.CATEGORIES_COLUMN));
        builder.data(contentValues.getAsString(ReportsDBHelper.REPORT_DATA_COLUMN));
        return builder.build();
    }

    public static DoseReport fromDatabaseValues(ContentValues contentValues) {
        DoseReport.Builder builder = new DoseReport.Builder();
        Log.d(TAG, "fromDatabaseValues" + contentValues.getAsString(ReportsDBHelper
                .REPORT_DATA_COLUMN));
        builder.date(contentValues.getAsInteger(ReportsDBHelper.MONTH_COLUMN),
                contentValues.getAsInteger(ReportsDBHelper.YEAR_COLUMN));
        builder.caseId(contentValues.getAsString(ReportsDBHelper.CASE_ID_COLUMN));
        builder.categories(contentValues.getAsString(ReportsDBHelper
                .CATEGORIES_COLUMN));
        builder.status(contentValues.getAsInteger(ReportsDBHelper.REPORT_STATUS_COLUMN));
        builder.archived(contentValues.getAsBoolean(ReportsDBHelper.ARCHIVED_COLUMN));
        builder.data(contentValues.getAsString(ReportsDBHelper.REPORT_DATA_COLUMN));
        return builder.build();
    }

    private DoseReport(Builder builder) {
        this.month = builder.month;
        this.year = builder.year;
        this.categoriesJson = builder.categoriesJson;
        this.categories = builder.categories;
        this.dataJson = builder.dataJson;
        this.data = builder.data;

        this.caseId = builder.caseId;
        this.status = builder.status;
        this.archived = builder.archived;

        calendar = Calendar.getInstance();

        if(month != null && year != null && month > 0 && month <= 12) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month-1);
        }

        if(BuildConfig.FLAVOR.equals("nep")) {
            calendar = convertToNepali(calendar);
        }

        Log.d(TAG, "todays calendar date " + calendar.getTime().toString());

        month = calendar.get(Calendar.MONTH) + 1;
        year = calendar.get(Calendar.YEAR);

        if(categories == null) {
            categories = loadDefaultCategories();
        }

        if(data == null) {
            if(archived) {
                AppController.mReportsManager.hasExisitingReport(month, year);
            } else {
                if(status == 1) {
                    // Find CommCare Case
                } else {
                    // Look in Dose Database mark as dirty submit with Form
                }
            }
        }

        startDate = getStartDate();
        endDate = getEndDate();
    }

    public String getDateKey() {
        return year + "-" + String.format("%02d", month);
    }

    public String getTwoDigitMonth() {
        return String.format("%02d", month);
    }

    public void addRecordedDoses(List<Dose> doses) {
        JSONObject unpackedReport = new JSONObject();
    }

    private static Map<String, Map<Integer, int[]>> buildDoseData(List<Dose> doses, int
            patientAge) {
        /* Existing Report Data? */
        Map<String, Map<Integer, int[]>> reportData = new HashMap<>();
        List<int[]> categories = loadDefaultCategories();
        int categoriesSize = categories.size();
        int categoryBucket = categoryForAge(categories, patientAge);
        for(Dose dose : doses) {
            String antigen= dose.getAntigen();
            Integer doseNumber = dose.getDoseNumber();
            Map<Integer, int[]> series = reportData.get(antigen);
            if(series == null) {
                series = new HashMap<>();
                series.put(doseNumber, new int[categoriesSize]);
            }
            int[] values = series.get(doseNumber);
            values[categoryBucket] += 1;
            series.put(dose.getDoseNumber(), values);
            reportData.put(dose.getAntigen(), series);
        }
        return reportData;
    }

    private static int categoryForAge(List<int[]> categories, int age) {
        int i = 0;
        for(int[] range : categories) {
            if(age >= range[0] && age <= range[1]) {
                return i;
            }
        }
        return -1;
    }

    public boolean hasAntigen(String antigen) {
        return data.containsKey(antigen);
    }

    public static class Builder {
        private Integer month;
        private Integer year;
        private Date startDate;
        private Date endDate;
        private String categoriesJson;
        private List<int[]> categories;
        private String dataJson;
        private Map<String, Map<Integer, int[]>> data;

        private String caseId;
        private String userId;
        private Date lastModified;
        private Integer status;
        private boolean archived;

        public Builder date(Integer month, Integer year) {
            Log.d(TAG, "Builder " + year + " " + month);
            this.month = month;
            this.year = year;
            return this;
        }

        public Builder categories(String categories) {
            this.categoriesJson = categories;
            this.categories = parseCategories(categories);
            return this;
        }

        public Builder data(String data) {
            Log.d(TAG, "Data " + data);
            this.dataJson = data;
            this.data = parseJSON(data);
            return this;
        }

        public Builder data(Map<String, Map<Integer, int[]>> data) {
            this.data = data;
            return this;
        }

        public Builder caseId(String caseId) {
            this.caseId = caseId;
            return this;
        }

        public Builder status(Integer status) {
            this.status = status;
            return this;
        }

        public Builder archived(boolean archived) {
            this.archived = archived;
            return this;
        }

        public Builder lastModified(String lastModified) {
            try {
                this.lastModified = dateFormat.parse(lastModified);
            } catch (ParseException e) {
                Log.e(TAG, "last modified date parse exception in builder", e);
            }
            return this;
        }

        public Builder startDate(String startDate) {
            try {
                this.startDate = dateFormat.parse(startDate);
            } catch (ParseException e) {
                Log.e(TAG, "start date parse exception in builder", e);
            }
            return this;
        }

        public Builder endDate(String endDate) {
            try {
                this.endDate = dateFormat.parse(endDate);
            } catch (ParseException e) {
                Log.e(TAG, "end date parse exception in builder", e);
            }
            return this;
        }

        public Builder user(String userId) {
            this.userId = userId;
            return this;
        }

        public DoseReport build() {
            return new DoseReport(this);
        }
    }

    public static List<int[]> parseCategories(String jsonCategories) {
        JSONArray categoriesArr = null;
        try {
            categoriesArr = new JSONArray(jsonCategories);
        } catch (JSONException e) {
            Log.d(TAG, "JSON Exception parsing Report Categories", e);
        }
        if(categoriesArr != null) {
            List<int[]> reportCategories = new ArrayList<>();
            try {
                for(int i = 0; i < categoriesArr.length(); i++) {
                    JSONArray values = categoriesArr.getJSONArray(i);
                    int[] categoryRanges = new int[values.length()];
                    for(int j = 0; j < values.length(); j++) {
                        categoryRanges[j] = values.getInt(j);
                    }
                    reportCategories.add(i, categoryRanges);
                }
                return reportCategories;
            } catch (JSONException e) {
                Log.d(TAG, "Exception iterating over categories array");
            }
        }
        return null;
    }

    public static Map<String, Map<Integer, int[]>> parseJSON(String jsonReportData) {
        JSONObject json = null;
        try {
            json = new JSONObject(jsonReportData);
        } catch (JSONException e) {
            Log.d(TAG, "JSON Exception parsing Report Data " + e.getMessage());
        }

        Map<String, Map<Integer, int[]>> reportData = new HashMap<>();
        JSONObject report;
        if(json != null) {
            try {
                 for(Iterator<String> keysIterator = json.keys(); keysIterator.hasNext();) {

                     String key = keysIterator.next();
                     Log.d(TAG, "reports key " + key);
                     JSONObject series = json.getJSONObject(key);
                     Map<Integer,int[]> antigenSeries = new HashMap<>();
                     for(Iterator<String> seriesIterator = series.keys(); seriesIterator.hasNext
                             ();) {
                         String seriesNumber = seriesIterator.next();
                         Log.d(TAG, "series key " + seriesNumber);
                         JSONArray categoryCounts = series.getJSONArray(seriesNumber);
                         int[] values = new int[categoryCounts.length()];
                         for(int i = 0; i < categoryCounts.length(); i++) {
                             Log.d(TAG, "category " + i + " " + categoryCounts.getString(i));
                             values[i] = categoryCounts.getInt(i);
                         }
                         antigenSeries.put(Integer.parseInt(seriesNumber), values);
                     }
                     reportData.put(key.toLowerCase(), antigenSeries);
                 }
            } catch (JSONException e) {
                Log.d(TAG, "JSONException while fetching report json " + e.getMessage());
            }
        }
        return reportData;
    }

    public ReportData[] parseGson(String jsonReportData) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ReportData.class, ReportTypeAdapter.class);
        Gson gson = gsonBuilder.create();
        return gson.fromJson(jsonReportData, ReportData[].class);
    }

    public List<int[]> getCategories() {
        return categories;
    }

    public int getCategoryCount() {
        return categories.size();
    }

    public Map<Integer, int[]> getAntigenSeries(String antigen) {
        return data.get(antigen.toLowerCase());
    }

    public void sumDoses(int month, int year) {
        Calendar startDate = getStartDate();
        Calendar endDate = getEndDate();
        SimpleDateFormat sqliteDateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
        Cursor allDoses = getDoseDatabase().allRowsItemsBetween(TIMESTAMP_COLUMN, sqliteDateFormat.format
                (startDate.getTime()), sqliteDateFormat.format(endDate));
        Cursor uniqueAntigens = getDoseDatabase().uniqueItemsFromColumn(VACCINE_COLUMN);

        int timestampIndex = uniqueAntigens.getColumnIndex(TIMESTAMP_COLUMN);
        int vaccineIndex = uniqueAntigens.getColumnIndex(VACCINE_COLUMN);
        int ageIndex = uniqueAntigens.getColumnIndex(AGE_COLUMN);

        for(uniqueAntigens.moveToFirst(); uniqueAntigens.isLast(); uniqueAntigens.moveToNext()) {
            String name = uniqueAntigens.getString(vaccineIndex);
            String timestamp = uniqueAntigens.getString(timestampIndex);
            String age = uniqueAntigens.getString(ageIndex);
        }

        allDoses.close();
        uniqueAntigens.close();
    }

    public Bundle getProperties() {
        Bundle properties = new Bundle();
        // attributes
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.CASE_ID, caseId);
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.LAST_MODIFIED, dateFormat.format
                (lastModified));
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.USER_ID, user);
        // create block
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.CASE_NAME, caseId);
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.CASE_TYPE, ReportCase.TYPE);
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.OWNER_ID, user);
        // update block
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.YEAR, year.toString());
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.MONTH, String.format("%02d", month));
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.DATE_START, dateFormat.format(startDate.getTime()));
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.DATE_END, dateFormat.format(endDate.getTime()));
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.PERIOD, period);
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.CATEGORIES, categoriesJson);
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.REPORT_DATA, dataJson);
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.ARCHIVED, archived ? "1" : "0");
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.REPORT_STATUS, status.toString());
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.REVISIONS, revisions.toString());
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.LAST_DOSE_TIMESTAMP, timestampFormat.format
                (lastDoseTimestamp));
        properties.putString(ReportCase.REPORT_PREFIX + ReportCase.LAST_SENT, dateFormat.format(lastSent));
        return properties;
    }

    public int getMonth() {
        return this.month;
    }

    public int getYear() {
        return this.year;
    }

    private DoseDBHandler getDoseDatabase() {
        return AppController.mCommCareDataManager.doseDB;
    }

    private static void findReportCase() {

    }

    public String toString() {
        StringBuilder reportData = new StringBuilder();
        for(Map.Entry<String, Map<Integer, int[]>> entry : data.entrySet()) {
            for(Map.Entry<Integer, int[]> series : entry.getValue().entrySet()) {
                reportData.append(entry.getKey())
                        .append(" ")
                        .append(series.getKey())
                        .append(" ")
                        .append(Arrays.toString(series.getValue()));
            }
            reportData.append("\n");
        }
        return reportData.toString();
    }

    private static void findReportCases() {
        new ReportsCaseTask(new ReportsCaseTask.TaskListener() {
            @Override
            public void onFinished(Cursor result) {
                Log.d(TAG, "report cases async task finished");
                Log.d(TAG, "report cases size " + result.getCount());
                reportCases = result;
                inflateReports();
            }
        }).execute(MONTHLY_CASE_TYPE);
    }

    private static void inflateReports() {
        if(reportCases.moveToFirst()) {
            int caseIdColumn = reportCases.getColumnIndex(CommCareDataManager.CASE_ID_COLUMN);
            Map<String,String> contents = new HashMap<String, String>();
            do {
                try {
                    AppController.mCommCareDataManager.loadCaseFromCommCare(reportCases.getString
                            (caseIdColumn));
                    int key_position = reportCases.getColumnIndex(CommCareDataManager.KEY_COLUMN);
                    int data_position = reportCases.getColumnIndex(CommCareDataManager.DATA_COLUMN);
                    contents.put(reportCases.getString(key_position), reportCases.getString
                            (data_position));
                }catch (CursorIndexOutOfBoundsException e){
                    Log.e(TAG, e.getMessage());
                }
            } while (reportCases.moveToNext());
        }
    }

    private static List<int[]> loadDefaultCategories() {
        Context context = AppController.getAppContext();
        int[] start = context.getResources().getIntArray(R.array.reporting_categories_start);
        int[] end = context.getResources().getIntArray(R.array.reporting_categories_end);
        List<int[]> reportCategories = new ArrayList<>();
        for(int i = 0, categoriesLength = start.length; i < categoriesLength; i++) {
            reportCategories.add(new int[]{start[i], end[i]});
        }
        return reportCategories;
    }

    private Date getMonthStartDate(int month, int year) {
       return null;
    }

    private Calendar getStartDate() {
        calendar.set(year, month, 1);
        if(BuildConfig.FLAVOR.equals("nep")) {
            return convertToGregorian(calendar);
        }

        return calendar;
    }

    private Calendar getEndDate() {
        calendar.set(year, month, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar;
    }

    private Calendar convertToNepali(Calendar calendar) {

        dateconverter.Date nepaliDate = nepaliDateConverter.fromGregorianDate(nepaliDateFormat
                .format(calendar.getTime()));

        Calendar nepaliCal = Calendar.getInstance();
        nepaliCal.set(Calendar.MONTH, nepaliDate.getMonth());
        nepaliCal.set(Calendar.YEAR, nepaliDate.getYear());
        nepaliCal.set(Calendar.DAY_OF_MONTH, nepaliDate.getDay());

        return nepaliCal;
    }

    private Calendar convertToGregorian(Calendar calendar) {
        dateconverter.Date gregorianDate = nepaliDateConverter.toGregorianDate(nepaliDateFormat
                .format(calendar.getTime()));
        Calendar gregorianCalendar = Calendar.getInstance();
        gregorianCalendar.set(gregorianDate.getYear(),
                gregorianDate.getMonth(),
                gregorianDate.getDay());

        return gregorianCalendar;
    }

    private static class ReportsCaseTask extends AsyncTask<String, Void, Cursor> {

        public interface TaskListener {
            public void onFinished(Cursor result);
        }

        private final TaskListener listener;

        public ReportsCaseTask(TaskListener listener) {
            this.listener = listener;
        }

        @Override
        protected Cursor doInBackground(String... params) {
            String caseType = params[0];
            return AppController.mCommCareDataManager.findCaseType(caseType);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            listener.onFinished(cursor);
        }
    }
}
