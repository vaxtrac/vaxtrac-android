package com.vaxtrac.android.vaxtracapp.preferences;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.R;

public class PreferencesFragment extends android.preference.PreferenceFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("PreferencesFragment", "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        Log.d("PreferencesFragment", "Developer Preference " + String.valueOf(findPreference(getString(R
                .string.pref_developer)) != null));
        setRetainInstance(true);
    }


}
