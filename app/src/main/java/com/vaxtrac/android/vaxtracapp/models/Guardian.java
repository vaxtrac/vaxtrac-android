package com.vaxtrac.android.vaxtracapp.models;

import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Guardian {

    private static final String TAG = Guardian.class.getName();
    private Map<String,String> attributes = null;

    private static final String GUARDIAN_ID_KEY = "guardian_id";
    private static final String GUARDIAN_NAME_KEY = "guardian_name";
    private static final String MOTHERS_NAME_KEY = "mothers_name";
    private static final String FATHERS_NAME_KEY = "fathers_name";

    private static final String CHILD_NAME_KEY = "child_name";
    private static final String CHILD_ID_KEY = "child_id";
    private static final String CHILD_DOB_KEY = "child_dob";
    private static final String CHILD_SEX_KEY = "child_sex";
    private static final String RELATION_TYPE_KEY = "relation_type";
    private static final String DOB_KEY = "date_of_birth";
    private static final String GUARDIAN_DOB_KEY = "guardian_date_of_birth";
    private static final String BIRTHPLACE_KEY = "birthplace";
    private static final String GUARDIAN_SEX_KEY = "guardian_sex";
    private static final String GUARDIAN_VISIT_COUNT = "guardian_visit_count";
    private static final String LAST_VISIT_KEY = "last_visit";

    public Guardian(){

    }

    public void loadSearchObject(String childID, SearchObject object){
        attributes = new HashMap<>();
        Map<String,String> data = object.getProperties();

        Date now = Calendar.getInstance().getTime();
        SimpleDateFormat df1 = new UnlocalizedDateFormat("yyyy-MM-d");
        String todayString = df1.format(now);

        String mothersName = data.get(MOTHERS_NAME_KEY);
        String fathersName = data.get(FATHERS_NAME_KEY);

        if(mothersName != null && fathersName != null) {
            attributes.put(MOTHERS_NAME_KEY, mothersName);
            attributes.put(FATHERS_NAME_KEY, fathersName);
        }

        //make new GuardianID
        String guardianID = UUID.randomUUID().toString();
        attributes.put(GUARDIAN_ID_KEY, guardianID);
        /* We assign guardian name to be the case name so it shouldn't be null */
        if(data.get(GUARDIAN_NAME_KEY) == null) {
            attributes.put(GUARDIAN_NAME_KEY, "");
        } else {
            attributes.put(GUARDIAN_NAME_KEY, data.get(GUARDIAN_NAME_KEY));
        }

        List<String> fingers = object.getFingerFields();
        //fingerprints
        String pref = "fingerprint_";
        for(String finger: fingers){
            String template = data.get(finger);
            if (template != null){
                attributes.put(pref+finger, template);
            }else{
                attributes.put(pref+finger, "");
            }
        }
        attributes.put(CHILD_NAME_KEY, data.get(CHILD_NAME_KEY));
        attributes.put(CHILD_ID_KEY, childID);
        attributes.put(CHILD_DOB_KEY, data.get(CHILD_DOB_KEY));
        attributes.put(CHILD_SEX_KEY, data.get(CHILD_SEX_KEY));
        attributes.put(RELATION_TYPE_KEY, data.get(RELATION_TYPE_KEY));

        String dob = data.get(DOB_KEY);
        if(dob != null){
            attributes.put(GUARDIAN_DOB_KEY, dob);
        }else{
            attributes.put(GUARDIAN_DOB_KEY, "");
        }

        String birthplace = data.get(BIRTHPLACE_KEY);
        if(birthplace != null){
            attributes.put(BIRTHPLACE_KEY, birthplace);
        }else{
            attributes.put(BIRTHPLACE_KEY, "");
        }

        attributes.put(GUARDIAN_SEX_KEY, "");
        attributes.put(GUARDIAN_VISIT_COUNT, "1");
        attributes.put(LAST_VISIT_KEY, todayString);
    }

    public Map<String,String> getAttributes(){
        return attributes;
    }
}
