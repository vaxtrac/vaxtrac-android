package com.vaxtrac.android.vaxtracapp.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.DemographicPresenter;

public class DemographicView extends LinearLayout {

    DemographicPresenter presenter;

    Button editPatient;
    Button addGuardian;
    Button editGuardian;
    Button addQRCode;
    Button exitButton;

    public DemographicView(Context context, AttributeSet attrs) {
        super(context, attrs);
        presenter = new DemographicPresenter(context, this);
    }

    protected void onFinishInflate() {

        editPatient = (Button) findViewById(R.id.edit_patient_info);
        editPatient.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.editPatient();
            }
        });

        addGuardian = (Button) findViewById(R.id.add_guardian_info);
        addGuardian.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addGuardian();
            }
        });

        editGuardian = (Button) findViewById(R.id.edit_guardian_info);
        editGuardian.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.editGuardian();
            }
        });

        addQRCode = (Button) findViewById(R.id.add_qr_code);
        addQRCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addQRCode();
            }
        });

        exitButton = (Button) findViewById(R.id.exit_demographics);
        exitButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.exit();
            }
        });

        super.onFinishInflate();
    }
}