package com.vaxtrac.android.vaxtracapp.models;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class VaccineHistory {

    final private String TAG = "VaccineHistory";

    private JSONObject reportedVaccines;
    private JSONObject recordedVaccines;
    private Map<String, List<Date>> history;
    private Map<String, List<Dose>> newRecorded;
    private Map<String, List<Dose>> newReported;

    private Map<String,List<Dose>> computedSchedule;
    public boolean computed_schedule_ready = false;

    private Date dateOfBirth;
    private boolean was_updated = true;

    @SuppressWarnings("serial")
    public VaccineHistory(JSONObject reported, JSONObject recorded, final String dob_string) {
        reportedVaccines = reported;
        recordedVaccines = recorded;
        newRecorded = new HashMap<String, List<Dose>>();
        newReported = new HashMap<String, List<Dose>>();
        computedSchedule = new HashMap<String, List<Dose>>();

        //Such Java hackiness...
        dateOfBirth = dateList(new ArrayList<String>() {{
            add(dob_string);
        }}).get(0);
        Map<String,List<String>> _combined = combineHistory(reported, recorded);
        history = date_history(_combined);
    }

    private boolean isToday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Calendar today = Calendar.getInstance();
        return (cal.get(Calendar.YEAR) == today.get(Calendar.YEAR)
                && cal.get(Calendar.MONTH) == today.get(Calendar.MONTH)
                && cal.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH));
    }

    private void displayWarningDialog(String message, MainActivity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setTitle(R.string.vaccinecard_dialog_title);
        builder.setPositiveButton(R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void reportDose(String antigen, Dose d, MainActivity activity){
        if (d.getDate().before(dateOfBirth)) {
            displayWarningDialog(activity.getResources().getString(
                    R.string.vaccinecard_date_before_dob_dialog_message), activity);
            return;
        }

        if (isToday(d.getDate())) {
            displayWarningDialog(activity.getResources().getString(
                    R.string.vaccinecard_date_today_dialog_message), activity);
            return;
        }

        if (d.getDate().after(Calendar.getInstance().getTime())) {
            displayWarningDialog(activity.getResources().getString(
                    R.string.vaccinecard_date_in_future_dialog_message), activity);
            return;
        }

        List<Dose> doses = newReported.get(antigen);
        if (doses == null){
            doses = new ArrayList<Dose>();
            doses.add(d);
        }else{
            doses.add(d);
        }
        newReported.put(antigen, doses);
        Log.d(TAG, antigen + " now has " + Integer.toString(doses.size()) + " new reported doses.");
    }

    public void recordDose(String antigen, Dose d){
        List<Dose> doses = newRecorded.get(antigen);
        if (doses == null){
            doses = new ArrayList<Dose>();
        }
        doses.add(d);
        newRecorded.put(antigen, doses);
        Log.d(TAG, antigen + " now has " + Integer.toString(doses.size()) + " new recorded doses.");
    }

    public void removeRecordedDose(String antigen, Dose d){
        List<Dose> doses = newRecorded.get(antigen);
        int size = doses.size();
        if (doses == null){
            return;
        }else{
            doses.remove(d);
        }
        //failed to remove, looking for non-identical dose with identical features
        if (doses.size() == size){
            Dose toRemove = null;
            for (Dose dose: doses){
                if(dose.getDoseNumber() == d.getDoseNumber() && (d.getDate().compareTo(dose
                        .getDate()) == 0)){
                    toRemove = dose;
                }
            }
            if (toRemove != null){
                doses.remove(toRemove);
            }
        }
        newRecorded.put(antigen, doses);
        Log.d(TAG, antigen + " now has " + Integer.toString(doses.size()) + " new recorded doses.");
    }


    //Removes by date from newReported
    public void removeReportedDose(String antigen, Dose d){
        List<Dose> doses = newReported.get(antigen);
        if (doses == null){
            return;
        }else{
            Dose toRemove = null;
            for (Dose dose: doses){
                if (dose.getDate().equals(d.getDate())){
                    toRemove = dose;
                    break;
                }
            }
            if (toRemove != null){
                doses.remove(toRemove);
            }
        }
        newReported.put(antigen, doses);
        Log.d(TAG, antigen + " now has " + Integer.toString(doses.size()) + " new reported doses.");
    }

    public boolean isNewRecorded(Dose dose){
        List<Dose> newRecorded = getNewRecorded(dose.getAntigen());
        if (newRecorded == null){
            return false;
        }
        for(Dose d: newRecorded){
            if (d.getDoseNumber() == dose.getDoseNumber()){
                Log.i(TAG, String.format("Newly Recorded Vaccine| %s : %s", dose.getAntigen(), dose.doseNumber));
                return true;
            }
        }
        return false;
    }

    public boolean isNewReported(Dose dose){
        List<Dose> newReported = getNewReported(dose.getAntigen());
        if (newReported == null){
            return false;
        }
        for(Dose d: newReported){
            if (d.getDate().equals(dose.getDate())){
                Log.i(TAG, String.format("Newly Reported Vaccine| %s", dose.getAntigen()));
                return true;
            }
        }
        return false;
    }

    public List<Dose> getNewReported(String antigen){
        return newReported.get(antigen);
    }

    public List<Dose> getNewRecorded(String antigen){
        return newRecorded.get(antigen);
    }

    public Map<String, List<Dose>> getComputedSchedule() {
        return computedSchedule;
    }

    public boolean wasUpdated() {
        return this.was_updated;
    }

    public Date getDOB() {
        return this.dateOfBirth;
    }

    public List<Date> getAntigenHistory(String antigen){
        List<Date> dates;
        //Log.d(TAG, "antigen " + antigen);

        if (history.get(antigen) == null) {
            dates = new ArrayList<>();
        } else {
            /* Deep Copy */
            dates = new ArrayList<>(history.get(antigen).size());
            for(Date date : history.get(antigen)) {
                dates.add((Date)date.clone());
            }
        }

        List<Dose> addedReported = newReported.get(antigen);
        if (addedReported != null){
            for(Dose d: addedReported){
                dates.add((Date)d.getDate().clone());
            }
        }
        List<Dose> addedRecorded = newRecorded.get(antigen);
        if (addedRecorded != null){
            for(Dose d: addedRecorded){
                dates.add((Date)d.getDate().clone());
            }
        }

        Collections.sort(dates);
        return dates;
    }



    //commits doses to json
    public void finalizeDoses(){
        SimpleDateFormat dateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");

        for (String antigen: newRecorded.keySet()){
            List<Dose> doses= newRecorded.get(antigen);
            if (doses == null){
                continue;
            }else{
                JSONArray previous;
                try {
                    previous = (JSONArray) recordedVaccines.get(antigen);
                } catch (JSONException e) {
                    previous = new JSONArray();
                }
                if (previous==null){
                    previous = new JSONArray();
                }
                for(Dose d:doses){
                    previous.put(dateFormat.format(d.getDate()));
                }
                try {
                    recordedVaccines.put(antigen, previous);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        for (String antigen: newReported.keySet()){
            List<Dose> doses= newReported.get(antigen);
            if (doses == null){
                continue;
            }else{
                JSONArray previous;
                try {
                    previous = (JSONArray) reportedVaccines.get(antigen);
                } catch (JSONException e) {
                    previous = new JSONArray();
                }
                if (previous==null){
                    previous = new JSONArray();
                }
                for(Dose d:doses){
                    previous.put(dateFormat.format(d.getDate()));
                }
                try {
                    reportedVaccines.put(antigen, previous);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public String reportedString(){
        if (reportedVaccines == null){
            return "{}";
        }
        return reportedVaccines.toString();
    }

    public String recordedString(){
        if (reportedVaccines == null){
            return "{}";
        }
        return recordedVaccines.toString();
    }

    private Map<String, List<Date>> date_history(Map<String, List<String>> _combined) {
        Map<String, List<Date>> _history = new HashMap<String, List<Date>>();
        Iterator<String>keys = _combined.keySet().iterator();
        Log.d(TAG,"Combined history to date has " + Integer.toString(_combined.size())+ " keys.");
        while(keys.hasNext()){
            String k = keys.next();
            //Log.i(TAG,"Adding doses of:" + k);
            _history.put(k, dateList(_combined.get(k)));
        }
        return _history;
    }

    @SuppressLint("SimpleDateFormat")
    private List<Date> dateList(List<String> list) {
        List<Date> dates = new ArrayList<Date>();
        DateFormat df = new UnlocalizedDateFormat("yyyy-MM-dd");
        for(String date : list) {
            try {
                dates.add(df.parse(date));
                //Log.d(TAG, "Added dose on " + date);
            } catch (ParseException e) {
                //Log.d(TAG,"Couldn't add date: " + date);
                e.printStackTrace();
            }
        }
        Collections.sort(dates);
        return dates;
    }

    private Map<String, List<String>> combineHistory(JSONObject reported,
                                                     JSONObject recorded) {
        Log.d(TAG, "Combining History of " + Integer.toString(reported.length()+recorded.length()) + " vaccines.");
        Map<String,List<String>> both = new HashMap<String, List<String>>();
        Map<String,List<String>> rep = digest_json(reported);
        Map<String,List<String>> rec = digest_json(recorded);
        Set<String> all_keys = new HashSet<String>(rep.keySet());
        all_keys.addAll(rec.keySet());
        Log.d(TAG, "Total unique vaccines: " + Integer.toString(all_keys.size()));
        Iterator<String> keys = all_keys.iterator();
        while (keys.hasNext()){
            String key = keys.next();
            List<String> l = combine_lists(rep.get(key), rec.get(key));
            //Log.d(TAG, Integer.toString(l.size())+" shots of vaccine: " + key);
            both.put(key, l);
        }
        return both;
    }

    private Map<String,List<String>> digest_json(JSONObject in){
        Map<String,List<String>> map = new HashMap<String, List<String>>();
        Iterator<?> keys = in.keys();
        while(keys.hasNext()){
            String k = (String) keys.next();
            try {
                JSONArray a = in.getJSONArray(k);
                List<String> items = new ArrayList<String>();
                if (a.length() > 0){
                    for (int i=0; i<a.length();i++){
                        items.add(a.getString(i));
                    }
                }
                map.put(k, items);
            } catch (JSONException e) {
                Log.i(TAG, "Couldn't get JSONArray for Antigen with key: " + k);
            }
        }
        return map;
    }
    private List<String> combine_lists(List<String> list, List<String> list2) {
        List<String> combined = new ArrayList<String>();
        if (list == null){
            Log.d(TAG,"Reported is null...");
            if (list2 == null){
                Log.d(TAG,"Recoded is null...");
                return combined;
            }else{
                Log.d(TAG,"Returning Recorded at length " + Integer.toString(list2.size()));
                return list2;
            }
        }else if (list2 != null){
            Log.d(TAG,"Neither is null...");
            combined.addAll(list);
            combined.addAll(list2);
            return combined;
        }else{
            Log.d(TAG,"Returning Reported at length " + Integer.toString(list.size()));
            return list;
        }
    }

    public List<Dose> getAllNewRecorded() {
        List<Dose> output = new ArrayList<>();
        for(String key: newRecorded.keySet()){
            List<Dose> doses = newRecorded.get(key);
            if(doses!=null){
                for (Dose d: doses){
                    output.add(d);
                }
            }
        }

        return output;
    }
}
