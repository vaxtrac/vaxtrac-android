package com.vaxtrac.android.vaxtracapp.ocr;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.models.Dose;
import com.vaxtrac.android.vaxtracapp.view.OCRImageDialogView;

import java.lang.ref.WeakReference;

public class OCRImagesDialog extends AlertDialog{

    static final String TAG = "OCRImagesDialog";

    static Dose dose;
    static WeakReference<OCRImageDialogView> view;

    public OCRImagesDialog(Context context, Dose dose) {
        super(context);
        Log.d(TAG, "Constructor " + dose.getAntigen() + " " + dose.getLabel());
        this.dose = dose;
    }

    public static void linkVial() {
        dose.setVial(view.get().getPresenter().getVial());
        view = null;
    }

    public void setView(View view) {
        this.view = new WeakReference<>((OCRImageDialogView)view);
        this.view.get().getPresenter().filter(dose.getAntigen());
        super.setView(view);
    }
}
