package com.vaxtrac.android.vaxtracapp.ocr;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.LocalDBHelper;
import com.vaxtrac.android.vaxtracapp.models.Vial;
import com.vaxtrac.android.vaxtracapp.utils.FilesUtil;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OCRManager {

    private static final String TAG = "OCRManager";

    private static final DateFormat sqlDateFormat = new UnlocalizedDateFormat
            ("yyyy-MM-dd");

    private SQLiteDatabase database;
    protected OCRDBHelper mDBHelper;
    private Context context;

    private Map<String, Integer> multiUseVials;
    private File vialsDirectory;

    Map<String,String> vialMap = new HashMap<>();

    public OCRManager(Context context) {
        this.context = context;
        mDBHelper = new OCRDBHelper(this.context);
        vialsDirectory = FilesUtil.getVialStorageDir(context);
        open();
    }

    public void init() {
        open();
        //loadTestData();
        //readData();
        multiUseVials = new HashMap<>();
        String[] multiuse = context.getResources().getStringArray(R.array.vial_multiuse);
        for(String item : multiuse) {
            String[] values = item.split(" ");
            if(values.length == 3) {
                multiUseVials.put(values[1], Integer.parseInt(values[2]));
            } else {
                Log.e(TAG, "Unexpected multiuse key value result " + Arrays.toString(values));
            }
        }
    }

    public void open() {
        if(database == null) {
            database = mDBHelper.getWritableDatabase();
        }
    }

    public void close() {
        mDBHelper.close();
    }

    public void insertVial(Vial vial) {
    }

    public void loadTestData() {
        if(database == null) {
            open();
        }

        ContentValues contentValues = new ContentValues();
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ocr);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] img = bos.toByteArray();
        contentValues.put("image_data", img);
        contentValues.put("vaccine_type", "bcg");
        contentValues.put("creation_date", new UnlocalizedDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format
                (new Date()));
        contentValues.put("expiration_date", "2016-01-01 00:00:00.000");
        database.insert("ocr", null, contentValues);
    }

    public String getTodayWhereClause() {
        Date today = Calendar.getInstance().getTime();
        return "date(" + OCRDBHelper.DATE_CREATED + ") = '" + new UnlocalizedDateFormat
                ("yyyy-MM-dd").format(today) + "' AND " + OCRDBHelper.HIDDEN + " != 1 AND " +
                OCRDBHelper.DISABLED + " != 1";
    }

    public String getMultiuseWhereClause() {
        Date today = Calendar.getInstance().getTime();
        return new UnlocalizedDateFormat("yyyy-MM-dd").format(today) + " BETWEEN ? AND ? ";
    }

    public boolean supportsMultiuse(String vaccineType) {
        return multiUseVials.get(vaccineType) != null;
    }

    public Integer getMultiuseDuration(String vaccineType) {
        return multiUseVials.get(vaccineType);
    }

    public String getVialsByCreationDate(Date creationDate) {
        return "select * from " + OCRDBHelper.TABLE_NAME + " where date(" + OCRDBHelper
                .DATE_CREATED + ") = '" + sqlDateFormat.format(creationDate) + "' AND " +
                OCRDBHelper.HIDDEN + " != 1 AND " + OCRDBHelper.DISABLED + " != 1 AND " +
                OCRDBHelper.MULTIUSE + " != 1";
    }

    public String getMultiUseVials() {
        return "select * from " + OCRDBHelper.TABLE_NAME + " where datetime('now','localtime') " +
                "between " + OCRDBHelper.DATE_CREATED + " AND " + OCRDBHelper.MULTIUSE_DATE +
                " AND " + OCRDBHelper.HIDDEN + " != 1 AND " + OCRDBHelper.DISABLED + " != 1 AND " +
                OCRDBHelper.MULTIUSE + " = 1";
    }

    /*
     select * from ocr where date(creation_date) = today and hidden != 1 and disabled != 1 and
     multiuse != 1
     union
     select * from ocr where datetime() between creation_date and multiuse_date and multiuse = 1
     */
    public ArrayList<Vial> getAllVials() {
        if(database == null) {
            open();
        }

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Cursor results = database.rawQuery(queryBuilder.buildUnionQuery(new
                String[]{getVialsByCreationDate(Calendar.getInstance().getTime()),
                getMultiUseVials()}, null, null), null);
        Log.d(TAG, "all vials result count " + results.getCount());

        ArrayList<Vial> vials = new ArrayList<>();

        if(results != null) {
            if(results.moveToFirst()) {
                do {
                    ContentValues cv = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(results, cv);
                    vials.add(new Vial(cv));
                } while (results.moveToNext());
            }
            results.close();
        } else {
            Log.d(TAG, "results are null");
        }
        return vials;
    }

    /*

     select vaccine_type, count(*) as total
     from ocr join localdb.dose on commcare_uuid = dose.vialid
     */
    public Map<String, Integer> getVialDosesUsed(String start, String end) {
        if(database == null) {
            open();
        }
        database.execSQL("attach database '" + context.getDatabasePath(LocalDBHelper
                .DATABASE_NAME).toString() + "' as localdb");
        Map<String, Integer> vialDoses = new HashMap<>();
        String vialDosesQuery =
                "select vaccine_type, count(*) as total " +
                "from ocr join localdb.dose as dose on ocr.commcare_uuid = dose.vialid " +
                "where dose.timestamp BETWEEN ? AND ? " +
                "group by vaccine_type";
        Cursor results = database.rawQuery(vialDosesQuery, new String[]{start, end});
        if(results.moveToFirst()) {
            int vialTypeColumn = results.getColumnIndex(OCRDBHelper.VACCINE_TYPE_COLUMN);
            int totalColumn = results.getColumnIndex("total");
            do {
              vialDoses.put(results.getString(vialTypeColumn), results.getInt(totalColumn));
            } while (results.moveToNext());
        } else {
            Log.d(TAG, "vial doses result is empty");
        }
        database.execSQL("detach localdb");
        results.close();
        return vialDoses;
    }

    public String getRangeWhereClause(String column, String start, String end) {
        return "date(" + column + ") BETWEEN '" + start + "' AND '" + end + "'";
    }

    public Map<String, Integer> getVialsCreated(String createdStart, String createdEnd) {
        if(database == null) {
            open();
        }
        Map<String, Integer> vialsCreated = new HashMap<>();
        String createdQuery = SQLiteQueryBuilder.buildQueryString(false, OCRDBHelper.TABLE_NAME,
                mDBHelper.columns,
                getRangeWhereClause(OCRDBHelper.DATE_CREATED, createdStart, createdEnd), null,
                null, null, null);
        Cursor results = database.query(OCRDBHelper.TABLE_NAME, new String[]{OCRDBHelper
                .VACCINE_TYPE_COLUMN, "count(*) as total"},
                getRangeWhereClause(OCRDBHelper.DATE_CREATED, createdStart, createdEnd), null,
                OCRDBHelper.VACCINE_TYPE_COLUMN, null, null);
        if(results.moveToFirst()) {
            int vialTypeColumn = results.getColumnIndex(OCRDBHelper.VACCINE_TYPE_COLUMN);
            int totalColumn = results.getColumnIndex("total");
            do {
                vialsCreated.put(results.getString(vialTypeColumn), results.getInt(totalColumn));
            } while (results.moveToNext());
        }
        results.close();
        return vialsCreated;
    }

    public int getMedianValue(String columnName, String vaccineType) {
        if(OCRDBHelper.columnSet.contains(columnName)) {
            if(database == null) {
                open();
            }
            Cursor results = database.rawQuery(
            "SELECT AVG(max_doses) as median "  +
                    "FROM (SELECT max_doses" +
                    "      FROM ocr" +
                    "      WHERE vaccine_type = ? AND max_doses != 0" +
                    "      ORDER BY max_doses" +
                    "      LIMIT 2 - (SELECT COUNT(*) FROM ocr WHERE vaccine_type = ? AND max_doses != 0) % 2" +
                    "      OFFSET (SELECT (COUNT(*) - 1) / 2" +
                    "              FROM ocr WHERE vaccine_type = ? AND max_doses != 0))", new
                            String[]{vaccineType, vaccineType, vaccineType});
            if(results != null) {
                if(results.moveToFirst()) {
                    do {
                        int median = results.getInt(0);
                        results.close();
                        return median;
                    } while (results.moveToNext());
                }
                results.close();
            } else {
                Log.d(TAG, "results are null");
            }
        }
        return 1;
    }

    public void readData() {
        if(database == null) {
            open();
        }

        ArrayList<ContentValues> rows = new ArrayList<>();
        Cursor results = database.query("ocr", mDBHelper.columns, null, null, null, null, null);
        if(results != null) {
            results.moveToFirst();
            do{
                ContentValues cv = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(results, cv);
            }while(results.moveToNext());
        } else {
            Log.d(TAG, "results are null");
        }
        results.close();
    }

    public ContentValues fillValues(Vial vial) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(OCRDBHelper.IMAGE_DATA_COLUMN, vial.getImageData());
        contentValues.put(OCRDBHelper.COMMCARE_UUID_COLUMN, vial.getCommcareId());
        contentValues.put(OCRDBHelper.CASE_EXISTS_COLUMN, vial.caseExists());
        contentValues.put(OCRDBHelper.VACCINE_TYPE_COLUMN, vial.getVaccineType());
        contentValues.put(OCRDBHelper.DATE_CREATED, vial.getCreationDateText());
        contentValues.put(OCRDBHelper.MANUFACTURE_DATE, vial.getManufactureDateText());
        contentValues.put(OCRDBHelper.EXPIRATION_DATE, vial.getExpirationDateText());
        contentValues.put(OCRDBHelper.MULTIUSE_DATE, vial.getMultiuseDate());
        contentValues.put(OCRDBHelper.RAW_OCR_TEXT, vial.getRawOCR());
        contentValues.put(OCRDBHelper.LOT_NUMBER, vial.getLotNumber());
        contentValues.put(OCRDBHelper.BATCH_NUMBER, vial.getBatchNumber());
        contentValues.put(OCRDBHelper.LOT_BATCH_NUMBER, vial.getLotBatchNumber());
        contentValues.put(OCRDBHelper.MAXIMUM_DOSES, vial.getMaxDoses());
        contentValues.put(OCRDBHelper.USED_DOSES, vial.getUsedDoses());
        contentValues.put(OCRDBHelper.MULTIUSE, vial.isMultiUse());
        contentValues.put(OCRDBHelper.HIDDEN, vial.isHidden());
        contentValues.put(OCRDBHelper.DISABLED, vial.isDisabled());

        return contentValues;
    }

    public void writeVial(Vial vial) {
        long id = database.insert(OCRDBHelper.TABLE_NAME, null, fillValues(vial));
        if(id != -1) {
            vial.setRowId(id);
        }
    }

    public void updateVial(Vial vial) {
        if(vial.getRowId() != null) {
            database.update(OCRDBHelper.TABLE_NAME, fillValues(vial), getRowWhereClause(vial.getRowId()), null);
        } else {
            Log.d(TAG, "Vial has no row id");
        }
    }

    public int updateVialCase(String caseId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(OCRDBHelper.CASE_EXISTS_COLUMN, 1);
        return database.update(OCRDBHelper.TABLE_NAME, contentValues, "commcare_uuid = ?", new
                String[]{caseId});
    }

    public String getRowWhereClause(Long rowId) {
        return OCRDBHelper.PRIMARY_KEY_COLUMN + " = " + rowId;
    }
}
