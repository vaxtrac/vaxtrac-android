package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.data.TestCursor;
import com.vaxtrac.android.vaxtracapp.data.TranslationCursor;
import com.vaxtrac.android.vaxtracapp.presenter.EditGuardianPickerPresenter;

public class EditGuardianPickerView extends GuardianPickerView {

    EditGuardianPickerPresenter presenter = null;
    private final String TAG = "EditGuardianPickerView";

    public EditGuardianPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.presenter = new EditGuardianPickerPresenter(context,this);

        table.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Action for clicking a listItem from the patientList
                Cursor selected = (Cursor) adapterView.getItemAtPosition(i);
                String selectedID = selected.getString(selected.getColumnIndex("docid"));
                presenter.chooseGuardian(selectedID);
            }
        });
    }



    public void verifyFailed() {
        //Toast.makeText(context, "VerifyFailed!", Toast.LENGTH_SHORT).show();
    }

    public void verifyOk() {
        //Toast.makeText(context, "VerifyOK!", Toast.LENGTH_SHORT).show();
    }

    public void scanFailed() {
        //Toast.makeText(context, "ScanFailed!", Toast.LENGTH_SHORT).show();
    }

    public void poke(){
        //Toast.makeText(context, "Poke!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void populateTable(Cursor cursor) {
        Cursor translationCursor = new TranslationCursor(cursor, new String[]{"relation"});

        //Make a test cursor to show if a row has biometrics attached

        lastCursor = new TestCursor(translationCursor, "hasbiometrics") {
            int testColumnIndex = super.getColumnIndex("leftthumb");

            @Override
            public boolean applyLogic() {
                if(super.getString(testColumnIndex).length()<=0){
                    return false;
                }
                return true;
            }
        };

        Log.i(TAG, String.format("Cursor size | %s",cursor.getCount()));
        adapter = new ColoredCursorAdapter(this.context,
                R.layout.row_guardian_picker,
                lastCursor,
                new String[]{"name","relation","hasbiometrics"},
                new int[]{R.id.list_item_text1, R.id.list_item_text2, R.id.list_item_text3});
        table.setAdapter(adapter);
    }

    public EditGuardianPickerPresenter getPresenter() {
        return this.presenter;
    }
}
