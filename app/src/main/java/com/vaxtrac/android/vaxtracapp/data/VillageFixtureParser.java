package com.vaxtrac.android.vaxtracapp.data;

import android.util.Log;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

public class VillageFixtureParser {

    private static final String TAG = VillageFixtureParser.class.getName();
    protected static final String VILLAGE_NAME_EXPRESSION = "//village/text()";
    protected static final String VILLAGE_LOCALE_EXPRESSION = "//village[@lang='%s']/text()";
    protected static final String VILLAGE_ID_EXPRESSION = "//id/text()";

    String localeAttribute;

    private Map<String, String> allVillages;
    private List<String> currentUserVillages;

    public VillageFixtureParser() {
        allVillages = new LinkedHashMap<>();
        currentUserVillages = new LinkedList<>();
    }

    public VillageFixtureParser(String locale) {
        this();
        localeAttribute = locale;
    }

    public void parse(String fixtureXml) {
        Log.d(TAG, "fixtureXml " + fixtureXml + " locale " + localeAttribute);
        try {
            XPath villagePath = XPathFactory.newInstance().newXPath();
            String expression = VILLAGE_NAME_EXPRESSION;
            NodeList villageIds = null;
            /* Nepali Fixture Has a different label and id */
            if(localeAttribute != null) {
                expression = String.format(VILLAGE_LOCALE_EXPRESSION, localeAttribute);
                villageIds = (NodeList) villagePath.evaluate(VILLAGE_ID_EXPRESSION, new InputSource(new
                        StringReader(fixtureXml)), XPathConstants.NODESET);
            }
            Log.d(TAG, "Expression " + expression);
            NodeList villageNodes = (NodeList) villagePath.evaluate(expression, new InputSource(new
                    StringReader(fixtureXml)), XPathConstants.NODESET);
            Log.d(TAG, "villages size " + villageNodes.getLength());

            /* Pray the Fixture is sorted */
            if(villageIds != null) {
                for (int i = 0, villageLength = villageNodes.getLength(); i < villageLength; i++) {
                    Log.d(TAG, "village " + villageNodes.item(i).getNodeValue() + " id " + villageIds.item(i)
                            .getNodeValue());
                    allVillages.put(villageNodes.item(i).getNodeValue(), villageIds.item(i)
                            .getNodeValue());
                    currentUserVillages.add(villageNodes.item(i).getNodeValue());
                }
            } else {
                for (int i = 0, villageLength = villageNodes.getLength(); i < villageLength; i++) {
                    String value = villageNodes.item(i).getNodeValue();
                    allVillages.put(value, value);
                    currentUserVillages.add(value);
                }
            }
        } catch (Exception exception) {
            Log.d(TAG, exception.getStackTrace().toString());
        }
    }

    public Map<String, String> getAllVillages() {
        return allVillages;
    }

    public List<String> getCurrentUserVillages() {
        return currentUserVillages;
    }
}
