package com.vaxtrac.android.vaxtracapp.models;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.ocr.OCRDBHelper;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

public class Vial {

    private static final String TAG = "Vial";
    private static final int EXPIRE_DAYS = 1;

    private Long rowId;

    private String commcareId;
    private boolean caseExists;

    private byte[] imageData;

    private Bitmap photo;

    private Date creationDate;
    private Date multiuseEnd;

    private String vaccineType;
    private Dose dose;
    private Date manufactureDate;
    private Date expirationDate;

    /* Consolidated never both present */
    private String lotBatchNumber;

    private String lotNumber;
    private String batchNumber;

    private Integer maxDoses;
    private Integer usedDoses;

    private boolean isMultiUse;
    private boolean hidden;
    private boolean disabled;

    private String rawOCR;

    private SimpleDateFormat dateFormatter = new UnlocalizedDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public Vial(String vaccineType) {
        this.vaccineType = vaccineType;
    }

    public Vial(Bundle bundle) {
        creationDate = new Date();
        for(String key : bundle.keySet()) {
            Log.d(TAG, "Bundle Extra Key " + key);
            switch(key) {
                case "raw_text":
                    rawOCR = bundle.getString(key);
                    Log.d(TAG, bundle.getString(key));
                    break;
                case "vaccine_type":
                case "selected_vaccine":
                    vaccineType = bundle.getString(key);
                    Log.d(TAG, bundle.getString(key));
                    break;
                case "image_data":
                    imageData = bundle.getByteArray(key);
                    Log.d(TAG, Arrays.toString(bundle.getByteArray(key)));
                    break;
            }
        }
        this.commcareId = getUUID();
        if(supportsMultiuse()) {
            makeMultiuseVial();
        }
    }

    public Vial(String ocrOutput, byte[] image) {
        this(null, ocrOutput, image);
    }

    public Vial(String vaccineType, String ocrOutput, byte[] image) {
        this.vaccineType = vaccineType;
        this.rawOCR = ocrOutput;
        this.imageData = image;
        this.creationDate = new Date();
        this.commcareId = getUUID();
        if(supportsMultiuse()) {
            makeMultiuseVial();
        }
    }

    /* Vaccine type must be changed prior to multiuse flag */
    public void makeMultiuseVial() {
            if(supportsMultiuse()) {
                this.isMultiUse = true;
                Calendar multiuseDate = Calendar.getInstance();
                /* Round up to last full day */
                multiuseDate.setTime(creationDate);
                multiuseDate.add(Calendar.DAY_OF_MONTH, getMultiuseDuration());
                multiuseDate.set(Calendar.HOUR_OF_DAY, multiuseDate.getActualMaximum(Calendar
                        .HOUR_OF_DAY));
                multiuseDate.set(Calendar.MINUTE, multiuseDate.getActualMaximum(Calendar.MINUTE));
                multiuseDate.set(Calendar.SECOND, multiuseDate.getActualMaximum(Calendar.SECOND));
                multiuseDate.set(Calendar.MILLISECOND, multiuseDate.getActualMaximum(Calendar
                        .MILLISECOND));
                multiuseEnd = multiuseDate.getTime();
                Log.d(TAG, "make multiuse end date " + multiuseEnd.toString());
            } else {
                this.isMultiUse = false;
            }
    }

    public boolean supportsMultiuse() {
        return AppController.mOCRManager.supportsMultiuse(vaccineType);
    }

    public Integer getMultiuseDuration() {
        return AppController.mOCRManager.getMultiuseDuration(vaccineType);
    }

    public String getUUID() {
        return UUID.randomUUID().toString();
    }

    public Vial(ContentValues contentValues) {
        this.rowId = contentValues.getAsLong(OCRDBHelper.PRIMARY_KEY_COLUMN);
        this.commcareId = contentValues.getAsString(OCRDBHelper.COMMCARE_UUID_COLUMN);
        this.caseExists = intToBool(contentValues.getAsInteger(OCRDBHelper.CASE_EXISTS_COLUMN));
        this.imageData = contentValues.getAsByteArray(OCRDBHelper.IMAGE_DATA_COLUMN);
        this.vaccineType = contentValues.getAsString(OCRDBHelper.VACCINE_TYPE_COLUMN);
        this.isMultiUse = intToBool(contentValues.getAsInteger(OCRDBHelper.MULTIUSE));

        try {
            this.creationDate = dateFormatter.parse(contentValues.getAsString(OCRDBHelper
                    .DATE_CREATED));
        } catch (ParseException parseException) {
            Log.d(TAG, "Creation Date Parsing Error");
            this.creationDate = new Date();
        }

        try {
            if(contentValues.getAsString(OCRDBHelper.MANUFACTURE_DATE) != null) {
                this.manufactureDate = dateFormatter.parse(contentValues.getAsString(OCRDBHelper
                        .MANUFACTURE_DATE));
            } else {
                this.manufactureDate = new Date();
            }
        } catch (ParseException parseException) {
            Log.d(TAG, "Manufacture Date Parsing Error");
            this.manufactureDate = new Date();
        }

        try {
            if(contentValues.getAsString(OCRDBHelper.EXPIRATION_DATE) != null) {
                this.expirationDate = dateFormatter.parse(contentValues.getAsString(OCRDBHelper
                        .EXPIRATION_DATE));
            } else {
                this.expirationDate = new Date();
            }
        } catch (ParseException parseException) {
            Log.d(TAG, "Expiration Date Parsing Error");
            this.expirationDate = new Date();
        }

        if(isMultiUse()) {
            try {
                if (contentValues.getAsString(OCRDBHelper.MULTIUSE_DATE) != null) {
                    this.multiuseEnd = dateFormatter.parse(contentValues.getAsString(OCRDBHelper
                            .MULTIUSE_DATE));
                }
            } catch (ParseException parseException) {
                Log.e(TAG, "Multiuse Date Parsing Error", parseException);
            }
        }

        this.rawOCR = contentValues.getAsString(OCRDBHelper.RAW_OCR_TEXT);
        this.lotNumber = contentValues.getAsString(OCRDBHelper.LOT_NUMBER);
        this.batchNumber = contentValues.getAsString(OCRDBHelper.BATCH_NUMBER);
        this.lotBatchNumber = contentValues.getAsString(OCRDBHelper.LOT_BATCH_NUMBER);
        this.maxDoses = contentValues.getAsInteger(OCRDBHelper.MAXIMUM_DOSES);
        this.usedDoses = contentValues.getAsInteger(OCRDBHelper.USED_DOSES);
        this.hidden = intToBool(contentValues.getAsInteger(OCRDBHelper.HIDDEN));
        this.disabled = intToBool(contentValues.getAsInteger(OCRDBHelper.DISABLED));
    }

    public void bind(HashMap<String, String[]> fields) {
        if(fields.get("batch_number") != null) {
            this.batchNumber = fields.get("batch_number")[0];
            this.lotBatchNumber = fields.get("batch_number")[0];
        }

        if(fields.get("lot_number") != null) {
            this.lotNumber = fields.get("lot_number")[0];
            this.lotBatchNumber = fields.get("lot_number")[0];
        }

        if(fields.get("manufacture_date") != null) {
            String[] dateFields = fields.get("manufacture_date");
            Calendar manufactureCalendar = Calendar.getInstance();

            try {
                Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(dateFields[0]);
                manufactureCalendar.setTime(date);
            } catch (ParseException exception) {
                Log.d(TAG, "Error Parsing Vial Month String");
                try {
                    manufactureCalendar.set(Calendar.MONTH, Integer.parseInt(dateFields[0]) - 1);
                } catch (NumberFormatException numFormatException) {
                    Log.d(TAG, "Error Parsing Vial Month Integer");
                }
            }

            try {
                manufactureCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateFields[1]));
            } catch (NumberFormatException exception) {
                Log.d(TAG, "Error Parsing Vial Day");
            }

            Log.d(TAG, manufactureCalendar.toString());


            try {
                manufactureCalendar.set(Calendar.YEAR, Integer.parseInt(dateFields[2]));
            } catch (NumberFormatException exception) {
                Log.d(TAG, "Number Format Exception Vial Year");
            }

            Log.d(TAG, manufactureCalendar.toString());

            manufactureDate = manufactureCalendar.getTime();
        }


        if(fields.get("expiration_date") != null) {
            String[] dateFields = fields.get("expiration_date");
            Calendar expirationCalendar = Calendar.getInstance();

            try {
                Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(dateFields[0]);
                expirationCalendar.setTime(date);
            } catch (ParseException exception) {
                Log.d(TAG, "Error Parsing Vial Month");
                try {
                    expirationCalendar.set(Calendar.MONTH, Integer.parseInt(dateFields[0]) - 1);
                } catch (NumberFormatException numFormatException) {
                    Log.d(TAG, "Error Parsing Vial Month Integer");
                }
            }

            try {
                expirationCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateFields[1]));
            } catch (NumberFormatException exception) {
                Log.d(TAG, "Number Format Exception Vial Day");
            }

            try {
                expirationCalendar.set(Calendar.YEAR, Integer.parseInt(dateFields[2]));
            } catch (NumberFormatException exception) {
                Log.d(TAG, "Number Format Exception Vial Year");
            }

            expirationDate = expirationCalendar.getTime();
        }
    }

    public void hide() {
        this.hidden = true;
        save();
    }

    public void save() {
        /* New Vial */
        if(rowId == null) {
            AppController.mOCRManager.writeVial(this);
        } else {
            /* Update */
            AppController.mOCRManager.updateVial(this);
        }
    }

    public boolean intToBool(Integer number) {
        return number != null && number == 1;
    }

    public void incrementVialDoses() {
        usedDoses++;
        save();
    }

    public boolean isExpired() {
        return expirationDate.before(new Date());
    }

    public Long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getCommcareId() {
        return commcareId;
    }

    public boolean caseExists() {
        return caseExists;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getCreationDateText() {
        return dateFormatter.format(creationDate);
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getVaccineType() {
        return vaccineType;
    }

    public void setVaccineType(String vaccineType) {
        this.vaccineType = vaccineType;
    }

    public Dose getDose() {
        return dose;
    }

    public void setDose(Dose dose) {
        this.dose = dose;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public String getManufactureDateText() {
        if(manufactureDate != null) {
            return dateFormatter.format(manufactureDate);
        }
        return null;
    }

    public void setManufactureDate(Date manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public String getExpirationDateText() {
        if(expirationDate != null) {
            return dateFormatter.format(expirationDate);
        }
        return null;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLotBatchNumber() {
        return lotBatchNumber;
    }

    public void setLotBatchNumber(String lotBatchNumber) {
        this.lotBatchNumber = lotBatchNumber;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public int getUsedDoses() {
        if(usedDoses == null) {
            usedDoses = 0;
        }
        return usedDoses;
    }

    public void setUsedDoses(int usedDoses) {
        this.usedDoses = usedDoses;
    }

    public int getMaxDoses() {
        return maxDoses != null ? maxDoses : 0;
    }

    public void setMaxDoses(int maxDoses) {
        this.maxDoses = maxDoses;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getRawOCR() {
        return rawOCR;
    }

    public void setRawOCR(String rawOCR) {
        this.rawOCR = rawOCR;
    }

    public String getMultiuseDate() {
        if(multiuseEnd != null) {
            return dateFormatter.format(multiuseEnd);
        }
        return null;
    }

    public void setMultiuseDate(Date multiuseDate) {
        this.multiuseEnd = multiuseDate;
    }

    public boolean isMultiUse() { return isMultiUse;}

    public void setMultiUse(boolean multiUse) {
        Log.d(TAG, "Set multiuse prev " + this.isMultiUse + " new " + multiUse);
        /* If not marked as multiuse on creation */
        this.isMultiUse = multiUse;
        if(multiUse) {
            makeMultiuseVial();
        }
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
