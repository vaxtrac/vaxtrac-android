package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.AppVersion;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.VaccineProvisionPresenter;

public class VaccinationView extends RelativeLayout {

    private final String TAG = "VaccinationView";
    VaccineProvisionPresenter presenter;
    Context context;

    public VaccinationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        presenter = new VaccineProvisionPresenter(this.context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        TableLayout table  = (TableLayout) findViewById(R.id.vaccination_table);

        ImageView ocrButton = (ImageView) findViewById(R.id.camera_ocr);
        if(AppVersion.isEnabled("ocr")) {
            ocrButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.cameraOCRClicked();
                }
            });
        }else{
            ocrButton.setVisibility(View.GONE);
        }
        if (AppController.mPatient!= null) {
            presenter.fill_table(table, inflater);
        }
        else{
            Toast.makeText(getContext(), "No patient Loaded!", Toast.LENGTH_SHORT).show();
        }
    }
}


