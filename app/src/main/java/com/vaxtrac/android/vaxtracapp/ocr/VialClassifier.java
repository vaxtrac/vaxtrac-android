package com.vaxtrac.android.vaxtracapp.ocr;

import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VialClassifier {
    static final String TAG = "VialClassifier";
    String[] lines;
    static final String MFG = "MFG";
    static final String EXP = "EXP";
    static final String BNO = "B.*NO";
    static final String BN0 = "B.*N0";
    static final String MONTHS[] = {"JAN", "FEB" , "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
            "SEP", "OCT", "0CT", "NOV", "N0V", "DEC"};
    static final String YEAR_PREFIX = "20";
    static final String WILDCARD = ".*";

    final int STARTING_YEAR = 2000;
    final int MAX_YEAR = 2030;

    final int MAX_DAY = 31;

    boolean foundBno;
    boolean foundMfg;
    boolean foundExp;
    boolean foundDate;
    boolean foundDateMonthTwoDigitYear;
    boolean foundDateNumeric;
    boolean foundDate2digitYear;
    boolean foundDateMonthYear;
    boolean foundMonth;
    boolean foundMonthNumeric;
    boolean foundYear;
    boolean foundDay;

    HashMap<String, String[]> bestGuess = new HashMap<>();

    HashMap<String, Pair<Integer, String>> keywords = new HashMap<>();

    HashMap<Integer, String[]> dateCandidates = new LinkedHashMap<>();
    HashMap<Integer, String[]> dateNumericCandidates = new LinkedHashMap<>();
    HashMap<Integer, String[]> dateMonth2DigitYearCandidates = new LinkedHashMap<>();
    HashMap<Integer, String[]> date2digitYearCandidates = new LinkedHashMap<>();
    HashMap<Integer, String[]> dateMonthYearCandidates = new LinkedHashMap<>();

    HashMap<Integer, String> yearCandidates = new HashMap<>();

    HashMap<Integer, String> monthCandidates = new HashMap<>();
    HashMap<Integer, String> monthNumericCandidates = new HashMap<>();

    HashMap<Integer, String> dayCandidates = new HashMap<>();

    Pattern mfgPattern = Pattern.compile(WILDCARD + "(" + MFG + ")" + WILDCARD, Pattern.DOTALL);
    Pattern expPattern = Pattern.compile(WILDCARD + "(" + EXP + ")" + WILDCARD, Pattern.DOTALL);
    Pattern bnoPattern = Pattern.compile(WILDCARD + "(" + BNO + "|" + BN0 + ")" + WILDCARD,
            Pattern.DOTALL);

    /* JAN 2015 */
    Pattern datePattern = Pattern.compile(WILDCARD + "(" + monthStringPatternBuilder() + ")" +
            WILDCARD + "(" + yearStringPatternBuilder() + ")" + WILDCARD, Pattern.DOTALL);

    /* JAN 15 */
    Pattern dateMonthTwoDigitYearPattern = Pattern.compile(WILDCARD + "(" +
            monthStringPatternBuilder() + ")" +
            WILDCARD + "(" + twoDigitYearStringPatternBuilder() + ")" + WILDCARD, Pattern.DOTALL);

    /* 2015.01.31 */
    Pattern datePatternNumerals = Pattern.compile(WILDCARD + "(" + yearStringPatternBuilder() +
            ")" + WILDCARD + "(" + monthNumberPatternBuilder() + ")" + WILDCARD + "(" +
                    dayPatternBuilder() + ")" + WILDCARD, Pattern.DOTALL);

    /* 01 JAN 15 */
    Pattern datePatternTwoDigitYear = Pattern.compile(WILDCARD + "(" +
            dayPatternBuilder() + ")" + WILDCARD + "(" + monthStringPatternBuilder() + ")" +
            WILDCARD + "(" + twoDigitYearStringPatternBuilder() + ")" + WILDCARD, Pattern.DOTALL);

    /* 01 2015 */
    Pattern datePatternMonthYearNumerals = Pattern.compile(WILDCARD + "(" +
            monthNumberPatternBuilder() + ")" + WILDCARD + "(" + yearStringPatternBuilder() + ")"
            + WILDCARD, Pattern.DOTALL);


    Pattern yearPattern = Pattern.compile(WILDCARD + "(" + yearStringPatternBuilder() + ")" +
                    WILDCARD,
            Pattern.DOTALL);

    Pattern monthPattern = Pattern.compile(WILDCARD + "(" + monthStringPatternBuilder() + ")" +
            WILDCARD,
            Pattern.DOTALL);

    Pattern monthNumericPattern = Pattern.compile(WILDCARD + "(" + monthNumberPatternBuilder() +
                    ")" +
                    WILDCARD,
            Pattern.DOTALL);

    Pattern dayPattern = Pattern.compile(WILDCARD + "(" + dayPatternBuilder() +
                    ")" +
                    WILDCARD,
            Pattern.DOTALL);

    public VialClassifier(String string) {
        lines = getLines(string);
        Log.d(TAG, "Number of Lines" + lines.length);
        Log.d(TAG, "Months group : " + monthStringPatternBuilder() + " Years group : " +
                yearStringPatternBuilder());
        Log.d(TAG, "date pattern : " + datePattern.toString());
        Log.d(TAG, "date pattern numerals : " + datePatternNumerals.toString());
        Log.d(TAG, "date pattern 2digityear : " + datePatternTwoDigitYear.toString());
        int count = 0;

        Matcher dayMatcher;
        Matcher monthNumericMatcher;
        Matcher monthMatcher;
        Matcher yearMatcher;
        Matcher bnoMatcher;
        Matcher mfgMatcher;
        Matcher expMatcher;

        for(String line : getLines(string)) {
            Log.d(TAG, "Line " + count + " " + line);
            Log.d(TAG, "Line length " + line.length());

            yearMatcher = yearPattern.matcher(line);

            monthMatcher = monthPattern.matcher(line);
            monthNumericMatcher = monthNumericPattern.matcher(line);

            dayMatcher = dayPattern.matcher(line);

            if(!foundBno) {
                bnoMatcher = bnoPattern.matcher(line);

                if(bnoMatcher.matches()) {
                    //Log.d(TAG, "batch number : " + bnoMatcher.group(1));
                    Log.d(TAG, "batch number start : " + bnoMatcher.start(1));
                    Log.d(TAG, "batch number end : " + bnoMatcher.end(1));
                    foundBno = true;

                    keywords.put("bno", new Pair<Integer, String>(count,
                            line.substring(bnoMatcher.end(1))));
                } else {
                    Log.d(TAG, "No batch number");
                }
            }

            if(!foundMfg) {
                mfgMatcher = mfgPattern.matcher(line);

                if(mfgMatcher.matches()) {
                    Log.d(TAG, "mfg date : " + mfgMatcher.group(1));
                    foundMfg = true;
                    keywords.put("mfg", new Pair<Integer, String>(count,
                            line.substring(mfgMatcher.end(1))));
                } else {
                    Log.d(TAG, "No mfg date");
                }
            }

            if(!foundExp) {
                expMatcher = expPattern.matcher(line);

                if(expMatcher.matches()) {
                    Log.d(TAG, "exp date : " + expMatcher.group(1));
                    foundExp = true;
                    keywords.put("exp", new Pair<Integer, String>(count,
                            line.substring(expMatcher.end(1))));
                } else {
                    Log.d(TAG, "No exp date");
                }
            }

            dateMatcher(count, line);

            dateMonthTwoDigitYearMatcher(count, line);

            dateNumericMatcher(count, line);

            dateTwoDigitYearMatcher(count, line);

            dateNumericMonthYear(count, line);

            if(yearMatcher.matches()) {
                foundYear = true;
                Log.d(TAG, "year : " + yearMatcher.group(1));
                yearCandidates.put(count, yearMatcher.group(1));
            } else {
                Log.d(TAG, "No year matches");
            }

            if(monthMatcher.matches()) {
                foundMonth = true;
                Log.d(TAG, "month : " + monthMatcher.group(1));
                monthCandidates.put(count, monthMatcher.group(1));
            } else {
                Log.d(TAG, "No month matches");
            }

            if(monthNumericMatcher.matches()) {
                foundMonthNumeric = true;
                Log.d(TAG, "month numeric :" + monthNumericMatcher.group(1));
                monthNumericCandidates.put(count, monthNumericMatcher.group(1));
            }

            if(dayMatcher.matches()) {
                foundDay = true;
                Log.d(TAG, "found day : " + dayMatcher.group(1));
                dayCandidates.put(count, dayMatcher.group(1));
            }

            count++;
        }

        classify();
    }

    public HashMap<String, String[]> getBestGuess() {
        return bestGuess;
    }

    public void classify() {
        if(foundBno) {
            Pair<Integer, String> bnoText = keywords.get("bno");
            if(hasSpaceSeparator(bnoText.second)) {
                String longestToken = findLongestSubstring(bnoText.second.split(" "));
                Log.d(TAG, "Longest Token " + longestToken);
                bestGuess.put("batch_number", new String[]{longestToken});
            } else {
                Log.d(TAG, "Batch Number " + bnoText.second);
                bestGuess.put("batch_number", new String[]{bnoText.second});
            }
        }

        if(foundMfg) {
            Pair<Integer, String> expText = keywords.get("mfg");

            String[] dateGroups = dateCandidates.get(expText.first);
            if(dateGroups != null) {
                bestGuess.put("manufacture_date", new String[]{dateGroups[0], "1", dateGroups[1]});
            }
        }

        if(foundExp) {
            Pair<Integer, String> expText = keywords.get("exp");
            String[] dateGroups = dateCandidates.get(expText.first);
            if(dateGroups != null) {
                bestGuess.put("expiration_date", new String[]{dateGroups[0], "1", dateGroups[1]});
            }
        }

        if(!dateCandidates.isEmpty()) {
            if(dateCandidates.size() > 1) {
                bestGuess.put("expiration_date", new String[]{dateCandidates.get(0)[0], "1",
                        dateCandidates.get(0)[1]});
            } else {
                /* Assume Expiration */
                if(dateCandidates.get(0) != null) {
                    for(String key : dateCandidates.get(0)) {
                        Log.d(TAG, "key " + key);
                    }
                } else {
                    Log.d(TAG, "Date Candidates is null");
                }
            }
        }

        if(!dateNumericCandidates.isEmpty()) {
            if(dateNumericCandidates.size() > 1) {
                int[] lineList = new int[dateNumericCandidates.size()];
                int i = 0;
                for(Integer key : dateNumericCandidates.keySet()) {
                    lineList[i] = key;
                    i++;
                }
                switch(dateNumericCandidates.size()) {
                    case 2:
                        if(lineList[0] > 0) {
                            bestGuess.put("lot_number", new String[] {findLongestSubstring(Arrays
                                    .copyOfRange(lines, 0, lineList[0]))});
                            bestGuess.put("manufacture_date", new String[]{dateNumericCandidates.get
                                    (lineList[0])
                                    [1],
                                    dateNumericCandidates.get(lineList[0])[2],
                                    dateNumericCandidates.get(lineList[0])[0]});

                            bestGuess.put("expiration_date", new String[]{dateNumericCandidates.get
                                    (lineList[1])[1],
                                    dateNumericCandidates.get(lineList[1])[2],
                                    dateNumericCandidates.get(lineList[1])[0]});
                        } else if (lineList[0] == 0) {
                            bestGuess.put("lot_number", new String[]{lines[0]});
                            bestGuess.put("manufacture_date", new String[]{dateNumericCandidates.get
                                    (lineList[1])
                                    [1],
                                    dateNumericCandidates.get(lineList[1])[2],
                                    dateNumericCandidates.get(lineList[1])[0]});

                            bestGuess.put("expiration_date", new String[]{dateNumericCandidates.get
                                    (lineList[1])[1],
                                    dateNumericCandidates.get(lineList[1])[2],
                                    dateNumericCandidates.get(lineList[1])[0]});
                        }
                        break;
                    case 3:
                        bestGuess.put("lot_number", new String[]{lines[lineList[0]]});
                        bestGuess.put("manufacture_date", new String[]{dateNumericCandidates.get
                                (lineList[1])
                                [1],
                                dateNumericCandidates.get(lineList[1])[2],
                                dateNumericCandidates.get(lineList[1])[0]});
                        bestGuess.put("expiration_date", new String[]{dateNumericCandidates.get
                                (lineList[2])[1],
                                dateNumericCandidates.get(lineList[2])[2],
                                dateNumericCandidates.get(lineList[2])[0]});
                        break;
                }
            } else {
                /* Assume Expiration */
                double lineNumber = 0.0;
                int index =  0;
                for(Integer key : dateNumericCandidates.keySet()) {
                    lineNumber = (double) key;
                    index = key;
                    break;
                }
                if((lineNumber+1) / lines.length < .33) {

                } else if ((lineNumber+1) / lines.length > .33 && (lineNumber+1) / lines.length <
                        .66) {
                    bestGuess.put("manufacture_date", new String[]{dateNumericCandidates.get
                            (index)
                            [1],
                            dateNumericCandidates.get(index)[2],
                            dateNumericCandidates.get(index)[0]});
                } else {
                    bestGuess.put("expiration_date", new String[]{dateNumericCandidates.get
                            (index)[1],
                            dateNumericCandidates.get(index)[2],
                            dateNumericCandidates.get(index)[0]});
                }
            }
        }

        if(!date2digitYearCandidates.isEmpty()) {
            Log.d(TAG, "Has 2 digit year");
            Integer lineNumber = null;
            for(Integer line : date2digitYearCandidates.keySet()) {
                lineNumber = line;
            }

            if(lineNumber != null) {
                bestGuess.put("manufacture_date", new String[]{date2digitYearCandidates.get(lineNumber)
                        [1], date2digitYearCandidates.get(lineNumber)[0], "20" +
                        date2digitYearCandidates.get(lineNumber)
                        [2]});
            }
        }

        if(!dateMonthYearCandidates.isEmpty()) {
            Log.d(TAG, "Has month 2 digit year");
            Integer lineNumber = null;
            for(Integer line : dateMonthYearCandidates.keySet()) {
                lineNumber = line;
            }
            bestGuess.put("expiration_date", new String[]{dateMonthYearCandidates.get
                    (lineNumber)[0], "1", dateMonthYearCandidates.get(lineNumber)[1]});
        }

        if(!date2digitYearCandidates.isEmpty() && !dateMonthYearCandidates.isEmpty()) {
            Log.d(TAG, "Locating Lot Number");
            /* Look for Lot Number */
            Integer start = null;
            for(Integer line : date2digitYearCandidates.keySet()) {
                Log.d(TAG, "line key " + line);
                start = line;
                Log.d(TAG, "start " + start);
            }

            Integer end = null;
            for(Integer line : dateMonthYearCandidates.keySet()) {
                Log.d(TAG, "line key " + line);
                end = line;
                Log.d(TAG, "end " + end);
            }

            if((end - start) == 2) {
                bestGuess.put("lot_number", new String[]{lines[start+1]});
            } else {
                bestGuess.put("lot_number", new String[] {findLongestSubstring(Arrays
                        .copyOfRange(lines, start + 1, end - 1))});
            }
        }

        if(!dateMonth2DigitYearCandidates.isEmpty() && (!foundDate2digitYear ||
                !foundDateMonthYear)) {
            Integer lineNumber = null;
            for(Integer key : dateMonth2DigitYearCandidates.keySet()) {
                lineNumber = key;
            }

            if(lineNumber != null) {
                bestGuess.put("expiration_date", new String[]{dateMonth2DigitYearCandidates.get
                        (lineNumber)[0], "1", "20"+dateMonth2DigitYearCandidates.get(lineNumber)
                        [1]});
                bestGuess.put("batch_number", new String[]{findLongestSubstring(Arrays.copyOfRange
                        (lines, 0, lineNumber))});
            }
        }

        if(!dateMonthYearCandidates.isEmpty()) {
            /* Always Expiration */
        }

        if(!yearCandidates.isEmpty()) {
            if(yearCandidates.size() > 1) {

            } else {
                /* Assume Expiration */

            }
        }

        if(!monthCandidates.isEmpty()) {
            if(monthCandidates.size() > 1) {

            } else {
                /* Assume Expiration */

            }
        }

        if(!monthNumericCandidates.isEmpty()) {
            if(monthNumericCandidates.size() > 1) {

            } else {
                /* Assume Expiration */

            }
        }

        if(!dayCandidates.isEmpty()) {
            if(dayCandidates.size() > 1) {

            } else {
                /* Assume Expiration */
            }

        }
    }

    public List<Integer> pruneLines() {
        List<Integer> skip = new ArrayList<>();
        for(int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if(line.length() < 3) {
                skip.add(i);
            }
        }
        return skip;
    }

    public List<Integer> lineHits(int line) {
        List<Integer> hits = new ArrayList<>();
        if(dateCandidates.get(line) != null) {
            hits.add(line);
        }

        if(date2digitYearCandidates.get(line) != null) {
            hits.add(line);
        }

        if(dateMonth2DigitYearCandidates.get(line) != null) {
            hits.add(line);
        }

        if(dateMonthYearCandidates.get(line) != null) {
            hits.add(line);
        }

        if(dateNumericCandidates.get(line) != null) {
            hits.add(line);
        }

        if(yearCandidates.get(line) != null) {
            hits.add(line);
        }

        if(monthCandidates.get(line) != null) {
            hits.add(line);
        }

        if(monthNumericCandidates.get(line) != null) {
            hits.add(line);
        }

        if(dayCandidates.get(line) != null) {
            hits.add(line);
        }
        return hits;
    }

    public boolean hasSpaceSeparator(String input) {
        String[] lines = input.split(" ");
        return lines.length > 1;
    }

    public String findLongestSubstring(String[] lines) {

        String longest = lines[0];
        for(int i = 1; i < lines.length; i++) {
            if(longest.length() < lines[i].length()) {
                longest = lines[i];
            }
        }
        return longest;
    }

    public void dateMatcher(int lineNumber, String input) {
        Matcher dateMatcher = datePattern.matcher(input);

        if(dateMatcher.matches()) {
            foundDate = true;
            Log.d(TAG, "Group 1: " + dateMatcher.group(1));
            Log.d(TAG, "Group 2: " + dateMatcher.group(2));

            dateCandidates.put(lineNumber, new String[]{dateMatcher.group(1),
                    dateMatcher.group(2)});
        } else {
            Log.d(TAG, "No date matches");
        }

    }

    public void dateMonthTwoDigitYearMatcher(int lineNumber, String input) {
        Matcher dateMatcher = dateMonthTwoDigitYearPattern.matcher(input);

        if(dateMatcher.matches()) {
            foundDateMonthTwoDigitYear = true;
            Log.d(TAG, "Group 1: " + dateMatcher.group(1));
            Log.d(TAG, "Group 2: " + dateMatcher.group(2));

            Log.d(TAG, "month 2 digit year group " + lineNumber);
            dateMonth2DigitYearCandidates.put(lineNumber, new String[]{dateMatcher.group(1),
                    dateMatcher.group(2)});
        } else {
            Log.d(TAG, "No date month 2 digit year matches");
        }
    }

    public void dateNumericMatcher(int lineNumber, String input) {
        Matcher dateMatcher = datePatternNumerals.matcher(input);

        if(dateMatcher.matches()) {
            foundDateNumeric = true;
            Log.d(TAG, "Group 1: " + dateMatcher.group(1));
            Log.d(TAG, "Group 2: " + dateMatcher.group(2));
            Log.d(TAG, "Group 3: " + dateMatcher.group(3));

            dateNumericCandidates.put(lineNumber, new String[]{dateMatcher.group(1),
                    dateMatcher.group(2), dateMatcher.group(3)});
        } else {
            Log.d(TAG, "No date numeric matches");
        }
    }

    public void dateTwoDigitYearMatcher(int lineNumber, String input) {
        Matcher dateMatcher = datePatternTwoDigitYear.matcher(input);

        if(dateMatcher.matches()) {
            foundDate2digitYear = true;
            Log.d(TAG, "Group 1: " + dateMatcher.group(1));
            Log.d(TAG, "Group 2: " + dateMatcher.group(2));
            Log.d(TAG, "Group 3: " + dateMatcher.group(3));

            Log.d(TAG, "2 digit year group " + lineNumber);
            date2digitYearCandidates.put(lineNumber, new String[]{dateMatcher.group(1),
                    dateMatcher.group(2), dateMatcher.group(3)});
        } else {
            Log.d(TAG, "No date 2 digit year matches");
        }
    }

    public void dateNumericMonthYear(int lineNumber, String input) {
        Matcher dateMatcher = datePatternMonthYearNumerals.matcher(input);

        if(dateMatcher.matches()) {
            foundDateMonthYear = true;
            Log.d(TAG, "Group 1: " + dateMatcher.group(1));
            Log.d(TAG, "Group 2: " + dateMatcher.group(2));

            dateMonthYearCandidates.put(lineNumber, new String[]{dateMatcher.group(1),
                    dateMatcher.group(2)});
        } else {
            Log.d(TAG, "No date numeric month year matches");
        }
    }

    public String dayPatternBuilder() {
        boolean first = true;
        StringBuilder dayPattern = new StringBuilder();
        for (int day = 1; day <= MAX_DAY; day++) {
            if(!first) {
                if(day < 10) {
                    dayPattern.append("|0");
                    dayPattern.append(day);
                } else {
                    dayPattern.append("|");
                    dayPattern.append(day);
                }
            } else {
                dayPattern.append("0");
                dayPattern.append(day);
                first = false;
            }
        }
        return dayPattern.toString();
    }

    public String monthStringPatternBuilder() {
        boolean first = true;
        StringBuilder monthPattern = new StringBuilder();
        for (String month : MONTHS) {
            if(!first) {
                monthPattern.append("|");
                monthPattern.append(month);
            } else {
                monthPattern.append(month);
                first = false;
            }
        }
        return monthPattern.toString();
    }

    public String monthNumberPatternBuilder() {
        boolean first = true;
        StringBuilder monthNumberPattern = new StringBuilder();
        for (int startingMonth = 1; startingMonth < 13; startingMonth++) {
            if(!first) {
                if(startingMonth < 10) {
                    monthNumberPattern.append("|0");
                    monthNumberPattern.append(startingMonth);
                } else {
                    monthNumberPattern.append("|");
                    monthNumberPattern.append(startingMonth);
                }
            } else {
                monthNumberPattern.append("0");
                monthNumberPattern.append(startingMonth);
                first = false;
            }
        }
        return monthNumberPattern.toString();
    }

    public String twoDigitYearStringPatternBuilder() {
        boolean first = true;
        StringBuilder yearPattern = new StringBuilder();
        for (int startingYear = STARTING_YEAR; startingYear < MAX_YEAR; startingYear++) {
            String year = String.valueOf(startingYear).substring(2,4).replaceAll("0", "O");
            if(!first) {
                yearPattern.append("|");
                yearPattern.append(String.valueOf(startingYear).substring(2,4));
                yearPattern.append("|");
                yearPattern.append(year);
            } else {
                yearPattern.append(String.valueOf(startingYear).substring(2,4));
                yearPattern.append("|");
                yearPattern.append(year);
                first = false;
            }
        }
        return yearPattern.toString();
    }

    public String yearStringPatternBuilder() {
        boolean first = true;
        StringBuilder yearPattern = new StringBuilder();
        for (int startingYear = STARTING_YEAR; startingYear < MAX_YEAR; startingYear++) {
            String year = String.valueOf(startingYear).replaceAll("0", "O");
            if(!first) {
                yearPattern.append("|");
                yearPattern.append(startingYear);
                yearPattern.append("|");
                yearPattern.append(year);
            } else {
                yearPattern.append(startingYear);
                yearPattern.append("|");
                yearPattern.append(year);
                first = false;
            }
        }
        return yearPattern.toString();
    }

    public String[] getLines(String string) {
        return string.split("\\r?\\n");
    }
}
