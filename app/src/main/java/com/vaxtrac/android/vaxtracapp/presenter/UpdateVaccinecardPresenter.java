package com.vaxtrac.android.vaxtracapp.presenter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.models.Dose;
import com.vaxtrac.android.vaxtracapp.models.Dose.Status;
import com.vaxtrac.android.vaxtracapp.models.Schedule;
import com.vaxtrac.android.vaxtracapp.models.VaccineHistory;
import com.vaxtrac.android.vaxtracapp.view.UpdateVaccineCardView;
import com.vaxtrac.nepalidatepickerlib.NepaliDateAlertBuilder;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import dateconverter.Date;
import dateconverter.NepaliDateConverter;

public class UpdateVaccinecardPresenter extends  Presenter{

    private UpdateVaccineCardView view;
    private Context context;
    private MainActivity activity;
    private Calendar mCalendar;
    private String currentAntigen = null;
    private DatePickerDialog.OnDateSetListener date_listener;
    private DialogInterface.OnClickListener nepali_date_listener;
    private String TAG = "UpdateVaccinecardPresenter";

    private final Date nepaliDate;
    private NepaliDateConverter converter;
    private boolean isNepaliLocale;

    public UpdateVaccinecardPresenter(final Context context, UpdateVaccineCardView view) {
        this.context = context;
        this.activity = (MainActivity) context;
        this.view = view;
        this.nepaliDate = new Date(0,0,0);

        isNepaliLocale = context.getResources().getConfiguration().locale.getISO3Language()
                .equals("nep");

        if (isNepaliLocale) {
            mCalendar = Calendar.getInstance();
            converter = new NepaliDateConverter();
            nepali_date_listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Date gregorianDate = converter.toGregorianDate(nepaliDate.toString());
                    mCalendar.set(Calendar.YEAR, gregorianDate.getYear());
                    mCalendar.set(Calendar.MONTH, gregorianDate.getMonth()-1);
                    mCalendar.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
                    updateDose(mCalendar);
                    dialog.dismiss();
                }
            };
        }

        else {
            mCalendar = Calendar.getInstance();
            date_listener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    // TODO Auto-generated method stub
                    mCalendar.set(Calendar.YEAR, year);
                    mCalendar.set(Calendar.MONTH, monthOfYear);
                    mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateDose(mCalendar);
                }
            };
        }
    }

    public void setView(View view) {this.view = (UpdateVaccineCardView) view;}

    private UpdateVaccineCardView getView() {return this.view;}

    public View.OnClickListener getCalendarListener(final String antigen){
        View.OnClickListener listener;

        if (isNepaliLocale) {
            NepaliDateAlertBuilder builder = new NepaliDateAlertBuilder(context, nepaliDate, nepali_date_listener);
            final AlertDialog dialog = builder.create();

            listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentAntigen = antigen;
                    dialog.show();
                }
            };

        }

        else
            listener = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    currentAntigen = antigen;
                    DatePickerDialog picker = new DatePickerDialog(context, date_listener, mCalendar
                            .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                            mCalendar.get(Calendar.DAY_OF_MONTH));
                    picker.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
                    picker.show();
                }
            };

        return listener;
    }

    private boolean isSameDay(Calendar c1, Calendar c2) {
        if (c1 == null || c2 == null)
            return false;
        return (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
                && c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH));
    }

    private void updateDose(Calendar c) {
        if (currentAntigen != null){
            Log.i(TAG, "update dose!");
            Dose dose = new Dose(currentAntigen);
            dose.setDate(c.getTime());

            AppController.mPatient.getHistory().reportDose(currentAntigen, dose, activity);
            currentAntigen = null;
            getView().reDrawTable();

        }else{
            Log.d(TAG, "Double call from calendar");
                /*
                Handler mHandler = new Handler();
                mHandler.postDelayed(new Runnable() {
                    @Override public void run() {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }, 100);
                */
        }

    }

    public void fill_table(TableLayout tableIn, LayoutInflater inflater){
        Schedule schedule = AppController.mPatient.getSchedule();
        if(schedule == null) {
            schedule = ((MainActivity) context).getSchedule();
            if(schedule == null) {
                Log.d(TAG, "MainActivity Schedule is null");
            }
        }
        VaccineHistory history = AppController.mPatient.getHistory();
        List<String> antigens = schedule.requiredVaccines();
        for(String antigen:antigens){
            //TableRow row = new TableRow(context);
            LinearLayout row = new LinearLayout(context);
            inflater.inflate(R.layout.vaccine_table_row_horizontal, row);
            LinearLayout spots =(LinearLayout) row.findViewById(R.id.vaccine_table_row_space);

            List<Dose> doses = history.getComputedSchedule().get(antigen);
            if (doses == null || history.wasUpdated()){
                doses = schedule.compareDoses(history, antigen);
                history.getComputedSchedule().put(antigen, doses);
            }

            Dose titleDose = new Dose(GlobalTranslations.translate(antigen));
            View titleDoseView =titleDose.inflateDoseName(context, inflater);
            titleDoseView.setOnClickListener(getCalendarListener(antigen));
            spots.addView(titleDoseView);
            for(Dose d: doses){
                if (d.getStatus() == Status.received_not_valid || d.getStatus() == Status
                        .received_valid) {
                    View dose = d.inflateHistoricDose(context, inflater);
                    spots.addView(dose);
                }
            }
            tableIn.addView(row);

        }
             /*
            TODO KILL
             */
        Map<Integer,String> ageSlots = schedule.getAgeSlots();
        for (Integer age: ageSlots.keySet()){
            Log.i(TAG, String.format("%s | %s", age, ageSlots.get(age)));
        }
    }
}
