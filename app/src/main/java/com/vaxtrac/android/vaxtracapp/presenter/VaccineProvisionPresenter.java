package com.vaxtrac.android.vaxtracapp.presenter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.models.Dose;
import com.vaxtrac.android.vaxtracapp.models.Dose.Status;
import com.vaxtrac.android.vaxtracapp.models.Schedule;
import com.vaxtrac.android.vaxtracapp.models.VaccineHistory;
import com.vaxtrac.android.vaxtracapp.view.VaccinationView;

import java.util.ArrayList;
import java.util.List;

public class VaccineProvisionPresenter extends Presenter {

    private VaccinationView view;
    private Context context;
    private MainActivity activity;
    private static final String TAG = "VaccineViewPresenter";

    private static final boolean OCR_ENABLED = true;
    private AlertDialog OCRAlertDialog;
    public static final int OCR_CODE = 3350;
    public static List<String> requiredAntigens;


    public VaccineProvisionPresenter(Context context) {
        this.context = context;
        this.activity = (MainActivity) context;
        requiredAntigens = new ArrayList<>();
    }

    public void setView(View view) {this.view = (VaccinationView) view;}

    public void invalidate() {
        Log.d(TAG, "invalidating view");
        view.invalidate();
    }

    public void fill_table(TableLayout table_in, LayoutInflater inflater){
        Schedule schedule = AppController.mPatient.getSchedule();
        if(schedule == null) {
            schedule = ((MainActivity) context).getSchedule();
            if(schedule == null) {
                Log.d(TAG, "MainActivity Schedule is null");
            }
        }

        VaccineHistory history = AppController.mPatient.getHistory();
        List<String> antigens = schedule.requiredVaccines();
        int requiredDoses = 0;

        for(String antigen:antigens){
            //TableRow row = new TableRow(context);
            LinearLayout row = new LinearLayout(context);
            inflater.inflate(R.layout.vaccine_table_row_horizontal, row);
            LinearLayout spots =(LinearLayout) row.findViewById(R.id.vaccine_table_row_space);

            List<Dose> doses = history.getComputedSchedule().get(antigen);
            if (doses == null || history.wasUpdated()){
                doses = schedule.compareDoses(history, antigen);
                history.getComputedSchedule().put(antigen, doses);
            }

            boolean required = false;
            Dose titleDose = new Dose(antigen);
            spots.addView(titleDose.inflateDoseName(context, inflater));
            titleDose.updateBackgroundDrawable(context.getResources().getDrawable((R.drawable.grad_dose_orange)));
            for(Dose d: doses){
                if (d.getStatus() == Status.eligible){
                    row.setOnClickListener(d.registerRowApplyListener(row,titleDose, context));
                    required = true;
                }else if (d.getStatus() == Status.received_valid && history.isNewRecorded(d)){
                    titleDose.updateBackgroundDrawable(context.getResources().getDrawable(R.drawable.grad_dose_received));
                    row.setOnClickListener(d.registerRowUnApplyListener(row,titleDose, context));
                    required = true;
                    if(d.hasVial()) {
                        Log.d(TAG, "Has Vial");
                        titleDose.updateBackgroundDrawable(context.getResources().getDrawable(R
                                .drawable.dose_layer));
                    } else {
                        Log.d(TAG, "No Vial");
                    }
                }
                View dose = d.inflateDose(context, inflater);
                spots.addView(dose);
            }
            if (required){
                table_in.addView(row);
                requiredAntigens.add(antigen);
                requiredDoses++;
            }
        }

        if (requiredDoses == 0) {
            LinearLayout row = new LinearLayout(context);
            inflater.inflate(R.layout.vaccine_table_row_horizontal, row);
            LinearLayout spots =(LinearLayout) row.findViewById(R.id.vaccine_table_row_space);
            TextView text = new TextView(context);
            text.setText(R.string.no_vaccine_required);
            text.setTextColor(context.getResources().getColor(R.color.white));
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            spots.addView(text);
            table_in.addView(row);
        }
    }

    public void cameraOCRClicked() {
        Log.d(TAG, "cameraOCRClicked");
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Acquire Vial Image From:");
        String[] choices = new String[]{"Existing Image", "Camera Image"};
        builder1.setItems(choices, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int button) {
                switch(button) {
                    case 0:
                        /* Existing Image */
                        AppController.getFlow().goTo(new Screens.OCRImagesScreen());
                        break;
                    case 1:
                        /* Camera Image */
                        Intent intent = new Intent("edu.sfsu.cs.orange.ocr.OCR");
                        intent.putExtra("required_vaccines", requiredAntigens.toArray());
                        //intent.setComponent(new ComponentName("edu.sfsu.cs.orange.ocr",
                        //        "edu.sfsu.cs.orange.ocr.CaptureActivity"));
                        activity.startActivityForResult(intent, OCR_CODE);
                }
            }
        });

        OCRAlertDialog = builder1.create();
        OCRAlertDialog.show();
    }
}
