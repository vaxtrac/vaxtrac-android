package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*Temporary database used to back the DOSE table up on file*/
public class TemporaryDoseDBHelper extends SQLiteOpenHelper {
    public final String TAG = "TmpDoseDBHelper";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "tmpdose.db";

    public static final String DOSE_TABLE_NAME = "DOSE";
    public static final String COLUMN_DOCID = "docid";
    public static final String COLUMN_USERID = "userid";
    public static final String COLUMN_PATIENTID = "patientid";
    public static final String COLUMN_VILLAGE = "village";
    public static final String COLUMN_VIALID = "vialid";
    public static final String COLUMN_VACCINENAME = "vaccinename";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String COLUMN_PATIENTAGE = "patientage";
    public static final String COLUMN_SERIES = "series";
    public static final String COLUMN_MONTH = "month";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_SESSIONTYPE = "sessiontype";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DOSE_TABLE_NAME + " (" +
            COLUMN_DOCID + " TEXT PRIMARY KEY," +
            COLUMN_USERID + TEXT_TYPE + COMMA_SEP +
            COLUMN_PATIENTID + TEXT_TYPE + COMMA_SEP +
            COLUMN_VILLAGE + TEXT_TYPE + COMMA_SEP +
            COLUMN_VIALID + TEXT_TYPE + COMMA_SEP +
            COLUMN_VACCINENAME + TEXT_TYPE + COMMA_SEP +
            COLUMN_TIMESTAMP + TEXT_TYPE + COMMA_SEP +
            COLUMN_PATIENTAGE + TEXT_TYPE + COMMA_SEP +
            COLUMN_SERIES + TEXT_TYPE + COMMA_SEP +
            COLUMN_MONTH + TEXT_TYPE + COMMA_SEP +
            COLUMN_YEAR + TEXT_TYPE + COMMA_SEP +
            COLUMN_SESSIONTYPE + TEXT_TYPE +
            " )";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DOSE_TABLE_NAME;

    public TemporaryDoseDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
