package com.vaxtrac.android.vaxtracapp.models;

import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class Patient {

    public Map<String,String> attributes;
    private Guardian guardian;
    private List<Guardian> guardians;
    private VaccineHistory history;
    private Schedule schedule;
    private boolean isNewPatient = false;

    final private String TAG = "Patient";

    public Patient() {
    }

    public Patient(Schedule schedule) {
        this.schedule = schedule;
    }

    public Patient(boolean isNewPatient, Schedule schedule){
        this.isNewPatient = isNewPatient;
        this.schedule = schedule;
    }

    public boolean isNewPatient() {
        return this.isNewPatient;
    }

    public void test_schedule(){
        List<String> antigens = schedule.requiredVaccines();
        for(String antigen:antigens){
            Log.i(TAG, "Antigen: " + antigen);
            List<Dose> doses = schedule.compareDoses(history, antigen);
            for(Dose d: doses){
                try{
                    Log.i(TAG,d.date.toString() + d.status.toString());
                }catch (Exception e){
                    Log.i(TAG, d.status.toString() + "... no date given");
                }
            }
        }
    }

    public VaccineHistory getHistory() {
        return this.history;
    }

    public Schedule getSchedule() {
        return this.schedule;
    }

    public Guardian getGuardian() {
        return this.guardian;
    }

    public List<Guardian> getGuardians() {
        return this.guardians;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttribute(String key, String value) {
        attributes.put(key, value);
    }

    public String getAttribute(String key) {
        return attributes.get(key);
    }

    public void loadMap(String caseID, Map<String,String> info){
        this.attributes = info;
        attributes.put("case_id", caseID);
        String reportedString = info.get("reported_vaccines");
        String recordedString = info.get("recorded_vaccines");
        String dobString = info.get("date_of_birth");
        Log.i(TAG, String.format("Loaded: \nREP: %s \nREC %s", reportedString, recordedString));
        load_history(reportedString, recordedString, dobString);
    }

    public void loadSearchObject(SearchObject object){
        //Making a New Patient from Registration Terms
        final Map<String,String> sMap = new HashMap<String,String>(){{
            put("patient_name","child_name");
            put("date_of_birth","child_dob");
            put("sex","child_sex");
            put("address", "address");
            put("village","village");
            put("card_num","");
            put("contact_number","mobile");
            }
        };

        this.attributes = new HashMap<>();
        String childID = UUID.randomUUID().toString();
        attributes.put("case_id", childID);
        guardian = new Guardian();
        guardian.loadSearchObject(childID, object);
        Map<String,String> data = object.getProperties();
        for(String ccKey: sMap.keySet()){
            String formKey = sMap.get(ccKey);
            this.attributes.put(ccKey, data.get(formKey));
        }
        String reportedString = null;
        String recordedString = null;
        String dobString = data.get("child_dob");

        load_history(reportedString, recordedString, dobString);
    }

    public void loadFromCommCare(String patientID){
        Map<String,String> info = AppController.mCommCareDataManager.loadCaseFromCommCare(patientID);
        loadMap(patientID, info);
    }

    private void load_history(String rep_str, String rec_str, String dob_string){

        JSONObject recorded_vaccines = null;
        JSONObject reported_vaccines = null;

        if (rep_str != null){
            try {
                reported_vaccines = new JSONObject(rep_str);
                Log.i(TAG, "Got Reported Vaccinations from String");
            } catch (JSONException e) {
                Log.i(TAG,"Invalid JSON in reported vaccines: " + rep_str);
            }
        }else{
            reported_vaccines = new JSONObject();
            Log.i(TAG,"No reported Vaccines");
        }
        if (rec_str != null){
            try {
                recorded_vaccines = new JSONObject(rec_str);
                Log.i(TAG, "Got Recorded Vaccinations from String");
            } catch (JSONException e) {
                Log.i(TAG,"Invalid JSON in recorded vaccines: " + rec_str);
            }
        }else{
            recorded_vaccines = new JSONObject();
            Log.i(TAG,"No recorded Vaccines");
        }
        history = new VaccineHistory(reported_vaccines, recorded_vaccines, dob_string);
    }

}