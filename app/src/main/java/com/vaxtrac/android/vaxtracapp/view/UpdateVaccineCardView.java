package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.UpdateVaccinecardPresenter;

public class UpdateVaccineCardView extends LinearLayout{

    private final String TAG = "UpdateVaccineCardView";
    UpdateVaccinecardPresenter presenter;
    TableLayout table;
    LayoutInflater inflater;
    Context context;

    public UpdateVaccineCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        presenter = new UpdateVaccinecardPresenter(this.context, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        inflater = LayoutInflater.from(getContext());
        table  = (TableLayout) findViewById(R.id.vaccination_table);
        reDrawTable();
    }

    public void reDrawTable(){
        table.removeAllViews();
        if (AppController.mPatient != null) {
            presenter.fill_table(table, inflater);
            table.invalidate();
        }
        else{
            Toast.makeText(context, "No patient Loaded!", Toast.LENGTH_SHORT).show();
        }
    }
}
