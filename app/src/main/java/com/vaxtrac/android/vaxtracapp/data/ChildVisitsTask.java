package com.vaxtrac.android.vaxtracapp.data;

import android.database.Cursor;
import android.os.AsyncTask;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ChildVisitsTask extends AsyncTask<Void, Void, Integer> {

    private static final String TAG = "ChildVisitsTask";

    public interface TaskListener {
        void onFinished(int count);
    }

    private TaskListener listener;

    private Calendar startDate;
    private Calendar endDate;
    private SimpleDateFormat sqlDateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");

    private static final DateFormat isoDateFormat =
            new UnlocalizedDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");
    CommCareDataManager.DoseDBHandler doseDB = AppController.mCommCareDataManager.doseDB;

    public ChildVisitsTask(Calendar startDate, Calendar endDate) {
        this.startDate = startDate;
        if(endDate != null) {
            this.endDate = endDate;
        } else {
            this.endDate = startDate;
        }
    }

    public void setTaskListener(TaskListener listener) {
        this.listener = listener;
    }

    public String getStartDate() {
        return sqlDateFormat.format(startDate.getTime());
    }

    public String getEndDate() {
        return sqlDateFormat.format(endDate.getTime());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(Void[] params) {
        Cursor visits = doseDB.allRowsBetweenGroup(CommCareDataManager.DoseDBHandler
                .TIMESTAMP_COLUMN,
                getStartDate(), getEndDate(), new String[]{
                        CommCareDataManager.DoseDBHandler.PATIENT_ID_COLUMN,
                        CommCareDataManager.DoseDBHandler.TIMESTAMP_COLUMN});
        int visitCount = visits.getCount();
        visits.close();
        return visitCount;
    }

    @Override
    protected void onPostExecute(Integer visits) {
        super.onPostExecute(visits);
        listener.onFinished(visits);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}