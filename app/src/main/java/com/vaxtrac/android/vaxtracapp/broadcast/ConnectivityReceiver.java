package com.vaxtrac.android.vaxtracapp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;


public class ConnectivityReceiver extends BroadcastReceiver {

    private static final String TAG = "ConnectivityReceiver";
    public static final String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Received Commcare Broadcast " + intent.getAction());

        switch (intent.getAction()) {
            case CONNECTIVITY_ACTION:
                Log.i(TAG, "Received Connectivity Change Broadcast");
                ConnectivityManager cm =
                        (ConnectivityManager) AppController.getAppContext().getSystemService(Context
                        .CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                    Log.d(TAG, "Network Type: " + activeNetwork.getTypeName());
                    AppController.commCareAPI.sync();
                } else {
                    Log.d(TAG, "Network Lost");
                }
                break;
            default:
                Log.i(TAG, "Connectivity Broadcast matched no known actions");
        }
    }
}
