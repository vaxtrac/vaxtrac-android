package com.vaxtrac.android.vaxtracapp.reports;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.models.DoseReport;
import com.vaxtrac.android.vaxtracapp.models.Schedule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import hugo.weaving.DebugLog;

public class ReportAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ReportAdapter.class.toString();
    private static final int ROW_VIEW_TYPE = 0;
    private static final int HEADER_VIEW_TYPE = 1;
    private static final int SPACER_VIEW_TYPE = 2;

    private static final int CELL_STYLE = R.style.LocalReportCell;
    private static final int CELL_STYLE_BOLD = R.style.LocalReportCellBold;

    DoseReport report;
    Cursor cursor;
    LinkedHashMap<String, int[]> rows;
    ArrayList<String> rowIndexing;
    ArrayList<Integer> spacerIndexing;

    String[] groupLabels;
    Map<String, Integer> groupOrder;
    String defaultGroup;
    boolean hasGroups;
    int groupSize = 1;

    int[] sumTotals;

    String[] categoryLabels;
    int categorySize;

    Schedule schedule;
    int requiredVaccinesCount;

    public ReportAdapter(Cursor cursor, List<int[]> categories, Schedule schedule) {
        this.cursor = cursor;
        this.categoryLabels = getCategoryLabels(categories);
        this.categorySize = categories.size();
        this.schedule = schedule;
        requiredVaccinesCount = schedule.requiredVaccines().size();
        rows = new LinkedHashMap<>();
        rowIndexing = new ArrayList<>();
        spacerIndexing = new ArrayList<>();
        loadData();
        addPlaceholders(schedule.requiredVaccines());
    }

    public ReportAdapter(Cursor cursor, String[] groups, List<int[]> categories, Schedule
            schedule) {
        this.cursor = cursor;
        if(groups != null) {
            this.hasGroups = true;
            this.groupLabels = groups;
            this.defaultGroup = groups[0];
            this.groupSize = groups.length;
            this.groupOrder = new ArrayMap<>(groupSize);
            for(int i = 0; i < groupSize; i++) {
                groupOrder.put(groups[i], i);
            }
        }
        this.categoryLabels = getCategoryLabels(categories);
        this.categorySize = categories.size();
        this.schedule = schedule;
        requiredVaccinesCount = schedule.requiredVaccines().size();
        rows = new LinkedHashMap<>();
        rowIndexing = new ArrayList<>();
        spacerIndexing = new ArrayList<>();
        loadData();
        sumTotals();
        addPlaceholders(schedule.requiredVaccines());
    }

    public ReportAdapter(DoseReport report, Schedule schedule) {
        this.report = report;
        this.categoryLabels = getCategoryLabels(report.getCategories());
        this.categorySize = report.getCategoryCount();
        this.schedule = schedule;
        rows = new LinkedHashMap<>();
        rowIndexing = new ArrayList<>();
        spacerIndexing = new ArrayList<>();
        extractReportData();
    }


    public void extractReportData() {
        int ordering = 0;
        for(String antigen : schedule.requiredVaccines()) {
            Map<Integer, int[]> series = report.getAntigenSeries(antigen);
            int doseCount = schedule.requiredVaccinationsForAntigen(antigen).size();
            if(series != null && series.size() > 0) {
                for(int i = 0; i < doseCount; i++) {
                    String antigenKey = antigen + "_" + i;
                    if(series.containsKey(i)) {
                        rows.put(antigenKey, series.get(i));
                    } else {
                        rows.put(antigenKey, new int[categorySize]);
                    }
                    rowIndexing.add(ordering, antigenKey);
                    ordering++;
                }
                rowIndexing.add(ordering, "SPACER");
                spacerIndexing.add(ordering);
                ordering++;
            } else {
                for(int i = 0; i < doseCount; i++) {
                    String antigenKey = antigen + "_" + i;
                    rows.put(antigenKey, new int[categorySize]);
                    rowIndexing.add(ordering, antigenKey);
                    ordering++;
                }
                rowIndexing.add(ordering, "SPACER");
                spacerIndexing.add(ordering);
                ordering++;
            }
        }
    }


    public void loadData() {
        if(cursor != null && cursor.moveToFirst()) {
            int antigenColumn = cursor.getColumnIndex("vaccinename");
            int seriesColumn = cursor.getColumnIndex("series");
            int binColumn = cursor.getColumnIndex("categories");
            int totalColumn = cursor.getColumnIndex("totals");
            do {
                String antigen = cursor.getString(antigenColumn);
                int series = cursor.getInt(seriesColumn);
                String reportKey = antigen + "_" + series;
                if(hasGroups) {
                    int sessionColumn = cursor.getColumnIndex("sessiontype");
                    String sessionType = cursor.getString(sessionColumn);
                    if (sessionType == null) {
                        sessionType = defaultGroup;
                    }
                    reportKey += "_" + sessionType;
                }
                rows.put(reportKey, getTotalValues(cursor.getString
                        (binColumn), cursor.getString(totalColumn), categorySize));
            } while (cursor.moveToNext());
        }
    }


    /* Add blank antigen and series rows which have no data */
    public void addPlaceholders(List<String> antigens) {
        int ordering = 0;
        for(String antigen: antigens) {
            int doseCount = schedule.requiredVaccinationsForAntigen(antigen).size();
            for(int i = 0; i < doseCount; i++) {
                String antigenKey = antigen + "_" + i;
                if(!rows.containsKey(antigenKey)) {
                    rows.put(antigenKey, new int[categorySize]);
                }
                rowIndexing.add(ordering, antigenKey);
                ordering++;
            }
            rowIndexing.add(ordering, "SPACER");
            spacerIndexing.add(ordering);
            ordering++;
        }
    }


    private int[] getTotalValues(String bins, String values, int categorySize) {
        /* maintain sort ordering */
        String[] categories = bins.split(",");
        String[] split = values.split(",");
        int[] totals = new int[categorySize];
        for(int i = 0; i < categories.length; i++) {
            totals[Integer.parseInt(categories[i])] = Integer.parseInt(split[i]);
        }
        return totals;
    }


    private void sumTotals() {
        int columns = groupSize * categorySize;
        sumTotals = new int[columns];
        boolean multipleGroups;
        if(groupSize > 1) {
            for(Map.Entry<String, int[]> row : rows.entrySet()) {
                int[] values = row.getValue();
                int offset = getGroupOffset(row.getKey());
                for (int j = 0; j < values.length; j++) {
                    sumTotals[offset * categorySize + j] += values[j];
                }
            }
        } else {
            for(Map.Entry<String, int[]> row : rows.entrySet()) {
                int[] values = row.getValue();
                for(int i = 0; i < values.length; i++) {
                    sumTotals[i] += values[i];
                }
            }
        }
    }

    private int getGroupOffset(String group) {
        String[] rowKey = group.split("_");
        return groupOrder.get(rowKey[rowKey.length-1]);
    }

    public int[] getReportTotals() {
        return sumTotals;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == SPACER_VIEW_TYPE) {
             return new ReportSpacerViewHolder(((LinearLayout)LayoutInflater.from(parent
                     .getContext())
                     .inflate(R.layout.report_spacer_row, parent, false)));
        } else {
             return new ReportViewHolder((LinearLayout)LayoutInflater.from(parent.getContext())
                     .inflate(R.layout.report_row, parent, false), groupSize * categorySize);
        }
    }

    @Override

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType() == ROW_VIEW_TYPE) {
            ReportViewHolder rowHolder = (ReportViewHolder) holder;

            rowHolder.layout.setBackgroundResource(highlightColor(getColorGroup(position)));
            String antigenSeriesKey = rowIndexing.get(position);
            String[] keys = antigenSeriesKey.split("_");
            String antigen;

            /* pnuemo_conj edge case, could pick a better delimiter */
            if(keys.length > 2) {
                antigen = keys[0] + "_" + keys[1];
            } else {
                antigen = keys[0];
            }

            int series = Integer.parseInt(keys[keys.length - 1]);
            int[][] values = new int[groupSize][categorySize];

            if(groupSize > 1) {
                for (int i = 0; i < groupSize; i++) {
                    values[i] = rows.get(antigenSeriesKey + "_" + groupLabels[i]);
                    if(values[i] == null) {
                        values[i] = new int[categorySize];
                    }
                }
            } else {
                values[0] = rows.get(antigenSeriesKey);
            }

            rowHolder.label.setText(getRowLabel(antigen, series));
            Context context = rowHolder.label.getContext();

            /* RecyclerView will not reset style so we must specify it everytime */
            for(int i = 0; i < groupSize; i++) {
                for (int j = 0; j < categorySize; j++) {
                    rowHolder.categories[i * categorySize + j].setTextAppearance(context, getCellStyle
                            (values[i][j] != 0));
                    rowHolder.categories[i * categorySize + j].setText(String.valueOf(values[i][j]));
                }
            }
        }
    }

    private int getCellStyle(boolean hasValue) {
        return hasValue ? CELL_STYLE_BOLD : CELL_STYLE;
    }
    private int getCellBackgroundHighlight() { return R.color.fbutton_color_orange; }

    private boolean getColorGroup(int position) {
        for(int i = 0; i < spacerIndexing.size(); i++) {
            if(spacerIndexing.get(i) > position) {
                return i % 2 == 0;
            }
        }
        return spacerIndexing.size() % 2 == 0;
    }

    private int highlightColor(boolean needsHighlight) {
        return needsHighlight ? android.R.color.holo_orange_dark : R.color.fbutton_color_carrot;
    }

    private String getRowLabel(String antigen, int series) {
        return GlobalTranslations.translate(antigen) + " - " +
                (schedule.hasBooster(antigen) ? series : series + 1);
    }


    private String[] getCategoryLabels(List<int[]> categories) {
        String[] labels = new String[categories.size()];
        int index = 0;
        for(int[] range : categories) {
            labels[index] = getCategoryMonthLabel(range[0], range[1]);
            index++;
        }
        return labels;
    }

    private String getCategoryMonthLabel(int start, int end) {
        return ((int)(start / (365 / 12.0))) + " - " + ((int)(end / (365 / 12.0)));
    }

    @Override
    public int getItemViewType(int position) {
        return spacerIndexing.contains(position) ? SPACER_VIEW_TYPE : ROW_VIEW_TYPE;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        /* Includes header */
        return rowIndexing.size();
    }

    @Override

    public String toString() {
        StringBuilder adapterDescription = new StringBuilder();
        for(Map.Entry<String, int[]> entry : rows.entrySet()) {
            String antigen = entry.getKey();
            String series = antigen.split("_")[1];
            int[] values = entry.getValue();

            adapterDescription
                    .append("\n")
                    .append(antigen)
                    .append(" ")
                    .append(series)
                    .append(" ");

            for(int i = 0; i < values.length; i++) {
                adapterDescription.append(" group ")
                        .append(i)
                        .append(" ")
                        .append(values[i]);
            }
        }
        return adapterDescription.toString();
    }

    public static class ReportViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        TextView label; // antigen & series label
        TextView[] categories;


        public ReportViewHolder(LinearLayout itemView, int cells) {
            super(itemView);
            Context context = AppController.getAppContext();
            layout = itemView;
            label = (TextView) itemView.findViewById(R.id.report_label);
            categories = new TextView[cells];
            LayoutInflater inflater = LayoutInflater.from(context);
            for(int i = 0; i < cells; i++) {
                categories[i] = (TextView) inflater.inflate(R.layout.report_row_cell, null);
                categories[i].setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup
                        .LayoutParams.WRAP_CONTENT, 1));
                layout.addView(categories[i]);
            }
        }
    }

    public static class ReportSpacerViewHolder extends RecyclerView.ViewHolder {

        public ReportSpacerViewHolder(LinearLayout itemView) {
            super(itemView);
        }
    }

    public static class ReportHeaderViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        TextView header1;
        TextView header2;
        TextView header3;
        TextView header4;

        public ReportHeaderViewHolder(LinearLayout itemView) {
            super(itemView);
        }
    }
}
