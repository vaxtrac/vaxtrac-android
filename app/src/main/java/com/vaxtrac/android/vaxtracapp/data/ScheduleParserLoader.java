package com.vaxtrac.android.vaxtracapp.data;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.models.Schedule;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class ScheduleParserLoader extends AsyncTaskLoader<Schedule> {

    private static final String TAG = ScheduleParserLoader.class.getName();

    private static final String MACHINE_EXPRESSION = "//machine_schedule";
    private static final String READABLE_EXPRESSION = "//readable_schedule";
    private static final String BOOSTER_EXPRESSION = "//booster_schedule";
    private static final String ORDER_EXPRESSION = "//vaccine_order";
    private static final String VERSION_EXPRESSION = "//version";

    private static String[] expressions = new String[]{ MACHINE_EXPRESSION, READABLE_EXPRESSION,
            BOOSTER_EXPRESSION, ORDER_EXPRESSION, VERSION_EXPRESSION};

    private String scheduleXml;

    public ScheduleParserLoader(Context context, String xml) {
        super(context);
        scheduleXml = xml;
    }

    public Schedule parse(String fixtureXml) {
        Log.d(TAG, "Immunization Schedule XML " + fixtureXml);

        XPath schedulePath = XPathFactory.newInstance().newXPath();

        Map<String, String> schedulesMap = evaluateExpresions(schedulePath, fixtureXml);

        for(Map.Entry entry : schedulesMap.entrySet()) {
            Log.d(TAG, entry.getKey() + ": " + entry.getValue());
        }

        return Schedule.loadSchedule(
                schedulesMap.get("machine_schedule"),
                schedulesMap.get("readable_schedule"),
                schedulesMap.get("booster_schedule"),
                schedulesMap.get("vaccine_order"),
                Integer.parseInt(schedulesMap.get("version")));
    }

    public NodeList evaluateNodeList(XPath path, String xml, String expression) {
        try {
            return (NodeList) path.evaluate(expression, new InputSource(new StringReader(xml)),
                    XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Map.Entry<String,String> getNodeListValue(NodeList nodeList) {
        Node node = nodeList.item(0);
        Log.d(TAG, "Node " + node.getNodeName() + " : " + node.getTextContent());
        return new AbstractMap.SimpleEntry<String,String>(node.getNodeName(),node.getTextContent());
    }

    public Map<String,String> evaluateExpresions(XPath path, String xml) {
        Map<String, String> nodesValues = new HashMap<>();
        for(String expression : expressions) {
            Log.d(TAG, "expression " + expression);
            Map.Entry<String,String> nodeData = getNodeListValue(evaluateNodeList(path, xml,
                    expression));
            nodesValues.put(nodeData.getKey(), nodeData.getValue());
        }
        return nodesValues;
    }

    @Override
    public Schedule loadInBackground() {
        Log.d(TAG, "loadInBackground()");
        return parse(scheduleXml);
    }
}
