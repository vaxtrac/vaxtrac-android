package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.models.DoseReport;
import com.vaxtrac.android.vaxtracapp.models.Schedule;
import com.vaxtrac.android.vaxtracapp.reports.ReportAdapter;
import com.vaxtrac.android.vaxtracapp.reports.ReportsManager;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;
import com.vaxtrac.android.vaxtracapp.view.LocalReportView;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import hugo.weaving.DebugLog;

public class LocalReportPresenter {

    public static final String TAG = "LocalReportPresenter";

    private MainActivity activity;
    private LocalReportView view;

    private Context context;
    private Resources resources;
    private Locale locale;

    protected static CommCareDataManager.DoseDBHandler doseDB;
    protected static ReportsManager reportsManager;

    public Calendar calendar;
    private int currentMonth;
    private int currentYear;

    private List<Integer> reportYears;

    private List<int[]> bins;
    private ReportAdapter adapter;

    private Schedule schedule;

    private ReportType currentReportType;
    protected Date currentStartDate;
    protected Date currentEndDate;

    private static final String SQLITE_END_CHAR = "Z";

    private final DateFormat dbDateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy LLL dd");
    private final SimpleDateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");
    protected final DateFormat dateRangeFormat = new SimpleDateFormat("MMM dd yyyy");

    public LocalReportPresenter(Context context, View view){
        this.context = context;
        this.activity = (MainActivity) context;
        this.view = (LocalReportView) view;

        resources = context.getResources();
        locale = resources.getConfiguration().locale;
        schedule = activity.getSchedule();

        doseDB = AppController.mCommCareDataManager.doseDB;
        reportsManager = AppController.mReportsManager;
        getAvailableYears();

        calendar = Calendar.getInstance();
        currentMonth = calendar.get(Calendar.MONTH);
        currentYear = calendar.get(Calendar.YEAR);
    }

    public String getDailyDateLabel() {
        return dateFormat.format(calendar.getTime());
    }

    public String getWeeklyDateLabel() {
        return dateFormat.format(getWeeklyStartDate(calendar)) + " - " + dateFormat.format
                (getWeeklyEndDate(calendar));
    }

    public String getUnitForPeriod(ReportType reportType) {
        switch (reportType) {
            case DAILY:
                return resources.getString(R.string.day);
            case WEEKLY:
                return resources.getString(R.string.week);
            default:
                return resources.getString(R.string.day);
        }
    }

    public String[] getPeriodItems() {
        return new String[] {
                resources.getString(R.string.daily),
                resources.getString(R.string.weekly),
                resources.getString(R.string.monthly)
        };
    }

    public int getCurrentYear() {
        return currentYear;
    }

    public int getCurrentMonth() {
        return currentMonth;
    }

    public String getReportDescription() {
        return getReportTitle() + " - " + getReportPeriod() + " \t \t " + getReportDateRange();
    }

    public String getReportTitle() {
        return getCurrentUser();
    }

    public String getReportPeriod() {
        return getPeriodItems()[currentReportType.ordinal()];
    }

    public void getAvailableYears() {
        reportYears = reportsManager.getReportYears();
    }


    public Integer[] getAvailableMonths(String year) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        int parsedYear = -1;
        try {
            parsedYear = numberFormat.parse(year).intValue();
        } catch (ParseException e) {
            Log.e(TAG, "Parse Exception ", e);
        }
        List<Integer> monthList = reportsManager.getReportMonths(parsedYear);
        Cursor monthCursor = doseDB.distinctMonthsForYear(parsedYear);
        if(monthCursor != null) {
            if(monthCursor.moveToFirst()) {
                int monthColumn = monthCursor.getColumnIndex("month");
                do {
                    monthList.add(monthCursor.getInt(monthColumn));
                } while (monthCursor.moveToNext());
            }
            monthCursor.close();
        }
        Set<Integer> monthSet = new TreeSet<>(monthList);
        return monthSet.toArray(new Integer[monthSet.size()]);
    }


    public Integer[] getYears(){
        Cursor cursor = doseDB.uniqueItemsFromColumn(doseDB.YEAR_COLUMN);
        Set<Integer> years = new HashSet<>(reportYears);
        int col = cursor.getColumnIndex(doseDB.ID_COLUMN);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            years.add(cursor.getInt(col));
            Log.d(TAG, "Found year: " + cursor.getInt(col));
        }
        Integer[] sortedYears = years.toArray(new Integer[0]);
        Arrays.sort(sortedYears);
        return sortedYears;
    }



    public String[] getAdapterYears() {
        Integer[] years = getYears();
        String[] adapterYears = new String[years.length];
        for(int i = 0; i < years.length; i++) {
            adapterYears[i] = String.valueOf(years[i]);
        }
        return adapterYears;
    }


    public String[] getAdapterMonths(String year) {
        Integer[] months = getAvailableMonths(year);
        String[] adapterMonths = new String[months.length];
        for(int i = 0; i < months.length; i++) {
            adapterMonths[i] = String.format("%02d", months[i]);
        }
        return adapterMonths;
    }

    public void setCurrentReportType(ReportType reportType) {
        currentReportType = reportType;
    }

    public void setCurrentReportType(int position) {
        currentReportType = reportForPosition(position);
    }

    public void setCurrentReportDate(Date startDate, Date endDate) {
        currentStartDate = startDate;
        currentEndDate = endDate;
    }

    public String getReportDateRange() {
        return dateRangeFormat.format(currentStartDate) + (currentEndDate == null ? "" :
                (" - " + dateRangeFormat.format(currentEndDate)));
    }

    public void getDailyReport() {
        Log.d(TAG, "getDailyReport " + calendar.toString());
        setCurrentReportDate(calendar.getTime(), null);
        String startDate = dbDateFormat.format(calendar.getTime());
        new ReportLoader(ReportType.DAILY, startDate, startDate + SQLITE_END_CHAR).execute();
    }

    public void getWeeklyReport() {
        Log.d(TAG, "getWeeklyReport " + calendar.getTime().toString());
        Date firstDay = getWeeklyStartDate(calendar);
        Date lastDay = getWeeklyEndDate(calendar);
        setCurrentReportDate(firstDay, lastDay);
        new ReportLoader(ReportType.WEEKLY, dbDateFormat.format(firstDay),
                dbDateFormat.format(lastDay) + SQLITE_END_CHAR).execute();
    }

    public Date getWeeklyStartDate(Calendar cal) {
        Calendar weeklyCalendar = Calendar.getInstance();
        weeklyCalendar.setTime(cal.getTime());
        weeklyCalendar.set(Calendar.DAY_OF_WEEK, weeklyCalendar.getFirstDayOfWeek());
        return weeklyCalendar.getTime();
    }

    public Date getWeeklyEndDate(Calendar cal) {
        Calendar weeklyCalendar = Calendar.getInstance();
        weeklyCalendar.setTime(cal.getTime());
        weeklyCalendar.set(Calendar.DAY_OF_WEEK, weeklyCalendar.getFirstDayOfWeek());
        // Sunday - Sunday or Sunday - Saturday?
        weeklyCalendar.add(Calendar.DAY_OF_MONTH, 6);
        return weeklyCalendar.getTime();
    }

    public Date getMonthlyStartDate(Calendar cal) {
        Calendar monthlyCalendar = Calendar.getInstance();
        monthlyCalendar.setTime(cal.getTime());
        monthlyCalendar.set(Calendar.DAY_OF_MONTH, 1);
        return monthlyCalendar.getTime();
    }

    public Date getMonthlyEndDate(Calendar cal) {
        Calendar monthlyCalendar = Calendar.getInstance();
        monthlyCalendar.setTime(cal.getTime());
        monthlyCalendar.set(Calendar.DAY_OF_MONTH, monthlyCalendar.getActualMaximum(Calendar
                .DAY_OF_MONTH));
        return monthlyCalendar.getTime();
    }


    public void getMonthlyReport(final Calendar selectedDate) {
        Log.d(TAG, "getMonthlyReport " + selectedDate.get(Calendar.YEAR) + " " + selectedDate.get
                (Calendar.MONTH));
        int year = selectedDate.get(Calendar.YEAR);
        int month = selectedDate.get(Calendar.MONTH);
        DoseReport savedReport = reportsManager.hasCachedReport(month + 1, year);
        Date firstDay = getMonthlyStartDate(selectedDate);
        Date lastDay = getMonthlyEndDate(selectedDate);
        setCurrentReportDate(firstDay, lastDay);
        if(savedReport != null) {
            Log.d(TAG, "Saved Report Exists");
            loadReport(savedReport);
        } else {
            Log.d(TAG, "No Saved Report");
            new ReportLoader(ReportType.MONTHLY, dbDateFormat.format(firstDay),
                    dbDateFormat.format(lastDay) + SQLITE_END_CHAR).execute();
        }
    }


    public void loadReport(DoseReport report) {
        Log.d(TAG, "loading DoseReport Object");
        setHeaders(null, getCategories(report.getCategories()));
        adapter = new ReportAdapter(report, schedule);
        setReportAdapter(adapter);
        view.showReport(false);
    }


    public void loadDoses(ReportType reportType, String startDate, String endDate) {
        Log.d(TAG, "loadDoses " + reportType + " start " + startDate + " end " + endDate);
        bins = new ArrayList<>();

        String[] reportingGroups = null;

        try {
            reportingGroups = context.getResources().getStringArray(R.array.reporting_groups);
        } catch (Resources.NotFoundException exception) {
            Log.d(TAG, "Couldn't find reporting group resource");
        }

        String[] categoriesStart = context.getResources().getStringArray(R.array
                .reporting_categories_start);
        String[] categoriesEnd = context.getResources().getStringArray(R.array
                .reporting_categories_end);

        for(int i = 0, categorySize = categoriesStart.length; i < categorySize; i++) {
            bins.add(i, new int[]{Integer.valueOf(categoriesStart[i]), Integer
                    .valueOf(categoriesEnd[i])});
        }

        if(reportingGroups != null && reportingGroups.length > 0) {
            setHeaders(reportingGroups, getCategories(bins));
            Cursor groupCursor = doseDB.doseAggregateReport(startDate, endDate, reportingGroups,
                    categoriesStart, categoriesEnd);

            if (groupCursor != null && groupCursor.moveToFirst()) {
                adapter = new ReportAdapter(groupCursor, reportingGroups, bins, schedule);
                setReportAdapter(adapter);
                view.setReportTotals(adapter.getReportTotals());
            } else {
                Log.d(TAG, "Report Cursor is Empty");
                view.showReport(true);
            }
        } else {
            setHeaders(null, getCategories(bins));
            Cursor reportCursor = doseDB.doseAggregateReport(startDate, endDate, categoriesStart,
                categoriesEnd);

            if (reportCursor != null && reportCursor.moveToFirst()) {
                adapter = new ReportAdapter(reportCursor, bins, schedule);
                setReportAdapter(adapter);
                view.setReportTotals(adapter.getReportTotals());
            } else {
                Log.d(TAG, "Report Cursor is Empty");
                view.showReport(true);
            }
        }
    }

    public int getCalendarYear() {
        return calendar.get(Calendar.YEAR);
    }

    public int getCalendarMonth() {
        return calendar.get(Calendar.MONTH);
    }

    public int getCalendarDay() {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public void setCalendarYear(int year) {
        calendar.set(Calendar.YEAR, year);
    }

    public void setCalendarMonth(int month) {
        calendar.set(Calendar.MONTH, month);
    }

    public void setCalendarDay(int day) {
        calendar.set(Calendar.DAY_OF_MONTH, day);
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar cal) {
        calendar = cal;
    }

    public void setReportAdapter(RecyclerView.Adapter adapter) {
        view.setReportAdapter(adapter);
    }


    public String[] getCategories(List<int[]> cats) {
        String[] categoryHeaders = new String[cats.size()];
        String monthSuffix = getMonthSuffix();
        int index = 0;
        for(int[] ranges : cats) {
            categoryHeaders[index] = getCategoryMonthLabel(ranges[0], ranges[1]) + monthSuffix;
            index += 1;
        }
        return categoryHeaders;
    }

    public void setHeaders(String[] groups, String[] categories) {
        view.setReportHeader(groups, categories);
    }

    private String getCategoryMonthLabel(int start, int end) {
        return ((int)(start / (365 / 12.0))) + " - " + ((int)(end / (365 / 12.0)));
    }

    private String getMonthSuffix() {
        return resources.getString(R.string.month_abbreviation);
    }

    public String getCurrentUser() {
        return AppController.commCareAPI.getLastUser().toUpperCase().replace("_", " ");
    }

    public Locale getLocale() {
        return locale;
    }

    public enum ReportType{
        DAILY,
        WEEKLY,
        MONTHLY
    }

    public ReportType reportForPosition(int position){
        if (position == ReportType.DAILY.ordinal()) {
            return ReportType.DAILY;
        } else if (position == ReportType.WEEKLY.ordinal()) {
            return ReportType.WEEKLY;
        } else if (position == ReportType.MONTHLY.ordinal()) {
            return ReportType.MONTHLY;
        } else {
            return ReportType.DAILY;
        }
    }

    private class ReportLoader extends AsyncTask<Void, Void, Void> {

        ReportType reportType;
        String startDate;
        String endDate;

        ReportLoader(ReportType reportType, String startDate, String endDate) {
            this.reportType = reportType;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        @Override
        protected Void doInBackground(Void... params) {
            loadDoses(this.reportType, this.startDate, this.endDate);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

}
