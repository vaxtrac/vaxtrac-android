package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.BuildConfig;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager.GuardianDBHandler;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager.PatientDBHandler;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.data.MemoryDBHandler;
import com.vaxtrac.android.vaxtracapp.data.TranslationCursor;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;
import com.vaxtrac.android.vaxtracapp.view.CallbacksListView;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CallbacksListPresenter extends Presenter {

    private static final String TAG = "CallbacksListPresenter";

    protected Map<String, String> villageList;
    protected String village;
    private Date startDate;
    private int duration = -1;
    private boolean overdue;
    public Map<String, String> sexMap;
    public static final String[] sexes = new String[] {"", "male", "female"};
    SimpleDateFormat df;
    private static final int overdueCallbackWindow = 180;

    public static Cursor cursor;
    public Cursor parseCursor;
    protected WeakReference<Context> context;
    private WeakReference<CallbacksListView> view;

    private PatientDBHandler patientDB;

    private static final String PATIENT_TABLE_NAME = PatientDBHandler.TABLE_NAME;
    private static final String GUARDIAN_TABLE_NAME = GuardianDBHandler.TABLE_NAME;

    public static final String ID_COLUMN = PatientDBHandler.ID_COLUMM;
    public static final String CALLBACK_DATE_COLUMN = PatientDBHandler.CALLBACK_DATE_COLUMN;
    public static final String VILLAGE_COLUMN = PatientDBHandler.VILLAGE_COLUMN;
    public static final String ADDRESS_COLUMN = PatientDBHandler.ADDRESS_COLUMN;
    public static final String SEX_COLUMN = PatientDBHandler.SEX_COLUMN;
    public static final String DOB_COLUMN = PatientDBHandler.DOB_COLUMN;
    public static final String NAME_COLUMN = PatientDBHandler.NAME_COLUMN;
    public static final String MOBILE_COLUMN = PatientDBHandler.MOBILE_COLUMN;

    private static final String GUARDIAN_NAME_COLUMN = GuardianDBHandler.NAME_COLUMN;
    private static final String CHILD_ID_COLUMN = GuardianDBHandler.CHILD_ID_COLUMN;

    private String username;
    private String userId;

    protected String start;
    protected String end;

    private boolean childNameAsc = true;
    private boolean childDobAsc = true;
    private boolean callbackdateAsc = true;
    private boolean villageNameAsc = true;
    private boolean childSexAsc = true;

    private boolean isNepaliLocale;

    protected static final String ASC = "ASC";
    protected static final String DESC = "DESC";

    public CallbacksListPresenter(Context context, View view) {
        this.context = new WeakReference<Context>(context);
        this.view = new WeakReference<>((CallbacksListView)view);
        patientDB = AppController.mCommCareDataManager.patientDB;
        getUser();
        df = new UnlocalizedDateFormat("yyyy-MM-dd");
        sexMap = new HashMap<>();
        for(String sex: sexes){
            sexMap.put(GlobalTranslations.translate(sex), sex);
        }

        isNepaliLocale = context.getResources().getConfiguration().locale.getISO3Language()
                .equals("nep");
    }

    public void init(boolean overdue, String village, Date
            startDate, int
            duration) {
        this.overdue = overdue;
        this.village = village;
        this.startDate = startDate;
        this.duration = duration;
        this.villageList = AppController.mCommCareDataManager.getAllVillages();
    }

    public void prepareCursor() {
        Calendar calendar = Calendar.getInstance();
        if (!overdue) {
            calendar.setTime(startDate);
            calendar.add(Calendar.DATE, duration);
            start = df.format(startDate);
            end = df.format(calendar.getTime());
        } else {
            calendar.add(Calendar.DATE, -1);
            Date yesterday = calendar.getTime();
            calendar.add(Calendar.DATE, -1 * overdueCallbackWindow);
            start = df.format(calendar.getTime());
            end = df.format(yesterday);
        }

        cursor = getOrderedCursor(village, start, end, CALLBACK_DATE_COLUMN, ASC);
        parseCursor = getOrderedCursor(village, start, end, CALLBACK_DATE_COLUMN, ASC);
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public boolean isOverdue() {
        return overdue;
    }

    public void getUser() {
        SharedPreferences sharedPreferences = AppController.commCareAPI.getCommCareSharedPrefs();
        username = AppController.commCareAPI.getLastUser();
        if(sharedPreferences.contains(CommCareAPI.USERNAME_KEY)) {
            if(sharedPreferences.getString(CommCareAPI.USERNAME_KEY, "").equals(username)) {
                if(sharedPreferences.contains(CommCareAPI.USERID_KEY)) {
                    userId = sharedPreferences.getString(CommCareAPI.USERID_KEY, "");
                }
            }
        } else {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(CommCareAPI.USERNAME_KEY, username);
            userId = AppController.commCareAPI.getUserId();
            editor.putString(CommCareAPI.USERID_KEY, userId);
            editor.apply();
        }
    }

    protected Cursor processCursor(Cursor c, String[] translationColumns) {
        return new TranslationCursor(c, translationColumns, context.get());
    }

    public Cursor getCursor(String villageArg, String start, String finish){
        MemoryDBHandler db = AppController.mCommCareDataManager.patientDB;
        String select = CALLBACK_DATE_COLUMN + " BETWEEN ? AND ? ";
        String and = " AND ";
        String select2 = VILLAGE_COLUMN + " = ? ";
        select = (select + and + select2);
        String[] selectArgs  = new String[] { start, finish, villageArg};
        String orderBy = String.format("%s ASC, %s ASC", CALLBACK_DATE_COLUMN, ID_COLUMN);
        Cursor c = db.query(db.prependRowID(db.getColumnNames()), select, selectArgs, orderBy);
        Log.i(TAG, String.format("%s matching records for query", c.getCount()));
        return processCursor(c, new String[]{SEX_COLUMN, DOB_COLUMN,CALLBACK_DATE_COLUMN});
    }

    public Cursor getCursor(String start, String finish){
        Cursor c = AppController.mCommCareDataManager.patientDB.allRowsItemsBetween(CALLBACK_DATE_COLUMN, start,
                finish, "callbackdate, docid");
        Log.i(TAG, String.format("%s matching records for query", c.getCount()));
        return processCursor(c, new String[]{SEX_COLUMN, DOB_COLUMN,CALLBACK_DATE_COLUMN});
    }

    public Cursor getCursor(){return cursor;}

    public void closeCursor() {
        if(cursor != null) {
            cursor.close();
            cursor = null;
        }

        if(parseCursor != null) {
            parseCursor.close();
            parseCursor = null;
        }
    }

    public Cursor getOrderedCursor(String villageArg, String start, String finish,
                                   String field, String order){
        return getOrderedCursor(villageList.values(), villageList.get(villageArg), start, finish, field, order);
    }

    protected Cursor getOrderedCursor(Collection<String> villageList, String villageArg, String start, String finish,
                                      String field, String order){
        MemoryDBHandler db = AppController.mCommCareDataManager.patientDB;

        StringBuilder query = new StringBuilder("SELECT ");

        String[] columnNames = {getPatientColumnName("rowid as _id"),
                getPatientColumnName(ID_COLUMN, ID_COLUMN),
                getPatientColumnName(NAME_COLUMN, NAME_COLUMN),
                getPatientColumnName(SEX_COLUMN, SEX_COLUMN),
                getPatientColumnName(DOB_COLUMN, DOB_COLUMN),
                getPatientColumnName(CALLBACK_DATE_COLUMN, CALLBACK_DATE_COLUMN),
                getPatientColumnName(MOBILE_COLUMN, MOBILE_COLUMN),
                getPatientColumnName(VILLAGE_COLUMN, VILLAGE_COLUMN),
                getPatientColumnName(ADDRESS_COLUMN, ADDRESS_COLUMN),
                "group_concat(trim(" + getGuardianColumnName(GUARDIAN_NAME_COLUMN) + "), ', ')" +
                        " as guardianname"};

        StringBuilder joinClause = new StringBuilder(" FROM ")
                .append(PATIENT_TABLE_NAME)
                .append(" INNER JOIN ")
                .append(GUARDIAN_TABLE_NAME)
                .append(" ON ")
                .append(getPatientColumnName(ID_COLUMN))
                .append("=")
                .append(getGuardianColumnName(CHILD_ID_COLUMN));

        String[] selectArgs;

        StringBuilder selectClause = new StringBuilder(CALLBACK_DATE_COLUMN)
                .append(" BETWEEN ? AND ? ");
        if (villageArg != null) {
            selectClause.append("AND ")
                    .append(getPatientColumnName(VILLAGE_COLUMN))
                    .append(" = ? ");
            selectArgs = new String[]{start, finish, villageArg};
        }
        else {
            selectClause.append("AND ")
                    .append(getPatientColumnName(VILLAGE_COLUMN))
                    .append(" IN ")
                    .append(createInItems(new ArrayList<String>(villageList)));
            selectArgs = new String[]{start, finish};
        }

        String nameFilter = view.get().getNameFilter();
        if (!nameFilter.equals("")) {
            selectClause.append("AND name LIKE '%")
                    .append(nameFilter)
                    .append("%'");
        }

        String orderBy = String.format("LOWER(%s) %s", getPatientColumnName(field), order);

        for(int i = 0; i < columnNames.length; i++) {
            query.append(columnNames[i]);
            if(i != (columnNames.length - 1)) {
                query.append(",");
            }
        }

        query.append(joinClause)
                .append(" WHERE ")
                .append(selectClause.toString())
                .append(" GROUP BY _id ")
                .append(" ORDER BY ")
                .append(orderBy);

        Log.d(TAG, "callback query " + query.toString() + " args " + Arrays.toString(selectArgs));
        Cursor c = db.rawQuery(query.toString(), selectArgs);

        /*
        Cursor c = db.query(db.prependRowID(db.getColumnNames()), selectClause.toString(),
                selectArgs, orderBy);
        */

        Log.i(TAG, String.format("%s matching records for query", c.getCount()));



        /* Select * from patient inner join guardian on patient.docid = guardian.childid */

        if(BuildConfig.FLAVOR.equals("nepal")) {
            return processCursor(c, new String[]{SEX_COLUMN,
                    DOB_COLUMN, CALLBACK_DATE_COLUMN, VILLAGE_COLUMN});
        } else {
            return processCursor(c, new String[]{SEX_COLUMN});
        }
    }

    private String getPatientColumnName(String columnName) {
        return PATIENT_TABLE_NAME + "." + columnName;
    }

    private String getPatientColumnName(String columnName, String alias) {
        return getPatientColumnName(columnName) + " as " + alias;
    }

    private String getGuardianColumnName(String columnName) {
        return GUARDIAN_TABLE_NAME + "." + columnName;
    }

    private String getGuardianColumnName(String columnName, String alias) {
        return getGuardianColumnName(columnName) + " as " + alias;
    }

    private String createInItems(List<String> items) {
        StringBuilder inItems = new StringBuilder("(");
        for(int i = 0, length = items.size(); i < length; i++) {
            inItems.append("\"")
                    .append(items.get(i))
                    .append("\"");
            if(i != length-1) {
                inItems.append(",");
            } else {
                inItems.append(", \"\")");
            }
        }
        Log.d(TAG, "in village list " + inItems.toString());
        return inItems.toString();
    }

    public void callbackItemSelected(final Cursor item) {
        String docid = item.getString(item.getColumnIndex(ID_COLUMN));

        Map<String, String> info = new HashMap<String, String>(){{
            put("name", item.getString(item.getColumnIndex(NAME_COLUMN)));
            put("nextVisit", item.getString(item.getColumnIndex(CALLBACK_DATE_COLUMN)));
            put("village", item.getString(item.getColumnIndex(VILLAGE_COLUMN)));
            put("mobile", item.getString(item.getColumnIndex(MOBILE_COLUMN)));
        }};
        Screens.CallbackCardScreen callbackCardScreen = new Screens.CallbackCardScreen
                (docid, info);
        AppController.mScreen = callbackCardScreen;
        AppController.getFlow().goTo(callbackCardScreen);
    }

    public Cursor updateCursor(String field){
        String order;
        switch(field) {
            case NAME_COLUMN:
                order = childNameAsc ? ASC : DESC;
                childNameAsc = !childNameAsc;
                childDobAsc = true;
                childSexAsc = true;
                villageNameAsc = true;
                callbackdateAsc = true;
                break;
            case DOB_COLUMN:
                order = childDobAsc ? ASC : DESC;
                childDobAsc = !childDobAsc;
                childNameAsc = true;
                childSexAsc = true;
                villageNameAsc = true;
                callbackdateAsc = true;
                break;
            case SEX_COLUMN:
                order = childSexAsc ? ASC : DESC;
                childSexAsc = !childSexAsc;
                childNameAsc = true;
                childDobAsc = true;
                villageNameAsc = true;
                callbackdateAsc = true;
                break;
            case VILLAGE_COLUMN:
                order = villageNameAsc ? ASC : DESC;
                villageNameAsc = !villageNameAsc;
                childNameAsc = true;
                childDobAsc = true;
                childSexAsc = true;
                callbackdateAsc = true;
                break;
            case CALLBACK_DATE_COLUMN:
                order = callbackdateAsc ? ASC : DESC;
                callbackdateAsc = !callbackdateAsc;
                childNameAsc = true;
                childDobAsc = true;
                childSexAsc = true;
                villageNameAsc = true;
                break;
            default:
                order = ASC;
                break;
        }
            cursor = getOrderedCursor(village, start, end, field, order);
            return cursor;
    }

    protected boolean isChildNameAsc() {
        return childNameAsc;
    }

    protected void setChildNameAsc(boolean childNameAsc) {
        this.childNameAsc = childNameAsc;
    }

    protected boolean isChildDobAsc() {
        return childDobAsc;
    }

    protected void setChildDobAsc(boolean childDobAsc) {
        this.childDobAsc = childDobAsc;
    }

    protected boolean isChildSexAsc() {
        return childSexAsc;
    }

    protected void setChildSexAsc(boolean childSexAsc) {
        this.childSexAsc = childSexAsc;
    }

    protected boolean isVillageNameAsc() {
        return villageNameAsc;
    }

    protected void setVillageNameAsc(boolean villageNameAsc) {
        this.villageNameAsc = villageNameAsc;
    }

    public boolean isCallbackdateAsc() {
        return callbackdateAsc;
    }

    public void setCallbackdateAsc(boolean callbackdateAsc) {
        this.callbackdateAsc = callbackdateAsc;
    }
}

