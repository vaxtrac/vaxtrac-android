package com.vaxtrac.android.vaxtracapp.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.support.percent.PercentRelativeLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.CustomStatisticsPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.StatisticsReportPresenter;
import com.vaxtrac.nepalidatepickerlib.NepaliDateAlertBuilder;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import dateconverter.Date;
import dateconverter.NepaliDateConverter;

public class StatisticsReportView extends FrameLayout {
    private static final String TAG = "StatisticsReportView";

    protected Context context;
    private LayoutInflater layoutInflater;
    private LinearLayout periodLayout;
    private LinearLayout endDateLayout;
    private LinearLayout vialsAddedLayout;
    private PercentRelativeLayout vialDetailsOverlay;
    private LinearLayout vialDetailsContainer;
    private LinearLayout vialDosesLayout;
    private PercentRelativeLayout vialDosesOverlay;
    private LinearLayout vialDosesContainer;
    private Spinner reportPeriod;
    protected TextView startDate;
    protected TextView endDate;
    private TextView dateRange;
    private ImageView previousDate;
    private ImageView nextDate;

    private TextView childrenRegistered;
    private TextView childrenFollowups;
    private TextView vialsAdded;
    private TextView vialsUsed;

    protected StatisticsReportPresenter presenter;

    protected Calendar calendar;
    protected Calendar startCal;
    protected Calendar endCal;
    protected int reportPosition;

    private DatePickerDialog startDatePicker;
    private DatePickerDialog endDatePicker;

    protected Locale locale;
    private NumberFormat numberFormat;
    private DateFormat dateFormat;

    private String[] vaccineTypes;
    private String[] vaccineTypesLocal;

    public StatisticsReportView(Context context) {
        super(context);
        this.context = context;
        presenter = new CustomStatisticsPresenter(context, this);
    }

    public StatisticsReportView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        locale = getResources().getConfiguration().locale;
        presenter = new CustomStatisticsPresenter(context, this);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        calendar = Calendar.getInstance();
        startCal = Calendar.getInstance();
        endCal = Calendar.getInstance();

        vaccineTypes = context.getResources().getStringArray(R.array.antigen_array_en);
        vaccineTypesLocal = context.getResources().getStringArray(R.array.antigen_array);

        dateFormat = DateFormat.getDateInstance();
        numberFormat = NumberFormat.getNumberInstance(locale);

        /* Hide Soft Keyboard Hack */
        ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams
                .SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onFinishInflate() {
        startDate = (TextView) findViewById(R.id.summary_start_date);
        startDate.setText(getStartDateLabel());
        startDatePicker = new DatePickerDialog(context, getOnStartDateSetListener(), startCal.get
                (Calendar.YEAR), startCal.get(Calendar.MONTH), startCal.get(Calendar.DAY_OF_MONTH));
        startDate.setOnClickListener(getStartDateListener());

        endDate = (TextView) findViewById(R.id.summary_end_date);
        endDate.setText(getEndDateLabel());
        endDatePicker = new DatePickerDialog(context, getOnEndDateSetListener(), endCal.get
                (Calendar.YEAR), endCal.get(Calendar.MONTH), endCal.get(Calendar.DAY_OF_MONTH));
        endDate.setOnClickListener(getEndDateListener());

        periodLayout = (LinearLayout) findViewById(R.id.period_container);
        periodLayout.requestFocus();

        endDateLayout = (LinearLayout) findViewById(R.id.end_date_container);
        vialsAddedLayout = (LinearLayout) findViewById(R.id.vials_added_container);
        vialDetailsOverlay = (PercentRelativeLayout) findViewById(R.id.vial_details_overlay);
        vialDetailsContainer = (LinearLayout) findViewById(R.id.vial_details_container);

        vialDosesLayout = (LinearLayout) findViewById(R.id.vial_doses_used_container);
        vialDosesOverlay = (PercentRelativeLayout) findViewById(R.id.vial_doses_overlay);
        vialDosesContainer = (LinearLayout) findViewById(R.id.vial_doses_details_container);

        reportPeriod = (Spinner) findViewById(R.id.summary_period);
        Log.d(TAG, "spinner padding left " + reportPeriod.getPaddingLeft());
        Log.d(TAG, "spinner padding start " + reportPeriod.getPaddingStart());

        ArrayAdapter reportAdapter = new ArrayAdapter<>(context, R.layout
                .period_spinner_dropdown_item, presenter.getReportPeriods());
        reportPeriod.setAdapter(reportAdapter);
        reportPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemSelected parent " + parent.toString() + " view " + view
                        .toString());
                toggleDateElements(position);
                reportPosition = position;
                parent.clearFocus();
                view.clearFocus();
                hideKeyboard();
                if(position == StatisticsReportPresenter.RANGE) {
                    toggleDateControls(INVISIBLE);
                } else {
                    toggleDateControls(VISIBLE);
                }
                presenter.reporPeriodChanged(position, getStartDate(), getEndDate());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        reportPeriod.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch " + v.toString() + " event " + event);
                v.clearFocus();
                hideKeyboard();
                return false;
            }
        });

        dateRange = (TextView) findViewById(R.id.summary_date_range);
        previousDate = (ImageView) findViewById(R.id.previous_date);
        previousDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.getPreviousDate();
            }
        });

        nextDate = (ImageView) findViewById(R.id.next_date);
        nextDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.getNextDate();
            }
        });

        NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
        String none = numberFormat.format(0);
        childrenRegistered = (TextView) findViewById(R.id.children_registered);
        childrenRegistered.setText(none);
        childrenFollowups = (TextView) findViewById(R.id.children_followups);
        childrenFollowups.setText(none);

        vialsAdded = (TextView) findViewById(R.id.vials_added);
        vialsAdded.setText(none);
        vialsAddedLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleVialDetails();
            }
        });
        vialDetailsOverlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleVialDetails();
            }
        });
        vialDosesLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleVialDoseDetails();
            }
        });
        vialDosesOverlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleVialDoseDetails();
            }
        });
        vialsUsed = (TextView) findViewById(R.id.vials_used);
        vialsUsed.setText(none);
        super.onFinishInflate();

    }

    protected String getDateLabel(Calendar cal) {
        return dateFormat.format(cal.getTime());
    }

    public void updateStartDate(Calendar newDate) {
        startCal.setTime(newDate.getTime());
    }

    public void updateEndDate(Calendar newDate) {
        endCal.setTime(newDate.getTime());
    }

    public String getStartDateLabel() {
        return getDateLabel(startCal);
    }

    public String getEndDateLabel() {
        return getDateLabel(endCal);
    }

    public void setStartDateLabel(String label) {
        startDate.setText(label);
    }

    public void setEndDateLabel(String label) {
        endDate.setText(label);
    }

    public void setEndDateLabel(Calendar newDate) {
        endDate.setText(getDateLabel(newDate));
    }

    private void toggleVialDetails() {
        if(vialDetailsOverlay.getVisibility() == VISIBLE) {
            vialDetailsOverlay.setVisibility(GONE);
        } else {
            vialDetailsOverlay.setVisibility(VISIBLE);
        }
    }

    private void toggleVialDoseDetails() {
        if(vialDosesOverlay.getVisibility() == VISIBLE) {
            vialDosesOverlay.setVisibility(GONE);
        } else {
            vialDosesOverlay.setVisibility(VISIBLE);
        }
    }

    private void toggleDateControls(int visibility) {
        previousDate.setVisibility(visibility);
        nextDate.setVisibility(visibility);
    }

    public Calendar getStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startCal.getTime());
        return calendar;
    }

    public Calendar getEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endCal.getTime());
        return calendar;
    }

    public Calendar getPreviousDay(Calendar date) {
        Calendar previousDay = Calendar.getInstance();
        previousDay.setTime(date.getTime());
        previousDay.add(Calendar.DAY_OF_MONTH, -1);
        return previousDay;
    }

    public Calendar getNextDay(Calendar date) {
        Calendar nextDay = Calendar.getInstance();
        nextDay.setTime(date.getTime());
        nextDay.add(Calendar.DAY_OF_MONTH, 1);
        return nextDay;
    }

    protected View.OnClickListener getStartDateListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                startDatePicker.updateDate(startCal.get(Calendar.YEAR), startCal.get(Calendar
                        .MONTH), startCal.get(Calendar.DAY_OF_MONTH));
                startDatePicker.show();
            }
        };
    }

    protected View.OnClickListener getEndDateListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                endDatePicker.updateDate(endCal.get(Calendar.YEAR), endCal.get(Calendar
                        .MONTH), endCal.get(Calendar.DAY_OF_MONTH));
                endDatePicker.show();
            }
        };
    }

    protected DatePickerDialog.OnDateSetListener getOnStartDateSetListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                startCal.set(Calendar.YEAR, year);
                startCal.set(Calendar.MONTH, monthOfYear);
                startCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDate.setText(dateFormat.format(startCal.getTime()));
                presenter.reporPeriodChanged(reportPosition, getStartDate(), getEndDate());
            }
        };
    }

    protected DatePickerDialog.OnDateSetListener getOnEndDateSetListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                endCal.set(Calendar.YEAR, year);
                endCal.set(Calendar.MONTH, monthOfYear);
                endCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                endDate.setText(dateFormat.format(endCal.getTime()));
                presenter.reporPeriodChanged(reportPosition, getStartDate(), getEndDate());
            }
        };
    }

    public void updateRegisteredCount(int count) {
        childrenRegistered.setText(numberFormat.format(count));
    }

    public void updateChildrenVisits(int count) {
        childrenFollowups.setText(numberFormat.format(count));
    }

    public void updateVialCount(int count) {
        vialsAdded.setText(numberFormat.format(count));
    }

    public void updateVialCount(Map<String, Integer> vials) {
        vialDetailsContainer.removeAllViews();
        int length = vials.size();
        int totalVials = 0;
        if(length == 0) {
            vialsAdded.setText(numberFormat.format(0));
            vialsAdded.setPaintFlags(vialsAdded.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
        } else {
            LinearLayout row;
            for (Map.Entry<String, Integer> vial : vials.entrySet()) {
                row = (LinearLayout) layoutInflater.inflate(R.layout
                        .summary_vials_added_row, null);
                TextView vialType = (TextView) row.findViewById(R.id.vaccine_vial_type);
                vialType.setText(translateAntigen(vial.getKey()));
                TextView vialTotal = (TextView) row.findViewById(R.id.total_vial_count);
                vialTotal.setText(numberFormat.format(vial.getValue()));
                totalVials += vial.getValue();
                vialDetailsContainer.addView(row);
            }
            vialsAdded.setPaintFlags(vialsAdded.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            vialsAdded.setText(numberFormat.format(totalVials));
        }
    }

    public void updateVialDoses(Map<String, Integer> vialDoses) {
        vialDosesContainer.removeAllViews();
        int length = vialDoses.size();
        int totalVialDoses = 0;
        if(length == 0) {
            vialsUsed.setText(numberFormat.format(0));
            vialsUsed.setPaintFlags(vialsUsed.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
        } else {
            LinearLayout row;
            for(Map.Entry<String, Integer> vial : vialDoses.entrySet()) {
                row = (LinearLayout) layoutInflater.inflate(R.layout.summary_vial_doses_row, null);
                TextView vialType = (TextView) row.findViewById(R.id.vaccine_vial_type);
                vialType.setText(translateAntigen(vial.getKey()));
                TextView vialTotal = (TextView) row.findViewById(R.id.total_doses_count);
                vialTotal.setText(numberFormat.format(vial.getValue()));
                totalVialDoses += vial.getValue();
                vialDosesContainer.addView(row);
            }
            vialsUsed.setPaintFlags(vialsUsed.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            vialsUsed.setText(numberFormat.format(totalVialDoses));
        }
    }

    private String translateAntigen(String antigen) {
        if(antigen != null) {
            for (int i = 0; i < vaccineTypes.length; i++) {
                if (antigen.equalsIgnoreCase(vaccineTypes[i])) {
                    return vaccineTypesLocal[i];
                }
            }
        } else {
            return "";
        }
        return antigen;
    }

    public void toggleDateElements(int newPeriod) {
        if(newPeriod == StatisticsReportPresenter.RANGE) {
            endDateLayout.setVisibility(VISIBLE);
        } else {
            endDateLayout.setVisibility(INVISIBLE);
        }
    }

    public void updateDateRange(String range) {
        dateRange.setText(range);
    }

    public int getReportPosition() {
        return reportPosition;
    }

    private boolean hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService
                (Context.INPUT_METHOD_SERVICE);
        return imm.hideSoftInputFromWindow(reportPeriod.getWindowToken(), 0);
    }
}
