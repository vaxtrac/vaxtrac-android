package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.data.ChildVisitsTask;
import com.vaxtrac.android.vaxtracapp.data.ChildrenRegisteredTask;
import com.vaxtrac.android.vaxtracapp.data.VialDosesUsedTask;
import com.vaxtrac.android.vaxtracapp.data.VialsCreatedTask;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;
import com.vaxtrac.android.vaxtracapp.view.StatisticsReportView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Map;

import dateconverter.NepaliDateConverter;
import dateconverter.NepaliMonthList;

public class StatisticsReportPresenter extends Presenter {
    private static final String TAG = StatisticsReportPresenter.class.getName();

    private Context context;

    protected StatisticsReportView view;
    private String[] reportPeriods;

    public static final int DAILY = 0;
    public static final int WEEKLY = DAILY + 1;
    public static final int MONTHLY = WEEKLY + 1;
    public static final int RANGE = MONTHLY + 1;

    private ChildrenRegisteredTask registeredTask;
    private VialsCreatedTask vialsCreatedTask;
    private ChildVisitsTask visitsTask;
    private VialDosesUsedTask vialDosesUsedTask;

    private DateFormat dateFormat;

    public StatisticsReportPresenter(Context context, View view) {
        this.context = context;
        this.view = (StatisticsReportView) view;
        reportPeriods = context.getResources().getStringArray(R.array.summary_report_periods);
        dateFormat = DateFormat.getDateInstance();
    }

    public String[] getReportPeriods() {
        return reportPeriods;
    }

    public void reporPeriodChanged(int selection, Calendar startDate, Calendar endDate) {
        Log.d(TAG, "Getting report for " + reportPeriods[selection] + " " + startDate
                .getTime().toString() + " to " + endDate.getTime().toString());
        if(selection == DAILY) {
            loadDailyReport(startDate);
        } else if (selection == WEEKLY) {
            loadWeeklyReport(startDate);
        } else if (selection == MONTHLY) {
            loadMonthlyReport(startDate);
        } else if (selection == RANGE) {
            if(startDate.after(endDate)) {
                updateDateRange(startDate, endDate);
                Toast.makeText(context, "End Date should be after Start Date", Toast.LENGTH_LONG).show();
                return;
            }
            loadRangeReport(startDate, endDate);
        }
    }

    public void loadDailyReport(Calendar date) {
        fetchSummaryStats(getDayStart(date), getDayEnd(date));
    }

    public void loadWeeklyReport(Calendar date) {
        fetchSummaryStats(getFirstDayOfWeek(date), getLastDayOfWeek(date));
    }

    public void loadMonthlyReport(Calendar date) {
        fetchSummaryStats(getFirstDayOfMonth(date), getLastDayOfMonth(date));
    }

    public void loadRangeReport(Calendar start, Calendar end) {
        fetchSummaryStats(setTimeStart(start), setTimeEnd(end));
    }

    public Calendar getDayStart(Calendar date) {
        Calendar cal = (Calendar) date.clone();
        return setTimeStart(cal);
    }

    public Calendar getDayEnd(Calendar date) {
        Calendar cal = (Calendar) date.clone();
        return setTimeEnd(cal);
    }

    public Calendar getFirstDayOfWeek(Calendar date) {
        Calendar cal = (Calendar) date.clone();
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        return setTimeStart(cal);
    }

    public Calendar getLastDayOfWeek(Calendar date) {
        Calendar cal = (Calendar) date.clone();
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        cal.add(Calendar.DAY_OF_MONTH, 6);
        return setTimeEnd(cal);
    }

    protected Calendar getFirstDayOfMonth(Calendar date) {
        Calendar monthStart = Calendar.getInstance();
        monthStart.setTime(date.getTime());
        monthStart.set(Calendar.DAY_OF_MONTH, date.getActualMinimum(Calendar.DAY_OF_MONTH));
        return monthStart;
    }

    protected Calendar getLastDayOfMonth(Calendar date) {
        Calendar monthEnd = Calendar.getInstance();
        monthEnd.setTime(date.getTime());
        monthEnd.set(Calendar.DAY_OF_MONTH, date.getActualMaximum(Calendar.DAY_OF_MONTH));
        return monthEnd;
    }

    public Calendar setTimeEnd(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, date.getActualMaximum(Calendar.HOUR_OF_DAY));
        date.set(Calendar.MINUTE, date.getActualMaximum(Calendar.MINUTE));
        date.set(Calendar.SECOND, date.getActualMaximum(Calendar.SECOND));
        date.set(Calendar.MILLISECOND, date.getActualMaximum(Calendar.MILLISECOND));
        return date;
    }

    public Calendar setTimeStart(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, date.getActualMinimum(Calendar.HOUR_OF_DAY));
        date.set(Calendar.MINUTE, date.getActualMinimum(Calendar.MINUTE));
        date.set(Calendar.SECOND, date.getActualMinimum(Calendar.SECOND));
        date.set(Calendar.MILLISECOND, date.getActualMinimum(Calendar.MILLISECOND));
        return date;
    }

    public Calendar incrementCalendarField(Calendar calendar, int field) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(calendar.getTime());
        cal.add(field, 1);
        return cal;
    }

    public Calendar decrementCalendarField(Calendar calendar, int field) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(calendar.getTime());
        cal.add(field, -1);
        return cal;
    }

    public void getPreviousDate() {
        Calendar currentDate = view.getStartDate();
        switch (view.getReportPosition()) {
            case DAILY:
                Calendar previousDay = getPriorDay(currentDate);
                view.updateStartDate(previousDay);
                view.setStartDateLabel(view.getStartDateLabel());
                reporPeriodChanged(DAILY, previousDay, previousDay);
                break;
            case WEEKLY:
                Calendar previousWeek = getPriorWeek(currentDate);
                view.updateStartDate(previousWeek);
                view.setStartDateLabel(view.getStartDateLabel());
                reporPeriodChanged(WEEKLY, previousWeek, previousWeek);
                break;
            case MONTHLY:
                Calendar previousMonth = getPriorMonth(currentDate);
                view.updateStartDate(previousMonth);
                view.setStartDateLabel(view.getStartDateLabel());
                reporPeriodChanged(MONTHLY, previousMonth, previousMonth);
                break;
            case RANGE:
                break;
            default:
        }
    }

    public void getNextDate() {
        Calendar currentDate = view.getStartDate();
        switch (view.getReportPosition()) {
            case DAILY:
                Calendar nextDay = getNextDay(currentDate);
                view.updateStartDate(nextDay);
                view.setStartDateLabel(view.getStartDateLabel());
                reporPeriodChanged(DAILY, nextDay, nextDay);
                break;
            case WEEKLY:
                Calendar nextWeek = getNextWeek(currentDate);
                view.updateStartDate(nextWeek);
                view.setStartDateLabel(view.getStartDateLabel());
                reporPeriodChanged(WEEKLY, nextWeek, nextWeek);
                break;
            case MONTHLY:
                Calendar nextMonth = getNextMonth(currentDate);
                view.updateStartDate(nextMonth);
                view.setStartDateLabel(view.getStartDateLabel());
                reporPeriodChanged(MONTHLY, nextMonth, nextMonth);
                break;
            case RANGE:
                break;
            default:
        }
    }

    protected Calendar getPriorDay(Calendar currentDay) {
        Calendar priorDay = Calendar.getInstance();
        priorDay.setTime(currentDay.getTime());
        priorDay.add(Calendar.DAY_OF_MONTH, -1);
        return priorDay;
    }

    protected Calendar getNextDay(Calendar currentDay) {
        Calendar nextDay = Calendar.getInstance();
        nextDay.setTime(currentDay.getTime());
        nextDay.add(Calendar.DAY_OF_MONTH, 1);
        return nextDay;
    }

    protected Calendar getPriorWeek(Calendar currentDay) {
        Calendar priorWeek = Calendar.getInstance();
        priorWeek.setTime(currentDay.getTime());
        priorWeek.add(Calendar.WEEK_OF_MONTH, -1);
        return priorWeek;
    }

    protected Calendar getNextWeek(Calendar currentDay) {
        Calendar nextWeek = Calendar.getInstance();
        nextWeek.setTime(currentDay.getTime());
        nextWeek.add(Calendar.WEEK_OF_MONTH, 1);
        return nextWeek;
    }

    protected Calendar getPriorMonth(Calendar currentDay) {
        Calendar priorMonth = Calendar.getInstance();
        priorMonth.setTime(currentDay.getTime());
        priorMonth.add(Calendar.MONTH, -1);
        return priorMonth;
    }

    protected Calendar getNextMonth(Calendar currentDay) {
        Calendar nextMonth = Calendar.getInstance();
        nextMonth.setTime(currentDay.getTime());
        nextMonth.add(Calendar.MONTH, 1);
        return nextMonth;
    }

    public void setChildrenRegistered(int count) {
        view.updateRegisteredCount(count);
    }
    public void setVialsCreated(int count) {view.updateVialCount(count);}
    public void setVialsCreated(Map<String, Integer> vials) {view.updateVialCount(vials);}
    public void setChildVists(int count) {view.updateChildrenVisits(count);}
    public void setVialDosesUsed(Map<String, Integer> vialDoses) {view.updateVialDoses(vialDoses);}

    protected void updateDateRange(Calendar startDate, Calendar endDate) {
        if(view.getReportPosition() == DAILY) {
            view.updateDateRange(dateFormat.format(startDate.getTime()));
        } else {
            view.updateDateRange(dateFormat.format(startDate.getTime()) + " - " +
                    dateFormat.format(endDate.getTime()));
        }
    }

    public void fetchSummaryStats(Calendar startDate, Calendar endDate) {
        updateDateRange(startDate, endDate);
        if(registeredTask != null) {
            registeredTask.cancel(true);
        }
        if(vialsCreatedTask != null) {
            vialsCreatedTask.cancel(true);
        }
        if(visitsTask != null) {
            visitsTask.cancel(true);
        }
        if(vialDosesUsedTask != null) {
            vialDosesUsedTask.cancel(true);
        }

        registeredTask = new ChildrenRegisteredTask(startDate, endDate, getCurrentUser());
        vialsCreatedTask = new VialsCreatedTask(startDate, endDate);
        visitsTask = new ChildVisitsTask(startDate, endDate);
        vialDosesUsedTask = new VialDosesUsedTask(startDate, endDate);

        registeredTask.setTaskListener(new ChildrenRegisteredTask.TaskListener() {

            @Override
            public void onFinished(int count) {
                setChildrenRegistered(count);
            }
        });
        registeredTask.execute();

        vialsCreatedTask.setTaskListener(new VialsCreatedTask.TaskListener() {
            @Override
            public void onFinished(Map<String, Integer> vials) {
                setVialsCreated(vials);

            }
        });
        vialsCreatedTask.execute();


        visitsTask.setTaskListener(new ChildVisitsTask.TaskListener() {

            @Override
            public void onFinished(int count) {
                setChildVists(count);
            }
        });
        visitsTask.execute();

        vialDosesUsedTask.setTaskListener(new VialDosesUsedTask.TaskListener() {
            @Override
            public void onFinished(Map<String, Integer> vialDoses) {
                setVialDosesUsed(vialDoses);
            }
        });
        vialDosesUsedTask.execute();

    }

    private String getCurrentUser() {
        return AppController.commCareAPI.getUserId();
    }
}
