package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by sam on 29/09/2016.
 */

public class VillageMappingCursor  extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    private String TAG = "VillageMappingCursor";
    Set<Integer> mappingColumns = null;
    Map<String, String> villageList = null;
    private WeakReference<Context> context;

    public VillageMappingCursor(Cursor cursor, String[] columnsForMapping, Map<String, String> villageList, Context context) {
        this(cursor, columnsForMapping);
        this.villageList = villageList;
        this.context = new WeakReference<Context>(context);
    }

    public VillageMappingCursor(Cursor cursor, String[] columnsForMapping) {
        super(cursor);
        mappingColumns = new HashSet<Integer>();
        for (String columnName : columnsForMapping) {
            try {
                int i = cursor.getColumnIndex(columnName);
                Log.d(TAG, "Column Name " + columnName + " Index " + i);
                mappingColumns.add(i);
            } catch (Exception e) {/*pass*/}
        }
    }

    @Override
    public String getString(int columnIndex) {
        if (!mappingColumns.contains(columnIndex) || villageList.get(super.getString(columnIndex)) == null){
            try {
                return super.getString(columnIndex);
            } catch (Exception exception) {
                Log.d(TAG, exception.toString() + " ColumnIndex : " + columnIndex);
                return "";
            }
        }
        try {
                return villageList.get(super.getString(columnIndex));
        }
        catch (Exception e){
            Log.i(TAG, String.format("Couldn't map village id | %s",e.toString()));
            return "";
        }
    }
}
