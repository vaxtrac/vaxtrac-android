package com.vaxtrac.android.vaxtracapp.reports;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.data.ReportsDBHelper;
import com.vaxtrac.android.vaxtracapp.models.DoseReport;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ReportsManager {

    private static final String TAG = ReportsManager.class.getName();
    private CommCareDataManager commCareDataManager;
    private Context context;
    private SQLiteDatabase database;
    private ReportsDBHelper helper;

    private Cursor reportsCache;
    private ArrayList<DoseReport> reports;
    private ArrayList<ContentValues> reportCases;
    private Map<Integer, Set<Integer>> reportsByDate;

    private CommCareReportsTask reportTask;
    private boolean reportCaseTaskError;
    private boolean reportTaskStarted;

    public ReportsManager(Context context) {
        this.context = context;
        reports = new ArrayList<>();
        reportsByDate = new TreeMap<>();
        helper = new ReportsDBHelper(context);
        helper.allReports(new ReportsDBHelper.QueryTaskListener() {
            @Override
            public void onTaskCompleted(Cursor cursor) {
                Log.d(TAG, "all reports task finished");
                if(cursor != null) {
                    Log.d(TAG, "all reports size " + cursor.getCount());
                }
                reportsCache = cursor;
                parseReports();
                Log.d(TAG, "all reports parsed");
            }
        });
        reportTask = new CommCareReportsTask();
        commCareDataManager = AppController.mCommCareDataManager;

    }

    public DoseReport getCurrentReport() {
        Calendar today = Calendar.getInstance();
        return getReport(today.get(Calendar.MONTH), today.get(Calendar.YEAR));
    }

    public List<Integer> getReportYears() {
        return new ArrayList<>(reportsByDate.keySet());
    }

    public List<Integer> getReportMonths(Integer year) {
        Log.d(TAG, "report months for year " + year);
        if(reportsByDate.containsKey(year)) {
            return new ArrayList<>(reportsByDate.get(year));
        }
        return new ArrayList<>();
    }

    public void saveCasesToDatabase() {

    }

    public boolean reportTaskFailure() {
        return reportCaseTaskError;
    }

    public void startReportTask() {
        reportTaskStarted = true;
        try {
            reportTask.execute();
        } catch (IllegalStateException exception) {
            Log.e(TAG, "Report Task Already Executed", exception);
        }
    }

    public boolean wasReportTaskStarted() {
        return reportTaskStarted;
    }

    public AsyncTask.Status reportTaskStatus() {
        return reportTask.getStatus();
    }

    public boolean hasCachedReports() {
        return reports != null && reports.size() > 0;
    }

    public boolean hasReportCases() {
        return reportCases != null && !reportCases.isEmpty();
    }

    public DoseReport hasCachedReport(int month, int year) {
        Log.d(TAG, "hasCachedReport " + month + " " + year);
        if(hasCachedReports()) {
            for (DoseReport report : reports) {
                Log.d(TAG, "report month " + report.getMonth() + " year " + report.getYear());
                if(report.getMonth() == month && report.getYear() == year) {
                    return report;
                }
            }
        }
        return null;
    }

    public DoseReport getReport(int month, int year) {
        if(hasCachedReports()) {
            Log.d(TAG, "getting cached report");
            return getCachedReport(month, year);
        } else if (hasReportCases()){
            Log.d(TAG, "getting report case");
            return getReportCase(month, year);
        }
        return null;
    }

    private DoseReport getCachedReport(int month, int year) {
        Log.d(TAG, "cached report size " + reports.size());
        for(DoseReport report: reports) {
            Log.d(TAG, "cached month " + report.getMonth() + " year " + report.getYear());
            if(report.getMonth() == month && report.getYear() == year) {
                return report;
            }
        }
        return null;
    }

    private DoseReport getReportCase(int month, int year) {
        for(ContentValues report : reportCases) {
            for(Map.Entry<String, Object> values : report.valueSet()) {
                Log.d(TAG, "case get key " + values.getKey() + " values " + values.getValue());
            }
            if(report.getAsInteger("month") == month && report.getAsInteger("year") == year) {
                return DoseReport.loadFromCommCare(report);
            }
        }
        return null;
    }

    private ContentValues findInCommCare(int month, int year) {
        return commCareDataManager.findReportCase(DoseReport.MONTHLY_CASE_TYPE, month, year);
    }

    public boolean hasExisitingReport(int month, int year) {
        return helper.hasReport(month, year);
    }

    public void parseReports() {
        if(reportsCache != null) {
            if(reportsCache.moveToFirst()) {
               do {
                   ContentValues contentValues = new ContentValues();
                   DatabaseUtils.cursorRowToContentValues(reportsCache, contentValues);
                   DoseReport doseReport = DoseReport.fromDatabaseValues(contentValues);
                   reports.add(doseReport);
                   Integer yearKey = doseReport.getYear();
                   if(reportsByDate.containsKey(yearKey)) {
                       reportsByDate.get(yearKey).add(doseReport.getMonth());
                   } else {
                       Set<Integer> months = new LinkedHashSet<>();
                       months.add(doseReport.getMonth());
                       reportsByDate.put(yearKey, months);
                   }
               } while (reportsCache.moveToNext());
            } else {
                Log.d(TAG, "Reports Cursor is Empty");
            }
        } else {
            Log.d(TAG, "Reports Cache is Null");
        }
        reportsCache.close();
    }

    public void parseCases() {
    }

    private class CommCareReportsTask extends AsyncTask<Void, Void, Boolean> {

        private Exception exception;

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                reportCases = commCareDataManager.fetchAllReports();
                return true;
            } catch (Exception exception) {
                this.exception = exception;
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if(success) {
                parseCases();
            } else {
                reportCaseTaskError = true;
                Log.d(TAG, "CommCare Reports Task failed with exception ", exception);
            }
        }
    }
}
