package com.vaxtrac.android.vaxtracapp;

import android.Manifest;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.biometrics.BiometricsManager;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.data.CustomVillageFixtureParser;
import com.vaxtrac.android.vaxtracapp.data.ScheduleParserLoader;
import com.vaxtrac.android.vaxtracapp.models.Patient;
import com.vaxtrac.android.vaxtracapp.models.Schedule;
import com.vaxtrac.android.vaxtracapp.models.SearchObject;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.presenter.BiometricSearchPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.DemographicSearchPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.OCRImagePresenter;
import com.vaxtrac.android.vaxtracapp.presenter.Presenter;
import com.vaxtrac.android.vaxtracapp.presenter.VerifyGuardianPresenter;
import com.vaxtrac.android.vaxtracapp.view.ContainerView;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import flow.Backstack;
import flow.Flow;

import static com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager.SCHEDULE_FIXTURE_URL;

public class MainActivity extends Activity implements Flow.Listener, Flow.Callback,
        ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "MainActivity";
    //private static boolean finished = false;
    //public static boolean gettingScans = false;
    public static ResumeState resumeState = null;
    private Flow flow;
    private ContainerView container;

    private static final int SCHEDULE_LOADER_ID = 0;
    private static final int PARSE_SCHEDULES_ID = 1;


    private int PERMISSIONS_REQUEST_CODE = 9340;

    private Schedule schedule;

    //These screens return to CommCare via finish (cancel) on back press
    private final Set commCareReturnScreens = new HashSet<Class>(){
        {
            add(Screens.BiometricSearchScreen.class);
            add(Screens.RegistrationBiometricCheckView.class);
            add(Screens.TabScreen.class);
            add(Screens.DemographicScreen.class);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate " + this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if(savedInstanceState != null) {
            Presenter.setBundle(savedInstanceState.getBundle("presenter"));
        }

        setContentView(R.layout.activity_main);

        container = (ContainerView) findViewById(R.id.container);

        //Default case
        AppController.commCareAPI.setMainActivity(this);

        getSchedule();

        if(savedInstanceState != null && flow == null) {
            //Load Last State Stack
            try {
                flow = new Flow(AppController.getFlow().getBackstack(), this);
                AppController.saveFlow(flow);
                AppController.getFlow().resetTo(flow.getBackstack().current().getScreen());
            }catch (NullPointerException e){
                Log.e(TAG, "Couldn't grab backstack. Making a new one");
                setupFlow();
            }
        } else {
            //New Homescreen Stack
            setupFlow();
        }
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause " + this);
        AppController.mFormSessionManager.saveSession(flow);
        AppController.mPatient = null;
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop " + this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy " + this);
        if (AppController.getMainActivity() == this) {
            AppController.setCurrentActivity(null);
            AppController.commCareAPI.setMainActivity(null);
            AppController.saveFlow(null);
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Log.d(TAG, "onResume " + this);
        if(isResumeState(ResumeState.GETTING_SCANS)){
            Log.i(TAG, "Returned from getting Scans, ignoring dispatches, etc...");
            super.onResume();
            return;
        }
        if(getIntent().getExtras() != null) {
            loadSession();
            if (AppController.mFormSessionManager.alreadySynced()) {
                Log.i(TAG, "Not returning from finished.");
                AppController.commCareAPI.setMainActivity(this);
                super.onResume();
                return;

            } else {
                Log.i(TAG, "Dispatch onResume.");
                try{
                    List<String> intentParts = Arrays.asList(getIntent().getAction().split("\\."));
                    Action action = Action.valueOf(intentParts.get(intentParts.size()-1));
                    dispatchAction(action);
                    //We don't mess with the old states if we get an actionable intent
                    super.onResume();
                    return;
                }catch (IllegalArgumentException e){
                    Log.i(TAG, String.format("Couldn't dispatch Action | %s", getIntent().getAction()));
                }
            }
        }
        if (isResumeState(ResumeState.FINISHED)) {
            Log.e(TAG, "Coming back from finished!");
            AppController.setCurrentActivity(this);
            setupFlow();
            //Log.i(TAG, "Loading last patient since there's no session");
            //AppController.mPatient = AppController.mFormSessionManager.getLastPatient(); //TODO KILL

            AppController.commCareAPI.setMainActivity(this);
            AppController.getFlow().goTo(new Screens.DemographicScreen());
            super.onResume();
            return;
        }
        if(!AppController.commCareAPI.isAuthenticated()) {
            if (!((Screen) flow.getBackstack().current().getScreen()).getName().equals(Screens
                    .LoginScreen.class.getName())) {
                Log.d(TAG, "Not Authenticated forwarding to LoginScreen");
                AppController.getFlow().goTo(new Screens.LoginScreen());
                if(AppController.commCareAPI.hasKeys()) {
                    if(!hasPermissions()) {
                        requestPermissions();
                    }
                }
            }
        }else{
            AppController.commCareAPI.setMainActivity(this);
        }

        if(isResumeState(ResumeState.FAILED_SCAN)){
            AppController.setCurrentActivity(this);
            setupFlow();
            super.onResume();
            return;
        }
        super.onResume();
   }

    @Override
    public void onNewIntent(Intent intent) {
        Log.i(TAG, "onNewIntent " + intent);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBundle("presenter", Presenter.getBundle());
        if(resumeState != null) {
            savedInstanceState.putInt("resumeState", resumeState.ordinal() + 1);
        }
        Log.d(TAG, "onSaveInstanceState flow " + flow.getBackstack());
        AppController.saveFlow(flow);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    public Flow getFlow() {
        if(flow == null) {
            Log.d(TAG, "Activity flow is null");
            setupFlow();
        }
        return flow;
    }

    private void setupFlow() {
        Log.d(TAG, "Setup Flow");
        //Backstack backstack = Backstack.fromUpChain(new HomeScreen());
        Backstack backstack = Backstack.fromUpChain(new Screens.HomeScreen());
        flow = new Flow(backstack, this);
        AppController.saveFlow(flow);
        AppController.getFlow().replaceTo(flow.getBackstack().current().getScreen());
    }

    @Override
    public void go(Backstack nextBackstack, Flow.Direction direction, Flow.Callback callback) {
        Screen screen = (Screen) nextBackstack.current().getScreen();
        Log.d(TAG, "Go Flow " + screen.toString());
        Log.d(TAG, "Flow Backstack " + nextBackstack.toString());
        AppController.mScreen = screen;
        container.showScreen(screen);
        callback.onComplete();
    }

    public void switchScreen(Screen screen) {
        flow.goTo(screen);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged " + newConfig.toString());
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                flow.goUp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(Build.VERSION.SDK_INT >= 23) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            Log.d(TAG, "onRequestPermissionResult " + requestCode + " " + Arrays.toString(permissions)
                    + " " + Arrays.toString(grantResults));
        }
    }

    @Override
    public void onBackPressed() {
        String currentScreenName = ((Screen)flow.getBackstack().current().getScreen()).getName();
        Log.d(TAG, "onBackPressed current screen " + currentScreenName);
        if (currentScreenName.equals(Screens.HomeScreen.class.getName()) && flow.getBackstack().size() == 1) {
            // ignore at Home Screen root
        } else if (currentScreenName.equals(Screens.LoginScreen.class.getName())) {
            // could circumvent login if homescreen is in backstack
            return;
        }

        Iterator iter = flow.getBackstack().iterator();
        Backstack.Entry e = (Backstack.Entry) iter.next();
        Class cls = e.getScreen().getClass();
        if(commCareReturnScreens.contains(cls)){
            Log.i(TAG, String.format("%s backs into CommCare.",cls.toString()));
            //setResult(RESULT_CANCELED, new Intent());
            //finish();
            //backToCommCare();
            failToCommCare();
            return;
        }else{
            Log.i(TAG, String.format("%s does not back into CommCare.",cls.toString()));
        }

        flow.goBack();

        Log.i(TAG, "Going Backward");

    }

    public boolean hasPermissions() {
        return ContextCompat.checkSelfPermission(this, "org.commcare.dalvik.provider.cases.read")
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{"org.commcare.dalvik.provider.cases.read", android.Manifest
                        .permission.WRITE_EXTERNAL_STORAGE},
                PERMISSIONS_REQUEST_CODE);
    }

    private boolean loadSession(){

        try{
            String sessionID = getIntent().getExtras().getString("sessionID");
            if (sessionID != null) {
                return AppController.mFormSessionManager.loadSession(sessionID);
            }else{
                Log.i(TAG, "No previous session for: " + sessionID);
                return false;

            }
        }catch (Exception e){
            Log.e(TAG, "Couldn't get sessionID from bundle");
            return false;
        }
    }

    private void dispatchAction(Action action){
        Log.i(TAG, "DispatchAction " + action.toString());
        String actionString = getIntent().getAction();
        String patientId;

        if(loadSession()){
            Log.d(TAG, "Loading Session");
            AppController.mPatient = AppController.mFormSessionManager.getSessionPatient();
            if(AppController.mFormSessionManager.getSessionScreen() != null) {
                Screen sessionScreen = AppController.mFormSessionManager.getSessionScreen();
                if(sessionScreen != null) {
                    AppController.getFlow().replaceTo(sessionScreen);
                } else {
                    Log.d(TAG, "Session Screen is null");
                }
            }
            return;
        }

        Log.i(TAG, String.format("Caught Action %s", action.toString()));
        switch(action){
            case BIOMETRICSEARCH:
                AppController.mSearchObject = new SearchObject(getIntent().getExtras());
                AppController.getFlow().replaceTo(new Screens.BiometricSearchScreen());
                break;
            case BIOMETRICSCAN:
                BiometricsManager.getScans(this, BiometricsManager.BIOMETRAC_SCAN_REQUEST_CODE);
                break;
            case TEXTSEARCH:
                DemographicSearchPresenter.setSearchTerms(getIntent().getExtras());
                clearIntentExtras();
                AppController.getFlow().replaceTo(new Screens.DemographicSearchScreen());
                break;
            case OPENPATIENT:
                patientId = getIntent().getExtras().getString("patient_id");
                if(patientId != null) {
                    Map<String,String> info = AppController.mCommCareDataManager
                            .loadCaseFromCommCare(patientId);
                    if (info == null){
                        Cursor c = AppController.mCommCareDataManager.patientDB
                                .allRowsColumnEquals("docid", patientId);
                        if (c != null && c.getCount() != 0){
                            Toast.makeText(this, "Found local but not CC!",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(this, "NOT FOUND IN EITHER DB!",
                                    Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }
                    if(schedule == null) {
                        Log.d(TAG, "Open Patient Schedule is null");
                    }
                    AppController.mPatient = new Patient(schedule);
                    AppController.mPatient.loadMap(patientId, info);
                    AppController.getFlow().replaceTo(new Screens.TabScreen(0));
                }
                break;
            case OPENPATIENTVERIFY:
                // clear patient just in case...
                AppController.mPatient = new Patient(schedule);
                patientId = getIntent().getExtras().getString("patient_id");
                if(patientId != null) {
                    Map<String,String> info = AppController.mCommCareDataManager
                            .loadCaseFromCommCare(patientId);
                    if (info == null){
                        Cursor c = AppController.mCommCareDataManager.patientDB
                                .allRowsColumnEquals("docid", patientId);
                        if (c != null && c.getCount() !=0){
                            Toast.makeText(this, "Found local but not CC!",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(this, "NOT FOUND IN EITHER DB!",
                                    Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }
                    VerifyGuardianPresenter.setChildID(patientId);
                    AppController.getFlow().replaceTo(new Screens.VerifyGuardianScreen());
                }
                break;
            case REGISTER:
                AppController.mSearchObject = new SearchObject(getIntent().getExtras());
                AppController.getFlow().replaceTo(new Screens.RegistrationBiometricCheckView());
                break;
            case OCR:
                break;
            case CCSYNC:
                AppController.commCareAPI.sync();
                break;
            default:
                Log.e(TAG, String.format("Unknown action %s not handled", actionString));
                break;
        }
    }

    private void clearIntentExtras() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            for(String key : getIntent().getExtras().keySet()) {
                getIntent().removeExtra(key);
            }
        }
    }

    @Override
    public void onComplete() {
        Log.d(TAG, "Flow Callback Complete");
    }

    public Schedule getSchedule() {
        if(schedule == null) {
            Log.d(TAG, "Activity Local Schedule is null");
            schedule = Schedule.deserializeSchedules();
        }
        return schedule;
    }

    public void startScheduleLoader() {
        /* Asyncronously load schedules from fixture or SharedPreferences cache */
        if(Schedule.savedScheduleVersion() != -1 && Preferences.isScheduleSynced()) {
            Log.d(TAG, "schedule already synced");
            schedule = Schedule.deserializeSchedules();
        } else {
            Log.d(TAG, "load schedule from fixture");
            getLoaderManager().restartLoader(SCHEDULE_LOADER_ID, null, scheduleFixtureLoader);
        }
    }



    private LoaderManager.LoaderCallbacks<Cursor> scheduleFixtureLoader = new LoaderManager
            .LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            Log.d(TAG, "ScheduleFixtureLoader onCreateLoader");
            switch(id) {
                case SCHEDULE_LOADER_ID:
                    return new CursorLoader(
                            MainActivity.this,
                            Uri.parse(SCHEDULE_FIXTURE_URL),
                            null,
                            null,
                            null,
                            null
                    );
                default:
                    return null;
            }
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            Log.d(TAG, "ScheduleFixtureLoader onLoadFinished");
            switch(loader.getId()) {
                case SCHEDULE_LOADER_ID:
                    if(data != null && data.moveToFirst()) {
                        int contentColumn = data.getColumnIndex(CommCareDataManager.FIXTURE_CONTENT_COLUMN);
                        Bundle scheduleBundle = new Bundle();
                        scheduleBundle.putString("scheduleXml", data.getString(contentColumn));
                        getLoaderManager().initLoader(PARSE_SCHEDULES_ID, scheduleBundle,
                                scheduleXmlParser).forceLoad();
                    } else {
                        Log.d(TAG, "No Schedule Fixture");
                        startScheduleLoader();
                    }
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };


    private LoaderManager.LoaderCallbacks<Schedule> scheduleXmlParser = new
            LoaderManager.LoaderCallbacks<Schedule>() {

        @Override
        public Loader<Schedule> onCreateLoader(int id, Bundle args) {
            Log.d(TAG, "ScheduleXmlParser onCreateLoader");
            switch(id) {
                case PARSE_SCHEDULES_ID:
                    return new ScheduleParserLoader(MainActivity.this, args.getString
                            ("scheduleXml"));
                default:
                    return null;
            }
        }

        @Override
        public void onLoadFinished(Loader<Schedule> loader, Schedule data) {
            Log.d(TAG, "ScheduleXmlParser onLoadFinished");
            switch(loader.getId()) {
                case PARSE_SCHEDULES_ID:
                    Log.d(TAG, "data version " + data.getVersion() + " saved version " + Schedule
                            .savedScheduleVersion());
                    if(data.getVersion() > Schedule.savedScheduleVersion()) {
                        schedule = data;
                        schedule.persist();
                    } else {
                        schedule = Schedule.deserializeSchedules();

                    }
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences
                            (getApplicationContext()).edit();
                    Calendar syncDate = Calendar.getInstance();
                    syncDate.set(Calendar.HOUR, 0);
                    syncDate.set(Calendar.MINUTE, 0);
                    syncDate.set(Calendar.SECOND, 0);
                    syncDate.set(Calendar.MILLISECOND, 0);
                    editor.putLong(Preferences.PREF_SCHEDULE_LAST_UPDATE, syncDate.getTimeInMillis());
                    editor.apply();
            }
        }


        @Override
        public void onLoaderReset(Loader loader) {
            Log.d(TAG, "ScheduleXmlParser onLoaderReset");
        }
    };

    private enum Action{
        BIOMETRICSEARCH,
        BIOMETRICSCAN,
        TEXTSEARCH,
        OPENPATIENT,
        OPENPATIENTVERIFY,
        REGISTER,
        OCR,
        CCSYNC
    }

    public void returnToCommCare(Bundle bundle){
        resumeState = ResumeState.FINISHED;
        //finished = true;
        Bundle b = bundle;
        b.putString("finished_session", "true");
        Intent intent = new Intent();
        /* magic CommCare intent key enables auto-advance */
        intent.putExtra("odk_intent_data", "complete");
        intent.putExtra("odk_intent_bundle", b);
        AppController.mFormSessionManager.saveSession(flow);
        setResult(RESULT_OK, intent);
        Log.d(TAG, "Finishing Activity " + this);
        finish();
        return;
    }

    public void failToCommCare(){
        AppController.commCareAPI.setMainActivity(this);
        resumeState = ResumeState.FAILED_SCAN;
        backToCommCare(RESULT_CANCELED);
    }

    public void backToCommCare(int result){
        Bundle b = new Bundle();
        Intent intent = new Intent();
        intent.putExtra("odk_intent_bundle" , b);
        intent.putExtra("odk_intent_data", "incomplete");
        setResult(result, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Activity onActivityResult");
        switch(requestCode) {
            case CommCareAPI.KEY_REQUEST_CODE:
                AppController.commCareAPI.handleResult(resultCode, data);
                if (!hasPermissions()) {
                    requestPermissions();
                }
                break;
            case BiometricSearchPresenter.BIOMETRIC_SEARCH_SCAN_REQUEST_CODE:
                Log.i(TAG, "Caught results from biometric scan");
                resumeState = ResumeState.GETTING_SCANS;
                BiometricSearchPresenter.registerScanResult(resultCode, data);
                break;
            case VerifyGuardianPresenter.VERIFY_GUARDIAN_SCAN_CODE:
                Log.i(TAG, "Caught results from verify guardian scan");
                VerifyGuardianPresenter.registerScanResult(resultCode, data);
                break;
            case VerifyGuardianPresenter.VERIFY_GUARDIAN_REGISTER_NEW_CODE:
                if (resultCode == RESULT_OK) {
                    VerifyGuardianPresenter.guardianRegistrationOK();
                } else {
                    VerifyGuardianPresenter.guardianRegistrationFailed();
                }
                break;
            case BiometricsManager.BIOMETRAC_SCAN_REQUEST_CODE:
                returnToCommCare(data.getExtras());
                break;
            case OCRImagePresenter.OCR_CODE:
                Log.i(TAG, "Caught results from OCR Capture");
                OCRImagePresenter.ocrResult(resultCode, data);
                break;
        }
    }

    private void logData(Intent intent){
        Log.d(TAG, "Logging incoming intent from result");
        try {
            Bundle b = intent.getExtras();
            for(String key : b.keySet()) {
                Object value = b.get(key);
                try {
                    Log.d(TAG, String.format(" %s | %s", key, value.toString()));
                } catch (Exception exception) {
                        Bundle odkIntentBundle = (Bundle) value;
                        for (String bundleKey : odkIntentBundle.keySet()) {
                            Log.d(TAG, String.format("odkintentbundle %s | %s ", bundleKey,
                                    odkIntentBundle.getString(bundleKey)));
                        }
                }
            }
            Log.d(TAG, "action " + intent.getAction());
            Log.d(TAG, "data uri " + intent.getData());
            int flags = intent.getFlags();
            if((flags & Intent.FLAG_ACTIVITY_CLEAR_TASK) == Intent.FLAG_ACTIVITY_CLEAR_TASK) {
                Log.d(TAG, "Flag Activity Clear Task");
            }
            if((flags & Intent.FLAG_ACTIVITY_NEW_TASK) == Intent.FLAG_ACTIVITY_NEW_TASK) {
                Log.d(TAG, "Flag Activity New Task");
            }
            if((flags & Intent.FLAG_ACTIVITY_CLEAR_TOP) == Intent.FLAG_ACTIVITY_CLEAR_TOP) {
                Log.d(TAG, "Flag Activity Clear Top");
            }
            if((flags & Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED) == Intent
                    .FLAG_ACTIVITY_RESET_TASK_IF_NEEDED) {
                Log.d(TAG, "Flag Activity Reset Task If Needed");
            }
            if ((flags & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == Intent
                    .FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) {
                Log.d(TAG, "Flag Activity Launched From History");
            }
        } catch (NullPointerException e){Log.e(TAG, "Can't log incoming intent, null pointer.");}
    }

    public boolean isResumeState(ResumeState state){
        if (state == null || resumeState == null){
            return false;
        }
        if (state.equals(resumeState)){
            return true;
        }
        return false;
    }

    public enum ResumeState{
        FINISHED,
        GETTING_SCANS,
        FAILED_SCAN,
        START;
    }
}



