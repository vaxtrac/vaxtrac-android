package com.vaxtrac.android.vaxtracapp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;

public class CommcareReceiver extends BroadcastReceiver {

    private final String TAG = "CommcareReceiver";

    private CommCareAPI commCareAPI;
    private static final String UPDATE_BROADCAST = "org.commcare.dalvik.api.action.data.update";
    private static final String LOGIN_BROADCAST = "org.commcare.dalvik.api.action.session.login";
    private static final String LOGOUT_BROADCAST = "org.commcare.dalvik.api.action.session.logout";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Received Commcare Broadcast " + intent.getAction());

        switch(intent.getAction()) {
            case LOGIN_BROADCAST:
                Log.d(TAG, "Caught Login Broadcast");
                AppController.notifyLoginBroadcast(intent);
                AppController.notify("Logged In");
                AppController.syncCommCare();
                break;
            case LOGOUT_BROADCAST:
                Log.d(TAG, "Received Logout Broadcast");
                AppController.notifyLogoutBroadcast(intent);
                break;
            case UPDATE_BROADCAST:
                Log.d(TAG, "Received Update Broadcast");
                AppController.lastSyncStamp = System.currentTimeMillis();
                try {
                    AppController.notify("CommCare Sync Detected");
                    AppController.syncCommCare();
                }catch (Exception e){
                    Log.i(TAG, "Caught Broadcast | Couldn't send message.");
                }
                break;
        }

    }

}