package com.vaxtrac.android.vaxtracapp.biometrics;

import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import bmtafis.simple.Finger;
import bmtafis.simple.Fingerprint;
import bmtafis.simple.Person;

/**
 * Created by sarwar on 12/23/14.
 */

public class SearchCache {

    public List<Person> candidates = null;
    public Map<Integer,Person> candidates_map = null;
    public Map<String, Integer> int_alias = null;
    public Map<Integer, String> string_alias = null;
    private AtomicInteger _id_generator = null;

    protected ExecutorService executor;
    protected ArrayBlockingQueue<PrePerson> queue;
    protected List<TemplateExpander> expanderThreads;

    protected int THREAD_POOL_SIZE;

    public boolean cacheBuilt = false;

    private static final String TAG = "SearchCache";

    public SearchCache(){
        this(12);
    }

    public SearchCache(int threadPoolSize){
        this.THREAD_POOL_SIZE = threadPoolSize;
        candidates = new ArrayList<Person>();
        candidates_map = new HashMap<Integer, Person>();

        int_alias = new HashMap<String, Integer>();
        string_alias = new HashMap<Integer, String>();
        _id_generator = new AtomicInteger();
        executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        expanderThreads = new ArrayList<TemplateExpander>();
        for(int i =0; i<THREAD_POOL_SIZE; i++){
            expanderThreads.add(new TemplateExpander(this));
        }
        for(int i =0; i<THREAD_POOL_SIZE; i++){
            executor.submit(expanderThreads.get(i));
        }

    }

    public void clearCache(){
        Log.i(TAG, "Clearing Cache");
        candidates = new ArrayList<Person>();
        candidates_map = new HashMap<Integer, Person>();

        int_alias = new HashMap<String, Integer>();
        string_alias = new HashMap<Integer, String>();
        _id_generator = new AtomicInteger();
    }

    /*
    Loads a Cursor into this SearchCache
    cursorMap is k:v of fingername in form "hand_finger" : name of corresponding column in cursor
    uuidColumnName is the column which houses the patientID we're going to report out
    !!uuidColumn must contain strings.
     */
    public void loadCache(Cursor cursor, String uuidColumnName, Map<String,String> cursorMap){
        //Expander threads wake on feed.
        cacheBuilt = false;
        queue = new ArrayBlockingQueue<PrePerson>(cursor.getCount());
        Map<String, Integer> fingerPositions = new HashMap<String,Integer>();
        int uuid_position;
        try {
            uuid_position = cursor.getColumnIndex(uuidColumnName);
            Log.d(TAG, String.format("Found UUID cursor @%s", uuid_position));
        }catch(CursorIndexOutOfBoundsException e){
            Log.e(TAG, "uuidcolumn not found");
            /*
            Log.d(TAG, "Valid Columns...");
            List<String> columnNames = new ArrayList<String>(Arrays.asList(cursor.getColumnNames()));
            for (String columnName: columnNames){
                Log.d(TAG, columnName);
            }
            */
            throw new NullPointerException();
        }
        for(String fingerName: cursorMap.keySet()){
            int index = cursor.getColumnIndex(fingerName);
            //Log.d(TAG, String.format("%s : %s", fingerName, index));
            fingerPositions.put(fingerName,index);
        }
        while(cursor.moveToNext()){
            Map<String,String> templates = new HashMap<>();
            for(String fingerName: cursorMap.keySet()){
                String template = cursor.getString(fingerPositions.get(fingerName));
                if (template!= null){
                    templates.put(cursorMap.get(fingerName), cursor.getString(fingerPositions.get(fingerName)));
                    //Log.d(TAG,String.format("%s | %s", cursorMap.get(fingerName), cursor.getString(fingerPositions.get(fingerName))));
                }
            }
            if(templates.isEmpty()){
                Log.d(TAG, String.format("%s omitted from queue | empty", cursor.getString(uuid_position)));
            }else{
                Log.d(TAG, String.format("Pushed %s into queue", cursor.getString(uuid_position)));
                queue.add(new PrePerson(cursor.getString(uuid_position), templates));
            }

        }
        //waking up threads
        wakeUpThreads();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!queue.isEmpty()){
                    try {
                        Log.d(TAG, "Waiting for Queue completion...");
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Log.i(TAG,"Finished with Expansion Queue");
                cacheBuilt = true;
            }
        });
        t.start();
    }

    private void wakeUpThreads(){
        for(TemplateExpander e: expanderThreads){
            e.sleeping=false;
        }
    }

    public void addCandidateToCache(String uuid_in, Map<String, String> templates){

        int _ref_id = _id_generator.getAndIncrement();
        int_alias.put(uuid_in, _ref_id);
        string_alias.put(_ref_id, uuid_in);
        Person p = mapToPerson(_ref_id, templates);
        if (p==null){
            Log.e(TAG, String.format("Person %s had no valid templates | not added!",_ref_id));
            return;
        }
        Log.i(TAG, "Adding uuid:" + uuid_in + " as _id " + Integer.toString(_ref_id));
        candidates.add(p);
        candidates_map.put(_ref_id, p);
    }

    public static Person mapToPerson(Integer uuid, Map<String, String> templates){
        Log.d(TAG, String.format("Mapping new person %s", uuid));
        Person p = new Person();
        p.setId(uuid);
        List prints = new ArrayList<Fingerprint>();
        List<String> fingers = new ArrayList<String>(){{
            add("left_thumb");
            add("right_thumb");
            add("left_index");
            add("right_index");
        }};
        if (templates != null){
            int count = 0;
            for (String finger:fingers){
                String temp = templates.get(finger);
                if (temp != null){
                    Fingerprint f = new Fingerprint();
                    try {
                        f.setIsoTemplate(hexStringToByteArray(temp));
                    }catch(RuntimeException e){
                        //Log.e(TAG, String.format("%s failed with %s", finger, e.getMessage()));
                        continue;
                    }
                    Finger fingerType = Finger.valueOf(finger.toUpperCase());
                    //Log.i(TAG, String.format("Adding finger %s to person %s",fingerType.toString(),uuid));
                    f.setFinger(fingerType);
                    count +=1;
                    prints.add(f);
                }else{
                    Log.i(TAG, String.format("Templates were null for %s!", uuid));
                }
            }
            //No valid fingerprints created
            if (count ==0){
                Log.d(TAG, String.format("%s didn't have any useful fingerprints | ignoring", uuid));
                return null;
            }
            p.setFingerprints(prints);
            return p;
        }
        return null;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    /*
    Little wrapper class to pass people through the queue for expansion
     */
    private class PrePerson{
        String uuid;
        Map<String,String> templates;

        private PrePerson(String uuid, Map<String,String> templates){
            this.uuid = uuid;
            this.templates = templates;
        }
    }

    /*
    MultiThread-able Template Expansion Thread for Speed.
    [May need to reduce GC use to see speedup]
     */
    private class TemplateExpander implements Runnable{

        boolean running = true;
        boolean sleeping;
        SearchCache cache = null;

        private TemplateExpander(SearchCache cache){
            this.cache = cache;
            sleeping = true;

        }

        @Override
        public void run() {
            Log.i(TAG, "Start CacheThread");
            while(running){
                if (!sleeping){
                    Log.i(TAG, "Thread woke up for work...");
                }
                while(!sleeping){
                    try{
                        PrePerson pPerson = queue.poll();
                        if (pPerson == null){
                            Log.i(TAG, "Thread going to sleep!");
                            sleeping = true;
                            break;
                        }else{
                            cache.addCandidateToCache(pPerson.uuid, pPerson.templates);
                        }
                    }catch (Exception e){
                        Log.e(TAG, e.toString());
                        e.printStackTrace();
                        throw new NullPointerException();
                        /*
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        */
                    }
                }
                if (sleeping){
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            Log.i(TAG, "Killed Matcher");
       }
    }
}
