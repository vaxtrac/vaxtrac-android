package com.vaxtrac.android.vaxtracapp.commcare;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.BuildConfig;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.CustomVillageFixtureParser;
import com.vaxtrac.android.vaxtracapp.data.LocalDBHelper;
import com.vaxtrac.android.vaxtracapp.data.MemoryDBHandler;
import com.vaxtrac.android.vaxtracapp.data.MemoryDBHelper;
import com.vaxtrac.android.vaxtracapp.data.ReportsDBHelper;
import com.vaxtrac.android.vaxtracapp.models.CommCareCase;
import com.vaxtrac.android.vaxtracapp.models.DoseReport;
import com.vaxtrac.android.vaxtracapp.ocr.OCRManager;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CommCareDataManager {

    protected SQLiteOpenHelper mDBHelper;
    public GuardianDBHandler guardianDB;
    public PatientDBHandler patientDB;
    public DoseDBHandler doseDB;
    public ReportsDBHelper reportsDB;
    public OCRManager ocrManager;

    private Map<String, String> allVillages;
    private List<String> currentUserVillages;
    private String villageOwner;
    private Locale locale;

    protected Context mContext;
    private final String TAG = "CommCareDataManager";
    private final String CASE_DATA = "content://org.commcare.dalvik.case/casedb/data/";
    private static final String CASE_METADATA = "content://org.commcare.dalvik.case/casedb/case/";

    public static final String KEY_COLUMN = "datum_id";
    public static final String DATA_COLUMN = "value";
    public static final String CASE_ID_COLUMN = "case_id";

    private static final String FIXTURE_DATA = "content://org.commcare.dalvik.fixture/fixturedb/";
    public static final String FIXTURE_CONTENT_COLUMN = "content";
    public static final String GROUPS_FIXTURE_ID = "user-groups";
    public static final String SCHEDULE_FIXTURE_ID = "item-list:immunization_schedule";
    public static final String SCHEDULE_FIXTURE_URL = FIXTURE_DATA + SCHEDULE_FIXTURE_ID;
    public static final String COMMCARE_USER_CASE = "commcare-user";
    public static final String COMMCARE_HQ_ID = "hq_user_id";

    private static final DateFormat caseDateFormat =
            new UnlocalizedDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    private static final DateFormat dateFormat =
            new UnlocalizedDateFormat("yyyy-MM-dd");
    private static final DateFormat isoDateFormat =
            new UnlocalizedDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");

    public CommCareDataManager(Context context){
        mContext = context;
        mDBHelper = new MemoryDBHelper(mContext);
        guardianDB = new GuardianDBHandler();
        patientDB = new PatientDBHandler();
        doseDB = new DoseDBHandler();
        reportsDB = new ReportsDBHelper(context);
        ocrManager = new OCRManager(context);
    }

    public CommCareDataManager(Context context, boolean makeLocalDatabase){
        if (makeLocalDatabase){
            mContext = context;
            if(!makeLocalDatabase){
                mDBHelper = new MemoryDBHelper(mContext);
            }else{
                mDBHelper = new LocalDBHelper(mContext);
            }
            guardianDB = new GuardianDBHandler();
            patientDB = new PatientDBHandler();
            doseDB = new DoseDBHandler();
            reportsDB = new ReportsDBHelper(context);
            ocrManager = new OCRManager(context);
        }
    }

    public void addCase(String caseId, String caseType, String dateCreated, String dateModified){
        //TODO USE OR LOSE
        updateCase(caseId, caseType, dateCreated, dateModified);
    }

    public void updateCase(String caseId, String caseType, String dateOpened, String dateModified){
        try {
            Log.d(TAG, String.format("update case %s %s %s %s", caseId, caseType, dateOpened
                    , dateModified));
            /* Enums can't have hypens */
            switch (CaseType.valueOf(caseType.replace("-", ""))) {
                case guardian:
                    commitCaseToDB(guardianDB, caseId, dateOpened, dateModified);
                    break;
                case patient:
                    commitCaseToDB(patientDB, caseId, dateOpened, dateModified);
                    break;
                case monthlyreport:
                    insertOrUpdateReportCase(new CommCareCase(caseId));
                    break;
                case report:
                    insertOrUpdateReportCase(new CommCareCase(caseId));
                    break;
                case vial:
                    insertOrUpdateVialCase(caseId);
                    break;
            }
        }catch(IllegalArgumentException exception){
            Log.d(TAG, "Illegal Argument Exception ", exception);
            //doses and other unknown case_type(s) currently fall here
        }
    }

    public Map<String,String> loadCaseFromCommCare(String caseID){
        //Grab data from the CASE_DATA DB -> HashMap<datum_id, value>
        Cursor cursor = mContext.getContentResolver().query(Uri.parse(CASE_DATA + caseID), null,
                null,null,null);
        if (cursor == null){
            Log.e(TAG, "Cursor was null for ID: " + caseID);
        }
        else if(cursor.getCount()==0){
            Log.e(TAG, "Cursor was EMPTY for ID: " + caseID);
        }
        else {
            Log.d(TAG, String.format("Cursor of size %s found", cursor.getCount()));
            cursor.moveToFirst();
            int keyPosition = cursor.getColumnIndex(KEY_COLUMN);
            int dataPosition = cursor.getColumnIndex(DATA_COLUMN);
            Map<String,String> map = new HashMap<String, String>();
            do {
                try {
                    map.put(cursor.getString(keyPosition), cursor.getString(dataPosition));
                }catch (CursorIndexOutOfBoundsException e){
                    Log.e(TAG, String.format("Cursor not working for %s | %s", keyPosition,
                            dataPosition));
                }
            }while(cursor.moveToNext());
            cursor.close();
            return map;
        }
        return null;
    }

    public String matchCaseProperty(String caseID, String property) {
        Cursor caseCursor = mContext.getContentResolver().query(Uri.parse(CASE_DATA + caseID),
                null, null, null, null);
        String propertyValue = null;
        if (caseCursor != null) {
            try {
                int keyColumn = caseCursor.getColumnIndex(KEY_COLUMN);
                int dataColumn = caseCursor.getColumnIndex(DATA_COLUMN);
                if(caseCursor.moveToFirst()) {
                    do {
                        if (caseCursor.getString(keyColumn).equals(property)) {
                            propertyValue = caseCursor.getString(dataColumn);
                            break;
                        }
                    } while (caseCursor.moveToNext());
                }
            } finally {
                caseCursor.close();
            }
        }
        return propertyValue;
    }

    public Cursor findCaseType(String caseType) {
        return mContext.getContentResolver().query(Uri.parse(CASE_METADATA),
                null, "case_type = ?", new String[] {caseType}, null);
    }

    public ContentValues findReportCase(String caseType, int month, int year) {
        Cursor reportCases = findCaseType(caseType);
        ContentValues contentValues = new ContentValues();
        if(reportCases != null && reportCases.moveToFirst()) {
            Log.d(TAG, reportCases.getCount() + " report cases found ");
            int caseIdColumn = reportCases.getColumnIndex(CommCareDataManager.CASE_ID_COLUMN);
            do {
                /* username_year_month */
                String caseId = reportCases.getString(caseIdColumn);
                String[] dateKeys = caseId.split("_");
                if(Integer.parseInt(dateKeys[1]) == year && Integer.parseInt(dateKeys[2]) ==
                        month) {
                    Cursor reportCase = queryCaseId(caseId);
                    if(reportCase != null && reportCase.moveToFirst()) {
                        DatabaseUtils.cursorRowToContentValues(reportCase, contentValues);
                        reportCase.close();
                        return contentValues;
                    }
                }
            } while(reportCases.moveToNext());
            reportCases.close();
        }
        return null;
    }

    public ArrayList<ContentValues> fetchAllReports() {
        Cursor reportCases = findCaseType(DoseReport.MONTHLY_CASE_TYPE);
        if(reportCases != null && reportCases.moveToFirst()) {
            ArrayList<ContentValues> reports = new ArrayList<>(reportCases.getCount());
            int caseIdColumn = reportCases.getColumnIndex(CommCareDataManager.CASE_ID_COLUMN);
            do {
                String caseId = reportCases.getString(caseIdColumn);
                Cursor reportCase = queryCaseId(caseId);
                if (reportCase != null && reportCase.moveToFirst()) {
                    ContentValues report = new ContentValues();
                    report.put("case_id", reportCase.getString(reportCase.getColumnIndex
                            (CASE_ID_COLUMN)));
                    int keyColumn = reportCase.getColumnIndex(KEY_COLUMN);
                    int valueColumn = reportCase.getColumnIndex(DATA_COLUMN);
                    do {
                        report.put(reportCase.getString(keyColumn),
                                reportCase.getString(valueColumn));
                    } while (reportCase.moveToNext());
                    reportCase.close();
                    reports.add(report);
                } else {
                    Log.d(TAG, "report case was null or empty");
                }
            } while (reportCases.moveToNext());
            reportCases.close();
            return reports;
        } else {
            Log.d(TAG, "No report cases found");
        }
        return null;
    }

    public Cursor queryCaseId(String caseId) {
        return mContext.getContentResolver().query(Uri.parse(CASE_DATA + caseId), null,
                null,null,null);
    }

    public String getUserId(String username) {
        Cursor usersCursor = findCaseType(COMMCARE_USER_CASE);
        String userId = null;
        if (usersCursor != null) {
            try {
                int caseIdColumn = usersCursor.getColumnIndex(CASE_ID_COLUMN);
                Log.d(TAG, "Users Cases Count: " + usersCursor.getCount());
                if(usersCursor.moveToFirst()) {
                    do {
                        userId = matchCaseProperty(usersCursor.getString(caseIdColumn), COMMCARE_HQ_ID);
                    } while (usersCursor.moveToNext());
                }
            } finally {
                usersCursor.close();
            }
        }
        return userId;
    }

    private void commitCaseToDB(MemoryDBHandler handler, String caseId, String dateOpened, String
            dateModified){

        //Grabs a case from CC ContentProvider, sticks it in the localDB
        Cursor cursor = mContext.getContentResolver().query(Uri.parse(CASE_DATA + caseId), null,
                null,null,null);
        if(cursor != null && cursor.moveToFirst()) {
            String ccKey;
            String dbKey;
            String humanKey;
            int keyPosition = cursor.getColumnIndex(KEY_COLUMN);
            int dataPosition = cursor.getColumnIndex(DATA_COLUMN);
            ContentValues contentValues = new ContentValues();
            contentValues.put("docid", caseId);
            contentValues.put("docmod", dateModified);
            Date dateOpenedDate = new Date();
            try {
                dateOpenedDate = caseDateFormat.parse(dateOpened);
            } catch (ParseException e) {
                Log.e(TAG, "Couldn't parse date opened string", e);
            }
            contentValues.put("dateopened", isoDateFormat.format(dateOpenedDate));
            do {
                ccKey = cursor.getString(keyPosition);
                humanKey = handler.FIELD_TRANSLATION.get(ccKey);
                dbKey = handler.TABLE_MAP.get(humanKey);
                //Log.d(TAG, "Case datum_id " + cc_key + " value: " + cursor.getString(data_position));
                if (dbKey == null) {
                    continue;
                }
                else if (ccKey.equals("date_created")) {
                    /* Saved on creation to the properties, date opened is lost on restore */
                    String dateCreated = cursor.getString(dataPosition);
                    try {
                        Date dateCreatedDate = caseDateFormat.parse
                                (dateCreated);
                        if(dateOpenedDate.after(dateCreatedDate)) {
                            contentValues.put("dateopened", isoDateFormat.format(dateCreatedDate));
                            continue;
                        }
                    } catch (ParseException e) {
                        Log.e(TAG, "Couldn't parse date created", e);
                    }
                } else if (ccKey.equals("last_visit")) {
                    /* Last visit was never updated on followup , has no time component */
                    String lastVisit = cursor.getString(dataPosition);
                    try {
                        Date dayOpened = stripTime(dateOpenedDate);
                        Date lastVisitDate = dateFormat.parse(lastVisit);
                        if(dayOpened.after(lastVisitDate)) {
                            contentValues.put("dateopened", isoDateFormat.format(lastVisitDate));
                            continue;
                        }
                    } catch (ParseException e) {
                        Log.d(TAG, "Couldn't parse last visit");
                    }
                }
                contentValues.put(dbKey, cursor.getString(dataPosition));
            } while (cursor.moveToNext());
            cursor.close();
            handler.commitRow(contentValues);
        } else {
            Log.d(TAG, "Case cursor was empty");
        }
    }

    private Date stripTime(Date dateTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));
        return calendar.getTime();
    }

    private void insertOrUpdateReportCase(CommCareCase reportCase) {
        reportsDB.saveReport(reportCase);
    }

    private void insertOrUpdateVialCase(String caseId) {
        int rowsUpdated = ocrManager.updateVialCase(caseId);
    }

    private enum CaseType {
        guardian,
        patient,
        qrcode,
        dose,
        commcareuser,
        report,
        monthlyreport,
        vial
    }

    /*
    *    https://github.com/dimagi/commcare-odk/blob/master/test-app/src/com/dimagi/test/external/FixtureContentActivity.java
    *
    *    fixtures including user-groups (+) , item-list:fixturename (*)
    */
    public Cursor getFixturesList() {
        return mContext.getContentResolver().query(Uri.parse(FIXTURE_DATA), null, null, null, null);
    }

    /*
    *    https://github.com/dimagi/commcare-odk/blob/master/test-app/src/com/dimagi/test/external/FixtureContentActivity.java
    *
    *    Content Provider Schema [instance_id, content] columns
    */
    public Cursor getFixtureById(String fixtureId) {
        return mContext.getContentResolver().query(Uri.parse(FIXTURE_DATA + fixtureId), null, null,
                null, null);
    }

    public Cursor getVillagesFixture() {
        Log.d(TAG, "fixture: " + mContext.getString(R.string.village_fixture_id));
        return getFixtureById(mContext.getString(R.string.village_fixture_id));
    }

    public Cursor getScheduleFixture() {
        return getFixtureById(SCHEDULE_FIXTURE_ID);
    }

    public Cursor getUserGroupsFixture() {
        return getFixtureById(GROUPS_FIXTURE_ID);
    }

    public Cursor getUserFixture(String userId) {
        return null;
    }


    public void loadVillages() {
        if (AppController.commCareAPI.isAuthenticated() &&
                (allVillages == null || villageOwner == null ||
                        !villageOwner.equals(AppController.commCareAPI.getUserId()))) {
            villageOwner = AppController.commCareAPI.getUserId();
            new VillageFixtureLoader().execute(null, null, null);
        }
    }

    public Map<String, String> getAllVillages() {
        return allVillages;
    }

    public List<String> getCurrentUserVillages() {
        return currentUserVillages;
    }

    private class VillageFixtureLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "Loading villages");
            Cursor data = getVillagesFixture();
            if (data != null && data.moveToFirst()) {
                if(BuildConfig.FLAVOR.equals("nepal")) {
                    if(locale == null) {
                        locale = mContext.getResources().getConfiguration().locale;
                    }
                    String language = locale.getISO3Language().equals("eng") ? "en" : "nep";
                        /* Fixture element has language attribute */
                    CustomVillageFixtureParser parser = new CustomVillageFixtureParser(language);
                    parser.parse(data.getString(data.getColumnIndex(CommCareDataManager
                            .FIXTURE_CONTENT_COLUMN)));
                    allVillages = parser.getAllVillages();
                    currentUserVillages = parser.getCurrentUserVillages();
                } else {
                    CustomVillageFixtureParser parser = new CustomVillageFixtureParser();
                    parser.parse(data.getString(data.getColumnIndex(CommCareDataManager
                            .FIXTURE_CONTENT_COLUMN)));
                    allVillages = parser.getAllVillages();
                    currentUserVillages = parser.getCurrentUserVillages();
                }
                data.close();
            }
            return null;
        }
    }

    /*
    Stubs for database specific information.
    All methods handled by superclass MemoryDBHandler
     */

    public class GuardianDBHandler extends MemoryDBHandler {

        public static final String TABLE_NAME = "GUARDIAN";
        public static final String ID_COLUMN = "docid";
        public static final String DATE_MODIFIED_COLUMN = "docmod";
        public static final String DATE_OPENED_COLUMN = "dateopened";
        public static final String USER_ID_COLUMN = "userid";
        public static final String NAME_COLUMN = "name";
        public static final String CHILD_NAME_COLUMN = "childname";
        public static final String CHILD_ID_COLUMN = "childid";
        public static final String SEX_COLUMN = "sex";
        public static final String CHILD_SEX_COLUMN = "childsex";
        public static final String CHILD_DOB_COLUMN = "childdob";
        public static final String VILLAGE_COLUMN = "village";
        public static final String BIRTHPLACE_COLUMN = "pob";
        public static final String RELATION_TYPE_COLUMN = "relation";
        public static final String LAST_SEEN_COLUMN = "lastseen";
        public static final String LEFT_INDEX_COLUMN = "leftindex";
        public static final String LEFT_THUMB_COLUMN = "leftthumb";
        public static final String RIGHT_INDEX_COLUMN = "rightindex";
        public static final String RIGHT_THUMB_COLUMN = "rightthumb";

        public GuardianDBHandler(){
            super(mDBHelper);
            super.TABLE_NAME = TABLE_NAME;
            TABLE_MAP = new ArrayMap<>();

            //CC-Data Structure to Human Readable
            FIELD_TRANSLATION = new ArrayMap<String, String>(){{
                put("case_id","DOC_ID");
                put("date_modified","DOC_MOD");
                put("date_opened", "DATE_OPENED");
                put("userid", "USER_ID");
                put("guardian_name","NAME");
                put("child_name","CHILD_NAME");
                put("child_id","CHILD_ID");
                put("sex","SEX");
                put("child_sex", "CHILD_SEX");
                put("child_dob","CHILD_DOB");
                put("village","VILLAGE");
                put("birthplace","BIRTHPLACE");
                put("last_visit","LAST_SEEN");
                put("fingerprint_left_index", "LEFT_INDEX");
                put("fingerprint_left_thumb", "LEFT_THUMB");
                put("fingerprint_right_index", "RIGHT_INDEX");
                put("fingerprint_right_thumb", "RIGHT_THUMB");
                put("relation_type", "RELATION_TYPE");
            }};

            //Human Readable to SQLite
            TABLE_MAP = new ArrayMap<String, String>(){
                {
                    put("DOC_ID", ID_COLUMN);
                    put("DOC_MOD", DATE_MODIFIED_COLUMN);
                    put("DATE_OPENED", DATE_OPENED_COLUMN);
                    put("USER_ID", USER_ID_COLUMN);
                    put("NAME", NAME_COLUMN);
                    put("CHILD_NAME", CHILD_NAME_COLUMN);
                    put("CHILD_ID", CHILD_ID_COLUMN);
                    put("SEX", SEX_COLUMN);
                    put("CHILD_SEX", CHILD_SEX_COLUMN);
                    put("CHILD_DOB",CHILD_DOB_COLUMN);
                    put("VILLAGE", VILLAGE_COLUMN);
                    put("BIRTHPLACE", BIRTHPLACE_COLUMN);
                    put("RELATION_TYPE", RELATION_TYPE_COLUMN);
                    put("LAST_SEEN", LAST_SEEN_COLUMN);
                    put("LEFT_INDEX", LEFT_INDEX_COLUMN);
                    put("LEFT_THUMB", LEFT_THUMB_COLUMN);
                    put("RIGHT_INDEX", RIGHT_INDEX_COLUMN);
                    put("RIGHT_THUMB", RIGHT_THUMB_COLUMN);
                }};
            //When in memory, the table has to be created each run
            try{
                executeSql("create table if not exists GUARDIAN (docid text primary key, docmod " +
                        "text, dateopened text, userid text, name text, childname text, childid " +
                        "text, childdob text, sex text, childsex text, village text, pob text, " +
                        "relation text, lastseen text, leftindex text, leftthumb text, rightindex text, rightthumb text);");
                executeSql("create index if not exists childname_index on GUARDIAN" +
                        "(childname)");
            }catch (Exception e){
                Log.e(TAG, e.toString());
            }
        }

        @Override
        public String[] getColumnNames(){
            return new String[]{ID_COLUMN, DATE_MODIFIED_COLUMN, DATE_OPENED_COLUMN, NAME_COLUMN,
                    CHILD_NAME_COLUMN, CHILD_ID_COLUMN, CHILD_DOB_COLUMN, SEX_COLUMN,
                    CHILD_SEX_COLUMN, VILLAGE_COLUMN, BIRTHPLACE_COLUMN, RELATION_TYPE_COLUMN,
                    LAST_SEEN_COLUMN, LEFT_INDEX_COLUMN, LEFT_THUMB_COLUMN, RIGHT_INDEX_COLUMN,
                    RIGHT_THUMB_COLUMN};
        }
    }

    public class PatientDBHandler extends MemoryDBHandler{

        public static final String TABLE_NAME = "PATIENT";
        public static final String ID_COLUMM = "docid";
        public static final String DATE_MODIFIED_COLUMN = "docmod";
        public static final String DATE_OPENED_COLUMN = "dateopened";
        public static final String USER_ID_COLUMN = "userid";
        public static final String NAME_COLUMN = "name";
        public static final String DOB_COLUMN = "dob";
        public static final String SEX_COLUMN = "sex";
        public static final String VILLAGE_COLUMN = "village";
        public static final String ADDRESS_COLUMN = "address";
        public static final String VT_NUMBER_COLUMN = "vtnumber";
        public static final String CALLBACK_STATUS_COLUMN = "callbackstatus";
        public static final String CALLBACK_DATE_COLUMN = "callbackdate";
        public static final String MOBILE_COLUMN = "mobile";
        public static final String CREATED_BY_COLUMN = "createdby";
        public static final String CLINIC_VISITS_COLUMN = "clinicvisits";
        public static final String LAST_SEEN_COLUMN = "lastseen";

        public PatientDBHandler(){
            super(mDBHelper);
            super.TABLE_NAME = TABLE_NAME;

            //CC-Data Structure to Human Readable
            FIELD_TRANSLATION = new ArrayMap<String, String>(){{
                put("case_id","DOC_ID");
                put("date_modified","DOC_MOD");
                put("date_opened", "DATE_OPENED");
                put("userid", "USER_ID");
                put("patient_name","NAME");
                put("date_of_birth","DOB");
                put("sex","SEX");
                put("village","VILLAGE");
                put("address","ADDRESS");
                put("card_num","VTNUMBER");
                put("callback_status","CALLBACK_STATUS");
                put("callback_date","CALLBACK_DATE");
                put("contact_number","MOBILE");
                put("created_by", "CREATED_BY");
                put("clinic_visits", "CLINIC_VISITS");
                put("last_visit","LAST_SEEN");
            }};

            //Human Readable to SQLite
            TABLE_MAP = new ArrayMap<String, String>(){
                {
                    put("DOC_ID", ID_COLUMM);
                    put("DOC_MOD", DATE_MODIFIED_COLUMN);
                    put("DATE_OPENED", DATE_OPENED_COLUMN);
                    put("USER_ID", USER_ID_COLUMN);
                    put("NAME", NAME_COLUMN);
                    put("DOB", DOB_COLUMN);
                    put("SEX", SEX_COLUMN);
                    put("VILLAGE", VILLAGE_COLUMN);
                    put("ADDRESS", ADDRESS_COLUMN);
                    put("VTNUMBER", VT_NUMBER_COLUMN);
                    put("CALLBACK_STATUS", CALLBACK_STATUS_COLUMN);
                    put("CALLBACK_DATE", CALLBACK_DATE_COLUMN);
                    put("MOBILE", MOBILE_COLUMN);
                    put("CREATED_BY", CREATED_BY_COLUMN);
                    put("CLINIC_VISITS", CLINIC_VISITS_COLUMN);
                    put("LAST_SEEN", LAST_SEEN_COLUMN);
                }};
            //When in memory, the table has to be created each run
            try {
                executeSql("create table if not exists PATIENT (docid text primary key, docmod " +
                        "text, dateopened text, userid text, name text, dob text, sex text, village text, " +
                        "address text, vtnumber text, callbackstatus text, callbackdate text, " +
                        "mobile text, createdby text, clinicvisits text, lastseen text);");
                executeSql("create index if not exists vtnumber_index on PATIENT(vtnumber)");
                executeSql("create index if not exists userid_index on " +
                        "PATIENT(userid)");
                executeSql("create index if not exists callbackdate_index on " +
                        "PATIENT(callbackdate)");
                executeSql("create index if not exists village_index on PATIENT" +
                        "(village)");
                executeSql("create index if not exists name_index on PATIENT" +
                        "(name)");
            }catch (Exception e){
                Log.e(TAG, e.toString());
            }
        }
        @Override
        public String[] getColumnNames(){
            return new String[]{ID_COLUMM, DATE_MODIFIED_COLUMN, DATE_OPENED_COLUMN, USER_ID_COLUMN, NAME_COLUMN,
                    DOB_COLUMN, SEX_COLUMN, VILLAGE_COLUMN, ADDRESS_COLUMN, CALLBACK_STATUS_COLUMN,
                    CALLBACK_DATE_COLUMN, VT_NUMBER_COLUMN, MOBILE_COLUMN, CREATED_BY_COLUMN,
                    CLINIC_VISITS_COLUMN, LAST_SEEN_COLUMN};
        }
    }

    public class DoseDBHandler extends MemoryDBHandler{

        public static final String TABLE_NAME = "DOSE";
        public static final String ID_COLUMN = "_id";
        public static final String DOC_ID_COLUMN = "docid";
        public static final String USER_ID_COLUMN = "userid";
        public static final String PATIENT_ID_COLUMN = "patientid";
        public static final String VILLAGE_COLUMN = "village";
        public static final String VIAL_ID_COLUMN = "vialid";
        public static final String VACCINE_NAME_COLUMN = "vaccinename";
        public static final String TIMESTAMP_COLUMN = "timestamp";
        public static final String PATIENT_AGE_COLUMN = "patientage";
        public static final String SERIES_COLUMN = "series";
        public static final String MONTH_COLUMN = "month";
        public static final String YEAR_COLUMN = "year";
        public static final String SESSION_TYPE = "sessiontype";

        public DoseDBHandler(){
            super(mDBHelper);
            super.TABLE_NAME = TABLE_NAME;

            //CC-Data Structure to Human Readable
            FIELD_TRANSLATION = new ArrayMap<String, String>(){{
                put("doc_id","doc_id");
                put("userid", "user_id");
                put("patient_id","patient_id");
                put("village", "village");
                put("vial_id","vial_id");
                put("vaccine_name","vaccine_name");
                put("timestamp","timestamp");
                put("patient_age","patient_age");
                put("series","series");
                put("month","month");
                put("year","year");
                put("session_type","session_type");
            }};

            //Human Readable to SQLite
            TABLE_MAP = new ArrayMap<String, String>(){
                {
                    put("doc_id", DOC_ID_COLUMN);
                    put("user_id", USER_ID_COLUMN);
                    put("patient_id", PATIENT_ID_COLUMN);
                    put("village", VILLAGE_COLUMN);
                    put("vial_id", VIAL_ID_COLUMN);
                    put("vaccine_name",VACCINE_NAME_COLUMN);
                    put("timestamp", TIMESTAMP_COLUMN);
                    put("patient_age",PATIENT_AGE_COLUMN);
                    put("series", SERIES_COLUMN);
                    put("month", MONTH_COLUMN);
                    put("year", YEAR_COLUMN);
                    put("session_type", SESSION_TYPE);
                }};
            //When in memory, the table has to be created each run
            try {
                executeSql("create table if not exists DOSE (docid text primary key, " +
                        "userid text, patientid text, village text, vialid text, vaccinename text, timestamp text, " +
                        "patientage text, series text, month text, year text, sessiontype text);");
            }catch (Exception e){
                Log.e(TAG, e.toString());
            }
        }
        @Override
        public String[] getColumnNames(){
            return new String[]{DOC_ID_COLUMN, USER_ID_COLUMN, PATIENT_ID_COLUMN, VILLAGE_COLUMN,
                    VIAL_ID_COLUMN, VACCINE_NAME_COLUMN, TIMESTAMP_COLUMN, PATIENT_AGE_COLUMN, SERIES_COLUMN,
                    MONTH_COLUMN, YEAR_COLUMN, SESSION_TYPE};
        }
    }
}


