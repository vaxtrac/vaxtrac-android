package com.vaxtrac.android.vaxtracapp.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.models.Schedule;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

public class Preferences implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "Preferences";

    Context context;
    private static Context applicationContext;

    SharedPreferences defaultPreferences;

    SharedPreferences.OnSharedPreferenceChangeListener defaultListener;

    private static Map<String, Preference> preferences = new LinkedHashMap<>();

    /* General */
    public static final String PREF_LANGUAGE = "pref_language";
    public static final String PREF_LANGUAGE_TITLE = "pref_language_title";
    public static final String PREF_LANGUAGE_CHOICES = "pref_language_choices";
    public static final String PREF_SOUND = "pref_sound";
    public static final String PREF_SOUND_TITLE = "pref_sound_title";

    /* Appearence */
    public static final String PREF_THEME = "pref_theme";
    public static final String PREF_THEME_TITLE = "pref_theme_title";

    /* CommCare */
    public static final String PREF_AUTO_LOGIN = "pref_auto_login";
    public static final String PREF_AUTO_LOGIN_TITLE = "pref_auto_login_title";
    public static final String PREF_LOGIN_TIMEOUT = "pref_login_timeout";
    public static final String PREFS_LOGIN_TIMEOUT_TITLE = "pref_login_timeout_title";
    public static final String PREF_LOGIN_TIMEOUT_CHOICES = "pref_login_timeout_choices";
    public static final String PREF_MAX_RETRIES = "pref_max_retries";
    public static final String PREF_MAX_RETRIES_TITLE = "pref_max_retries_title";
    public static final String PREF_SYNC_INTERVAL = "pref_sync_interval";
    public static final String PREF_SYNC_INTERVAL_TITLE = "pref_sync_interval_title";
    public static final String PREF_LAST_SUBMISSION = "pref_last_submission";
    public static final String PREF_LAST_SYNC = "pref_last_sync";
    public static final String PREF_VACCINE_SCHEDULE = "pref_schedule";
    public static final String PREF_SCHEDULE_VERSION = "pref_schedule_version";
    public static final String PREF_SCHEDULE_LAST_UPDATE = "pref_schedule_last_update";

    /* Developer */
    public static final String PREF_PHONE_SEARCH = "pref_phone_search";
    public static final String PREF_NAME_SEARCH = "pref_name_search";
    public static final String PREF_DEVELOPER = "pref_developer";
    public static final String PREF_DEVELOPER_TITLE = "pref_developer_title";
    public static final String PREF_CRASH_REPORTING = "pref_crash_reporting";
    public static final String PREF_CRASH_REPORTING_TITLE = "pref_crash_reporting_title";
    public static final String PREF_LOGOUT = "pref_logout";
    public static final String PREF_LOGOUT_TITLE = "pref_logout_title";
    public static final String PREF_DEMO_MODE = "pref_demo_mode";
    public static final String PREF_DEMO_MODE_TITLE = "pref_demo_mode_title";
    public static final String PREF_ROTATION_LOCK = "pref_rotation_lock";
    public static final String PREF_ROTATION_LOCK_TITLE = "pref_rotation_lock_title";
    public static final String ROTATION_LOCK_CHOICES = "pref_rotation_lock_choices";
    public static final String PREF_BATTERY_SAVER = "pref_battery_saver";
    public static final String PREF_BATTERY_SAVER_TITLE = "pref_battery_saver_title";
    public static final String PREF_DUMP_DATABASE = "pref_dump_database";
    public static final String PREF_BACKUP_DB = "pref_backup_db";
    public static final String PREF_RESTORE_DB = "pref_restore_db";
    public static final String PREF_USERGUIDE = "pref_userguide";
    public static final String PREF_HELP_VIDEOS = "pref_help_videos";
    public static final String PREF_FINGERPRINTING = "pref_fingerprinting";
    public static final String PREF_SESSION_LOCATION = "pref_session_location";
    public static final String PREF_SESSION_LOCATION_TYPE = "pref_session_location_type";

    public enum PrefsType {
        STRING, INTEGER, BOOLEAN, STRING_ARRAY, INTEGER_ARRAY
    }

    public enum Prefs {
        PREF_LANGUAGE (Preferences.PREF_LANGUAGE, PREF_LANGUAGE_TITLE, PREF_LANGUAGE_CHOICES,
                PrefsType.STRING_ARRAY),
        PREF_SOUND (Preferences.PREF_SOUND, PREF_SOUND_TITLE, PrefsType.BOOLEAN),
        PREF_THEME (Preferences.PREF_THEME, PREF_THEME_TITLE, PrefsType.STRING_ARRAY),
        PREF_AUTO_LOGIN (Preferences.PREF_AUTO_LOGIN, PREF_AUTO_LOGIN_TITLE, PrefsType.BOOLEAN),
        PREF_LOGIN_TIMEOUT (Preferences.PREF_LOGIN_TIMEOUT, PREFS_LOGIN_TIMEOUT_TITLE,
                PREF_LOGIN_TIMEOUT_CHOICES, PrefsType.INTEGER_ARRAY),
        PREF_MAX_RETRIES (Preferences.PREF_MAX_RETRIES, PREF_MAX_RETRIES_TITLE, PrefsType.INTEGER),
        PREF_SYNC_INTERVAL (Preferences.PREF_SYNC_INTERVAL, PREF_SYNC_INTERVAL_TITLE, PrefsType.INTEGER),
        PREF_DEVELOPER (Preferences.PREF_DEVELOPER, PREF_DEVELOPER_TITLE, PrefsType.BOOLEAN),
        PREF_CRASH_REPORTING (Preferences.PREF_CRASH_REPORTING, PREF_CRASH_REPORTING_TITLE,
                PrefsType.BOOLEAN),
        PREF_LOGOUT (Preferences.PREF_LOGOUT, PREF_LOGOUT_TITLE, PrefsType.BOOLEAN),
        PREF_DEMO_MODE (Preferences.PREF_DEMO_MODE, PREF_DEMO_MODE_TITLE, PrefsType.BOOLEAN),
        PREF_ROTATION_LOCK (Preferences.PREF_ROTATION_LOCK, PREF_ROTATION_LOCK_TITLE, PrefsType
                .STRING_ARRAY),
        PREF_BATTERY_SAVER (Preferences.PREF_BATTERY_SAVER, PREF_BATTERY_SAVER_TITLE, PrefsType.BOOLEAN);

        private String key;
        private String title;
        private String entries;
        private PrefsType type;

        Prefs(String key, String title, String entries, PrefsType type) {
            this.key = key;
            this.title = title;
            this.entries = entries;
            this.type = type;
        }

        Prefs(String key, String description, PrefsType type) {
            this.key = key;
            this.title = description;
            this.type = type;
        }

        String key() {
            return applicationContext.getString(applicationContext.getResources().getIdentifier(key,
                    "string", applicationContext.getPackageName()));
        }

        String title() {
            return applicationContext.getString(applicationContext.getResources().getIdentifier
                    (title, "string", applicationContext.getPackageName()));
        }

        String entries() {
            if(entries != null) {
                return applicationContext.getString(applicationContext.getResources().getIdentifier
                        (entries, "string", applicationContext.getPackageName()));
            } else {
                return null;
            }
        }

        PrefsType type() {
            return type;
        }
    }

    public Preferences() {
        this.applicationContext = AppController.getAppContext();
        defaultPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        defaultPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    public void handlePreferenceChange(SharedPreferences sharedPreferences, String key) {
        switch(key) {
            case PREF_LANGUAGE:
                 //changeLanguage(key);
                 break;
            case PREF_SOUND:
                 //changeSound(key);
                 break;
            case PREF_THEME:
                 //changeTheme(key);
                 break;
            case PREF_AUTO_LOGIN:
                 //changeAutoLogin(key);
                 break;
            case PREF_LOGIN_TIMEOUT:
                 //changeLoginTimeout(key);
                 break;
            case PREF_MAX_RETRIES:
                 //changeMaxRetries(key);
                 break;
            case PREF_SYNC_INTERVAL:
                 //changeSyncInterval(key);
                 break;
            case PREF_LAST_SUBMISSION:
                 //changeLastSubmission(key);
                 break;
            case PREF_LAST_SYNC:
                 //changeLastSync(key);
                 break;
            case PREF_DEVELOPER:
                 //changeDeveloper(key);
                 break;
            case PREF_CRASH_REPORTING:
                 //changeCrashReporting(key);
                 break;
            case PREF_LOGOUT:
                 //changeLogout(key);
                 break;
            case PREF_DEMO_MODE:
                 //changeDemoMode(key);
                 break;
            case PREF_ROTATION_LOCK:
                 //changeRotationLock(key);
                 break;
            case PREF_BATTERY_SAVER:
                 //changeBatterySaver(key);
                 break;
            case PREF_DUMP_DATABASE:
                 break;
            default:
                break;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "Shared Preference Changed " + key);
        handlePreferenceChange(sharedPreferences, key);
    }

    public static String getKey(String resourceKey) {
        return applicationContext.getString(applicationContext.getResources().getIdentifier(resourceKey,
                "string", applicationContext.getPackageName()));
    }

    public static String getLocale() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getString
                (PREF_LANGUAGE, "en");
    }

    public static Boolean isFingerprintingEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean
                (PREF_FINGERPRINTING, true);
    }

    public static Boolean sessionLocationEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean
                (PREF_SESSION_LOCATION, false);
    }

    public static String getSessionLocationType() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getString
                (PREF_SESSION_LOCATION_TYPE, getResourceString(R.string.fixed_site));
    }

    public static boolean isOutreachSession() {
        return getSessionLocationType().equals(getResourceString(R.string.outreach_site));
    }

    public static void saveSessionLocationType(String sessionLocationType) {
        putString(PREF_SESSION_LOCATION_TYPE, sessionLocationType);
    }

    public static boolean isCrashReporting() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean(Preferences
                .PREF_CRASH_REPORTING, false);
    }

    public static boolean isPhoneSearch() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext)
                .getBoolean(Preferences.PREF_PHONE_SEARCH, false);
    }

    public static boolean isNameSearch() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext)
                .getBoolean(Preferences.PREF_NAME_SEARCH, false);
    }

    public static int getScheduleVersion() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getInt
                (Preferences.PREF_SCHEDULE_VERSION, -1);
    }

    public static long getScheduleLastUpdate() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getLong
                (Preferences.PREF_SCHEDULE_LAST_UPDATE, -1L);
    }

    public static boolean isScheduleSynced() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        Log.d(TAG, "Today's Date " + today.getTime());
        Calendar lastSync = Calendar.getInstance();
        lastSync.setTimeInMillis(getScheduleLastUpdate());
        Log.d(TAG, "Last Synced Date " + lastSync.getTime());
        lastSync.add(Calendar.DAY_OF_MONTH, Schedule.update_frequency_days);
        Log.d(TAG, "Next Sync Date " + lastSync.getTime());
        return today.before(lastSync);

    }

    public static String getVaccineSchedule() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext).getString
                (Preferences.PREF_VACCINE_SCHEDULE, "");
    }

    public static CharSequence[] getChoices(String key) {
        return ((EntryListPreference)getPreferenceByKey(key)).getEntries();
    }

    public static boolean hasEntries(String key) {
        if(getPreferenceByKey(key) instanceof EntryListPreference) {
            return true;
        }
        return false;
    }

    public static void addPreference(Preference preference) {
        preferences.put(preference.getKey(), preference);
        Log.d(TAG, "key " + preference.getKey() + " title " + preference.getTitle() + " order " +
                preference
                .getOrder());
        if(preference instanceof CheckPreference) {
            Log.d(TAG, "checkpreference");
            Log.d(TAG, "is Checked " + ((CheckPreference) preference).isChecked());
        } else if (preference instanceof TextPreference) {
            Log.d(TAG, "textpreference");
            Log.d(TAG, "text " + ((TextPreference) preference).getText());
        } else if (preference instanceof EntryListPreference) {
            Log.d(TAG, "entrylistpreference");
            Log.d(TAG, "entry " + ((EntryListPreference) preference).getValue());
        } else if (preference instanceof ButtonPreference) {
            Log.d(TAG, "buttonpreference");
            Log.d(TAG, "button " + ((ButtonPreference) preference).getTitle());
        }
    }

    public static String getResourceString(int resource) {
        return applicationContext.getResources().getString(resource);
    }

    public static Preference getPreferenceByKey(String key) {
        return preferences.get(key);
    }

    public static Map<String, Preference> getPreferences() {
        return preferences;
    }

    public static void putString(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext()).edit().putString(key,
                value).apply();
    }

    public static void putInt(String key, int value) {
        PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext()).edit().putInt(key,
                value).apply();
    }

}
