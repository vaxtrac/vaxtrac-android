package com.vaxtrac.android.vaxtracapp.commcare;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.ReportsDBHelper;
import com.vaxtrac.android.vaxtracapp.models.CommCareCase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CommCareConsistencyManager {

    private static final String TAG = "CCConsistencyManager";

    /* CommCare MetaData */
    public static final String CASE_ID = "case_id";
    public static final String CASE_TYPE = "case_type";
    public static final String OWNER_ID = "owner_id";
    public static final String STATUS = "status";
    public static final String CASE_NAME = "case_name";
    public static final String DATE_OPENED = "date_opened";
    public static final String LAST_MODIFIED = "last_modified";

    public static final String CASE_LISTING = "content://org.commcare.dalvik.case/casedb/case";
    Map<String,String> cases;
    //We may want to do some blocking before the first sync. Not after the first
    static boolean finishedFirstUpdate = false;
    private Context mContext;
    private CommCareDataManager mDataManager;

    public CommCareConsistencyManager(Context context, CommCareDataManager dataManager) {
        mDataManager = dataManager;
        mContext = context;
        cases = new HashMap<>();

        try {
            updateCaseMod();
            Log.i(TAG, "Updated CaseMod data from LocalDB");
        } catch (Exception e) {
            Log.e(TAG, "Error updating case Mod info | " + e.toString());
        }
    }

    private void updateCaseMod(){
        updateCaseModFromCursor(mDataManager.patientDB.allItems());
        updateCaseModFromCursor(mDataManager.guardianDB.allItems());
        updateReportCaseMod(mDataManager.reportsDB.allItems());
    }

    private void updateCaseModFromCursor(Cursor cursor){
        int idPos  = cursor.getColumnIndex("docid");
        int modPos = cursor.getColumnIndex("docmod");
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            try {
                cases.put(cursor.getString(idPos), cursor.getString(modPos));
            }catch (Exception e){
                Log.d(TAG, "Couldn't get info on a case_mod |" + e.toString());
            }
        }
        cursor.close();
    }

    private void updateReportCaseMod(Cursor cursor) {
        int caseIdPos = cursor.getColumnIndex(ReportsDBHelper.CASE_ID_COLUMN);
        int dateModPos = cursor.getColumnIndex(ReportsDBHelper.LAST_MODIFIED_COULMN);
        if(cursor.moveToFirst()) {
            do {
                cases.put(cursor.getString(caseIdPos), cursor.getString(dateModPos));
                new CommCareCase(cursor.getString(caseIdPos)).toString();
            } while (cursor.moveToNext());
        } else {
            Log.d(TAG, "Cursor was empty while updating report case mod");
        }
    }

    public static boolean isFinishedFirstUpdate(){
        return finishedFirstUpdate;
    }

    public void update(){
        //This MUST run on a thread.
        //CommCare must have an active user logged in
        Log.i(TAG, "Connecting to CaseDB (This may take a while)");
        AppController.notify(String.format(AppController.getStringResource(R.string.commcare_update_start)));
        Cursor c;
        try{
            c = mContext.getContentResolver().query(Uri.parse(CASE_LISTING), null, null,
                    null, null);
        }catch (NullPointerException e) {
            Log.e(TAG, "CommCare Session not active.", e);
            return;
        } catch(IllegalStateException e) {
            Log.e(TAG, "CommCare Illegal State Exception ", e);
            return;
        }

        if(c != null && c.moveToFirst()) {
            Log.i(TAG, "Finished pulling cursor");
            int caseTypeCol = c.getColumnIndex(CASE_TYPE);
            int caseIdCol = c.getColumnIndex(CASE_ID);
            int statusCol = c.getColumnIndex(STATUS);
            int caseModCol = c.getColumnIndex(LAST_MODIFIED);
            int dateOpenedCol = c.getColumnIndex(DATE_OPENED);

            Log.d(TAG, "CaseDB Column Names: " + Arrays.toString(c.getColumnNames()));
            String caseId = "";
            String caseMod = "";
            String oldCaseMod = "";
            int allCounter = 0;
            int updateCounter = 0;
            int newCounter = 0;
            do {
                try {
                    allCounter += 1;
                    caseId = c.getString(caseIdCol);
                    oldCaseMod = cases.get(caseId);
                    caseMod = c.getString(caseModCol);
                    if (oldCaseMod != null) {
                        if (!caseMod.equals(oldCaseMod)) {
                            updateCounter += 1;
                            cases.put(caseId, caseMod);
                            mDataManager.updateCase(caseId, c.getString(caseTypeCol), c
                                    .getString(dateOpenedCol), c.getString(caseModCol));
                        }
                    } else {
                        newCounter += 1;
                        cases.put(caseId, caseMod);
                        mDataManager.addCase(caseId, c.getString(caseTypeCol), c.getString
                                (dateOpenedCol), c.getString(caseModCol));
                    }
                } catch (Exception e) {
                    Log.i(TAG, "CCDB Error | " + e.toString());
                }
            } while (c.moveToNext());
            c.close();
            AppController.notify(String.format(AppController.getStringResource
                    (R.string.commcare_update_finished), newCounter));
            Log.i(TAG, String.format("Cases Parsed| Seen:%s | New: %s, | Updated: %s",
                    allCounter, newCounter, updateCounter));
            finishedFirstUpdate = true;
            //TODO Add cleanup of oldcases on device no longer found in index
        }
    }
}
