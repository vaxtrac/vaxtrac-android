package com.vaxtrac.android.vaxtracapp.presenter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.biometrics.BiometricsManager;
import com.vaxtrac.android.vaxtracapp.biometrics.Engine;
import com.vaxtrac.android.vaxtracapp.data.TranslationCursor;
import com.vaxtrac.android.vaxtracapp.models.SearchObject;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.view.BiometricSearchView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
This class controls both RegistrationBiometricCheck AND BiometricSearch
 */
public class BiometricSearchPresenter extends  Presenter {
    private static final String TAG = "BiometricSearchPresent";

    private WeakReference<Context> context;
    private WeakReference<MainActivity> activity;
    private BiometricSearchView view;
    LocalBroadcastManager broadcastManager;

    public static final int BIOMETRIC_SEARCH_SCAN_REQUEST_CODE = 400784;

    public BiometricSearchPresenter(Context context) {
        this.context = new WeakReference<Context>(context);
        activity = new WeakReference<>((MainActivity)context);

        /* reread the intent extra in case we've backed out of fingerprints */
        String skipFingerprintsExtra = activity.get().getIntent().getStringExtra
                (SearchObject.SKIP_FINGERPRINTS_EXTRA);

        //No Fingerprints
        if (!Preferences.isFingerprintingEnabled()
                || AppController.mSearchObject.skipFingerprints()
                || (skipFingerprintsExtra != null &&
                    skipFingerprintsExtra.equalsIgnoreCase("Yes"))) {
            Log.i(TAG, "Fingerprints DISABLED. Skipping");
            noMatch(true);
            return;
        }
        if(BiometricsManager.matches != null){
            Log.i(TAG, "Existing biometric Matches Found. Displaying");
            delayedMatchDisplay();
        }
        else{
            Log.i(TAG, "No existing biometric Matches Found. Going to Scans");
            getScans();
        }
        broadcastManager = LocalBroadcastManager.getInstance(this.context.get
                ());
        broadcastManager.registerReceiver(getSearchNoMatchReceiver(), new IntentFilter(BiometricsManager.SEARCH_NO_MATCH));
        broadcastManager.registerReceiver(getSearchCompleteReceiver(), new IntentFilter(BiometricsManager.SEARCH_DONE));
        broadcastManager.registerReceiver(getScanFailedReceiver(), new IntentFilter(BiometricsManager.SCAN_FAILED));
        broadcastManager.registerReceiver(getSearchUpdateReceiver(), new IntentFilter(BiometricsManager.UPDATE_BIOMETRIC_STATUS));
    }

    public void getScans(){
        Log.i(TAG, "Starting Scan Intent");
        BiometricsManager.getScans(activity.get(), BIOMETRIC_SEARCH_SCAN_REQUEST_CODE);
    }

    public void setView(View view) {
        if(view != null) {
            this.view = (BiometricSearchView) view;
        } else {
            this.view = null;
        }
    }

    public void invalidate() {
        Log.d(TAG, "invalidating view");
        view.invalidate();
    }

    public void unregisterReceivers() {
        broadcastManager.unregisterReceiver(getSearchNoMatchReceiver());
        broadcastManager.unregisterReceiver(getSearchCompleteReceiver());
        broadcastManager.unregisterReceiver(getScanFailedReceiver());
        broadcastManager.unregisterReceiver(getSearchUpdateReceiver());
    }

    public static void registerScanResult(int resultCode, final Intent intent){
        Log.i(TAG, "Caught data from biometric scan in presenter");
        if (resultCode != Activity.RESULT_OK){
            Log.i(TAG, "Scan Failed...");
            BiometricsManager.scanFailed();
        }else{
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    AppController.mSearchObject.setBiometrics(intent.getExtras());
                    Cursor c = AppController.mSearchObject.getPotentialMatches();
                    AppController.mSearchObject.getPotentialBiometricMatches(c, "docid");
                }
            });
            t.start();

        }

    }

    private BroadcastReceiver getSearchCompleteReceiver(){
        BroadcastReceiver r = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try{
                    prepareMatchDisplay();
                }catch(Exception e){
                    Log.i(TAG, "BroadcastReceiver couldn't trigger action");
                }
            }
        };
        return r;
    }

    private BroadcastReceiver getSearchNoMatchReceiver(){
        BroadcastReceiver r = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try{
                    noMatch(false);
                }catch(Exception e){
                    Log.i(TAG, "BroadcastReceiver couldn't trigger action");
                }
            }
        };
        return r;
    }

    private BroadcastReceiver getScanFailedReceiver(){
        BroadcastReceiver r = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try{
                    scanFailed();
                }catch(Exception e){
                    Log.i(TAG, "BroadcastReceiver couldn't trigger action");
                }
            }
        };
        return r;
    }

    private void scanFailed() {
        Toast.makeText(context.get(), context.get().getString(R.string.fingerprints_required), Toast
                .LENGTH_SHORT).show();
        Log.d(TAG, "Scan Failed");
        updateSearchStatus("ScanFAILED!");
        activity.get().failToCommCare();
    }

    private BroadcastReceiver getSearchUpdateReceiver(){
        BroadcastReceiver r = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try{
                    updateSearchStatus(intent.getStringExtra("STATUS"));
                }catch(Exception e){
                    Log.i(TAG, "BroadcastReceiver couldn't trigger action");
                }
            }
        };
        return r;
    }

    private void delayedMatchDisplay(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "Waiting to show matches");
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "Showing delayed Matches");
                prepareMatchDisplay();
            }
        });
        t.start();

    }

    private void prepareMatchDisplay(){
        Log.i(TAG, "Preparing Display of Match");
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Engine.Match> matches = BiometricsManager.matches;
                List<String> rows = new ArrayList<>();
                for (Engine.Match match: matches){
                    rows.add(match.uuid);
                }
                List<String> columns = new ArrayList<String>(Arrays.asList(AppController.mCommCareDataManager.guardianDB.getColumnNamesWithRowID()));
                columns.add("score");
                //adding score and ordering by score
                MatrixCursor specialCursor = new MatrixCursor(columns.toArray(new String[0]), rows.size());
                for (Engine.Match match: matches){
                    MatrixCursor.RowBuilder rowBuilder = specialCursor.newRow();
                    rowBuilder.add("score", Integer.toString((int) Math.round(match.score)));
                    Cursor cursor = AppController.mCommCareDataManager.guardianDB.allRowsColumnEquals("docid", match.uuid);
                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        columns = new ArrayList<String>(Arrays.asList(cursor.getColumnNames()));
                        for (String key: columns){
                            try {
                                rowBuilder.add(key, cursor.getString(cursor.getColumnIndex(key)));
                                //Log.d(TAG, String.format("%s | %s", key, cursor.getString(cursor.getColumnIndex(key))));
                            }catch(IllegalStateException e){
                                Log.e(TAG, e.toString());
                            }
                        }
                    }
                    cursor.close();
                }
                specialCursor.moveToFirst();
                TranslationCursor transCursor = new TranslationCursor(specialCursor,new String[]{"childsex", "relation"});
                specialCursor.close();
                showMatchDisplay(transCursor);
            }
        });
        t.start();
    }

    private void noMatch(final boolean skipMatching) {
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    noMatchFound(skipMatching);
                }
            });
        }catch (NullPointerException e){
            Log.e(TAG, "NoMatchFound Updated to Null View. User moved on?");
        }

    }

    public void showMatchDisplay(final Cursor cursor){
        try {
            view.setLastCursor(cursor);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    drawTable();
                }
            });
        }catch (NullPointerException e){
            Log.e(TAG, "MatchDisplay Updated to Null View. User moved on?");
        }
    }

    private void updateSearchStatus(final String status) {
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    updateStatus(status);
                }
            });
        }catch (NullPointerException e){
            Log.e(TAG, "SearchStatus Updated to Null View. User moved on?");
        }
    }

    private void updateStatus(final String status) {
        if(view != null) {
            view.updateSearchStatus(status);
        }
    }

    private void drawTable() {
        if(view != null) {
            view.drawTable();
        }
    }

    private void noMatchFound(boolean skipMatching) {
        if(view != null) {
            view.noMatchFound(skipMatching);
        }
    }
}
