package com.vaxtrac.android.vaxtracapp.models;

import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;

import org.json.JSONObject;

import java.util.Map;

public class CommCareCase {
    private static final String TAG = CommCareCase.class.getName();

    public static final Uri CASE_METADATA_URI =
            Uri.parse("content://org.commcare.dalvik.case/casedb/case");
    public static final Uri CASE_DATA_URI = Uri.parse("content://org.commcare.dalvik" +
            ".case/casedb/data/");
    public static final Uri CASE_INDEX_URI = Uri.parse("content://org.commcare.dalvik" +
            ".case/casedb/index/");
    public static final Uri CASE_ATTACHMENT_URI =
            Uri.parse("content://org.commcare.dalvik.case/casedb/attachment/");

    /* Meta Data in CaseDB Content Provider */
    public static final String CASE_ID = "case_id";
    public static final String CASE_TYPE = "case_type";
    public static final String OWNER_ID = "owner_id";
    public static final String STATUS = "status";
    public static final String CASE_NAME = "case_name";
    public static final String DATE_OPENED = "date_opened";
    public static final String LAST_MODIFIED = "last_modified";
    public static final String INTERNAL_ID = "_id";

    /* Warning not the last user submitter, same as owner id */
    public static final String USER_ID = "userid";

    public static final String DATUM_ID = "datum_id";
    public static final String VALUE = "value";

    public static final String INDEX_ID = "index_id";
    public static final String INDEX_CASE_TYPE = "case_type";
    public static final String INDEX_VALUE = "value";

    public static String[] CASE_ATTRIBUTES = {
            CASE_ID,
            LAST_MODIFIED,
            USER_ID
    };

    public static String[] CREATE_PROPERTIES = {
            CASE_NAME,
            CASE_TYPE,
            OWNER_ID
    };

    private static ContentResolver contentResolver =
            AppController.getAppContext().getContentResolver();

    String caseId;
    ContentValues caseData;
    Bundle formBundle;
    boolean isNewCase;

    public CommCareCase() {

    }

    public CommCareCase(String caseId) {
        caseData = new ContentValues();
        this.caseId = caseId;
        /* synchronous */
        copyMetaData();
        Log.d(TAG, "Meta Data " + this.toString());
        copyCaseData();
        Log.d(TAG, "Case Data " + this.toString());
        discardInternalId();

        //LoaderManager loaderManager = AppController.getMainActivity().getLoaderManager();
        //loaderManager.initLoader(metaDataLoaderId, null, null);
        //loaderManager.initLoader(caseDataLoaderId, null, null);
    }

    public CommCareCase(JSONObject caseJson) {

    }

    public void copyMetaData() {
        copyMetaData(getMetaDataListing());
    }

    public void copyMetaData(Cursor cursor) {
        if(cursor != null) {
            if(cursor.moveToFirst()) {
                DatabaseUtils.cursorRowToContentValues(cursor, caseData);
            }
            cursor.close();
        }
    }

    public void discardInternalId() {
        if(caseData.containsKey(INTERNAL_ID)) {
            caseData.remove(INTERNAL_ID);
        }
    }

    public void copyCaseData() {
        copyCaseData(getCaseDataListing());
    }

    public void copyCaseData(Cursor cursor) {
        if(cursor != null) {
            if(cursor.moveToFirst()) {
                int keyIndex = cursor.getColumnIndex(DATUM_ID);
                int valueIndex = cursor.getColumnIndex(VALUE);
                do {
                    String key = cursor.getString(keyIndex);
                    /* Case Properties last modified will overwrite more accurate Meta last
                    modified */
                    if(!key.equals(LAST_MODIFIED)) {
                        caseData.put(key, cursor.getString(valueIndex));
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }

    public ContentValues getContents() {
        return caseData;
    }

    @Override
    public String toString() {
        if (caseData != null && caseData.size() > 0) {
            StringBuilder description = new StringBuilder();
            for(Map.Entry<String, Object> entry: caseData.valueSet()) {
                description.append("key: ")
                        .append(entry.getKey())
                        .append(" value: ")
                        .append(entry.getValue())
                        .append("\n");
            }
            return description.toString();
        } else {
            return "CommCare Case with id " + caseId + " is empty";
        }
    }

    public Cursor getCaseListing(Uri uri) {
        return contentResolver.query(Uri.withAppendedPath(uri, caseId), null, null,
            null, null);
    }

    public Cursor getMetaDataListing() {
        return getCaseListing(CASE_METADATA_URI);
    }

    public Cursor getCaseDataListing() {
        return getCaseListing(CASE_DATA_URI);
    }

    public Cursor getCaseIndexListing() {
        return getCaseListing(CASE_INDEX_URI);
    }

    public Cursor getCaseAttachmentListing(String caseId) {
        return getCaseListing(CASE_ATTACHMENT_URI);
    }

    private static final int metaDataLoaderId = 101;
    LoaderManager.LoaderCallbacks<Cursor> metaDataLoader = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            if(id == metaDataLoaderId) {
                return new CursorLoader(AppController.getAppContext(),
                        Uri.withAppendedPath(CASE_METADATA_URI, caseId),
                        null,
                        null,
                        null,
                        null);
            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            if(loader.getId() == metaDataLoaderId) {
                copyMetaData(data);
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    private static final int caseDataLoaderId = 102;
    LoaderManager.LoaderCallbacks<Cursor> caseDataLoader = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            if(id == caseDataLoaderId) {
                return new CursorLoader(AppController.getAppContext(),
                        Uri.withAppendedPath(CASE_DATA_URI, caseId),
                        null,
                        null,
                        null,
                        null);
            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            if(loader.getId() == caseDataLoaderId) {
                copyCaseData(data);
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    LoaderManager.LoaderCallbacks<Cursor> indexDataLoader;
    LoaderManager.LoaderCallbacks<Cursor> attachmentDataLoader;
}
