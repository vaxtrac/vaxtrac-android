package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;

import com.vaxtrac.android.vaxtracapp.view.HistoryView;

public class HistoryPresenter extends Presenter {
    private HistoryView view;
    private Context context;

    public HistoryPresenter(Context context, HistoryView view) {
        this.context = context;
        this.view = view;
    }
}
