package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.utils.AssetsUtil;
import com.vaxtrac.android.vaxtracapp.view.HelpVideosView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HelpVideosPresenter {

    private static final String TAG = "HelpVideosPresenter";
    private static final String HELP_VIDEOS_PATH = "videos";
    private

    Context context;
    HelpVideosView view;
    AssetManager assetManager;
    List<String> existingFiles = new ArrayList<>();
    List<String> assetFiles = new ArrayList<>();

    public HelpVideosPresenter(Context context, HelpVideosView view) {
        this.context = context;
        this.view = view;
        this.assetManager = context.getAssets();
        existingFiles = checkForExisting();
        assetsToCopy();
        copyVideoAssets();
    }

    private List<String> checkForExisting() {
        File videosFolder = new File(context.getExternalFilesDir(null) + "/" + HELP_VIDEOS_PATH);
        if(videosFolder.exists()) {
            Log.d(TAG, "Videos directory exists");
            existingFiles = findExistingVideos(videosFolder);
        } else {
            boolean created = videosFolder.mkdir();
            Log.d(TAG, "Created videos folder? " + created);
        }
        Log.d(TAG, "# of existing files " + existingFiles.size());
        return existingFiles;
    }

    private void assetsToCopy() {
        String[] fileList = null;
        try {
            fileList = assetManager.list(HELP_VIDEOS_PATH);
            Log.d(TAG, "number of asset files " + fileList.length);
        } catch (IOException exception) {
            Log.e(TAG, "IOException accessing Assets Folder");
        }
        if(fileList != null) {
            for(String file : fileList) {
                Log.d(TAG, "asset file " + file);
                if(!existingFiles.contains(file)) {
                    assetFiles.add(file);
                }
            }
        }
    }

    private List<String> findExistingVideos(File directory) {
        List<String> videoList = new ArrayList<>();
        String[] existingVideos = directory.list();
        for(String filename : existingVideos) {
            if(filename.contains(".mp4") || filename.contains(".avi")) {
                videoList.add(filename);
            }
        }
        Log.d(TAG, "number of existing videos " + videoList.size());
        return videoList;
    }

    public void copyVideoAssets() {
        for(String filename : assetFiles) {
                Log.d(TAG, "copying to external dir " + filename);
                CopyAssetTask copyAssetTask = new CopyAssetTask(HELP_VIDEOS_PATH, HELP_VIDEOS_PATH);
                copyAssetTask.execute(filename);
        }
    }

    class CopyAssetTask extends AsyncTask<String, String, String> {

        String origin;
        String destination;

        CopyAssetTask(String origin, String destination) {
            this.origin = origin;
            this.destination = destination;
        }

        @Override
        protected String doInBackground(String... filepath) {
            String path = filepath[0];
            AssetsUtil.copyFile(context, origin, destination, path);
            return path;
        }

        @Override
        protected void onPostExecute(String path) {
            view.notifyVideoCopied(path);
            Log.d(TAG, "Asset copied to external files directory " + path);
        }
    }
}
