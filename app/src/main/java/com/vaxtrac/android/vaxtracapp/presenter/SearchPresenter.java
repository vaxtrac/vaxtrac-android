package com.vaxtrac.android.vaxtracapp.presenter;


import android.content.Context;
import android.content.Intent;

import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.view.SearchView;

public class SearchPresenter extends Presenter {

    private Context context;
    private SearchView view;
    private boolean isOutreachSession;

    public SearchPresenter(Context context, SearchView view) {
        this.context = context;
        this.view = view;
    }

    public void launchQRSearchForm() {
        openForm(CommCareAPI.getQRSearchForm(Preferences.getSessionLocationType()));}

    public void launchFingerprintSearchForm() {
        openForm(CommCareAPI.getFingerprintSearchForm(Preferences.getSessionLocationType()));}

    public void launchDemographicSearchForm() {
        openForm(CommCareAPI.getDemographicSearchForm(Preferences.getSessionLocationType()));
    }

    public void launchVaccineCardSearchForm() {
        openForm(CommCareAPI.getCardNumberSearchForm(Preferences.getSessionLocationType()));
    }

    public void launchPhoneNumberSearchForm() {
        openForm(CommCareAPI.getPhoneSearchForm(Preferences.getSessionLocationType()));
    }

    public void launchNameSearchForm() {
        openForm(CommCareAPI.getNameSearchForm(Preferences.getSessionLocationType()));
    }

    public void openForm(Intent formIntent) {
        context.startActivity(formIntent);
    }
}
