package com.vaxtrac.android.vaxtracapp.data;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

/*
Builds a new column based on a abstract boolean test of the contents of a row.
 */

public abstract class TestCursor extends CursorWrapper {

    String column;
    int newPosition;
    private String TAG = "TestCursor";



    public TestCursor(Cursor cursor, String newColumnName) {
        super(cursor);
        this.column = newColumnName;
        newPosition = cursor.getColumnCount() +1;
    }

    //Make space for our fake column
    @Override
    public int getColumnIndex(String columnName) {
        if(columnName.equals(column)){
            return newPosition;
        }
        return super.getColumnIndex(columnName);
    }

    @Override
    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
        if(columnName.equals(column)){
            return newPosition;
        }
        return super.getColumnIndexOrThrow(columnName);
    }

    //apply our logic to the new column
    @Override
    public String getString(int columnIndex) {
        if (columnIndex != newPosition){
            try {
                return super.getString(columnIndex);
            } catch (Exception exception) {
                Log.d(TAG, exception.toString() + " ColumnIndex : " + columnIndex);
                return "";
            }
        }
        if(applyLogic()){
            return translate("True");
        }
        return translate("False");
    }

    // Logic applied to a row the decides whether the value of our new column is true or false
    public abstract boolean applyLogic();

    private String translate(String text){
        return GlobalTranslations.translate(text);
    }
}
