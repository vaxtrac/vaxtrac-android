package com.vaxtrac.android.vaxtracapp.presenter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;

import com.etsy.android.grid.StaggeredGridView;
import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.models.Dose;
import com.vaxtrac.android.vaxtracapp.models.Vial;
import com.vaxtrac.android.vaxtracapp.ocr.VialAdapter;
import com.vaxtrac.android.vaxtracapp.ocr.VialClassifier;
import com.vaxtrac.android.vaxtracapp.view.OCRImageView;

import java.util.Arrays;
import java.util.HashMap;

public class OCRImagePresenter extends Presenter {

    private static final String TAG = "OCRImagePresenter";
    Context context;
    OCRImageView view;
    VialAdapter vialAdapter;
    public static final int OCR_CODE = 3350;
    public static final String LAST_VIAL_TYPE = "last_vial_type";

    private Dose dose;

    public OCRImagePresenter(Context context, OCRImageView view) {
        Log.d(TAG, "Constructor");
        this.context = context;
        this.view = view;
        vialAdapter = new VialAdapter(context, AppController.mOCRManager.getAllVials(),
                LayoutInflater.from(context));
    }

    public void newOcrClick() {
        Intent intent = new Intent("edu.sfsu.cs.orange.ocr.OCR");
        //intent.setComponent(new ComponentName("edu.sfsu.cs.orange.ocr",
        //        "edu.sfsu.cs.orange.ocr.CaptureActivity"));
        ((MainActivity)context).startActivityForResult(intent, OCR_CODE);
    }

    public void setDose(Dose dose) {
        this.dose = dose;
    }

    public String getDoseAntigen() {
        return dose.getAntigen();
    }

    public boolean hasDose() {
        return dose != null;
    }

    public void attachVialToDose(Vial vial) {
        Log.d(TAG, "attaching vial to dose " + vial);
        dose.setVial(vial);
    }

    public void filter(String text) {
        vialAdapter.getFilter().filter(text);
    }

    public void persistTypeFilter(int selected) {
        bundle.putInt("vaccine_selected", selected);
    }

    public int restoreTypeFilter() {
        Log.d(TAG, "Bundle Vaccine Type " + bundle.getInt("vaccine_selected"));
        return bundle.getInt("vaccine_selected", -1);
    }

    public void saveLastVialSelection(int item) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(AppController.getAppContext())
                .edit();
        editor.putInt(LAST_VIAL_TYPE, item);
        editor.apply();
    }

    public void populateImages(StaggeredGridView grid) {
        grid.setAdapter(vialAdapter);
    }

    public Vial get(int position) {
        return vialAdapter.getItem(position);
    }

    public static void ocrResult(int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            int count = data.getIntExtra("count",0);
            Log.d(TAG, "Count " + data.getIntExtra("count", 0));
            Vial[] vials = new Vial[count];
            if(count > 0) {
                for(int i = 0; i < count; i++) {
                    Log.d(TAG, "Raw Text " + data.getStringExtra("raw_text_" + (i + 1)));
                    VialClassifier classifier = new VialClassifier(data.getStringExtra
                            ("raw_text_" + (i + 1)));
                    HashMap<String, String[]> ocrFields = classifier.getBestGuess();
                    Log.d(TAG, "Image Data " + Arrays.toString(data.getByteArrayExtra("image_data_" + (i + 1))));
                    Vial vial = new Vial(data.getStringExtra("raw_text_" + (i + 1)),
                            data.getByteArrayExtra("image_data_" + (i + 1)));
                    vial.bind(ocrFields);
                    vials[i] = vial;
                }
            }

            //Vial vial = new Vial(data.getExtras());
            Screens.OCRDetailScreen detailScreen = new Screens.OCRDetailScreen(vials);
            AppController.mScreen = detailScreen;

            AppController.getFlow().goTo(detailScreen);
            //AppController.mOCRManager.writeVial(vial);
            //AppController.mOCRManager.readData();
        }
    }
}
