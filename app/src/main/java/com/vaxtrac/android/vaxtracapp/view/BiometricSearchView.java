package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.biometrics.BiometricsManager;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.models.Patient;
import com.vaxtrac.android.vaxtracapp.presenter.BiometricSearchPresenter;

import java.lang.ref.WeakReference;
import java.util.Map;

public class BiometricSearchView extends LinearLayout {

    BiometricSearchPresenter presenter;
    WeakReference<MainActivity> activity;
    WeakReference<Context> mContext;
    ColoredCursorAdapter adapter = null;
    Cursor lastCursor = null;
    TextView text;
    Button button;
    LinearLayout spinnerArea;
    ProgressBar spinner;
    ListView table;
    boolean isRegistration = false;

    private final String TAG = "BiometricSearchView";

    public BiometricSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = new WeakReference<Context>(context);
        activity = new WeakReference<MainActivity>((MainActivity)mContext.get());
        presenter = new BiometricSearchPresenter(mContext.get());
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.d(TAG, "onDetachedFromWindow");
        presenter.setView(null);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d(TAG, "onAttachedToWindow");
        presenter.setView(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        presenter.setView(this);

        text = (TextView) findViewById(R.id.biometricsearch_textview);
        table = (ListView) findViewById(R.id.biometricsearch_listview);
        spinner = (ProgressBar) findViewById(R.id.biometricsearch_spinner);
        spinnerArea = (LinearLayout) findViewById(R.id.biometricsearch_spinnerspace);
        button = (Button) findViewById(R.id.biometric_search_button);
        setButtonAction();
        table.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Action for clicking a listItem from the patientList
                Cursor selected = (Cursor) adapterView.getItemAtPosition(i);
                String selectedID = selected.getString(selected.getColumnIndex("childid"));
                Log.i(TAG, String.format("Selected DocID %s", selectedID));
                Map<String, String> info = AppController.mCommCareDataManager.loadCaseFromCommCare(selectedID);
                if (info == null) {
                    Cursor c = AppController.mCommCareDataManager.patientDB.allRowsColumnEquals("docid", selectedID);
                    if (c != null && c.getCount() != 0) {
                        Toast.makeText(mContext.get(), "Found local but not CC!", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(mContext.get(), "NOT FOUND IN EITHER DB!", Toast.LENGTH_SHORT)
                                .show();
                    }
                    return;
                }
                presenter.unregisterReceivers();
                AppController.mPatient = new Patient(activity.get().getSchedule());
                AppController.mPatient.loadMap(selectedID, info);
                AppController.getFlow().goTo(new Screens.TabScreen());
                BiometricsManager.matches = null;
            }
        });
        super.onFinishInflate();
    }

    public void setLastCursor(Cursor cursor){
        lastCursor = cursor;
    }

    protected void setButtonAction(){
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.unregisterReceivers();
                AppController.mSearchObject = null;
                AppController.getFlow().goTo(new Screens.SearchScreen());
            }
        });
    }

    public void drawTable(){
        if (lastCursor != null){
            Log.d(TAG, "Drawing Table");
            adapter = new ColoredCursorAdapter(getContext(),
                    R.layout.row_biometric_searchview,
                    lastCursor,
                    new String[]{"name","relation","childname","childsex","childdob"},
                    new int[]{R.id.list_item_text1, R.id.list_item_text2, R.id.list_item_text3, R.id.list_item_text4,R.id.list_item_text5});
            table.setAdapter(adapter);
            spinnerArea.setVisibility(View.GONE);
            table.setVisibility(View.VISIBLE);
        }else{
            Log.d(TAG, "Not Drawing Table | No Cursor");
        }
    }

    public void updateSearchStatus(String status) {
        Log.d(TAG, "Updated status with" + status);
        text.setText(status);
    }

    public void noMatchFound(boolean skipMatching){
        noMatchFound();
    }

    public void noMatchFound() {
        spinner.setVisibility(View.GONE);
        text.setText(getResources().getString(R.string.no_match_found));
        button.setVisibility(View.VISIBLE);
    }
}
