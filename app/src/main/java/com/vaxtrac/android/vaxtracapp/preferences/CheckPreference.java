package com.vaxtrac.android.vaxtracapp.preferences;

import android.content.Context;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;

public class CheckPreference extends CheckBoxPreference {

    public CheckPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Preferences.addPreference(this);
    }

    public CheckPreference(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }
}
