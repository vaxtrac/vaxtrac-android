package com.vaxtrac.android.vaxtracapp.data;

import android.database.Cursor;
import android.os.AsyncTask;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager.PatientDBHandler;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.util.Calendar;

public class ChildrenRegisteredTask extends AsyncTask<Void, Void, Integer> {

    private static final String TAG = "ChildrenRegisteredTask";

    public interface TaskListener {
        void onFinished(int count);
    }

    private TaskListener listener;

    private Calendar startDate;
    private Calendar endDate;
    private String user;

    private static final DateFormat isoDateFormat =
            new UnlocalizedDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");

    public ChildrenRegisteredTask(Calendar startDate, Calendar endDate, String user) {
        this.startDate = startDate;
        if(endDate != null) {
            this.endDate = endDate;
        } else {
            this.endDate = startDate;
        }
        this.user = user;
    }

    public void setTaskListener(TaskListener listener) {
        this.listener = listener;
    }

    public String getStartDate() {
        return isoDateFormat.format(startDate.getTime());
    }

    public String getEndDate() {
        return isoDateFormat.format(endDate.getTime());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(Void[] params) {
        PatientDBHandler patientDB =
                AppController.mCommCareDataManager.patientDB;
        /* Inclusive */
        Cursor patientsRegistered = patientDB.allRowsItemsBetween(
                PatientDBHandler.DATE_OPENED_COLUMN, getStartDate(), getEndDate(), user, null);
        return patientsRegistered.getCount();
    }

    @Override
    protected void onPostExecute(Integer count) {
        super.onPostExecute(count);
        listener.onFinished(count);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
