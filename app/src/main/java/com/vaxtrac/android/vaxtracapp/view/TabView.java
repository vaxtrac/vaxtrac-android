package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.BuildConfig;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.TabPresenter;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.SimpleDateFormat;

import dateconverter.NepaliDateConverter;
import dateconverter.Date;

public class TabView extends FrameLayout {

    private static final String TAG = "TabView";

    private TabPresenter presenter;
    private Context context;

    LinearLayout provisionTab;
    LinearLayout historicalTab;
    LinearLayout demographicTab;
    LinearLayout confirmTab;

    LinearLayout childContainer;
    LinearLayout provisionLayout;
    LinearLayout historicalLayout;
    //LinearLayout demographicLayout;
    LinearLayout confirmLayout;

    ImageView provisionImage;
    ImageView historicalImage;
    ImageView demographicImage;
    ImageView confirmImage;

    TextView mobileNumber;
    TextView childName;
    TextView childDob;
    TextView village;

    public TabView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Log.d(TAG, "TabView Constructor");

        Log.d(TAG, AppController.getFlow().getBackstack()
                .current().getScreen().toString());
        /*int startTab = ((Screens.TabScreen)AppController.getFlow().getBackstack()
                .current().getScreen()).tabIndex;*/
        //Log.d(TAG, "Start Tab " + startTab);

        //if(startTab > -1) {

        //}

        this.context = context;
        presenter = new TabPresenter(context, this);
    }

    @Override
    protected void onFinishInflate() {

        Log.d(TAG, "onFinishInflate");

        provisionImage = (ImageView) findViewById(R.id.provision_image);
        historicalImage = (ImageView) findViewById(R.id.historical_image);
        confirmImage = (ImageView) findViewById(R.id.confirm_image);

        provisionTab = (LinearLayout) findViewById(R.id.provision_tab);
        historicalTab = (LinearLayout) findViewById(R.id.historical_tab);
        confirmTab = (LinearLayout) findViewById(R.id.confirm_tab);

        childContainer = (LinearLayout) findViewById(R.id.child_container);

        provisionLayout = (LinearLayout) findViewById(R.id.provision_layout);
        historicalLayout = (LinearLayout) findViewById(R.id.historical_layout);

        childName = (TextView) findViewById(R.id.child_name);
        childDob = (TextView) findViewById(R.id.child_dob);
        mobileNumber = (TextView) findViewById(R.id.mobile);
        village = (TextView) findViewById(R.id.village);

        populatePatientInfo();

        provisionTab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadView(0);
            }
        });

        historicalTab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadView(1);
            }
        });

        confirmTab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadView(3);
            }
        });

        presenter.restoreIndex();

        Log.d(TAG, AppController.getFlow().getBackstack()
                .current().getScreen().toString());

        super.onFinishInflate();



        Log.d(TAG, AppController.getFlow().getBackstack()
                .current().getScreen().toString());
    }

    public void loadDefaultChildView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.screen_vaccination, childContainer);
    }

    public void populatePatientInfo(){
        try{
            mobileNumber.setText(AppController.mPatient.attributes.get("contact_number"));
        }catch (Exception e){
            Log.e(TAG, "No mobile in attributes.");
        }
        try{
            childName.setText(AppController.mPatient.attributes.get("patient_name"));
        }catch (Exception e){
            Log.e(TAG, "No child name in attributes.");
        }
        try{
            if (BuildConfig.FLAVOR.equals("sierraleone")) {
                village.setText(AppController.mCommCareDataManager.getAllVillages().get(
                        AppController.mPatient.attributes.get("village")));
            }
            else {
                village.setText(AppController.mPatient.attributes.get("village"));
            }
        }catch (Exception e){
            Log.e(TAG, "No village in attributes.");
        }
        try{
            if(context.getResources().getConfiguration().locale.getISO3Language()
                    .equals("nep")) {
                NepaliDateConverter dateConverter = new NepaliDateConverter();
                SimpleDateFormat simpleDateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
                java.util.Date nepaliDate = simpleDateFormat.parse(AppController.mPatient.attributes
                        .get("date_of_birth"));
                SimpleDateFormat nepaliFormat = new UnlocalizedDateFormat("MM-dd-yyyy");
                String nepaliDateString = nepaliFormat.format(nepaliDate);
                Date converted = dateConverter.fromGregorianDate(nepaliDateString);
                childDob.setText(dateConverter.toNepaliString(converted));
            } else {
                childDob.setText(AppController.mPatient.attributes.get("date_of_birth"));
            }
        }catch (Exception e){
            Log.e(TAG, e.getMessage());
            Log.e(TAG, "No dob in attributes.");
        }


    }

    public void loadChildView(int index) {
        switch(index) {
            case 0:
                addHighlight(provisionTab);
                provisionImage.setBackground(context.getResources().getDrawable(R.drawable.syringe_eligible_white));
                addChildView(R.layout.screen_vaccination);
                break;
            case 1:
                addHighlight(historicalTab);
                historicalImage.setBackground(context.getResources().getDrawable(R.drawable
                        .historical_doses_white));
                addChildView(R.layout.screen_updatevaccinecard);
                break;
            case 3:
                addHighlight(confirmTab);
                confirmImage.setBackground(context.getResources().getDrawable(R.drawable.exit_white));
                addChildView(R.layout.screen_confirm);
                break;
            default:
                break;
        }
    }

    public void removeHighlight(int index) {
        switch(index) {
            case 0:
                provisionTab.setBackground(null);
                provisionImage.setBackground(context.getResources().getDrawable(R.drawable.syringe_eligible));
                break;
            case 1:
                historicalTab.setBackground(null);
                historicalImage.setBackground(context.getResources().getDrawable(R.drawable
                        .historical_doses));
                break;
            case 3:
                confirmTab.setBackground(null);
                confirmImage.setBackground(context.getResources().getDrawable(R.drawable.exit));
                break;
        }
    }

    public void addHighlight(LinearLayout tab) {
        tab.setBackground(context.getResources().getDrawable(R.color.Orange));
    }

    public void addChildView(int resource) {
        childContainer.removeAllViews();
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        inflater.inflate(resource, childContainer);
    }

    public TabPresenter getPresenter() {
        return this.presenter;
    }
}
