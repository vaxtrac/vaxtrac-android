package com.vaxtrac.android.vaxtracapp.presenter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.models.Dose;
import com.vaxtrac.android.vaxtracapp.models.Dose.Status;
import com.vaxtrac.android.vaxtracapp.models.Schedule;
import com.vaxtrac.android.vaxtracapp.models.VaccineHistory;
import com.vaxtrac.android.vaxtracapp.view.ConfirmView;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ConfirmPresenter extends Presenter {

    private ConfirmView view;
    private Context context;
    private final String TAG = "ConfirmPresenter";

    public ConfirmPresenter(Context context, ConfirmView view) {
        this.context = context;
        this.view = view;
    }

    public void fill_table(TableLayout tableIn, LayoutInflater inflater){
        Schedule schedule = AppController.mPatient.getSchedule();
        if(schedule == null) {
            schedule = ((MainActivity) context).getSchedule();
            if(schedule == null) {
                Log.d(TAG, "MainActivity Schedule is null");
            }
        }
        VaccineHistory history = AppController.mPatient.getHistory();
        List<String> antigens = schedule.requiredVaccines();
        Map<Integer, String> allDoses = schedule.getAgeSlots();

        LinearLayout row = new LinearLayout(context);
        inflater.inflate(R.layout.vaccine_table_row_horizontal, row);
        LinearLayout spots = (LinearLayout) row.findViewById(R.id.vaccine_table_row_space);
        Dose titleDose = new Dose("");
        View titleDoseView = titleDose.inflateDoseName(context, inflater, true);
        titleDoseView.setVisibility(View.INVISIBLE);
        spots.addView(titleDoseView);

        for (String intervalName : allDoses.values()){
            spots.addView(Dose.getHeaderSlimDose(context, inflater, intervalName));
        }
        tableIn.addView(row);
        for(String antigen:antigens){
            row = new LinearLayout(context);
            inflater.inflate(R.layout.vaccine_table_row_horizontal, row);
            spots =(LinearLayout) row.findViewById(R.id.vaccine_table_row_space);
            List<Dose> doses = history.getComputedSchedule().get(antigen);
            if (doses == null || history.wasUpdated()){;
                doses = schedule.compareDoses(history, antigen);
                history.getComputedSchedule().put(antigen, doses);
            }
            titleDose = new Dose(GlobalTranslations.translate(antigen));
            titleDoseView = titleDose.inflateDoseName(context, inflater, true);
            spots.addView(titleDoseView);
            List<String> requiredDoseNames = schedule.requiredVaccinationsForAntigen(antigen);
            Map<Integer, Dose>  requiredDoses = new LinkedHashMap<>();
            int c = 0;
            for(String name: requiredDoseNames){
                Dose dose = new Dose(antigen, c);
                dose.setStatus(Status.future);
                requiredDoses.put(c, dose);
            }
            for(Dose dose: doses){
                requiredDoses.put(dose.getDoseNumber(), dose);
            }
            int validDoses = 0;
            int usedSpots = 0;
            for(Dose d: requiredDoses.values()){
                if (d.getStatus() != Status.received_not_valid) {
                    View dose = d.inflateCardDose(context, inflater);
                    int currentDoseSpot;
                    try{
                        currentDoseSpot = schedule.cardColumnForDoseNumber(antigen, d.getDoseNumber());
                    }catch (Exception e){
                        currentDoseSpot = 0;
                        Log.e(TAG, "Exception for " + antigen + " dose number " + d.getDoseNumber
                                (), e);
                    }
                    while(usedSpots < currentDoseSpot){
                        spots.addView(Dose.getBlankSlimDose(context, inflater));
                        usedSpots+=1;
                    }
                    usedSpots +=1;
                    spots.addView(dose);
                }
                if(d.getStatus() == Status.eligible){
                    titleDose.updateBackgroundDrawable(context.getResources().getDrawable((R.drawable.grad_dose_orange)));
                }
                if (d.getStatus() == Status.received_valid){
                    validDoses+=1;
                }
            }
            while(usedSpots < allDoses.size()) {
                spots.addView(Dose.getBlankSlimDose(context, inflater));
                usedSpots += 1;
            }
            if(validDoses >= requiredDoses.size()){
                titleDose.updateBackgroundDrawable(context.getResources().getDrawable((R.drawable.grad_dose_green)));
            }
            tableIn.addView(row);

        }
    }
}
