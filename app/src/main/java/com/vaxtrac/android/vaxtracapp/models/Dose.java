package com.vaxtrac.android.vaxtracapp.models;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.AppVersion;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.ocr.OCRImagesDialog;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import dateconverter.NepaliDateConverter;

public class Dose {

    private String antigen;
    public  Status status = null;
    public Type type = null;
    public Date date = null;
    public int doseNumber;
    private String label = null;
    private boolean changedInSession = false;
    private WeakReference<View> baseView;

    private Vial vial;

    private AlertDialog OCRAlertDialog;
    public static final int OCR_CODE = 3350;

    private final String TAG = "Dose";

    public Dose(String antigenName) {
        antigen = antigenName;
    }

    public Dose (String antigenName, int doseNumber) {
        this(antigenName);
        this.doseNumber = doseNumber;
    }

    public Dose (String antigenName, int doseNumber, Date date, Status status) {
        this.antigen = antigenName;
        this.doseNumber = doseNumber;
        this.date = date;
        this.status = status;
        this.type = Type.scheduled;

    }

    public Dose (String antigenName, int doseNumber, Date date, Status status, Type type){
        this(antigenName, doseNumber, date, status);
        this.type = type;
    }

    public String getLabel(){
        if (label == null){
            try{
                label = GlobalTranslations.translate(antigen);
                return label;
            }catch (Exception e){
                return antigen;
            }

        }
        return label;
    }

    public static View getBlankSlimDose(Context context, LayoutInflater inflater) {
        LinearLayout base = new LinearLayout(context);
        base.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams
                .MATCH_PARENT, 1));
        inflater.inflate(R.layout.dose_view_slim_empty, base);
        return base;
    }

    public static View getHeaderSlimDose(Context context, LayoutInflater inflater, String titleText) {
        LinearLayout base = new LinearLayout(context);
        base.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams
                .MATCH_PARENT, 1));
        inflater.inflate(R.layout.dose_view_slim, base);
        LinearLayout bottom_layout = (LinearLayout) base.findViewById(R.id.dose_base_layout);
        TextView title = (TextView) base.findViewById(R.id.dose_label_top);
        TextView body = (TextView) base.findViewById(R.id.dose_label_body);
        title.setText(titleText);
        body.setVisibility(View.GONE);
        bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.btn_dark_shape));
        return base;
    }


    public enum Status{
        eligible,
        received_valid,
        received_not_valid,
        next_valid,
        missed_booster,
        future
    }

    public enum Type{
        birth_booster,
        other_booster,
        scheduled
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return this.status;
    }

    public int getDoseNumber() {
        return this.doseNumber;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return this.date;
    }

    public View inflateDoseName(Context context, LayoutInflater inflater){
        return inflateDoseName(context, inflater, false);
    }

    public View inflateDoseName(Context context, LayoutInflater inflater, boolean slimTitle){
        LinearLayout base = new LinearLayout(context);
        if(slimTitle){
            inflater.inflate(R.layout.dose_title_slim, base);

        }else{
            inflater.inflate(R.layout.dose_title, base);
        }
        LinearLayout bottom_layout = (LinearLayout) base.findViewById(R.id.dose_base_layout);
        baseView = new WeakReference<View>(bottom_layout);
        TextView title = (TextView) base.findViewById(R.id.dose_label_top);
        title.setText(getLabel());
        return base;
    }

    public View inflateCardDose(Context context, LayoutInflater inflater) {
        LinearLayout base = new LinearLayout(context);
        base.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams
                .MATCH_PARENT, 1));
        inflater.inflate(R.layout.dose_view_slim, base);
        LinearLayout bottom_layout = (LinearLayout) base.findViewById(R.id.dose_base_layout);
        TextView title = (TextView) base.findViewById(R.id.dose_label_top);
        TextView body = (TextView) base.findViewById(R.id.dose_label_body);

        Schedule schedule = AppController.mPatient.getSchedule();
        VaccineHistory history = AppController.mPatient.getHistory();
        try {
            title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
        } catch (Exception e) {
            title.setText(doseNumber);
            Log.d(TAG, String.format("Couldn't pretty up format | %s", e.toString()));
        }
        switch(status){
            case received_valid:
                //For redraws, on rotation / on return to screen
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                if (history.isNewReported(this)) {
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.calendar_dose_given_green));
                }else if (history.isNewRecorded(this)){
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_received_needle_only));
                    title.setVisibility(View.GONE);
                    body.setVisibility(View.GONE);
                }
                else{
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_green));
                }
                break;
            case eligible:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_orange));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setVisibility(View.GONE);
                break;
            case future:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.btn_dark_shape));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                break;
            case missed_booster:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_very_dark));
                title.setText(schedule.getBoosterName(antigen));
                body.setText(dateToPrettyString(date, context));
                body.setVisibility(View.GONE);
                break;
            default:
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                break;
        }
        return base;
    }

    public View inflateCallbackDose(Context context, LayoutInflater inflater, Patient mPatient) {
        LinearLayout base = new LinearLayout(context);
        base.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams
                .MATCH_PARENT, 1));
        inflater.inflate(R.layout.dose_view_slim, base);
        LinearLayout bottom_layout = (LinearLayout) base.findViewById(R.id.dose_base_layout);
        TextView title = (TextView) base.findViewById(R.id.dose_label_top);
        TextView body = (TextView) base.findViewById(R.id.dose_label_body);

        Schedule schedule = mPatient.getSchedule();
        VaccineHistory history = mPatient.getHistory();

        try {
            title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
        } catch (Exception e) {
            title.setText(doseNumber);
            Log.d(TAG, String.format("Couldn't pretty up format | %s", e.toString()));
        }
        switch(status){
            case received_valid:
                //For redraws, on rotation / on return to screen
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                if (history.isNewReported(this)) {
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.calendar_dose_given_green));
                }else if (history.isNewRecorded(this)){
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_received_needle_only));
                    title.setVisibility(View.GONE);
                    body.setVisibility(View.GONE);
                }
                else{
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_green));
                }
                break;
            case eligible:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_orange));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setVisibility(View.GONE);
                break;
            case future:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.btn_dark_shape));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                break;
            case missed_booster:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_very_dark));
                title.setText(schedule.getBoosterName(antigen));
                body.setText(dateToPrettyString(date, context));
                body.setVisibility(View.GONE);
                break;
            default:
                Log.d(TAG, "default status");
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                break;
        }
        return base;
    }

    public View inflateHistoricDose(Context context, LayoutInflater inflater){
        LinearLayout base = new LinearLayout(context);
        inflater.inflate(R.layout.dose_view, base);
        LinearLayout bottom_layout = (LinearLayout) base.findViewById(R.id.dose_base_layout);
        TextView title = (TextView) base.findViewById(R.id.dose_label_top);
        TextView body = (TextView) base.findViewById(R.id.dose_label_body);
        ImageView undoIcon = (ImageView) base.findViewById(R.id.dose_label_remove);

        Schedule schedule = AppController.mPatient.getSchedule();
        final VaccineHistory history = AppController.mPatient.getHistory();

        try {
            title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
        }catch (Exception e){
            title.setText(doseNumber);
            Log.d(TAG, String.format("Couldn't pretty up format | %s", e.toString()));
        }
        body.setText(dateToPrettyString(date, context));
        if (history.isNewReported(this)){
            Log.e(TAG, "New Dose!");
            baseView = new WeakReference<View>(base);
            final Dose dose = this;
            base.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "Removing dose from view");
                    history.removeReportedDose(antigen, dose);
                    baseView.get().setVisibility(View.GONE);
                }
            });
        }
        switch(status){
            case received_valid:
                //For redraws, on rotation / on return to screen
                if (history.isNewReported(this)) {
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.calendar_dose_given_green));
                    undoIcon.setVisibility(View.VISIBLE);
                }else {
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_green));
                }
                body.setText(dateToPrettyString(date, context));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                break;
            case received_not_valid:
                if (history.isNewReported(this)) {
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.calendar_dose_given_yellow));
                    undoIcon.setVisibility(View.VISIBLE);
                }else {
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_yellow));
                }
                title.setText(GlobalTranslations.translate("Invalid"));
                body.setText(String.format("%s : %s", schedule.nameOfDoseNumber(antigen, doseNumber), dateToPrettyString(date, context)));
                break;
        }
        return base;
    }

    public View inflateDose(Context context, LayoutInflater inflater){
        LinearLayout base = new LinearLayout(context);
        inflater.inflate(R.layout.dose_view, base);
        LinearLayout bottom_layout = (LinearLayout) base.findViewById(R.id.dose_base_layout);
        TextView title = (TextView) base.findViewById(R.id.dose_label_top);
        TextView body = (TextView) base.findViewById(R.id.dose_label_body);

        Schedule schedule = AppController.mPatient.getSchedule();
        VaccineHistory history = AppController.mPatient.getHistory();

        //TODO put back...
        switch(status){
            case eligible:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_orange));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                baseView = new WeakReference<View>(bottom_layout);
                break;
            case received_valid:
                //For redraws, on rotation / on return to screen
                if (history.isNewRecorded(this)){
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_received_green));
                    bottom_layout.setPadding(0, 0, 0, 0);
                    body.setText("");
                    baseView = new WeakReference<View>(bottom_layout);
                }
                else{
                    bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_green));
                    body.setText(dateToPrettyString(date, context));
                }
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                break;
            case received_not_valid:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_yellow));
                title.setText(GlobalTranslations.translate("Invalid"));
                body.setText(String.format("%s |%s | %s", doseNumber, schedule.nameOfDoseNumber(antigen, doseNumber), dateToPrettyString(date, context)));
                break;
            case next_valid:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_grey));
                title.setText(GlobalTranslations.translate("Next"));
                body.setText(dateToPrettyString(date, context));
                break;
            case future:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.btn_dark_shape));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                break;

            case missed_booster:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_very_dark));
                title.setText(schedule.getBoosterName(antigen));
                body.setText(dateToPrettyString(date, context));
                body.setVisibility(View.GONE);
                break;

            default:
                bottom_layout.setBackground(context.getResources().getDrawable(R.drawable.grad_dose_grey));
                title.setText(schedule.nameOfDoseNumber(antigen, doseNumber));
                body.setText(dateToPrettyString(date, context));
                break;
        }
        return base;
    }

    private String dateToPrettyString(Date d, Context context){

        try {
            String date = "noDate";
            Locale current = context.getResources().getConfiguration().locale;

            if (current.getISO3Language().equals("nep")) {
                Calendar myCal = new GregorianCalendar();
                myCal.setTime(d);

                NepaliDateConverter conv = new NepaliDateConverter();
                dateconverter.Date nepDate = conv.fromGregorianDate((new dateconverter.Date(myCal.get(Calendar.MONTH)+1,
                        myCal.get(Calendar.DAY_OF_MONTH), myCal.get(Calendar.YEAR))).toString());
                date = conv.toNepaliString(nepDate, true);
            }
            else {
                DateFormat f = new SimpleDateFormat("LLL dd");
                date = f.format(d);
                //Log.d(TAG, "Real Date: " + d.toString() + " | Reported Date: " + date);
            }
            return date;
        }catch (Exception e){
            Log.d(TAG, "Exception formatting date " + e.getMessage());
            return "noDate";
        }

    }

    private String dateToPrettyString(Date d, boolean showYear, Context context){
        if (!showYear){
            return dateToPrettyString(d, context);
        }
        try {
            String date = "noDate";
            Locale current = context.getResources().getConfiguration().locale;

            if (current.getISO3Language().equals("nep")) {
                Calendar myCal = new GregorianCalendar();
                myCal.setTime(d);

                NepaliDateConverter conv = new NepaliDateConverter();
                dateconverter.Date nepDate = conv.fromGregorianDate((new dateconverter.Date(myCal.get(Calendar.MONTH)+1,
                        myCal.get(Calendar.DAY_OF_MONTH), myCal.get(Calendar.YEAR))).toString());
                date = conv.toNepaliString(nepDate, true);
            }
            else {
                DateFormat f = new SimpleDateFormat("LLL dd yyyy");
                date = f.format(d);
                //Log.d(TAG, "Real Date: " + d.toString() + " | Reported Date: " + date);
            }
            return date;
        }catch (Exception e){
            return "NoDate";
        }

    }

    public View.OnClickListener registerRowApplyListener(final LinearLayout row, final Dose title, final Context context){

        final Dose eligible = this;
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                /* Vial Chooser */
                if(AppVersion.isEnabled("ocr")){
                    chooseVial(context);
                }
                title.baseView.get().setBackground(context.getResources().getDrawable(R.drawable
                        .grad_dose_received));
                baseView.get().setBackground(context.getResources().getDrawable(R.drawable
                        .grad_dose_received_green));
                baseView.get().setPadding(0,0,0,0);
                TextView body = (TextView) eligible.baseView.get().findViewById(R.id
                        .dose_label_body);
                body.setText("");
                try{
                    AppController.mPatient.getHistory().recordDose(antigen, eligible);
                    row.setOnClickListener(registerRowUnApplyListener(row, title, context));
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        };
        return listener;
    }

    public void chooseVial(final Context context) {
        showVialDialog(context, Dose.this);
        /*
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Acquire Vial Image From:");
        String[] choices = new String[]{"Existing Image", "Camera Image"};
        builder1.setItems(choices, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int button) {
                switch(button) {
                    case 0:
                        Log.d(TAG, "Existing Image");

                        break;
                    case 1:
                        Log.d(TAG, "Camera Image");
                        Intent intent = new Intent("edu.sfsu.cs.orange.ocr.OCR");
                        ((Activity)context).startActivityForResult(intent, OCR_CODE);
                }
            }
        });

        OCRAlertDialog = builder1.create();
        OCRAlertDialog.show();
        */
    }

    public void showVialDialog(final Context context, Dose dose) {
        OCRImagesDialog vialDialog = new OCRImagesDialog(context, dose);
        //vialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View view = inflater.inflate(R.layout
                .ocr_dialog_picker, null);
        vialDialog.setView(view);
        vialDialog.setButton(Dialog.BUTTON_POSITIVE, context.getResources().getText(R.string
                        .attach),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "dialog click");
                OCRImagesDialog.linkVial();
            }
        });
        vialDialog.show();
    }

    public void setVial(Vial vial) {
        Log.d(TAG, "setting vial on Dose " + this);
        this.vial = vial;
    }

    public boolean hasVial() {
        return this.vial != null;
    }

    public Vial getVial() {
        return vial;
    }

    public View.OnClickListener registerRowUnApplyListener(final LinearLayout row, final Dose title, final Context context){

        final Dose eligible = this;
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                title.baseView.get().setBackground(context.getResources().getDrawable(R.drawable
                        .grad_dose_orange));
                baseView.get().setBackground(context.getResources().getDrawable(R.drawable
                        .grad_dose_orange));
                TextView body = (TextView) baseView.get().findViewById(R.id.dose_label_body);
                body.setText(dateToPrettyString(date, context));
                try{
                    AppController.mPatient.getHistory().removeRecordedDose(antigen, eligible);
                    row.setOnClickListener(registerRowApplyListener(row, title, context));
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        };
        return listener;
    }

    public void updateBackgroundDrawable(Drawable background){
        try{
            baseView.get().setBackground(background);
        }catch (Exception e){

        }
    }

    public String getAntigen() {
        return antigen;
    }


    @Override
    public String toString() {
        if (date!=null)
            return  antigen + doseNumber + " " + date.toString() + " " + status;
        else
            return  antigen + doseNumber + " noDate " + status;
    }

}