package com.vaxtrac.android.vaxtracapp.models;

import com.google.gson.annotations.JsonAdapter;
import com.vaxtrac.android.vaxtracapp.data.ReportTypeAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonAdapter(ReportTypeAdapter.class)
public class ReportData {
    /* penta_0, penta_1, penta_2 */
    List<String> antigensSeries;
    Map<String, List<Integer>> data;

    public ReportData() {
        antigensSeries = new ArrayList<>();
        data = new HashMap<>();
    }

    public void addAntigen(String seriesKey) {
        antigensSeries.add(seriesKey);
    }

    public void addSeries(String seriesKey, List<Integer> doseCounts) {
        data.put(seriesKey, doseCounts);
    }

    public Map<String, List<Integer>> getData() {
        return data;
    }
}
