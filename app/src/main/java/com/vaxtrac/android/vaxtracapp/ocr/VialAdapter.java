package com.vaxtrac.android.vaxtracapp.ocr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.models.Vial;

import java.io.ByteArrayInputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class VialAdapter extends ArrayAdapter<Vial> implements Filterable {

    public static final String TAG = "VialAdapter";

    Context context;
    Integer[] imageIds = {R.drawable.ocr,R.drawable.ocr_1,R.drawable.ocr_1,R.drawable.ocr,
            R.drawable.ocr, R.drawable.ocr_1};
    ArrayList<Vial> vials;
    ArrayList<Vial> filteredVials;
    LayoutInflater inflater;
    String[] vaccineArray;
    String[] vaccineArrayLocal;

    public VialAdapter(Context context, ArrayList<Vial> items, LayoutInflater inflater) {
        super(context, 0, items);
        this.context = context;
        this.vials = items;
        this.filteredVials = items;
        this.inflater = inflater;

        vaccineArray = context.getResources().getStringArray(R.array.antigen_array_en);
        vaccineArrayLocal = context.getResources().getStringArray(R.array.antigen_array);
    }

    public int getCount()
    {
        return filteredVials.size();
    }

    public Vial getItem(int position)
    {
        return filteredVials.get(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView " + position + " " + convertView);
        ViewHolder viewHolder;
        Vial vial = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.ocr_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (DynamicHeightImageView) convertView.findViewById(R.id
                    .vial_image);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.vial_caption);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        BitmapWorkerTask bitmapWorkerTask = new BitmapWorkerTask(viewHolder.imageView);
        bitmapWorkerTask.execute(vial.getImageData());

        /* Ugly UI Thread
        Bitmap bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(vial.getImageData()));
        if(bitmap != null) {
            viewHolder.imageView.setImageBitmap(bitmap);
        } else {
            Log.d(TAG, "Bitmap is null");
        } */

        viewHolder.textView.setText(context.getResources().getText(R.string.vaccine_name) + " : " + translateVaccineType(vial.getVaccineType()) + "\n" +
                                    context.getResources().getText(R.string.used_doses_label) + "" +
                " : " + vial.getUsedDoses());

        return convertView;
    }

    private String translateVaccineType(String vaccineType) {
        Log.d(TAG, "Vaccine Type : " + vaccineType);
        for (int i = 0; i < vaccineArray.length; i++) {
            Log.d(TAG, "Vaccine Item : " + vaccineArray[i]);
            if (vaccineType.equals(vaccineArray[i])) {
                return vaccineArrayLocal[i];
            }
        }
        Log.d(TAG, "No translation found for " + vaccineType);
        return vaccineType;
    }


    class BitmapWorkerTask extends AsyncTask<byte[], Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<>(imageView);
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(byte[]... params) {
            byte[] imageData = params[0];
            return BitmapFactory.decodeStream(new ByteArrayInputStream(imageData));
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filteredResults = new FilterResults();
                ArrayList<Vial> filteredResultsData = new ArrayList<>();

                if(constraint != null && constraint.length() > 0)  {
                    for(Vial vial : vials) {
                        Log.d(TAG, "Constraint " + constraint);
                        Log.d(TAG, "Vial Vaccine Type " + vial.getVaccineType());
                        if(vial.getVaccineType().equalsIgnoreCase(constraint.toString())) {
                            filteredResultsData.add(vial);
                        }
                    }
                    filteredResults.values = filteredResultsData;
                    filteredResults.count = filteredResultsData.size();
                } else {
                    filteredResults.values = vials;
                    filteredResults.count = vials.size();
                }
                return filteredResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredVials = (ArrayList<Vial>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    private static class ViewHolder {
        public DynamicHeightImageView imageView;
        public TextView textView;
    }
}
