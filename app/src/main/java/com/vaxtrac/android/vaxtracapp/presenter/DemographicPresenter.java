package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.data.PatientsCountHandler;
import com.vaxtrac.android.vaxtracapp.models.Patient;
import com.vaxtrac.android.vaxtracapp.view.DemographicView;

public class DemographicPresenter extends Presenter {
    private DemographicView view;
    private Context context;

    private static final String TAG = "DemographicPresenter";

    private static final String PATIENT_CASE_ID = "patient_case_id";
    private static final String CONTACT_NUMBER = "contact_number";
    private static final String SEX = "sex";
    private Patient lastPatient;

    public DemographicPresenter(Context context, DemographicView view) {
        this.context = context;
        this.view = view;
        if(AppController.mPatient != null){
            lastPatient = AppController.mPatient;
        }else {
            lastPatient = AppController.mFormSessionManager.getLastPatient();
        }
    }

    public void editPatient() {
        context.startActivity(CommCareAPI.getPatientEditForm(lastPatient.attributes.get
                ("case_id")));
    }

    public void editGuardian() {
        EditGuardianPickerPresenter.setChildID(lastPatient.attributes.get("case_id"));
        AppController.getFlow().goTo(new Screens.EditGuardianPickerScreen(lastPatient.attributes.get("case_id")));
    }

    public void addGuardian() {
        context.startActivity(CommCareAPI.getAddGuardianForm(lastPatient.attributes.get
                ("case_id")));
    }

    public void addQRCode() {
        context.startActivity(CommCareAPI.getAddQRForm(lastPatient.attributes.get
                ("case_id")));
    }

    public void exit(){
        lastPatient = null;
        if (AppController.getFlow() == null) {
            Log.d(TAG, "AppController Flow is null");
            if(((MainActivity)context).getFlow() ==  null) {
                Log.d(TAG, "MainActivity Flow is null");
            }
            ((MainActivity) context).getFlow().resetTo(new Screens.HomeScreen());
        } else {
            AppController.getFlow().resetTo(new Screens.HomeScreen());
        }
    }
}