package com.vaxtrac.android.vaxtracapp.models;

import android.os.Bundle;

import org.json.JSONObject;

public class ReportCase extends CommCareCase {

    public static final String TYPE = "monthly-report";
    /* Try to avoid conflicts with other case properites */
    public static final String REPORT_PREFIX = "report_";

    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String DATE_START = "date_start";
    public static final String DATE_END = "date_end";
    public static final String PERIOD = "period";
    public static final String CATEGORIES = "categories";
    public static final String REPORT_DATA = "data";
    public static final String ARCHIVED = "archived";
    public static final String REPORT_STATUS = "status";
    public static final String REVISIONS = "revisions";
    public static final String LAST_DOSE_TIMESTAMP = "last_dose_timestamp";
    public static final String LAST_SENT = "last_sent";

    public static String[] UPDATE_PROPERTIES = {
            YEAR,
            MONTH,
            DATE_START,
            DATE_END,
            PERIOD,
            CATEGORIES,
            REPORT_DATA,
            ARCHIVED,
            REPORT_STATUS,
            REVISIONS,
            LAST_DOSE_TIMESTAMP,
            LAST_SENT
    };

    private DoseReport report;

    public ReportCase(DoseReport report, boolean isNewCase) {
        this.report = report;
        this.isNewCase = isNewCase;
    }

    public ReportCase(String caseId) {
        super(caseId);
    }

    public ReportCase(JSONObject jsonObject) {
        super(jsonObject);
    }

    public Bundle exportToBundle() {
        return report.getProperties();
    }

    public void makeCreateBlock() {

    }

    public void makeUpdateBlock(boolean isNew) {
        if(isNew) {
            // exclude create field updates (case name, case type, owner id)
        } else {
            ;
        }
    }

    public void putStringWithPrefix(String key, String value) {
        formBundle.putString(REPORT_PREFIX + key, value);
    }
}
