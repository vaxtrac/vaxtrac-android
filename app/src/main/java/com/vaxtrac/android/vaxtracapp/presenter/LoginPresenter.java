package com.vaxtrac.android.vaxtracapp.presenter;


import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.view.LoginView;

public class LoginPresenter extends Presenter {
    private static final String TAG = "LoginPresenter";

    private LoginView view;

    private Context context;
    private MainActivity activity;
    private CommCareAPI commCareAPI;

    private static final int MAX_FAILURE_THRESHOLD = 3;

    private int failedAttempts = 0;

    private boolean requestedKeys = false;

    private static String username = "";

    public LoginPresenter(Context context) {
        this.context = context;
        activity = (MainActivity) context;
        commCareAPI = AppController.commCareAPI;
        commCareAPI.setLoginPresenter(this);

        if(!checkForAuth()) {
            Log.d(TAG, "View Request Keys");
            requestKeys();
        }
    }

    public static String getClinicName() {
        return username;
    }

    public void setView(LoginView view) {
        this.view = view;
    }

    public boolean checkForAuth() {
        return commCareAPI.hasKeys();
    }

    public void checkPermissions() {
        if(!activity.hasPermissions()) {
            activity.requestPermissions();
        }
    }

    public void requestKeys() {
        Log.d(TAG, "Requesting Keys");
        if(!requestedKeys) {
            requestedKeys = true;
            commCareAPI.requestKeys();
        }
    }

    public void loginClicked() {

        if(failedAttempts >= MAX_FAILURE_THRESHOLD) {
            /* keys could be stale and commcare could ignore login broadcast */
            Log.d(TAG, "Refreshing Keys");
            failedAttempts = 0;
            commCareAPI.requestKeys();
        } else {
            username = view.getUsername();
            String password = view.getPassword();
            if (username.isEmpty()) {
                view.focusUsername(context.getString(R.string.user_empty_warning));
                return;
            }
            if (password.isEmpty()) {
                view.focusPassword(context.getString(R.string.password_empty_warning));
                return;
            }

            commCareAPI.login(username, password);
            view.setProgressVisibility(ProgressBar.VISIBLE);
        }
    }

    public void loginFinished() {
        Log.d(TAG, "finishing login");
        view.setProgressVisibility(ProgressBar.INVISIBLE);
        /* Go up to Home so Login is removed from the stack */
        AppController.getFlow().replaceTo(new Screens.HomeScreen());
    }

    public void timeout() {
        failedAttempts++;
        Log.d(TAG, "failed Attempts " + failedAttempts);
        view.setProgressVisibility(ProgressBar.INVISIBLE);
        view.displayToast(context.getString(R.string.login_error));
    }

    public boolean isEmpty(String field) {
        return field.equals("");
    }

    public boolean hasLastUser() {
        return commCareAPI.hasLastUser();
    }

    public String getLastUser() {
        return commCareAPI.getLastUser();
    }

}
