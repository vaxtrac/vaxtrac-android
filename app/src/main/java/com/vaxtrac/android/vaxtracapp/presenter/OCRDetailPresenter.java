package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.models.Vial;
import com.vaxtrac.android.vaxtracapp.ocr.OCRDBHelper;
import com.vaxtrac.android.vaxtracapp.view.OCRDetailView;

import java.io.ByteArrayInputStream;
import java.lang.ref.WeakReference;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import info.hoang8f.widget.FButton;

public class OCRDetailPresenter extends Presenter {

    private static final String TAG = "OCRDetailPresenter";

    WeakReference<Context> context;
    OCRDetailView ocrDetailView;
    LayoutInflater inflater;
    Vial[] vials;
    String[] requiredVaccines;
    List<String> requiredList;
    ArrayAdapter<CharSequence> vaccineAdapter;
    NumberFormat nepaliNumberFormat = NumberFormat.getInstance();
    NumberFormat englishNumberFormat = NumberFormat.getInstance(Locale.US);

    public OCRDetailPresenter(Context context, OCRDetailView view, LayoutInflater inflater) {
        this.context = new WeakReference<Context>(context);
        this.ocrDetailView = view;
        this.inflater = inflater;
    }

    public void setRequired(List<String> required) {
        requiredList = required;
    }

    public void setVial(Vial[] vials) {
        Log.d(TAG, "setting vials");
        if(vials.length != 0) {
            this.vials = vials;
        }
        else {
            Log.i(TAG, "No Vial Objects");
        }
    }

    public void loadVials() {
        for(int index = 0; index < vials.length ; index++) {
            loadVialDetail(vials[index], index);
        }
    }

    public ArrayAdapter<CharSequence> getVaccineAdapter() {
        if(vaccineAdapter == null) {
            vaccineAdapter = ArrayAdapter.createFromResource(context.get(), R.array.antigen_array,
                    R.layout.antigen_spinner_dropdown_item);
        }
        return vaccineAdapter;
    }

    public int getVaccinePosition(String key) {
        if(key == null) {
            return 0;
        }
        String[] vaccineArray = context.get().getResources().getStringArray(R.array
                .antigen_array_en);
        for (int i = 0; i < vaccineArray.length; i++) {
            if(key.equalsIgnoreCase(vaccineArray[i])) {
                return i;
            }
        }
        return 0;
    }

    public int parseToNepaliNumber(int num) {
        int result;
        if (context.get().getResources().getConfiguration().locale.getISO3Language().equals
                ("nep")) {
            try {
                result = nepaliNumberFormat.parse(String.valueOf(num)).intValue();
            } catch (ParseException e) {
                Log.d(TAG, "Failed to Parse Nepali Number");
                result = num;
            }
            Log.d(TAG, "Nepali Number Format " + result);
            return result;
        } else {
            return num;
        }
    }

    public void loadVialDetail(final Vial vial, final int index) {

        final LinearLayout vialLayout = new LinearLayout(context.get());

        inflater.inflate(R.layout.ocr_vial, vialLayout);

        ImageView vialImage = (ImageView) vialLayout.findViewById(R.id.vial_image);
        if(vial == null) {
            Log.d(TAG, "Vial is null");
        }
        if(vial.getImageData() != null) {
            Log.d(TAG, "Vial Image " + Arrays.toString(vial.getImageData()));
        } else {
            Log.d(TAG, "No vial image data");
        }
        Bitmap bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(vial.getImageData()));
        if(bitmap != null) {
            vialImage.setImageBitmap(bitmap);
        } else {
            Log.d(TAG, "Bitmap is null");
        }

        final EditText maximumDoses = (EditText) vialLayout.findViewById(R.id.max_doses);

        if(vial.getMaxDoses() == 0) {
            Log.d(TAG, "MaxDoses is zero");
            maximumDoses.setText(String.valueOf(1));
        } else {
            Log.d(TAG, "MaxDoses is " + vial.getMaxDoses());
            maximumDoses.setText(String.valueOf(vial.getMaxDoses()));
        }

        Spinner vaccineType = (Spinner) vialLayout.findViewById(R.id.vaccine_type);
        vaccineType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int median = AppController.mOCRManager.getMedianValue
                        (OCRDBHelper.MAXIMUM_DOSES,
                                ocrDetailView.getVaccineType(vialLayout));
                maximumDoses.setText(median != 0 ? "" + median : "1");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        vaccineType.setAdapter(getVaccineAdapter());
        Log.d(TAG, "vaccine type " + vial.getVaccineType());
        String vialType = vial.getVaccineType();
        int selectedVial = vialType != null ? getVaccinePosition(vialType) : PreferenceManager.getDefaultSharedPreferences
                (AppController.getAppContext()).getInt(OCRImagePresenter.LAST_VIAL_TYPE, 0);
        vaccineType.setSelection(selectedVial);
        vaccineType.setFocusable(true);
        vaccineType.setFocusableInTouchMode(true);
        vaccineType.setSelected(true);

        EditText lotNumber = (EditText) vialLayout.findViewById(R.id.lot_number);
        lotNumber.setText(vial.getLotNumber());

        EditText batchNumber = (EditText) vialLayout.findViewById(R.id.batch_number);
        batchNumber.setText(vial.getBatchNumber());

        /*
        DatePicker manufactureDate = new DatePicker(context);
        manufactureDate.setTag("manufacture_date");
        manufactureDate.init(2000,0,1,null);
        vialLayout.addView(manufactureDate);

        DatePicker expirationDate = new DatePicker(context);
        expirationDate.setTag("expiration_date");
        vialLayout.addView(expirationDate);
        */

        DatePicker manufactureDate = (DatePicker) vialLayout.findViewById(R.id.manufacture_date);
        if(vial.getManufactureDate() != null) {
            Calendar mfgCal = Calendar.getInstance();
            mfgCal.clear();
            mfgCal.setTime(vial.getManufactureDate());
            manufactureDate.clearFocus();
            manufactureDate.init(mfgCal.get(Calendar.YEAR), mfgCal.get(Calendar.MONTH),
                    mfgCal.get(Calendar.DAY_OF_MONTH), null);
            manufactureDate.clearFocus();
        }

        DatePicker expirationDate = (DatePicker) vialLayout.findViewById(R.id.expiration_date);
        if(vial.getExpirationDate() != null) {
            Calendar expCal = Calendar.getInstance();
            expCal.clear();
            expCal.setTime(vial.getExpirationDate());
            expirationDate.init(expCal.get(Calendar.YEAR), expCal.get(Calendar.MONTH),
                    expCal.get(Calendar.DAY_OF_MONTH), null);
        }

        /*CalendarView calView = manufactureDate.getCalendarView();
        calView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                Log.d(TAG, "CalView Listener " + year + " " + month + " " + dayOfMonth);
            }
        });
        Calendar mfgDate = Calendar.getInstance();
        if(vial.getManufactureDate() != null) {
            mfgDate.setTime(vial.getManufactureDate());
            Log.d(TAG, "Vial Manufacture Date " + vial.getManufactureDate().toString());
            Log.d(TAG, "Manufacture Date " + mfgDate.toString());
        } else {
            Log.d(TAG, "Manufacture Date is null");
            Log.d(TAG, mfgDate.get(Calendar.YEAR) + " " + mfgDate.get(Calendar.MONTH) + " " +
                    mfgDate.get
                    (Calendar.DATE));
        }

        //manufactureDate.updateDate(mfgDate.get(Calendar.YEAR), mfgDate.get(Calendar.MONTH),
                //mfgDate.get(Calendar.DAY_OF_MONTH));
        manufactureDate.init(mfgDate.get(Calendar.YEAR), mfgDate.get(Calendar.MONTH),
                mfgDate.get(Calendar.DATE), new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Log.d(TAG, "MFG Listener " + year + " " + monthOfYear + " " + dayOfMonth);
                    }
                });

        manufactureDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "DatePicker Focus " + hasFocus);
            }
        });
        manufactureDate.updateDate(mfgDate.get(Calendar.YEAR), mfgDate.get(Calendar.MONTH),
                mfgDate.get(Calendar.DATE));
        manufactureDate.clearFocus();

        Log.d(TAG, "DatePicker " + manufactureDate.getYear() + manufactureDate.getMonth() +
                manufactureDate.getDayOfMonth());

        DatePicker expirationDate = (DatePicker) vialLayout.findViewById(R.id.expiration_date);
        /*Calendar expDate = Calendar.getInstance();
        if(vial.getExpirationDate() != null) {
            expDate.setTime(vial.getExpirationDate());
        }

        expirationDate.updateDate(expDate.get(Calendar.YEAR),expDate.get(Calendar.MONTH),expDate.get(Calendar.DAY_OF_MONTH));
*/
        final FButton discardButton = (FButton) vialLayout.findViewById(R.id.discard_vial);
        discardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discardVial(index);
                AppController.getFlow().goBack();
            }
        });

        final LinearLayout frozen = vialLayout;

        final FButton saveButton = (FButton) vialLayout.findViewById(R.id.save_vial);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid(frozen)) {
                    saveVial(index);
                    saveButton.setText("Saved");
                    saveButton.setEnabled(false);
                    discardButton.setVisibility(View.INVISIBLE);
                    if(vials.length == 1) {
                        exit();
                    } else {
                        Log.d(TAG, "Vials length " + vials.length);
                    }
                }
            }
        });

        ocrDetailView.addVialLayout(vialLayout, index);
        ocrDetailView.addToContainer(vialLayout);
    }

    public boolean isValid() {
        return true;
    }

    public void discardVial(int index) {
        vials[index].hide();
    }

    public void exit() {
        AppController.getFlow().goBack();
    }

    public boolean isValid(ViewGroup vialLayout) {
        if(ocrDetailView.getLotNumber(vialLayout).isEmpty() && ocrDetailView.getBatchNumber(vialLayout).isEmpty()) {
            Log.d(TAG, "Batch and Lot are empty");
            ((EditText)vialLayout.findViewById(R.id.lot_number)).setError("Both Lot Number and Batch Number can't be empty");
            return false;
        }

        if(ocrDetailView.getManufactureDate(vialLayout).after(ocrDetailView.getExpirationDate(vialLayout)))
        {
            Log.d(TAG, "Mfg after Exp date");
            Toast.makeText(context.get(), "Manufacture Date can't be after Expiration Date", Toast
                    .LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public boolean isManufacturePrior(Date mfg, Date exp) {
        return mfg.before(exp);
    }

    public void saveVial(int index) {
        ViewGroup layout = ocrDetailView.getVialLayout(index);
        Vial vial = vials[index];
        vial.setVaccineType(ocrDetailView.getVaccineType(layout));
        vial.setMaxDoses(ocrDetailView.getMaximumDoses(layout));
        vial.setLotNumber(ocrDetailView.getLotNumber(layout));
        vial.setBatchNumber(ocrDetailView.getBatchNumber(layout));
        vial.setManufactureDate(ocrDetailView.getManufactureDate(layout));
        vial.setExpirationDate(ocrDetailView.getExpirationDate(layout));
        vial.save();
    }
}
