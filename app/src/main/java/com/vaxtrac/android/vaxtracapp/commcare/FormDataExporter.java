package com.vaxtrac.android.vaxtracapp.commcare;

import android.os.Bundle;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.models.Dose;
import com.vaxtrac.android.vaxtracapp.models.Dose.Status;
import com.vaxtrac.android.vaxtracapp.models.DoseReport;
import com.vaxtrac.android.vaxtracapp.models.Guardian;
import com.vaxtrac.android.vaxtracapp.models.Patient;
import com.vaxtrac.android.vaxtracapp.models.Schedule;
import com.vaxtrac.android.vaxtracapp.models.VaccineHistory;
import com.vaxtrac.android.vaxtracapp.models.Vial;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class FormDataExporter {

    private final String TAG = "FormDataExporter";

    private Map<String,String> data;

    private boolean isNewPatient = false;
    private Patient patient = null;
    private Guardian guardian = null;
    private List<Guardian> guardians;
    private List<Dose> doses = null;
    private int age;
    private CommCareDataManager.DoseDBHandler doseDB;

    static SimpleDateFormat dateFormat = new UnlocalizedDateFormat("yyyy-MM-dd");

    private static final String DOSE_KEY_PREFIX = "dose";
    private static final String DOC_ID_KEY = "doc_id";
    private static final String CASE_ID_KEY = "case_id";
    private static final String PATIENT_ID_KEY = "patient_id";
    private static final String VACCINE_NAME_KEY = "vaccine_name";
    private static final String TIMESTAMP_KEY = "timestamp";
    private static final String PATIENT_AGE_KEY = "patient_age";
    private static final String MONTH_KEY = "month";
    private static final String YEAR_KEY = "year";
    private static final String SERIES_KEY = "series";
    private static final String BATCH_NUMBER_KEY = "batch_number";
    private static final String LOT_NUMBER_KEY = "lot_number";
    private static final String MANUFACTURE_DATE_KEY = "manufacture_date";
    private static final String EXPIRATION_DATE_KEY = "expiration_date";
    private static final String DOSES_RECEIVED_KEY = "doses_received";
    private static final String NEW_PATIENT_KEY = "is_new_patient";
    private static final String CALLBACK_DATE_KEY = "callback_date";
    private static final String CALLBACK_STATUS_KEY = "callback_status";
    private static final String VILLAGE_KEY = "village";
    private static final String ADDRESS_KEY = "address";
    private static final String CLINIC_VISITS_KEY = "clinic_visits";
    private static final String RECORDED_VACCINES_KEY = "recorded_vaccines";
    private static final String REPORTED_VACCINES_KEY = "reported_vaccines";

    private static final String DOSE_DOC_ID_KEY = "docid";
    private static final String DOSE_PATIENT_ID_KEY = "patientid";
    private static final String DOSE_VACCINE_NAME_KEY = "vaccinename";
    private static final String DOSE_PATIENT_AGE_KEY = "patientage";
    private static final String DOSE_SESSION_TYPE_KEY = "sessiontype";

    public FormDataExporter(){
        doseDB = AppController.mCommCareDataManager.doseDB;
    }

    //You must add all doses at once!
    private void addDoses(List<Dose> doses){
        this.doses = doses;
    }

    public void addPatient(Patient patient, boolean isNewPatient){
        this.patient = patient;
        this.isNewPatient = isNewPatient;
        addDoses(this.patient.getHistory().getAllNewRecorded());
        addGuardian(this.patient.getGuardian());
    }

    private void addGuardian(Guardian guardian){
        this.guardian = guardian;
        if(guardians == null) {
            guardians = new ArrayList<>();
        }
        guardians.add(guardian);
    }

    //requires a patient to be loaded.
    protected Map<String,String> packDoses(Map<String,String> data){
        data.put(DOSES_RECEIVED_KEY, Integer.toString(doses.size()));

        Date now = Calendar.getInstance().getTime();
        SimpleDateFormat df2 = new UnlocalizedDateFormat("kk:mm:ss");
        SimpleDateFormat df3 = new UnlocalizedDateFormat("yyyy");
        SimpleDateFormat df4 = new UnlocalizedDateFormat("MM");
        String timestamp = String.format("%sT%sZ", dateFormat.format(now), df2.format(now));

        Date dob = patient.getHistory().getDOB();

        int counter = 0;

        String sessionType = null;
        if(Preferences.sessionLocationEnabled()) {
            sessionType = Preferences.getSessionLocationType();
        }

        for(Dose dose: doses){
            Map<String,String> doseRow = new HashMap<>();

            Date doseDate = dose.getDate();
            String doseNumber = Integer.toString(dose.getDoseNumber());

            String doseFieldFormat = new StringBuilder(DOSE_KEY_PREFIX)
                    .append("_%s_")
                    .append(counter).toString();

            doseRow.put(DOSE_DOC_ID_KEY, UUID.randomUUID().toString());

            String patientId = patient.attributes.get(CASE_ID_KEY);
            data.put(String.format(doseFieldFormat, PATIENT_ID_KEY), patientId);
            doseRow.put(DOSE_PATIENT_ID_KEY, patientId);

            String antigen = dose.getAntigen();
            data.put(String.format(doseFieldFormat, VACCINE_NAME_KEY), antigen);
            doseRow.put(DOSE_VACCINE_NAME_KEY, antigen);

            data.put(String.format(doseFieldFormat, TIMESTAMP_KEY), timestamp);
            doseRow.put(TIMESTAMP_KEY, dateFormat.format(now));

            age = Schedule.daysBetween(dob, doseDate);
            String patientAge = Integer.toString(age);
            data.put(String.format(doseFieldFormat, PATIENT_AGE_KEY), patientAge);
            doseRow.put(DOSE_PATIENT_AGE_KEY, patientAge);

            doseRow.put(MONTH_KEY, df4.format(now));
            doseRow.put(YEAR_KEY, df3.format(now));
            //data.put(d+"clinic_id"+suf, ""); GETS PUT IN IN THE FORM

            data.put(String.format(doseFieldFormat, SERIES_KEY), doseNumber);
            doseRow.put(SERIES_KEY, doseNumber);

            if(sessionType != null) {
                doseRow.put(DOSE_SESSION_TYPE_KEY, sessionType);
            }

            doseDB.commitRow(doseRow);

            Vial vial = dose.getVial();
            if(vial != null) {
                vial.incrementVialDoses();
                data.put(String.format(doseFieldFormat, BATCH_NUMBER_KEY), vial.getBatchNumber() == null ? "" :
                        vial.getBatchNumber());
                data.put(String.format(doseFieldFormat, LOT_NUMBER_KEY), vial.getLotNumber() == null ? "" :
                        vial.getLotNumber());
                data.put(String.format(doseFieldFormat, MANUFACTURE_DATE_KEY), vial.getManufactureDate() == null ? ""
                        : vial.getManufactureDateText());
                data.put(String.format(doseFieldFormat, EXPIRATION_DATE_KEY), vial.getExpirationDate() == null ?
                        "" : vial.getExpirationDateText());
            } else {
                data.put(String.format(doseFieldFormat, BATCH_NUMBER_KEY), "");
                data.put(String.format(doseFieldFormat, LOT_NUMBER_KEY), "");
                data.put(String.format(doseFieldFormat, MANUFACTURE_DATE_KEY), "");
                data.put(String.format(doseFieldFormat, EXPIRATION_DATE_KEY), "");
            }

            counter += 1;
        }
        return data;
    }

    private Map<String, String> packReports(Map<String, String> data) {
        if(doses != null && doses.size() > 0) {
            DoseReport currentReport = findCurrentReport();
            if(currentReport != null) {
                currentReport.addRecordedDoses(doses);
            } else {
                DoseReport newReport = DoseReport.fromFormData(doses, age);
            }
        }

        return data;
    }

    private DoseReport findCurrentReport() {
        return AppController.mReportsManager.getCurrentReport();
    }

    protected Map<String,String> packGuardian(Map<String,String> data){
        Map<String,String> attrs = guardian.getAttributes();
        for(String key : attrs.keySet()){
            data.put(key, attrs.get(key));
        }
        return data;
    }

    protected Map<String,String> packPatient(Map<String,String> data){
        Map<String, String> attributes = patient.getAttributes();
        data.put(NEW_PATIENT_KEY, isNewPatient ? "true" : "false");

        //Add callback information
        Date nextVisit = null;
        Schedule schedule = patient.getSchedule();
        VaccineHistory history = patient.getHistory();
        for(String antigen: schedule.requiredVaccines()){
            List<Dose> doses = history.getComputedSchedule().get(antigen);
            if (doses == null || history.wasUpdated()){
                doses = schedule.compareDoses(history, antigen);
                history.getComputedSchedule().put(antigen, doses);
            }

            if(doses != null){
                for(Dose dose : doses){
                    if (dose.getStatus() == Status.next_valid){
                        Date doseDate = dose.getDate();
                        if (nextVisit != null){
                            if(nextVisit.after(doseDate)){
                                nextVisit = doseDate;
                                Log.d(TAG, String.format("Found more urgent dose: %s dose on %s",
                                        antigen, dateFormat.format(doseDate)));
                            }
                        }else{
                            Log.d(TAG, String.format("NextDose was null so we're sticking in %s " +
                                    "dose on %s", antigen, dateFormat.format(doseDate)));
                            nextVisit = doseDate;
                        }
                    }
                }
            }

        }

        if(nextVisit != null){
            patient.setAttribute(CALLBACK_DATE_KEY, dateFormat.format(nextVisit));
            patient.setAttribute(CALLBACK_STATUS_KEY,"true");
        }else{
            patient.setAttribute(CALLBACK_DATE_KEY, "finished");
            patient.setAttribute(CALLBACK_STATUS_KEY,"false");
        }

        if (patient.getAttribute(VILLAGE_KEY) == null) {
            patient.setAttribute(VILLAGE_KEY, "");
        }

        if (patient.getAttribute(ADDRESS_KEY) == null) {
            patient.setAttribute(ADDRESS_KEY, "");
        }

        String clinicVisits = patient.getAttribute(CLINIC_VISITS_KEY);
        if(clinicVisits != null) {
            try {
                JSONArray clinicVisitsJson = new JSONArray(clinicVisits);
                String userId = AppController.commCareAPI.getUserId();
                int visitsLength = clinicVisitsJson.length();
                if(!(clinicVisitsJson.get(visitsLength-1)).equals(userId)) {
                    clinicVisitsJson.put(visitsLength, userId);
                    patient.setAttribute(CLINIC_VISITS_KEY, clinicVisitsJson.toString());
                }
            } catch (JSONException exception) {
                Log.d(TAG, "Couldn't parse clinic_visits JSON " + clinicVisits);
            }
        } else {
            patient.setAttribute(CLINIC_VISITS_KEY, new JSONArray().put(AppController.commCareAPI
                    .getUserId()).toString());
        }

        history.finalizeDoses();

        data.putAll(patient.getAttributes());

        data.put(REPORTED_VACCINES_KEY, history.reportedString());
        data.put(RECORDED_VACCINES_KEY, history.recordedString());

        return data;
    }

    public Bundle getExportBundle(){
        data = new HashMap<>();
        if(isNewPatient){
            data = packGuardian(data);
        }
        data = packPatient(data);
        data = packDoses(data);
        //data = packReports(data);

        Bundle bundle = new Bundle();

        for(String key: data.keySet()){
            bundle.putString(key, data.get(key));
        }

        reportBundleContents(bundle);
        return bundle;
    }

    private void reportBundleContents(Bundle bundle){
        for(String key: bundle.keySet()){
             Log.d(TAG, String.format("BundleOut: %s | %s", key, bundle.getString(key)));
        }
    }
}
