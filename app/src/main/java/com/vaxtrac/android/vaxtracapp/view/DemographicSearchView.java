package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.data.FilteredCursor;
import com.vaxtrac.android.vaxtracapp.data.FilteredCursorFactory;
import com.vaxtrac.android.vaxtracapp.models.Patient;
import com.vaxtrac.android.vaxtracapp.presenter.CustomDemographicSearchPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.DemographicSearchPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.VerifyGuardianPresenter;

import java.lang.ref.WeakReference;

public class DemographicSearchView extends LinearLayout {

    public static final String TAG = "DemographicSearchView";

    CustomDemographicSearchPresenter presenter;
    WeakReference<MainActivity> activity;
    Context context;
    ListView list;
    public TextView dobView;
    public TextView sexView;
    EditText field;

    public DemographicSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        activity = new WeakReference<>((MainActivity)context);
        this.presenter = new CustomDemographicSearchPresenter(this.context);
        this.presenter.setView(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        list = (ListView) findViewById(R.id.demosearch_list);
        dobView = (TextView) findViewById(R.id.demosearch_dob_display);
        sexView = (TextView) findViewById(R.id.demosearch_sex_display);
        field = (EditText) findViewById(R.id.demosearch_field);
        presenter.readBundle();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.context);
        final boolean fingerprintEnabled = prefs.getBoolean("pref_fingerprinting", true);

        field.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null){
                    try {
                        Log.i(TAG, "ApplyFilter onTextChange");
                        presenter.adapter.getFilter().filter(s.toString());
                        presenter.adapter.notifyDataSetChanged();
                    }catch (NullPointerException e){/*Rotation Change triggers...*/}
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Action for clicking a listItem from the patientList
                Cursor selected = (Cursor) adapterView.getItemAtPosition(i);
                String selectedID = selected.getString(selected.getColumnIndex("childid"));
                Log.i(TAG, String.format("Selected DocID %s", selectedID));
                if(!fingerprintEnabled){
                    AppController.mPatient = new Patient(activity.get().getSchedule());
                    AppController.mPatient.loadFromCommCare(selectedID);
                    AppController.getFlow().replaceTo(new Screens.TabScreen(0));
                }else{
                    VerifyGuardianPresenter.setChildID(selectedID);
                    AppController.getFlow().goTo(new Screens.VerifyGuardianScreen());
                }
            }
        });
        PopulateDemoList task = new PopulateDemoList();
        task.execute();

    }

    public void setFilter(final String column){
        presenter.adapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence str) {
                if (str!= null){
                    final String parse_string = str.toString().toLowerCase();
                    Log.i(TAG, parse_string);
                    FilteredCursor filtered = FilteredCursorFactory.createUsingSelector(presenter.parseCursor, new FilteredCursorFactory.Selector() {
                        int selectedIndex = -1;

                        @Override
                        public boolean select(Cursor cursor) {
                            if (selectedIndex == -1) {
                                selectedIndex = cursor.getColumnIndex(column);
                            }
                            if (cursor.getString(selectedIndex).toLowerCase().contains(parse_string)) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    });
                    return filtered;
                }
                return presenter.parseCursor;
            }
        });
        list.setAdapter(presenter.adapter);
    }

    public class PopulateDemoList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            presenter.readyList();
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            presenter.adapter = new ColoredCursorAdapter(context,
                    R.layout.row_demographic_searchview,
                    presenter.baseCursor,
                    new String[]{"childname","childdob","relation","name","village"},
                    new int[]{R.id.demolist_item_text1, R.id.demolist_item_text2, R.id
                            .demolist_item_text3, R.id.demolist_item_text4,R.id
                            .demolist_item_text5});

            setFilter("childname");
            super.onPostExecute(aVoid);
        }
    }

}
