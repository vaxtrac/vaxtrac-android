package com.vaxtrac.android.vaxtracapp.preferences;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.data.ReportsDataBackup;
import com.vaxtrac.android.vaxtracapp.utils.AssetsUtil;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

public class PreferencesAdapter extends BaseAdapter implements SharedPreferences.OnSharedPreferenceChangeListener{

    private static final String TAG = "PreferencesAdapter";

    Context context;
    SharedPreferences sharedPreferences;
    Map<String, Object> items;
    Map<Integer, String> index;
    Map<String, Preference> preferencesMap;
    Map<Integer, String> preferencesIndex;
    LayoutInflater inflater;

    private static final int STRING_VIEW_TYPE = 0;
    private static final int BOOLEAN_VIEW_TYPE = 1;
    private static final int BUTTON_VIEW_TYPE = 2;

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(preferencesMap.containsKey(key)) {
            updatePreferenceItem(key);
        }
    }

    public enum PreferenceType {
        STRING, BOOLEAN, INTEGER
    };

    public PreferencesAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        items = new LinkedHashMap<>();
        index = new LinkedHashMap<>();
        preferencesMap = new LinkedHashMap<>();
        preferencesIndex = new LinkedHashMap<>();
        preferencesMap = Preferences.getPreferences();
        int counter = 0;

        for(Map.Entry<String, Preference> entry : preferencesMap.entrySet()) {
            preferencesIndex.put(counter++, entry.getKey());
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AppController
                .getAppContext());
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        counter = 0;
        for(Map.Entry<String, ?> entry : sharedPreferences.getAll().entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            items.put(key, value);
            index.put(counter, key);
            counter++;
        }

    }

    @Override
    public Preference getItem(int position) {
        return preferencesMap.get(preferencesIndex.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        Preference preference = getItem(position);
        if (preference instanceof CheckPreference) {
            return BOOLEAN_VIEW_TYPE;
        } else if (preference instanceof TextPreference) {
            return STRING_VIEW_TYPE;
        } else if (preference instanceof EntryListPreference) {
            return STRING_VIEW_TYPE;
        } else if (preference instanceof ButtonPreference) {
            return BUTTON_VIEW_TYPE;
        } else {
            return STRING_VIEW_TYPE;
        }
    }

    @Override
    public int getCount() {
        //return items.size();
        return preferencesMap.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Preference item = getItem(position);
        final String itemKey = (String)item.getKey();
        int itemViewType = getItemViewType(position);
        final Object itemValue;

        switch(itemViewType) {
            case STRING_VIEW_TYPE:
                itemValue = sharedPreferences.getString(item.getKey(), "");
                final boolean hasEntries = Preferences.hasEntries(itemKey);
                final ViewHolderStringPreference viewHolder;
                View view = convertView;
                if(view == null) {
                    view = inflater.inflate(R.layout.preference_item, parent, false);
                    viewHolder = new ViewHolderStringPreference();
                    viewHolder.preferenceDescription = (TextView) view.findViewById(R.id
                            .pref_description);
                    viewHolder.preferenceValue = (TextView) view.findViewById(R.id.pref_value);
                    view.setTag(viewHolder);
                } else {
                    viewHolder = (ViewHolderStringPreference) view.getTag();
                }
                viewHolder.preferenceDescription.setText(item.getTitle());
                viewHolder.preferenceValue.setText((String)itemValue);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(hasEntries) {
                            createListDialog(itemKey, (String)item.getTitle(),
                                    Preferences
                                    .getChoices
                                    (itemKey)).show();
                        } else {
                            createTextDialog(itemKey, (String)item.getTitle(), (String)itemValue)
                                    .show();
                        }
                    }
                });
                return view;
            case BOOLEAN_VIEW_TYPE:
                itemValue = sharedPreferences.getBoolean(item.getKey(), false);
                final ViewHolderBooleanPreference viewHolderCheck;
                View viewCheck = convertView;
                if(viewCheck == null) {
                    viewCheck = inflater.inflate(R.layout.preference_checkbox_item, parent, false);
                    viewHolderCheck = new ViewHolderBooleanPreference();
                    viewHolderCheck.preferenceDescription = (TextView) viewCheck
                            .findViewById(R.id.pref_checked_text_view);
                    viewHolderCheck.preferenceCheckBox = (CheckBox) viewCheck
                            .findViewById(R.id.pref_checkbox);
                    viewCheck.setTag(viewHolderCheck);
                } else {
                    viewHolderCheck = (ViewHolderBooleanPreference) viewCheck.getTag();
                }
                viewHolderCheck.preferenceDescription.setText(item.getTitle());
                viewHolderCheck.preferenceCheckBox.setChecked((Boolean) itemValue);
                viewCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toggleBooleanSetting(position);
                        viewHolderCheck.preferenceCheckBox.toggle();
                    }
                });
                return viewCheck;
            case BUTTON_VIEW_TYPE:
                final ViewHolderButtonPreference viewHolderButton;
                View buttonView = convertView;
                if(buttonView == null) {
                    buttonView = inflater.inflate(R.layout.preference_button_item, parent, false);
                    viewHolderButton = new ViewHolderButtonPreference();
                    viewHolderButton.preferenceDescription = (TextView) buttonView.findViewById(R.id
                            .pref_description_button);
                    buttonView.setTag(viewHolderButton);
                } else {
                    viewHolderButton = (ViewHolderButtonPreference) buttonView.getTag();
                }
                viewHolderButton.preferenceDescription.setText(item.getTitle());
                buttonView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (itemKey.equals(Preferences.PREF_BACKUP_DB)) {
                            ReportsDataBackup sd = new ReportsDataBackup(context);
                            sd.backupDB();
                        }
                        else if (itemKey.equals(Preferences.PREF_RESTORE_DB)) {
                            ReportsDataBackup sd = new ReportsDataBackup(context);
                            sd.restoreDB();
                        } else if (itemKey.equals(Preferences.PREF_USERGUIDE)) {
                            String userGuideFilename = context.getString(R.string
                                    .pref_userguide_filename);
                                    AssetsUtil.copyFile(context, "", "", userGuideFilename);
                            File file = new File(context.getExternalFilesDir(null),
                                    userGuideFilename);
                            Log.d(TAG, "File " + file.getAbsolutePath() + " exists? " + file.exists
                                    ());
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                            context.startActivity(intent);
                        } else if (itemKey.equals(Preferences.PREF_HELP_VIDEOS)) {
                            AppController.getFlow().goTo(new Screens.HelpVideosScreen());
                        }
                    }
                });
                return buttonView;
        }
        return null;
    }

    public Dialog createListDialog(final String key, String title, final CharSequence[] items) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Preferences.putString(key, (String)items[which]);
                        notifyDataSetChanged();
                    }
                })
                .create();
    }

    public Dialog createTextDialog(final String key, String title, String placeholder) {
        final EditText textView = new EditText(context);
        textView.setInputType(InputType.TYPE_CLASS_NUMBER);
        textView.setText(placeholder);
        textView.setSelection(textView.length());
        return new AlertDialog.Builder(context)
                .setView(textView)
                .setTitle(title)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Preferences.putString(key, textView.getText().toString());
            }
        }).create();
    }

    public void toggleBooleanSetting(int position) {
        Preference item  = getItem(position);
        String key = (String) item.getKey();
        Boolean newValue = !(Boolean)getValue(item, position);
        items.put(key, newValue);
        sharedPreferences.edit().putBoolean(key, newValue).commit();
    }

    public Object getValue(Preference preference, int position) {
        switch(getItemViewType(position)) {
            case STRING_VIEW_TYPE:
                return sharedPreferences.getString(preference.getKey(), "");
            case BOOLEAN_VIEW_TYPE:
                return sharedPreferences.getBoolean(preference.getKey(), false);
            default:
                return sharedPreferences.getString(preference.getKey(), "");
        }
    }

    private static class ViewHolderStringPreference {
        public TextView preferenceDescription;
        public TextView preferenceValue;
    }

    private static class ViewHolderBooleanPreference {
        public TextView preferenceDescription;
        public CheckBox preferenceCheckBox;
    }

    private static class ViewHolderButtonPreference {
        public TextView preferenceDescription;
    }

    private PreferenceType getViewByType(Object value) {
        Log.d(TAG, "getViewByType: " + value.toString());
        if (value instanceof String) {
            try {
                Integer.parseInt((String) value);
            } catch (Exception exception) {
                Log.d(TAG, "instanceof String");
                return PreferenceType.STRING;
            }
            Log.d(TAG, "instanceof Integer");
            return PreferenceType.INTEGER;
        } else if (value instanceof Boolean) {
            Log.d(TAG, "instanceof Boolean");
            return PreferenceType.BOOLEAN;
        } else if (value instanceof Integer) {
            Log.d(TAG, "instanceof Integer");
            return PreferenceType.INTEGER;
        } else {
            Log.d(TAG, "instanceof Default String");
            return PreferenceType.STRING;
        }
    }

    private void updatePreferenceItem(String key) {
        try {
            switch (getViewByType(items.get(key))) {
                case STRING:
                    items.put(key, sharedPreferences.getString(key, ""));
                    break;
                case BOOLEAN:
                    items.put(key, sharedPreferences.getBoolean(key, false));
                    break;
                case INTEGER:
                    items.put(key, sharedPreferences.getInt(key, 0));
                    break;
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "updatePrefItem", e);
        }
    }

    private void displayOptions(String key) {

    }
}
