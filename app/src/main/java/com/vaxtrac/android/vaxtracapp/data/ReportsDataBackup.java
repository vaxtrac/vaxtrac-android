package com.vaxtrac.android.vaxtracapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

public class ReportsDataBackup {
    private final String TAG = "ReportsDataBackup";
    private Context context;
    private final String backupDBPath;
    private final String doseDBPath;

    public ReportsDataBackup(Context context) {
        this.context = context;
        String publicDirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        backupDBPath = publicDirPath + "/vaxtrac_backup.db";
        Log.d(TAG, backupDBPath);
        doseDBPath = context.getDatabasePath(TemporaryDoseDBHelper.DATABASE_NAME).getAbsolutePath();
    }

    private void transferData(String srcDB, String dstDB) {
            try {
                File path = Environment.getExternalStorageDirectory();
                if (path.canWrite()) {
                    File srcFile = new File(srcDB);
                    File dstFile = new File(dstDB);

                    if (srcFile.getAbsoluteFile().exists()) {
                        FileChannel srcChannel = new FileInputStream(srcFile).getChannel();
                        FileChannel dstChannel = new FileOutputStream(dstFile).getChannel();
                        dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
                        srcChannel.close();
                        dstChannel.close();

                        Toast.makeText(context, "Data transfer to/from SD successful", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "Data transfer to/from SD successful");
                    } else {
                        Toast.makeText(context, srcFile + " does not exist", Toast.LENGTH_LONG).show();
                        Log.d(TAG, srcFile + " does not exist");
                    }
                }
            } catch (Exception e) {
                Toast.makeText(context, "Data transfer to/from SD failed", Toast.LENGTH_LONG).show();
                Log.e(TAG, "Data transfer to/from SD failed", e);
            }
    }

    private void copyDosesTable(SQLiteDatabase src, SQLiteDatabase dest) {
        Cursor cursor = src.query(TemporaryDoseDBHelper.DOSE_TABLE_NAME, null, null, null, null,
                null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ContentValues values = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, values);
            dest.insert(TemporaryDoseDBHelper.DOSE_TABLE_NAME, null, values);
            cursor.moveToNext();
        }

        cursor.close();
    }

    public void backupDB() {
        LocalDBHelper localHelper = new LocalDBHelper(context);
        SQLiteDatabase localDB = localHelper.getReadableDatabase();

        TemporaryDoseDBHelper tmpDoseHelper = new TemporaryDoseDBHelper(context);
        SQLiteDatabase tmpDoseDB = tmpDoseHelper.getWritableDatabase();

        copyDosesTable(localDB, tmpDoseDB);

        transferData(doseDBPath, backupDBPath);

        localDB.close();
        localHelper.close();
        tmpDoseDB.close();
        tmpDoseHelper.close();
        context.deleteDatabase(TemporaryDoseDBHelper.DATABASE_NAME);
    }

    public void restoreDB() {
        transferData(backupDBPath, doseDBPath);

        TemporaryDoseDBHelper tmpDoseHelper = new TemporaryDoseDBHelper(context);
        SQLiteDatabase tmpDoseDB = tmpDoseHelper.getReadableDatabase();

        LocalDBHelper localHelper = new LocalDBHelper(context);
        SQLiteDatabase localDB = localHelper.getWritableDatabase();

        copyDosesTable(tmpDoseDB, localDB);

        localDB.close();
        localHelper.close();
        tmpDoseDB.close();
        tmpDoseHelper.close();
        context.deleteDatabase(TemporaryDoseDBHelper.DATABASE_NAME);
    }
}
