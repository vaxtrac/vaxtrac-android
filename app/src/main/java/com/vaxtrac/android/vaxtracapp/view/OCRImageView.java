package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.etsy.android.grid.StaggeredGridView;
import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.presenter.OCRImagePresenter;

public class OCRImageView extends FrameLayout {

    private static final String TAG = "OCRImageView";
    Spinner vaccineTypeSpinner;
    Button newPhoto;
    StaggeredGridView gridView;
    OCRImagePresenter presenter;
    Context context;
    String[] vaccineArray = getResources().getStringArray(R.array.antigen_array_en);

    public OCRImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "Constructor");
        this.context = context;
        presenter = new OCRImagePresenter(context, this);
    }

    @Override
    protected void onFinishInflate() {
        Log.d(TAG, "onFinishInflate");

        vaccineTypeSpinner = (Spinner) findViewById(R.id.filter_type);
        final ArrayAdapter vaccineTypeAdapter = ArrayAdapter.createFromResource(context,
                R.array.antigen_array, R.layout.vaccine_type_item);
        vaccineTypeSpinner.setAdapter(vaccineTypeAdapter);

        vaccineTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "vaccine type position " + position);
                int selectedItem = vaccineTypeSpinner.getSelectedItemPosition();
                presenter.persistTypeFilter(selectedItem);
                presenter.filter(vaccineArray[selectedItem]);
                presenter.saveLastVialSelection(selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (presenter.restoreTypeFilter() != -1) {
            vaccineTypeSpinner.setSelection(presenter.restoreTypeFilter());
        }

        vaccineTypeSpinner.requestFocus();

        newPhoto = (Button) findViewById(R.id.obtain_ocr);
        newPhoto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.newOcrClick();
            }
        });

        gridView = (StaggeredGridView) findViewById(R.id.grid_view);
        presenter.populateImages(gridView);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Screens.OCRDetailScreen ocrDetailScreen = new Screens.OCRDetailScreen(presenter
                        .get(position));
                AppController.mScreen = ocrDetailScreen;
                AppController.getFlow().goTo(ocrDetailScreen);
            }
        });

        super.onFinishInflate();
    }
}
