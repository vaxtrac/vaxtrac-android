package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.view.GuardianPickerView;

public class GuardianPickerPresenter extends Presenter{

    protected GuardianPickerView view;
    protected Context context;
    protected Cursor guardians = null;
    protected static String childID = null;

    private static final String TAG = "GuardianPickerPresenter";

    public GuardianPickerPresenter(final Context context, GuardianPickerView view) {
        this.context = context;
        setView(view);
        populateList(childID);
    }

    protected void populateList(final String childID){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                guardians = AppController.mCommCareDataManager.guardianDB.allRowsColumnEquals("childid", childID);
                if (guardians.getCount() < 1){
                    noGuardianFound();
                    return;
                }else{
                    Log.i(TAG, String.format("Guardian Found for childID: %s | %s", childID, guardians.getCount()));
                    guardiansReady();
                }
            }
        });
        t.start();
    }

    public void setView(View view) {this.view = (GuardianPickerView) view;}

    public Cursor getGuardians(){
        return guardians;
    }

    public static void setChildID(String id){
        childID = id;
    }

    public static String getChildID() {return childID;}

    //MUST BE SUBCLASSED!
    protected void noGuardianFound(){
        Log.e(TAG, String.format("No Guardian Found for childID: %s", childID));
    }

    //MUST BE SUBCLASSED!
    protected void guardiansReady(){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
            try {
                view.populateTable(getGuardians());
            }catch (NullPointerException e){
                Log.e(TAG, "Couldn't populate GuardianPicker Table");
                e.printStackTrace();
            }
            }
        });
    }
}
