package com.vaxtrac.android.vaxtracapp.service;

/**
 * Created by sarwar on 12/22/14.
 */
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.R;


public class PersistenceService extends Service {

    private static final String TAG = "PersistenceService";
    private static boolean isForeground =false;
    public static int NOTE_ID = 20011;

    protected static Notification mNotification = null;
    protected static NotificationManager mNotificationManager = null;
    protected static Context mContext = null;


    public PersistenceService() {}

    @Override
    public void onCreate(){
        Log.i(TAG, "Started Persistence Service");
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mContext = this;
        showForeground();
    }

    private void showForeground(){
        getNotification("Service Started.");
        startForeground(NOTE_ID, mNotification);
    }

    @Override
    public void onDestroy() {stop();}

    @Override
    public IBinder onBind(Intent arg0) {return null;}

    public static void showMessage(String message){
        if (mNotificationManager == null){
            Log.i(TAG, "Can't show notification, no active service...");
            return;
        }
        getNotification(message);
        mNotificationManager.notify(NOTE_ID, mNotification);
    }

    private static void getNotification(String message){
        isForeground =true;

        Intent intent = new Intent(mContext, NotificationReceiver.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent kill_intent = PendingIntent.getService(mContext, 0, intent, 0);
        Builder builder = new Builder(mContext);
        builder.setContentTitle("VaxTrac");
        builder.setContentText(message);
        builder.setSmallIcon(R.drawable.vaxtracicon200);

        builder.addAction(android.R.drawable.ic_input_delete, "Stop Service", kill_intent);
        Intent notificationIntent = new Intent(mContext, PersistenceService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, 0);
        builder.setContentIntent(pendingIntent);
        mNotification = builder.build();
        return;
    }

    private void stop() {
        if (isForeground) {
            isForeground =false;
            stopForeground(true);
        }
    }

}
