package com.vaxtrac.android.vaxtracapp.view;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.vaxtrac.android.vaxtracapp.presenter.DeliveryPresenter;

public class DeliveryView extends LinearLayout {

    DeliveryPresenter presenter;

    public DeliveryView(Context context, AttributeSet attrs) {
        super(context, attrs);

        presenter = new DeliveryPresenter(context, this);
    }

    protected void onFinishInflate() {

    }
}
