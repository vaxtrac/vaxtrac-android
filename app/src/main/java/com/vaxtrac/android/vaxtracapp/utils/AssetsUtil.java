package com.vaxtrac.android.vaxtracapp.utils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public class AssetsUtil {

    public static void copyFile(Context context, String originPath, String destPath, String
            filename) {
        AssetManager assetManager = context.getAssets();

        InputStream in = null;
        OutputStream out = null;

        try {
            Log.d("AssetsUtil", "originPath " + originPath + " filename " + filename);
            if(originPath.equals("")) {
                in = assetManager.open(filename);
            } else {
                in = assetManager.open(originPath + "/" + filename);
            }
        } catch (Exception e) {
            Log.e("AssetsUtil", "Error opening assets file " + filename);
        }

        if (in == null) {
            try {
                in = assetManager.openFd(originPath + filename).createInputStream();
            } catch (IOException exception) {
                Log.e("AssetsUtil", exception.getMessage());
            }
        }

        Log.d("AssetsUtil", "External File Dirs" + Arrays.toString(context
                        .getExternalFilesDirs(null)));
        File newFileName = new File(new File(context.getExternalFilesDir(null).toString() + "/" +
                destPath), filename);
        Log.d("AssetsUtil", "destination " + newFileName);
        Log.d("AssetsUtil", "Storage State: " + Environment.getExternalStorageState());

        try {
            out = new FileOutputStream(newFileName);
        } catch (Exception exception) {
            Log.e("AssetsUtil", "Can't create FileOutputStream");
        }

        try {
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("AssetsUtil", e.getMessage());
        }

    }

    public static void copyVideoFile(Context context, String filename) {
        AssetManager assetManager = context.getAssets();

        try {
            AssetFileDescriptor assetFileDescriptor = assetManager.openFd(filename);
            InputStream inputStream = assetFileDescriptor.createInputStream();

        } catch (Exception e) {
            Log.e("AssetsUtil", e.getMessage());
        }
    }
}
