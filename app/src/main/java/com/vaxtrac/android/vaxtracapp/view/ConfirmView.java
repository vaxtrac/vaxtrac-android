package com.vaxtrac.android.vaxtracapp.view;


import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.commcare.FormDataExporter;
import com.vaxtrac.android.vaxtracapp.presenter.ConfirmPresenter;

public class ConfirmView extends LinearLayout {

    TableLayout table;
    LayoutInflater inflater;
    Context context;
    Button button;
    ConfirmPresenter presenter;
    MainActivity mainActivity;

    public ConfirmView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        mainActivity = (MainActivity) getContext();
        presenter = new ConfirmPresenter(context, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        inflater = LayoutInflater.from(getContext());
        table  = (TableLayout) findViewById(R.id.vaccination_table);
        button = (Button) findViewById(R.id.btn_save);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FormDataExporter exporter = new FormDataExporter();
                exporter.addPatient(AppController.mPatient, AppController.mPatient.isNewPatient());
                Bundle b = exporter.getExportBundle();
                mainActivity.returnToCommCare(b);

            }
        });
        reDrawTable();
    }

    public void reDrawTable(){
        table.removeAllViews();
        if (AppController.mPatient != null) {
            presenter.fill_table(table, inflater);
            table.invalidate();
        }
        else{
            Toast.makeText(context, "No patient Loaded!", Toast.LENGTH_SHORT).show();
        }
    }
}
