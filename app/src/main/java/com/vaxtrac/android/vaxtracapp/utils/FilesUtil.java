package com.vaxtrac.android.vaxtracapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FilesUtil {

    public static final String VIAL_DIRECTORY_NAME = "VialPhotos";

    public static File getVialStorageDir(Context context) {
        File file = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), VIAL_DIRECTORY_NAME);
        if(!file.mkdirs()) {
            Log.d("FilesUtil", "Vial Images Folder already exists");
        }
        return file;
    }

    public static File getVialImage(Context context, String filename) {
        return new File(getVialStorageDir(context), filename);
    }

    public static String writeVialImage(Context context, String filename, Bitmap bitmap) {
        File folder = getVialStorageDir(context);
        File image = new File(folder, filename);
        BufferedOutputStream outputStream = null;
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(image));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        } catch(java.io.IOException exception) {
            Log.e("FilesUtil", "Error writing file for vial image", exception);
        } finally {
            if(outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {}
            }
            return image.getAbsolutePath();
        }
    }

    public boolean isExternalStorageWritable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }
}
