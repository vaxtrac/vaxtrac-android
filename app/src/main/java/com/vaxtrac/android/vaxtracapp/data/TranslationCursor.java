package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TranslationCursor extends CursorWrapper {

    /*
    Cursor Wrapper for cursors where entire columns require a standardized translation.
    I.E. for a gender column, each male/female needs to be localized
     */

    private String TAG = "TranslationCursor";
    Set<Integer> translationColummns = null;
    Map<String,String> localTranslations = null;
    private WeakReference<Context> context;

    //columnsForTranslation are the SQL column names for the columns requiring modification
    public TranslationCursor(Cursor cursor, String[] columnsForTranslation, Context context) {
        this(cursor, columnsForTranslation);
        this.context = new WeakReference<Context>(context);
    }

    public TranslationCursor(Cursor cursor, String[] columnsForTranslation) {
        super(cursor);
        translationColummns = new HashSet<Integer>();
        for (String columnName: columnsForTranslation){
            try{
                int i = cursor.getColumnIndex(columnName);
                Log.d(TAG, "Column Name " + columnName + " Index " + i);
                translationColummns.add(i);
            }catch (Exception e){/*pass*/}
        }
        localTranslations = new HashMap<String, String>();
    }

    @Override
    public String getString(int columnIndex) {
        if (!translationColummns.contains(columnIndex)){
            try {
                return super.getString(columnIndex);
            } catch (Exception exception) {
                Log.d(TAG, exception.toString() + " ColumnIndex : " + columnIndex);
                return "";
            }
        }
        try {
            String local = localTranslations.get(super.getString(columnIndex));
            if (local != null){
                return local;
            }
            Log.i(TAG, String.format("No translation for %s | grabbing from globals", super.getString(columnIndex)));
            String globalString = GlobalTranslations.translate(super.getString(columnIndex),
                    context == null ? AppController.getAppContext() : context.get());
            localTranslations.put(super.getString(columnIndex), globalString);
            return globalString;
        }catch (Exception e){
            Log.i(TAG, String.format("Couldn't grab translation locally | %s",e.toString()));
            return "";
        }

    }
}
