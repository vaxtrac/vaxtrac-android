package com.vaxtrac.android.vaxtracapp.commcare;

import android.content.Intent;

import java.util.Map;

public class CommCareFormSession {

    private static final String TAG = CommCareFormSession.class.getName();

    public static final String SESSION_ACTION = "org.commcare.dalvik.action.CommCareSession";
    public static final String SESSION_ACTION_EXTRA = "ccodk_session_request";

    public static final String COMMAND_ID = "COMMAND_ID";
    public static final String PARAMS = "CASE_ID";

    public static final String MENU_DELIMITER = "-";
    public static final String SEPARATOR = " ";

    private String module;
    private String form;
    private String menu;

    private Map<String, String> requestParams;

    public CommCareFormSession(String module, String form, Map<String, String> requestParams) {
        this.module = module;
        this.form = form;
        this.requestParams = requestParams;
        menu = getMenu();
    }

    public CommCareFormSession(String module, String form) {
        this.module = module;
        this.form = form;
        menu = getMenu();
    }

    public Intent getIntent() {
        Intent intent = new Intent();
        intent.setAction(SESSION_ACTION);
        intent.putExtra(SESSION_ACTION_EXTRA, getActionExtra());
        return intent;
    }

    private String getMenu() {
        return String.format("%s%s%s", module, MENU_DELIMITER, form);
    }

    private String buildParamsList() {
        StringBuilder stringBuilder = new StringBuilder();
        if(requestParams != null && !requestParams.isEmpty()) {
            stringBuilder.append(SEPARATOR)
                    .append(PARAMS);
            for (Map.Entry<String, String> entry : requestParams.entrySet()) {
                stringBuilder.append(SEPARATOR)
                        .append(entry.getKey())
                        .append(SEPARATOR)
                        .append(entry.getValue());
            }
        }
        return stringBuilder.toString();
    }

    private String getActionExtra() {
        return new StringBuilder(COMMAND_ID)
                .append(SEPARATOR)
                .append(module)
                .append(buildParamsList())
                .append(SEPARATOR)
                .append(COMMAND_ID)
                .append(SEPARATOR)
                .append(menu)
                .toString();
    }
}
