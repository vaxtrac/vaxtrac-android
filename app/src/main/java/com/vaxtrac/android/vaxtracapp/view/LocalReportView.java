package com.vaxtrac.android.vaxtracapp.view;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.CustomReportPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.LocalReportPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.LocalReportPresenter.ReportType;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;

import hugo.weaving.DebugLog;

import static android.graphics.Typeface.BOLD;


public class LocalReportView extends LinearLayout {

    private static final String TAG = "LocalReportView";

    Context context;
    Resources resources;
    LocalReportPresenter presenter;

    LinearLayout filterContainer;
    LinearLayout filterOptions;
    ImageButton filterToggle;

    ImageView back;

    //1
    ReportType selectedPosition;
    Spinner period;
    ArrayAdapter<String> periodAdapter;

    //2
    LinearLayout monthLayout;
    Spinner month;
    ArrayAdapter<String> monthAdapter;

    //3
    LinearLayout yearLayout;
    Spinner year;
    ArrayAdapter<String> yearAdapter;

    //4
    LinearLayout selectDateLayout;
    TextView selectedDatePrompt;
    TextView selectedDate;

    LinearLayout reportHeader;
    LinearLayout reportGroupHeader;
    LinearLayout reportCategoryHeader;

    RecyclerView reportView;

    LinearLayout reportTotals;

    TextView emptyMessage;

    LinearLayout minimizedFilter;
    ImageButton smallBackButton;
    TextView description;
    ImageButton maximizeButton;

    public Calendar calendar;

    private String selectedYear;
    private String selectedMonth;
    private boolean monthDefaults = true;

    private static final DateFormat dateFormat = new UnlocalizedDateFormat("yyyy LLL dd");

    private static final ReportType DEFAULT_REPORT_PERIOD = ReportType.DAILY;

    public LocalReportView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        presenter = new CustomReportPresenter(this.context, this);
        resources = context.getResources();
        calendar = Calendar.getInstance();
        selectedPosition = DEFAULT_REPORT_PERIOD;
    }

    @Override

    protected void onFinishInflate() {
        super.onFinishInflate();

        filterContainer = (LinearLayout) findViewById(R.id.filter_container);
        filterOptions = (LinearLayout) findViewById(R.id.filter_options);

        back = (ImageView) findViewById(R.id.back_button);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goUp();
            }
        });

        minimizedFilter = (LinearLayout) findViewById(R.id.minimized_toolbar);
        smallBackButton = (ImageButton) findViewById(R.id.small_back_button);
        smallBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goUp();
            }
        });
        description = (TextView) findViewById(R.id.report_description);
        maximizeButton = (ImageButton) findViewById(R.id.maximize_button);
        maximizeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                filterContainer.setVisibility(VISIBLE);
                minimizedFilter.setVisibility(GONE);
            }
        });

        filterToggle = (ImageButton) findViewById(R.id.toggle_expand);
        filterToggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                filterContainer.setVisibility(GONE);
                minimizedFilter.setVisibility(VISIBLE);
                description.setText(presenter.getReportDescription());
            }
        });

        period = (Spinner) findViewById(R.id.period_spinner);
        periodAdapter = new ArrayAdapter<String>(context, R.layout.reports_spinner_dropdown_item);
        periodAdapter.addAll(presenter.getPeriodItems());
        period.setAdapter(periodAdapter);

        yearLayout = (LinearLayout) findViewById(R.id.year_select_display);
        year = (Spinner) findViewById(R.id.year_spinner);
        yearAdapter = new ArrayAdapter<>(context, R.layout.reports_spinner_dropdown_item);

        monthLayout = (LinearLayout) findViewById(R.id.month_select_display);
        month = (Spinner) findViewById(R.id.month_spinner);
        monthAdapter = new ArrayAdapter<>(context, R.layout.reports_spinner_dropdown_item);

        month.setOnItemSelectedListener(monthSelectListener());

        yearAdapter.addAll(presenter.getAdapterYears());

        month.setAdapter(monthAdapter);
        year.setAdapter(yearAdapter);

        int currentYear = presenter.getCurrentYear();
        int yearPosition = yearAdapter.getPosition(String.valueOf(currentYear));
        if(yearPosition != -1) {
            year.setSelection(yearPosition);
            selectedYear = (String) year.getItemAtPosition(yearPosition);
        } else if (year.getCount() > 0) {
            year.setSelection(0);
            selectedYear = (String) year.getItemAtPosition(0);
        }

        year.setOnItemSelectedListener(yearSelectListener());

        selectDateLayout = (LinearLayout) findViewById(R.id.day_select_display);
        selectedDatePrompt = (TextView) findViewById(R.id.date_label);
        selectedDate = (TextView) findViewById(R.id.date_field);
        selectedDate.setText(presenter.getDailyDateLabel());

        selectedDate.setOnClickListener(getDateDialogListener());

        reportHeader = (LinearLayout) findViewById(R.id.report_fixed_header);
        reportGroupHeader = (LinearLayout) findViewById(R.id.group_header);
        reportCategoryHeader = (LinearLayout) findViewById(R.id.categories_header);

        reportView = (RecyclerView) findViewById(R.id.report_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setAutoMeasureEnabled(true);
        reportView.setLayoutManager(layoutManager);

        reportTotals = (LinearLayout) findViewById(R.id.report_totals);

        emptyMessage = (TextView) findViewById(R.id.empty_message);

        period.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, final int position, long id) {
                Log.d(TAG, "Period Item Selected " + position + " " +
                        periodAdapter.getItem(position));
                Log.d(TAG, "Selected Date Calendar " + calendar.getTime().toString());
                presenter.setCurrentReportType(position);
                selectedPosition = presenter.reportForPosition(position);
                switch (selectedPosition) {
                    case DAILY:
                        monthLayout.setVisibility(View.GONE);
                        yearLayout.setVisibility(View.GONE);
                        selectDateLayout.setVisibility(View.VISIBLE);
                        selectedDatePrompt.setText(presenter.getUnitForPeriod(selectedPosition));
                        selectedDate.setText(presenter.getDailyDateLabel());
                        break;
                    case WEEKLY:
                        monthLayout.setVisibility(View.GONE);
                        yearLayout.setVisibility(View.GONE);
                        selectDateLayout.setVisibility(View.VISIBLE);
                        selectedDatePrompt.setText(presenter.getUnitForPeriod(selectedPosition));
                        selectedDate.setText(presenter.getWeeklyDateLabel());
                        break;
                    case MONTHLY:
                        monthLayout.setVisibility(View.VISIBLE);
                        yearLayout.setVisibility(View.VISIBLE);
                        selectDateLayout.setVisibility(View.GONE);
                        break;
                }
                generateReport(selectedPosition);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        TextView tv = (TextView) findViewById(R.id.report_title);
        try {
            tv.setText(resources.getString(R.string.reports) + " - " + presenter.getCurrentUser());
        } catch (Exception e) {
            Log.d(TAG, "could not get clinic username");
        }

    }


    private AdapterView.OnItemSelectedListener yearSelectListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String previousYear = selectedYear;
                if(monthDefaults) {
                    monthDefaults = false;
                    selectedYear = (String) year.getItemAtPosition(position);
                    monthAdapter.clear();
                    monthAdapter.addAll(presenter.getAdapterMonths((String)year.getSelectedItem()));
                    monthAdapter.notifyDataSetChanged();
                    month.invalidate();

                    if(monthAdapter.getPosition(String.valueOf(presenter.getCurrentMonth())) != -1 ){
                        month.setSelection(monthAdapter.getPosition(String.valueOf(presenter
                                .getCurrentMonth())));
                        selectedMonth = (String) month.getItemAtPosition(month
                                .getSelectedItemPosition());
                    } else {
                        if(month.getCount() > 0) {
                            month.setSelection(0);
                            selectedMonth = (String) month.getItemAtPosition(month
                                    .getSelectedItemPosition());
                        }
                    }
                } else {
                    if (previousYear.equals(year.getItemAtPosition(position))) {
                        Log.d(TAG, "Year hasn't changed");
                        return;
                    } else {
                        selectedYear = (String) year.getItemAtPosition(position);
                        monthAdapter.clear();
                        monthAdapter.addAll(presenter.getAdapterMonths((String) year.getSelectedItem()));
                        monthAdapter.notifyDataSetChanged();
                        month.invalidate();
                        if (monthDefaults) {
                            if (monthAdapter.getPosition(String.valueOf(presenter.getCurrentMonth()))
                                    != -1) {
                                month.setSelection(monthAdapter.getPosition(String.valueOf(presenter
                                        .getCurrentMonth())));
                                selectedMonth = (String) month.getItemAtPosition(month
                                        .getSelectedItemPosition());
                            } else {
                                month.setSelection(0);
                                selectedMonth = (String) month.getItemAtPosition(month
                                        .getSelectedItemPosition());
                            }
                        } else {
                            month.setSelection(0);
                            selectedMonth = (String) month.getItemAtPosition(month
                                    .getSelectedItemPosition());
                        }
                    }
                }

                if (month.getSelectedItemPosition() != AdapterView.INVALID_POSITION &&
                        year.getSelectedItemPosition() != AdapterView.INVALID_POSITION) {
                    Calendar monthlyCalendar = Calendar.getInstance();
                    monthlyCalendar.set(Calendar.YEAR,
                            Integer.parseInt((String) year.getSelectedItem()));
                    monthlyCalendar.set(Calendar.MONTH,
                            Integer.parseInt((String)month.getSelectedItem()) - 1);
                    getReport(ReportType.MONTHLY, monthlyCalendar);
                }else{
                    Log.d(TAG, "Invalid spinner selection");
                    //reportTable.removeAllViews();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG, "year nothing selected");
            }
        };
    }


    private AdapterView.OnItemSelectedListener monthSelectListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String previousMonth = selectedMonth;
                if(previousMonth.equals(month.getItemAtPosition(position))) {
                    Log.d(TAG, "Same month selected");
                    return;
                } else {
                    if (month.getSelectedItemPosition() != AdapterView.INVALID_POSITION &&
                            year.getSelectedItemPosition() != AdapterView.INVALID_POSITION) {
                        selectedMonth = (String) month.getItemAtPosition(position);
                        Calendar monthlyCalendar = Calendar.getInstance();
                        monthlyCalendar.set(Calendar.YEAR, Integer.parseInt((String) year.getSelectedItem()));
                        monthlyCalendar.set(Calendar.MONTH, Integer.parseInt((String) month
                                .getSelectedItem()) - 1);
                        getReport(ReportType.MONTHLY, monthlyCalendar);
                    } else {
                        Log.d(TAG, "Invalid month spinner selection");
                        //reportTable.removeAllViews();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG, "month nothing selected");
            }
        };
    }

    public View.OnClickListener getDateDialogListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog picker = new DatePickerDialog(context, getDialogDateSetListener(),
                        presenter.getCalendarYear(), presenter.getCalendarMonth(),
                        presenter.getCalendarDay());
                picker.getDatePicker().setCalendarViewShown(false);
                picker.getDatePicker().setSpinnersShown(true);
                picker.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
                picker.show();
            }
        };
    }

    public DatePickerDialog.OnDateSetListener getDialogDateSetListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                presenter.setCalendarYear(year);
                presenter.setCalendarMonth(monthOfYear);
                presenter.setCalendarDay(dayOfMonth);
                selectedDate.setText(dateFormat.format(presenter.getCalendar().getTime()));
                getReport(presenter.reportForPosition(period.getSelectedItemPosition()),
                        presenter.getCalendar());
            }
        };
    }

    public void setReportAdapter(final RecyclerView.Adapter adapter) {
        this.post(new Runnable() {
            public void run() {
                reportView.setAdapter(adapter);
                reportView.invalidate();
                showReport(false);
            }
        });
    }

    public void setReportGroups(final String[] reportGroups) {
        this.post(new Runnable() {
            public void run() {
                for (String group : reportGroups) {

                }
            }
        });
    }


    public void setReportHeader(final String[] groups, final String[] categories) {
        Log.d(TAG, "setting groups " + Arrays.toString(groups));
        Log.d(TAG, "setting categories " + Arrays.toString(categories));
        this.post(new Runnable() {
            public void run() {
                reportGroupHeader.removeAllViews();
                reportCategoryHeader.removeAllViews();
                if(groups != null) {
                    reportGroupHeader.addView(makeHeaderLabel("", true, 1));
                    reportCategoryHeader.addView(makeHeaderLabel("", true, 1));
                    for(String group : groups) {
                        reportGroupHeader.addView(makeHeaderLabel(group, false,
                                categories.length, true));
                        for (String category : categories) {
                            reportCategoryHeader.addView(makeHeaderLabel(category, false, 1));
                        }
                    }
                } else {
                    reportCategoryHeader.addView(makeHeaderLabel("", true, 1));
                    for (String category : categories) {
                        reportCategoryHeader.addView(makeHeaderLabel(category, false, 1));
                    }
                }
            }
        });
    }

    public View makeHeaderLabel(String label, boolean empty, float weight) {
        TextView textView;
        if(empty) {
            textView = (TextView) inflate(context, R.layout.report_header_cell_spacer, null);
        } else {
            textView = (TextView) inflate(context, R.layout.report_header_cell, null);
        }
        textView.setLayoutParams(new LayoutParams(0, LayoutParams.MATCH_PARENT, weight));
        textView.setText(label);
        return textView;
    }

    public View makeHeaderLabel(String label, boolean empty, float weight, boolean bold) {
        if(bold) {
            TextView textView = (TextView) makeHeaderLabel(label, empty, weight);
            textView.setTypeface(null, BOLD);
            return textView;
        } else {
            return makeHeaderLabel(label, empty, weight);
        }
    }

    public View makeTotalLabel(String label, boolean first, float weight, boolean bold) {
        TextView textView = (TextView) inflate(context, R.layout.report_header_cell, null);
        if(first) {
            textView.setGravity(Gravity.NO_GRAVITY);
        }
        if(bold) {
            textView.setTypeface(null, BOLD);
        }
        textView.setText(label);
        textView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams
                .WRAP_CONTENT, weight));
        return textView;
    }


    public void setReportTotals(final int[] totals) {
        this.post(new Runnable() {
            public void run() {
                reportTotals.removeAllViews();
                reportTotals.addView(makeTotalLabel(context.getString(R.string.totals), true, 1,
                        true));
                for (int i = 0; i < totals.length; i++) {
                    reportTotals.addView(makeTotalLabel(String.valueOf(totals[i]), false, 1,
                            true));
                }
            }
        });
    }

    private void getMonthlyReport(Calendar monthlyCalendar) {
        presenter.getMonthlyReport(monthlyCalendar);
    }

    private void getDailyReport() {
        presenter.getDailyReport();
    }

    private void getWeeklyReport() {
        presenter.getWeeklyReport();
    }

    public void showReport(final boolean isEmpty) {
        this.post(new Runnable() {
            public void run() {
                if (isEmpty) {
                    showEmptyMessage();
                } else {
                    unhideReportView();
                }
            }
        });
    }

    public void unhideReportView() {
        emptyMessage.setVisibility(GONE);
        reportHeader.setVisibility(VISIBLE);
        reportView.setVisibility(VISIBLE);
        reportTotals.setVisibility(VISIBLE);
    }

    public void showEmptyMessage() {
        emptyMessage.setVisibility(VISIBLE);
        reportHeader.setVisibility(GONE);
        reportView.setVisibility(GONE);
        reportTotals.setVisibility(GONE);
    }

    protected boolean isDailyReport() {
        return selectedPosition == ReportType.DAILY;
    }

    protected boolean isWeeklyReport() {
        return selectedPosition == ReportType.WEEKLY;
    }

    public void getReport(ReportType reportType, Calendar reportCalendar) {
        switch(reportType) {
            case DAILY:
                getDailyReport();
                break;
            case MONTHLY:
                getMonthlyReport(reportCalendar);
                break;
            case WEEKLY:
                getWeeklyReport();
                break;
        }
    }


    private void generateReport(ReportType reportType) {
        switch (reportType) {
            case DAILY:
                getDailyReport();
                break;
            case WEEKLY:
                getWeeklyReport();
                break;
            case MONTHLY:
                Log.d(TAG, "Monthly Selected Date " + selectedDate.getText().toString());
                Calendar monthlyCalendar = Calendar.getInstance();
                Log.d(TAG, "Year item " + year.getSelectedItem() + " position " +
                        year.getSelectedItemPosition());
                Log.d(TAG, "Month item " + month.getSelectedItem() + " position " +
                        month.getSelectedItemPosition());
                if(year.getSelectedItem() != null && month.getSelectedItem() != null) {
                    int selectedMonth = Integer.parseInt((String) month.getSelectedItem());
                    int selectedYear = Integer.parseInt((String) year.getSelectedItem());
                    monthlyCalendar.set(Calendar.YEAR, selectedYear);
                    monthlyCalendar.set(Calendar.MONTH, selectedMonth - 1);
                    monthlyCalendar.set(Calendar.DAY_OF_MONTH, 1);
                    getMonthlyReport(monthlyCalendar);
                }
                break;
        }
    }
}

