package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;

import com.vaxtrac.android.vaxtracapp.view.DeliveryView;

public class DeliveryPresenter extends Presenter {

    private DeliveryView view;
    private Context context;

    public DeliveryPresenter(Context context, DeliveryView view) {
        this.context = context;
        this.view = view;
    }
}
