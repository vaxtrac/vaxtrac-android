package com.vaxtrac.android.vaxtracapp.commcare;

import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.Screen;
import com.vaxtrac.android.vaxtracapp.biometrics.BiometricsManager;
import com.vaxtrac.android.vaxtracapp.models.Patient;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import flow.Flow;

public class FormSessionManager {

    Map<String, Class> screenMap;
    Map<String , Patient> patientMap;
    Map<String, Long> modifiedMap;
    Patient lastPatient = null;
    boolean sessionActive = false;
    String activeSessionID = null;

    public static final String TAG = "FormSessionManager";

    public FormSessionManager(){
        screenMap = new HashMap<>();
        patientMap = new HashMap<>();
        modifiedMap = new HashMap<>();
    }

    public void saveSession(Flow flow){
        Object activeScreen = null;
        if(flow != null) {
            activeScreen = flow.getBackstack().current().getScreen();
        } else {
            Log.d(TAG, "Flow is null");
        }
        Patient activePatient = AppController.mPatient;
        if(activeSessionID != null){
            Log.i(TAG, "Saving session with ID: " + activeSessionID);
            screenMap.put(activeSessionID, activeScreen.getClass());
            patientMap.put(activeSessionID, activePatient);
            modifiedMap.put(activeSessionID, System.currentTimeMillis());
            lastPatient = activePatient;
            activeSessionID = null;
            AppController.mPatient = null;
        }
        else{
            Log.e(TAG, "No active SessionID to save!");
            activeSessionID = null;
            if (AppController.mPatient != null)
                lastPatient = AppController.mPatient;
            AppController.mPatient = null;
        }
    }

    public boolean loadSession(String sessionID){
        try {
            if (sessionID == null) {
                Log.i(TAG, "No sessionID passed");
                activeSessionID = null;
                sessionActive = false;
                return false;
            }
            if (screenMap.get(sessionID) == null) {
                Log.i(TAG, String.format("New session with ID: %s. Killing stale matches.", sessionID));
                BiometricsManager.matches = null;
                activeSessionID = sessionID;
                sessionActive = false;
                return false;
            }
            if (patientMap.get(sessionID) == null) {
                Log.i(TAG, String.format("Session found with ID: %s but no patient attached.", sessionID));
                Log.i(TAG, "Trying to open screen anyway");
                activeSessionID = sessionID;
                sessionActive = true;
                return true;
            }
            Log.i(TAG, "Found session with ID: " + sessionID);
            activeSessionID = sessionID;
            sessionActive = true;
            return true;
        }catch(NullPointerException e){
            Log.e(TAG, e.toString());
            return false;
        }

    }

    public Screen getSessionScreen(){
        return getSessionScreen(activeSessionID);
    }

    private Screen getSessionScreen(String id){
        if(!sessionActive){return null;}
        Class screenClass = screenMap.get(id);
        Screen screen = null;
        try {
            Constructor constructor = screenClass.getConstructor();
            screen = (Screen) constructor.newInstance();
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "Couldn't create constructor");
            e.printStackTrace();
        } catch(IllegalAccessException e){
            Log.e(TAG, "Couldn't create constructor");
            e.printStackTrace();
        }catch (InvocationTargetException e){
            Log.e(TAG, "Couldn't create constructor");
            e.printStackTrace();
        }catch (InstantiationException e){
            Log.e(TAG, "Couldn't create constructor");
            e.printStackTrace();
        }
        Log.i(TAG, "Loading screen of type: " + screenClass.toString());
        return screen;
    }

    public Patient getLastPatient(){return lastPatient;}

    public Patient getSessionPatient(){
        return getSessionPatient(activeSessionID);
    }

    private Patient getSessionPatient(String id){
        if(!sessionActive){return null;}
        return patientMap.get(id);
    }

    public boolean alreadySynced(){
        if(modifiedMap.get(activeSessionID) == null){
            return false;
        }
        if(modifiedMap.get(activeSessionID) <= AppController.lastSyncStamp){
            Log.i(TAG,"System already synced this case!");
            return true;
        }else{
            Log.i(TAG,"Case hasn't been synced...");
            return false;
        }
    }
}
