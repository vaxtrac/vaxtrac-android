package com.vaxtrac.android.vaxtracapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import hugo.weaving.DebugLog;

public class MemoryDBHandler {

    private SQLiteOpenHelper mDBHelper;
    protected String TAG = "MemoryDBHandler";
    public Map<String,String> FIELD_TRANSLATION;
    public String TABLE_NAME;
    public Map<String,String> TABLE_MAP;
    protected SQLiteDatabase mDatabase;

    private static final DateFormat caseDateFormat =
            new UnlocalizedDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

    public MemoryDBHandler(SQLiteOpenHelper dbHelper){
        mDBHelper = dbHelper;
        mDatabase = mDBHelper.getWritableDatabase();
    }

    public void executeSql(String sql){
        mDatabase.execSQL(sql);
    }

    public Cursor rawQuery(String query, String[] selectionArgs) {
        return mDatabase.rawQuery(query, selectionArgs);
    }

    public void commitRow(ContentValues values){
        try{
            Log.i(TAG, "Adding row to Table: " + TABLE_NAME);
            Cursor cursor = mDatabase.rawQuery("select 1 from "+TABLE_NAME+" where docid=?",
                new String[] {values.get("docid").toString()});
            boolean exists = (cursor.getCount() > 0);
            cursor.close();

            if (!exists) {
                mDatabase.insert(TABLE_NAME, null, values);
            }
            else {
                mDatabase.update(TABLE_NAME, values, "docid=?", new String[] {values.get("docid").toString()});
            }

        }catch(Exception e){
            Log.e(TAG, "Couldn't insert asset into table, updateCase required?", e);
            try{
                mDatabase.replace(TABLE_NAME, null, values);
            }catch (Exception e2){
                Log.i(TAG, "Couldn't insert or updateCase item...");
            }
        }
    }

    public void commitRow(Map<String,String> data){
        ContentValues contentValues = new ContentValues();
        String[] fields = getColumnNames();
        if (data.get("docid")==null){
            Log.i(TAG, "DB Commit lacking primary key, aborting commit to " + TABLE_NAME);
            return;
        }

        for(String field: fields){
            contentValues.put(field, data.get(field));
        }
        Log.i(TAG, String.format("Pushing doc %s to %s", data.get("docid"),TABLE_NAME));
        commitRow(contentValues);
    }

    public String[] getColumnNames(){
        return new String[0];
    }

    public String[] getColumnNamesWithRowID(){
        String[] out = new String[getColumnNames().length+1];
        out[0] = "_id";
        System.arraycopy(getColumnNames(), 0, out, 1, getColumnNames().length);
        return out;
    }

    public Cursor allItems(){
        String[] items = prependRowID(getColumnNames());
        return mDatabase.query(TABLE_NAME, items, null, null, null, null,"docid"+ " ASC");
    }

    public Cursor allRowsMatchingItems(String columnName, String[] matchingKeys){
        String[] items = prependRowID(getColumnNames());
        String questionMarks = new String(new char[matchingKeys.length-1]).replace("\0", "?,");
        questionMarks += "?";
        String select = String.format("%s IN(%s)", columnName, questionMarks);
        Log.d(TAG, select);
        return mDatabase.query(TABLE_NAME, items, select, matchingKeys, null, null,"docid" + " ASC");
    }

    public Cursor allRowsFromColumn(String columnName){
        String[] items = prependRowID(new String[]{"docid", columnName});
        return mDatabase.query(TABLE_NAME, items, null,null,null,null,"docid" + " ASC");
    }

    public Cursor uniqueItemsFromColumn(String columnName){
        String[] items = new String[] {"DISTINCT " +columnName + " as _id"};
        return mDatabase.query(TABLE_NAME, items, null, null, null, null, columnName + " ASC");
    }

    public Cursor allRowsItemsLike(String columnName, String searchTerm){
        String[] items = prependRowID(getColumnNames());
        String select = columnName + " LIKE ? ";
        Log.i(TAG, "SearchTerm : " + searchTerm);
        String[] selectArgs  = new String[] { searchTerm+"%" };
        return mDatabase.query(TABLE_NAME, items, select, selectArgs, null, null, "docid" + " ASC");
    }

    public Cursor allRowsItemsBetween(String columnName, String searchStart, String searchEnd){
        return allRowsItemsBetween(columnName, searchStart, searchEnd, "docid");
    }

    public Cursor allRowsItemsBetween(String columnName, String searchStart, String searchEnd, String sortField){
        String[] items = prependRowID(getColumnNames());
        String select = columnName + " BETWEEN ? AND ? ";
        Log.i(TAG, String.format("SearchStart :  %s | SearchEnd: %s", searchStart, searchEnd));
        String[] selectArgs  = new String[] { searchStart, searchEnd };
        return mDatabase.query(TABLE_NAME, items, select, selectArgs, null, null, sortField + " ASC");
    }

    public Cursor allRowsItemsBetween(String columnName, String searchStart, String searchEnd,
                                      String createdBy, String sortField){
        String[] items = prependRowID(getColumnNames());
        String select = "";
        String[] selectArgs;
        if(createdBy != null && !createdBy.isEmpty()) {
            select = " createdby = ? AND ";
            selectArgs = new String[] {createdBy, searchStart, searchEnd};
        } else {
            selectArgs = new String[] {searchStart, searchEnd};
        }
        select += columnName + " BETWEEN ? AND ? ";
        Log.i(TAG, String.format("SearchStart :  %s | SearchEnd: %s", searchStart, searchEnd));
        if(sortField == null) {
            sortField = "docid";
        }
        return mDatabase.query(TABLE_NAME, items, select, selectArgs, null, null, sortField + " ASC");
    }

    public Cursor allRowsBetweenGroup(String columnName, String searchStart, String searchEnd,
                                      String[] groupBy) {
        String[] items = prependRowID(getColumnNames());
        String select = columnName + " BETWEEN ? AND ? ";
        Log.i(TAG, String.format("SearchStart :  %s | SearchEnd: %s", searchStart, searchEnd));
        String[] selectArgs  = new String[] { searchStart, searchEnd };
        return mDatabase.query(TABLE_NAME, items, select, selectArgs, TextUtils.join(",", groupBy), null, null);
    }

    public Cursor itemsLike(String columnName, String searchTerm){
        String[] items = prependRowID(new String[] {"docid",columnName});
        String select = columnName + " LIKE ? ";
        Log.i(TAG, "SearchTerm : " + searchTerm);
        String[] selectArgs  = new String[] { "%"+searchTerm+"%" };
        return mDatabase.query(TABLE_NAME, items, select, selectArgs, null, null, "docid" + " ASC");
    }

    public Cursor allRowsColumnEquals(String columnName, String searchTerm){
        return allRowsColumnEquals(columnName, searchTerm, "docid");
    }

    public Cursor allRowsColumnEquals(String columnName, String searchTerm, String orderField){
        String[] items = prependRowID(getColumnNames());
        String select = columnName + " = ? ";
        Log.i(TAG, "SearchTerm : " + searchTerm);
        String[] selectArgs  = new String[] { searchTerm };
        return mDatabase.query(TABLE_NAME, items, select, selectArgs, null, null, orderField + " ASC");
    }

    static public String [] prependRowID(String[] args){
        return prependArbitrary(args, "rowid as _id");
    }

    static private String [] prependArbitrary(String[] args, String toPrepend){
        String[] out = new String[args.length+1];
        out[0] = toPrepend;
        System.arraycopy(args, 0, out, 1, args.length);
        return out;
    }

    public Cursor query(String[] items, String select, String[] selectArgs, String orderString){
        return mDatabase.query(TABLE_NAME, items, select, selectArgs, null,null,orderString);
    }

    public Cursor distinctMonthsForYear(int year) {
        return mDatabase.query(true, TABLE_NAME, new String[]{"month"}, "year = ?", new
                        String[]{String.valueOf(year)}, null, null, null, null);
    }


    public Cursor doseAggregateReport(String startDate, String endDate, String[] group, String[]
                                      startAgeBin, String[] endAgeBin) {
        String rollupQuery =
                "select vaccinename, series, sessiontype, group_concat(bin) as categories, " +
                        "group_concat(total) as totals " +
                        "from  ( %s ) group by vaccinename, series, sessiontype";

        String[] columns = new String[]{"vaccinename", "series", "sessiontype",
                "count(*) as total", ""};
        String whereClause =
                "cast(patientage as integer) >= %s and cast(patientage as integer) <= %s " +
                        "and timestamp between '%s' and '%s'";
        String groupBy = "vaccinename, series, sessiontype, bin";
        String orderBy = "vaccinename, series, bin";

        if(startAgeBin.length != endAgeBin.length) {
            Log.e(TAG, "start ages and end ages must be the same length");
            return null;
        }

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_NAME);
        String[] subQueries = new String[startAgeBin.length];
        for(int i = 0, bins = startAgeBin.length; i < bins; i++){
            /* replace placeholder column with group integer */
            columns[columns.length - 1] = i + " as bin";
            subQueries[i] = queryBuilder.buildQuery(columns, String.format(whereClause,
                    startAgeBin[i], endAgeBin[i], startDate, endDate), groupBy, null, null, null);
        }
        return mDatabase.rawQuery(String.format(rollupQuery, queryBuilder.buildUnionQuery(subQueries,
                null, null), null), null);
    }

    public Cursor doseAggregateReport(String startDate, String endDate, String[] startAgeBin,
                                      String[] endAgeBin) {
        /*

        select vaccinename, series, 0 as bin, count(*) as total from doses
        where patientage >= startAgeBin[0] and patientage <= endAgeBin[0]
        group by vaccinename, series, bin

        union

        select vaccinename, series, 1 as bin, count(*) as total from doses
        where patientage >= startAgeBin[1] and patientage <= endAgeBin[1]
        group by vaccinename, series, bin

        union

        select vaccinename, series, 2 as bin, count(*) as total from doses
        where patientage >= startAgeBin[2] and patientage <= endAgeBin[2]
        group by vaccinename, series, bin

         */

        String rollupQuery =
                "select vaccinename, series, group_concat(bin) as categories, group_concat(total) as totals " +
                "from  ( %s ) group by vaccinename, series";

        String[] columns = new String[]{"vaccinename", "series", "count(*) as total", ""};
        String whereClause =
                "cast(patientage as integer) >= %s and cast(patientage as integer) <= %s " +
                "and timestamp between '%s' " +
                "and '%s'";
        String groupBy = "vaccinename, series, bin";
        String orderBy = "vaccinename, series, bin";

        if(startAgeBin.length != endAgeBin.length) {
            Log.e(TAG, "start ages and end ages must be the same length");
            return null;
        }

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_NAME);
        String[] subQueries = new String[startAgeBin.length];
        for(int i = 0, bins = startAgeBin.length; i < bins; i++){
            /* replace placeholder column with group integer */
            columns[columns.length - 1] = i + " as bin";
            subQueries[i] = queryBuilder.buildQuery(columns, String.format(whereClause,
                    startAgeBin[i], endAgeBin[i], startDate, endDate), groupBy, null, null, null);
        }
        Log.d(TAG, "dose aggregate report" + String.format(rollupQuery, queryBuilder
                        .buildUnionQuery(subQueries, null, null), null));
        return mDatabase.rawQuery(String.format(rollupQuery, queryBuilder.buildUnionQuery(subQueries,
                        null, null), null), null);
    }

    public ContentValues getMinMax(String column) {
        Cursor cursor = mDatabase.rawQuery(String.format("select min(%s) minimum, max(%s) " +
                        "maximum from %s", column, column, TABLE_NAME), null);
        ContentValues contentValues = new ContentValues();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contentValues;
    }

    public Cursor allRowsUniqueColumnSorted(String column, String sort) {
        return mDatabase.query(true, TABLE_NAME, new String[]{column}, null, null, null, null,
                column + " " + sort, null);
    }

    private void updateNewPatientsCount() {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        Calendar yesterdayMidnight = new GregorianCalendar(
                yesterday.get(Calendar.YEAR),
                yesterday.get(Calendar.MONTH),
                yesterday.get(Calendar.DATE),
                23,
                59
        );
        Timestamp tsMidnight = new Timestamp(yesterdayMidnight.getTime().getTime());

        Context context = AppController.getAppContext();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int patientsCount = sharedPref.getInt(context.getString(R.string.saved_registered_patients_count), 0);

        Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
        String savedTimestamp = sharedPref.getString(
                context.getString(R.string.saved_last_registered_patient_timestamp), now.toString());
        Timestamp tsLastModified = Timestamp.valueOf(savedTimestamp);

        if (tsLastModified.before(tsMidnight))
            patientsCount = 1;
        else
            patientsCount++;

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(context.getString(R.string.saved_registered_patients_count), patientsCount);
        editor.putString(context.getString(R.string.saved_last_registered_patient_timestamp), now.toString());
        editor.apply();

        StringBuilder sb = new StringBuilder();
        sb.append(patientsCount);
        sb.append(" ");
        sb.append(context.getString(R.string.patients_registered_today));
        Log.i(TAG, "patients count " + patientsCount);


       // Toast.makeText(context, sb.toString(), Toast.LENGTH_SHORT).show();
    }

}
