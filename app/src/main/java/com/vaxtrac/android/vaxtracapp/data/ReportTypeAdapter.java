package com.vaxtrac.android.vaxtracapp.data;

import android.util.Log;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.vaxtrac.android.vaxtracapp.models.ReportData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReportTypeAdapter extends TypeAdapter<ReportData> {

    private static final String TAG = ReportTypeAdapter.class.getName();

    private static final String test = "{       \"bcg\":{\"0\" : [1,1,1]}" +
            "                                   \"opv\":{\"0\" : [0,1,0]"  +
            "                                            \"1\" : [1,0,1]}" +
            "                            \"DTwPHibHep\":{\"0\" : [9,9,9]}}";

    @Override
    public void write(JsonWriter out, ReportData value) throws IOException {

    }

    @Override
    public ReportData read(JsonReader in) throws IOException {
        ReportData reportData = new ReportData();
        if (in.peek().equals(JsonToken.BEGIN_OBJECT)) {
            in.beginObject();
            while (!in.peek().equals(JsonToken.END_OBJECT)) {
                if(in.peek().equals(JsonToken.NAME)) {
                    /* Antigen Key */
                    String antigenKey = in.nextString();
                    Log.d(TAG, "antigenKey " + antigenKey);
                    while(!in.peek().equals(JsonToken.END_OBJECT)) {
                        if(in.peek().equals(JsonToken.BEGIN_OBJECT)) {
                            String seriesKey = in.nextString();
                            Log.d(TAG, "seriesKey " + seriesKey);
                            List<Integer> doseCounts = new ArrayList<>();
                            if (in.peek().equals(JsonToken.BEGIN_ARRAY)) {
                                while (!in.peek().equals(JsonToken.END_ARRAY)) {
                                    doseCounts.add(in.nextInt());
                                }
                                in.endArray();
                            }
                            reportData.addAntigen(antigenKey + "_" + seriesKey);
                            reportData.addSeries(antigenKey + "_" + seriesKey, doseCounts);
                        }
                    }
                    in.endObject();
                }
            }
            in.endObject();
        }
        return reportData;
    }
}
