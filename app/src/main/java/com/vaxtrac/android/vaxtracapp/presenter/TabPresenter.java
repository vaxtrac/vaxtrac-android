package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.view.TabView;


public class TabPresenter extends Presenter {

    private static final String TAG = "TabPresenter";

    private TabView view;
    private Context context;
    private int currIndex = -1;

    public TabPresenter(Context context, TabView view) {
        this.context = context;
        this.view = view;
    }


    public void restoreIndex() {
        loadView(0);
    }

    public void loadView(int index) {
        if(index != currIndex) {
            view.removeHighlight(currIndex);
            view.loadChildView(index);
            currIndex = index;
            bundle.putInt("tabIndex", index);
        }
    }
}
