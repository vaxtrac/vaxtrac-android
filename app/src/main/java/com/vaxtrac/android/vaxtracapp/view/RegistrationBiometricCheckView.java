package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.models.Patient;

import java.lang.ref.WeakReference;

public class RegistrationBiometricCheckView extends BiometricSearchView {

    private final String TAG = RegistrationBiometricCheckView.class.getName();
    private WeakReference<MainActivity> activity;
    boolean isRegistration = true;

    public RegistrationBiometricCheckView(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity = new WeakReference<>((MainActivity)context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.i(TAG, "Finished Inflating!");
    }

    /* Prevent memory leak instead of using WeakReference */
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.d(TAG, "onDetachedFromWindow");
        presenter.setView(null);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d(TAG, "onAttachedToWindow");
        presenter.setView(this);
    }

    @Override
    protected void setButtonAction() {
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity.get() == null) {
                    Log.d(TAG, "Activity is Null");
                } else if (activity.get().getSchedule() == null) {
                    Log.d(TAG, "Schedule is Null");
                }
                AppController.mPatient = new Patient(true, activity.get().getSchedule());
                AppController.mPatient.loadSearchObject(AppController.mSearchObject);
                AppController.getFlow().goTo(new Screens.TabScreen());
            }
        });

    }

    @Override
    public void noMatchFound(boolean skipMatching) {
        if(skipMatching) {
            button.performClick();
        } else {
            super.noMatchFound(false);
        }
    }
}
