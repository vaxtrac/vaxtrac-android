package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by sam on 6/12/15.
 */
public class PatientsCountHandler {
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private Timestamp tsMidnight;

    public PatientsCountHandler() {
        context = AppController.getAppContext();
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPref.edit();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        Calendar yesterdayMidnight = new GregorianCalendar(
                yesterday.get(Calendar.YEAR),
                yesterday.get(Calendar.MONTH),
                yesterday.get(Calendar.DATE),
                23,
                59
        );
        tsMidnight = new Timestamp(yesterdayMidnight.getTime().getTime());
    }

    public int getSeenPatientsCount() {
        Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
        String savedTimestamp = sharedPref.getString(
                context.getString(R.string.saved_last_registered_patient_timestamp), now.toString());
        Timestamp tsLastModified = Timestamp.valueOf(savedTimestamp);
        int patientsCount = sharedPref.getInt(context.getString(R.string.saved_registered_patients_count), 0);
        if (tsLastModified.before(tsMidnight)) {
            patientsCount = 0;
        }
        return patientsCount;
    }

    public int incrementSeenPatientsCount() {
        int patientsCount = getSeenPatientsCount();
        updateSharedPreferences(patientsCount+1);
        return patientsCount+1;
    }

    private void updateSharedPreferences(int patientsCount){
        Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
        editor.putInt(context.getString(R.string.saved_registered_patients_count), patientsCount);
        editor.putString(context.getString(R.string.saved_last_registered_patient_timestamp), now.toString());
        editor.apply();
    }
}
