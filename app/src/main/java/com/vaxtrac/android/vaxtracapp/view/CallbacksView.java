package com.vaxtrac.android.vaxtracapp.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.CallbacksPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.CustomCallbacksPresenter;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CallbacksView extends LinearLayout {

    private Context context;
    CallbacksPresenter presenter;


    private ImageView back;
    private LinearLayout container;
    private ToggleButton overdueToggle;
    protected Spinner villageSelect;
    private TextView startDateLabel;
    private TextView startDateDisplay;
    private TextView durationLabel;
    private Spinner durationSelect;
    private Button search;

    private ArrayAdapter<String> overdueAdapter;
    protected ArrayAdapter<String> villageAdapter;
    private ArrayAdapter<String> durationAdapter;

    private boolean overdue;
    protected List<String> villages;
    private String village;
    private Date startDate;
    private int duration;

    public CallbacksView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        //prevent keyboard from popping up
        ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        presenter = new CustomCallbacksPresenter(context, this);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        back = (ImageView) findViewById(R.id.back_button);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goBack();
            }
        });

        container = (LinearLayout) findViewById(R.id.callbacks_container);
        container.requestFocus();

        villageSelect = (Spinner) findViewById(R.id.village_spinner);
        villageSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("CallbacksView", "findFocus " + CallbacksView.this.findFocus().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        villageAdapter = new ArrayAdapter<String>(context, android.R.layout
                .simple_dropdown_item_1line);
        if (villages != null) {
            villageAdapter.addAll(getVillageLabels(villages));
        }
        villageSelect.setAdapter(villageAdapter);



        startDateLabel = (TextView) findViewById(R.id.date_display_textview);
        startDateDisplay = (TextView) findViewById(R.id.date_display);
        presenter.getStartingDate();
        startDateDisplay.setOnClickListener(presenter.getCalendarListener());

        durationLabel = (TextView) findViewById(R.id.duration_display_textview);
        durationSelect = (Spinner) findViewById(R.id.duration_display);
        durationSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("CallbacksView", "findFocus " + CallbacksView.this.findFocus().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        durationAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, presenter.getDurations()) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null) {
                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService
                            (Context.LAYOUT_INFLATER_SERVICE);
                    convertView = layoutInflater.inflate(android.R.layout
                            .simple_dropdown_item_1line, null);
                }
                TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
                textView.setText(NumberFormat.getInstance().format(Integer.parseInt
                        (getItem(position))));
                return convertView;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                textView.setText(NumberFormat.getInstance().format(Integer.parseInt(getItem
                        (position))));
                return textView;
            }
        };
        durationSelect.setAdapter(durationAdapter);

        overdueToggle = (ToggleButton) findViewById(R.id.overdue_switch);
        overdueToggle.setTextOn(context.getString(R.string.yes));
        overdueToggle.setTextOff(context.getString(R.string.no));
        overdueToggle.setChecked(false);
        overdueToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                overdue = isChecked;
                if (isChecked) {
                    durationSelect.setVisibility(View.GONE);
                    startDateDisplay.setVisibility(View.GONE);
                    startDateLabel.setVisibility(View.GONE);
                    durationLabel.setVisibility(View.GONE);
                } else {
                    durationSelect.setVisibility(View.VISIBLE);
                    startDateDisplay.setVisibility(View.VISIBLE);
                    startDateLabel.setVisibility(View.VISIBLE);
                    durationLabel.setVisibility(View.VISIBLE);
                }
            }
        });

        search = (Button) findViewById(R.id.viewCallbacks);
        search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.setResult(
                        overdue,
                        getVillage(),
                        getDuration());
            }
        });

    }

    public void setVillages(List<String> villages) {
        List<String> villageLabels = getVillageLabels(villages);
        /* CursorLoader might return before adapter is ready */
            if (villageAdapter != null) {
                villageAdapter.addAll(villageLabels);
            } else {
                this.villages = villages;
            }
    }

    protected List<String> getVillageLabels(List<String> villages) {
        List<String> villageLabels = new ArrayList<>(villages);
        villageLabels.add(0, AppController.getStringResource(R.string.all_villages));
        return villageLabels;
    }

    protected String getVillage() {
        int selectedPos = villageSelect.getSelectedItemPosition();
        return selectedPos > 0 ? villageAdapter.getItem(selectedPos) : null;
    }

    private int getDuration() {
        return Integer.parseInt(durationAdapter.getItem(durationSelect.getSelectedItemPosition()));
    }

    public void updateText(String date){
        startDateDisplay.setText(date);
    }

}
