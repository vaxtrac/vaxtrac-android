package com.vaxtrac.android.vaxtracapp.presenter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.biometrics.BiometricsManager;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;
import com.vaxtrac.android.vaxtracapp.models.Patient;
import com.vaxtrac.android.vaxtracapp.models.Schedule;
import com.vaxtrac.android.vaxtracapp.models.SearchObject;
import com.vaxtrac.android.vaxtracapp.view.VerifyGuardianView;

import java.lang.ref.WeakReference;

public class VerifyGuardianPresenter extends GuardianPickerPresenter {

    private static final String TAG = "VerifyGuardianPresenter";

    public static final int VERIFY_GUARDIAN_SCAN_CODE = 6541279;
    public static final int VERIFY_GUARDIAN_REGISTER_NEW_CODE = 6541280;
    private static final String PATIENT_CASE_ID = "patient_case_id";

    private WeakReference<MainActivity> mActivity;
    protected VerifyGuardianView view;

    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver scanFailedReceiver;
    BroadcastReceiver verifyOkReceiver;
    BroadcastReceiver verifyFailedReceiver;

    private static String guardianID;

    public VerifyGuardianPresenter(Context context, VerifyGuardianView view) {
        super(context, view);
        this.view = view;
        mActivity = new WeakReference<>((MainActivity) context);
        localBroadcastManager = LocalBroadcastManager.getInstance(context);
        scanFailedReceiver = getScanFailedReceiver();
        verifyOkReceiver = getVerifyOkReceiver();
        verifyFailedReceiver = getVerifyFailedReceiver();
        unregisterReceivers();
        registerReceivers();
    }

    private BroadcastReceiver getScanFailedReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            scanFailed();
                        } catch (NullPointerException e){

                        }
                    }
                });
            }
        };
    }

    private void scanFailed() {
        view.scanFailed();
    }

    private BroadcastReceiver getVerifyOkReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            openPatientCase(mActivity.get().getSchedule());
                        } catch (NullPointerException e){
                            Log.e(TAG, "Null Pointer on Verify Ok", e);
                        }
                    }
                });
            }
        };
    }



    private BroadcastReceiver getVerifyFailedReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            verifyFailed();
                        } catch (NullPointerException e){
                            Log.e(TAG, "Null Pointer on Verify Failed", e);
                        }}
                });
            }
        };
    }

    private void verifyFailed() {
        view.verifyFailed();
    }

    public void StartScan(String guardianID){
        //TODO Notify UI Scan Started
        Log.i(TAG, "Starting Scan Intent");
        this.guardianID = guardianID;
        BiometricsManager.getVerifyScans(mActivity.get(), VERIFY_GUARDIAN_SCAN_CODE);

    }

    public static void registerScanResult(int resultCode, final Intent data) {
        Log.i(TAG, "Caught data from biometric scan in presenter");
        if (resultCode != Activity.RESULT_OK){
            Log.i(TAG, "Scan Failed...");
            BiometricsManager.scanFailed();
        }else{
            final String guardian = guardianID;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    SearchObject verifyObject = new SearchObject(data.getExtras());
                    verifyObject.verifyGuardianIdentity(guardian);

                }
            });
            t.start();
        }
    }

    @Override
    protected void noGuardianFound() {
        super.noGuardianFound();
    }

    @Override
    protected void guardiansReady() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    populateTable(getGuardians());
                } catch (NullPointerException e) {
                    Log.e(TAG, "Couldn't populate GuardianPicker Table", e);
                }
            }
        });
    }

    private void populateTable(Cursor cursor) {
        view.populateTable(cursor);
    }

    public static void openPatientCase(Schedule schedule){
        AppController.mPatient = new Patient(schedule);
        AppController.mPatient.loadFromCommCare(getChildID());
        //todo CLEAR stack?
        AppController.getFlow().goTo(new Screens.TabScreen());
    }

    public void registerGuardian(){
        //TODO add guardian registration form
        mActivity.get().startActivityForResult(CommCareAPI.getAddGuardianForm(getChildID()),
                VERIFY_GUARDIAN_REGISTER_NEW_CODE);
    }

    public View getView(){
        return view;
    }

    public void setView(View view) {
        if(view != null) {
            this.view = (VerifyGuardianView) view;
        } else {
            this.view = null;
        }
    }

    public void unregisterReceivers() {
        localBroadcastManager.unregisterReceiver(verifyFailedReceiver);
        localBroadcastManager.unregisterReceiver(verifyOkReceiver);
        localBroadcastManager.unregisterReceiver(scanFailedReceiver);
    }

    public void destroyBroadcastManager() {
        verifyOkReceiver = null;
        verifyFailedReceiver = null;
        scanFailedReceiver = null;
        localBroadcastManager = null;
    }

    public void registerReceivers() {
        localBroadcastManager.registerReceiver(verifyFailedReceiver, new IntentFilter(BiometricsManager
                .VERIFY_FAILED));
        localBroadcastManager.registerReceiver(verifyOkReceiver, new IntentFilter(BiometricsManager
                .VERIFY_OK));
        localBroadcastManager.registerReceiver(scanFailedReceiver, new IntentFilter(BiometricsManager
                .SCAN_FAILED));
    }

    public static void guardianRegistrationOK(){
        Log.i(TAG, "Registered a new Guardian");
        AppController.getFlow().goTo(new Screens.SearchScreen());
    }
    public static void guardianRegistrationFailed(){
        Log.i(TAG, "Failed to register a new guardian");
        AppController.getFlow().goTo(new Screens.SearchScreen());
    }
}
