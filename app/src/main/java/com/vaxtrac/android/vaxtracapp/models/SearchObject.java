package com.vaxtrac.android.vaxtracapp.models;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.biometrics.BiometricsManager;
import com.vaxtrac.android.vaxtracapp.data.UnionCursor;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SearchObject {
    /*
    Search objects are used to build searches.
     */
    private static final String TAG = "SearchObject";
    public static final String SKIP_FINGERPRINTS_EXTRA = "skip_fingerprints";

    final private List<String> fingerFields = new ArrayList<String>(){{
        add("left_index");
        add("left_thumb");
        add("right_index");
        add("right_thumb");
    }};
    private Map<String,String> properties;

    public SearchObject(Bundle bundle){
        properties = new HashMap<>();
        if (bundle == null){
            Log.d(TAG, "Empty bundle sent to constructor.");
            return;
        }
        Set<String> keys = bundle.keySet();
        for (String key: keys){
            try {
                properties.put(key, bundle.getString(key));
                Log.d(TAG, String.format("Added %s to possible search terms\n%s", key, bundle.getString(key)));
            } catch (Exception exception) {
                Log.d(TAG, key + " key can't be cast to string");
                continue;
            }
        }
    }

    public Map<String,String> getProperties(){
        return properties;
    }

    public List<String> getFingerFields(){
        return fingerFields;
    }

    public boolean skipFingerprints() {
        return properties.get(SKIP_FINGERPRINTS_EXTRA) != null && properties.get(SKIP_FINGERPRINTS_EXTRA)
                .equalsIgnoreCase("Yes");
    }

    public void getPotentialBiometricMatches(Cursor cursor, String uuidField){
        BiometricsManager biometricsManager = AppController.mBiometricsManager;
        Map<String, String> templates = getBiometrics();
        Map<String, String> templatePlacement = new HashMap<>();
        for(String finger : fingerFields){
            templatePlacement.put(finger.replace("_", ""),finger);
            Log.d(TAG, String.format("%s --> %s",finger,finger.replace("_", "")));
        }
        if(cursor.getCount()<1){
            Log.i(TAG, "Empty Cursor passed to GetBiometricMatches, sending nomatch");
            BiometricsManager.noMatchFound();
            return;
        }
        biometricsManager.loadCache(cursor, uuidField, templatePlacement);
        biometricsManager.findMatches(templates);
    }

    public void verifyGuardianIdentity(String guardianID){
        BiometricsManager biometricsManager = AppController.mBiometricsManager;
        Map<String, String> templates = getBiometrics();
        biometricsManager.verifyGuardianIdentity(guardianID, templates);
    }

    public Cursor getPotentialMatches(){
        /*
        TODO Flesh out this to be more flexible
         */
        Cursor c1 = new MatrixCursor(new String[0],0);
        Cursor c2 = new MatrixCursor(new String[0],0);
        Cursor c3 = new MatrixCursor(new String[0],0);

        try{
            String sexTerm = properties.get("child_sex");
            c1 = AppController.mCommCareDataManager.guardianDB.allRowsColumnEquals("childsex", sexTerm);
            Log.d(TAG, String.format("Found %s potential matches on Sex.", c1.getCount()));
        }catch(Exception e){
            Log.d(TAG, "SexCursor Failed");
            e.printStackTrace();
        }
        try{
            String childDOB = properties.get("child_dob");
            DateFormat df = new UnlocalizedDateFormat("yyyy-MM-dd");
            DateFormat endFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
            Date dob = df.parse(childDOB);
            Calendar c = Calendar.getInstance();
            c.setTime(dob);
            c.add(Calendar.DATE, -30);
            Date startDate = c.getTime();
            c.add(Calendar.DATE, 60);
            Date endDate = c.getTime();

            String start = df.format(startDate);
            String finish = endFormat.format(endDate);
            Log.d(TAG, String.format("60 day window... %s | %s | %s",start, childDOB, finish));
            c2 = AppController.mCommCareDataManager.guardianDB.allRowsItemsBetween("childdob", start, finish);
            Log.d(TAG, String.format("Found %s potential matches on DOB.", c2.getCount()));
        }catch(Exception e){
            Log.d(TAG, "DOBCursor Failed");
            e.printStackTrace();
        }
        try{
            c3 = new UnionCursor(AppController.mCommCareDataManager.guardianDB.getColumnNamesWithRowID(),c1,c2);
        }catch(Exception e){
            Log.d(TAG, "UnionCursor Failed");
            e.printStackTrace();
        }
        Log.d(TAG, String.format("Found %s potential matches.", c3.getCount()));
        return c3;
    }

    public Map<String,String> getBiometrics(){
        Map<String,String> templateMap = new HashMap<>();
        for (String finger: fingerFields){
            String template = properties.get(finger);
            if (finger != null){
                templateMap.put(finger,template);
            }
        }
        if (templateMap.isEmpty()){return null;}
        return templateMap;
    }

    public void setBiometrics(Bundle bundle){
        for (String finger: fingerFields){
            String template = bundle.getString(finger);
            if (finger != null){
                properties.put(finger, template);
                Log.d(TAG, String.format("Added %s to bundle", finger));
            }
        }
    }
}
