package com.vaxtrac.android.vaxtracapp;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sarwar on 3/18/15.
 *
 * Default version feature controls.
 * Overridden in flavor specific AppVersion
 */
public class DefaultAppFlags {

    public static final Map<String, Boolean> defaultOptions = new HashMap<String, Boolean>(){{
        put("vaxtrac-number", false);
        put("card-number-search", false);
        put("phone-number-search", false);
        put("name-search", false);
        put("ocr", false);
        put("stock-management", false);
    }};

    protected static final String TAG = "AppFlags";

}
