package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

public class ResizingListView extends ListView {
    public ResizingListView(Context context) {
        super(context);
    }

    public ResizingListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ResizingListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int maxWidth = meathureWidthByChilds() + getPaddingLeft() + getPaddingRight();
        super.onMeasure(MeasureSpec.makeMeasureSpec(maxWidth, MeasureSpec.EXACTLY), heightMeasureSpec);
    }

    public int meathureWidthByChilds() {
        int maxWidth = 0;
        View view = null;
        if(getAdapter() != null) {
            for (int i = 0; i < getAdapter().getCount(); i++) {
                view = getAdapter().getView(i, view, this);
                //this measures the view before its rendered with no constraints from parent
                view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
                if (view.getMeasuredWidth() > maxWidth) {
                    maxWidth = view.getMeasuredWidth();
                }
            }
        }
        return maxWidth;
    }
}
