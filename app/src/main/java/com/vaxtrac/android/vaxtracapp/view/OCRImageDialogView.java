package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import com.etsy.android.grid.StaggeredGridView;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.OCRImageDialogPresenter;

import java.lang.ref.WeakReference;

public class OCRImageDialogView extends FrameLayout {

    WeakReference<Context> context;
    OCRImageDialogPresenter presenter;
    StaggeredGridView gridView;
    View highlighted;

    public OCRImageDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = new WeakReference<Context>(context);
        presenter = new OCRImageDialogPresenter(context, this);
    }

    public void onFinishInflate() {
        gridView = (StaggeredGridView) findViewById(R.id.grid_view);
        presenter.populateImages(gridView);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(highlighted != null) {
                    highlighted.setBackground(context.get().getResources()
                            .getDrawable(R.color.White));
                }

                highlighted = view.findViewById(R.id.vial_caption);
                highlighted.setBackground(context.get().getResources().getDrawable(R.color.Green));
                presenter.setSelectedVial(gridView.getItemAtPosition(position));
            }
        });

        super.onFinishInflate();
    }

    public OCRImageDialogPresenter getPresenter() {
        return presenter;
    }
}
