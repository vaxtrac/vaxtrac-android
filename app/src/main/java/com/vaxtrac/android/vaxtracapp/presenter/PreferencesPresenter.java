package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.vaxtrac.android.vaxtracapp.view.PreferencesView;

public class PreferencesPresenter extends Presenter {

    private Context context;
    private PreferencesView view;

    public PreferencesPresenter(Context context, PreferencesView view) {
        this.context = context;
        this.view = view;
    }

    public void handleClick(AdapterView<?> parent, View view, int position, long id) {
    }
}
