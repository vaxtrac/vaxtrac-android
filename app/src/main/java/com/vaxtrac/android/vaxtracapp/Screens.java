package com.vaxtrac.android.vaxtracapp;

import android.view.View;

import com.vaxtrac.android.vaxtracapp.models.Dose;
import com.vaxtrac.android.vaxtracapp.models.Vial;
import com.vaxtrac.android.vaxtracapp.presenter.CallbackCardPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.CallbacksListPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.OCRDetailPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.OCRImagePresenter;
import com.vaxtrac.android.vaxtracapp.presenter.Presenter;
import com.vaxtrac.android.vaxtracapp.view.OCRDetailView;
import com.vaxtrac.android.vaxtracapp.view.TabView;

import java.util.Date;
import java.util.Map;

import flow.HasParent;
import flow.Layout;

public final class Screens {

    @Layout(R.layout.screen_home_constraint)
    public static class ConstraintHomeScreen extends Screen {
    }

    @Layout(R.layout.screen_home)
    public static class HomeScreen extends Screen {
    }

    @Layout(R.layout.screen_callbacks)
    public static class CallbacksScreen extends Screen implements HasParent<HomeScreen> {
        public CallbacksScreen() {}

        @Override
        public HomeScreen getParent() {
            return new HomeScreen();
        }

    }

    @Layout(R.layout.screen_callbacklist)
    public static class CallbackListScreen extends Screen implements HasParent<HomeScreen> {
        CallbacksListPresenter presenter;

        String village;
        Date startingDate;
        int duration;
        boolean overdue;

        public CallbackListScreen() {}

        public CallbackListScreen(boolean overdue, String
                village, Date startingDate, int duration) {
            this.overdue = overdue;
            this.village = village;
            this.startingDate = startingDate;
            this.duration = duration;
        }

        @Override
        public void inject(Presenter presenter) {
            this.presenter = (CallbacksListPresenter) presenter;
            this.presenter.init(overdue, village, startingDate, duration);
        }

        @Override
        public HomeScreen getParent() {
            return new HomeScreen();
        }

    }

    @Layout(R.layout.screen_callback_card)
    public static class CallbackCardScreen extends Screen implements HasParent<CallbackListScreen> {
        String patientId;
        Map<String, String> info;

        public CallbackCardScreen() {
        }

        public CallbackCardScreen(String patientId, Map<String, String> info) {
            this.patientId = patientId;
            this.info = info;
        }

        @Override
        public void inject(Presenter presenter) {
            ((CallbackCardPresenter)presenter).init(patientId, info);
        }

        @Override
        public CallbackListScreen getParent() {
            return new CallbackListScreen();
        }
    }


    @Layout(R.layout.screen_login)
    public static class LoginScreen extends Screen implements HasParent<HomeScreen> {

        public LoginScreen() {}

        @Override
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_registration)
    public static class RegistrationScreen extends Screen implements HasParent<HomeScreen> {
        public RegistrationScreen() {}

        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_local_reporting)
    public static class LocalReportScreen extends Screen implements HasParent<HomeScreen>{
        public LocalReportScreen(){}
        public HomeScreen getParent() {return new HomeScreen();}
    }

    @Layout(R.layout.screen_summary_reporting)
    public static class SummaryReportScreen extends Screen implements HasParent<HomeScreen>{
        public SummaryReportScreen(){}
        public HomeScreen getParent() {return new HomeScreen();}
    }

    @Layout(R.layout.screen_search)
    public static class SearchScreen extends Screen implements HasParent<HomeScreen> {
        public SearchScreen() {}

        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_vaccination)
    public static class VaccinationScreen extends Screen implements HasParent<HomeScreen> {
        public VaccinationScreen() {}

        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_ocr_images)
    public static class OCRImagesScreen extends Screen implements HasParent<TabScreen> {
        public OCRImagePresenter presenter;
        public Dose dose;

        public OCRImagesScreen() {

        }

        public OCRImagesScreen(Dose dose) {
            this.dose = dose;
        }

        @Override
        public void inject(Presenter presenter) {
            this.presenter = (OCRImagePresenter) presenter;
            this.presenter.setDose(dose);
        }

        public TabScreen getParent() { return new TabScreen(); }
    }

    @Layout(R.layout.screen_ocr_detail)
    public static class OCRDetailScreen extends Screen implements HasParent<OCRImagesScreen> {
        public Vial[] vials;
        OCRDetailView view;
        OCRDetailPresenter presenter;
        public OCRDetailScreen(Vial... vials) {
            this.vials = vials;
        }

        public OCRImagesScreen getParent() {return new OCRImagesScreen(); }

        public void onViewCreated(View view) {
            this.view = (OCRDetailView) view;
        }

        @Override
        public void inject(Presenter presenter) {
            this.presenter = (OCRDetailPresenter) presenter;
            this.presenter.setVial(vials);
        }
    }

    @Layout(R.layout.screen_updatevaccinecard)
    public static class UpdateVaccinecardScreen extends Screen implements HasParent<HomeScreen> {
        public UpdateVaccinecardScreen() {}
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_confirm)
    public static class ConfirmScreen extends Screen implements HasParent<HomeScreen> {
        public ConfirmScreen() {}
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_guardian_picker)
    public static class GuardianPickerScreen extends Screen implements HasParent<HomeScreen> {
        public GuardianPickerScreen() {
        }
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_edit_guardian_picker)
    public static class EditGuardianPickerScreen extends Screen implements HasParent<HomeScreen> {
        private String childId;
        public EditGuardianPickerScreen(String childId) {
            this.childId = childId;
        }

        public String getChildId() {
            return this.childId;
        }

        public void onViewCreated(View view) {
            //((EditGuardianPickerView)view).getPresenter().setChildId(this.childId);
        }
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_verify_guardian)
    public static class VerifyGuardianScreen extends Screen implements HasParent<HomeScreen> {
        public VerifyGuardianScreen() {
        }
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_vtnumber)
    public static class VTNumberScreen extends Screen implements HasParent<HomeScreen> {
        public VTNumberScreen() {}

        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_biometric_search)
    public static class BiometricSearchScreen extends Screen implements HasParent<HomeScreen> {
        public BiometricSearchScreen() {
        }
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_registration_biometrics)
    public static class RegistrationBiometricCheckView extends Screen implements HasParent<HomeScreen> {
        public RegistrationBiometricCheckView() {
        }
        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_demographic_search)
    public static class DemographicSearchScreen extends Screen implements HasParent<HomeScreen>{
        public DemographicSearchScreen(){}
        public HomeScreen getParent(){return new HomeScreen();}
    }

    @Layout(R.layout.screen_demographics)
    public static class DemographicScreen extends Screen implements HasParent<DemographicScreen>{
        public DemographicScreen () {}
        public DemographicScreen getParent(){return new DemographicScreen();}
    }

    @Layout(R.layout.screen_tabs)
    public static class TabScreen extends Screen implements HasParent<HomeScreen> {
        public int tabIndex = -1;

        public TabScreen() {
        }

        public TabScreen(int startTab) {
            this.tabIndex = startTab;
        }

        @Override
        public void onViewCreated(View view) {
            if(tabIndex != -1) {
                ((TabView) view).getPresenter().loadView(tabIndex);
            }
        }

        public HomeScreen getParent() {
            return new HomeScreen();
        }
    }

    @Layout(R.layout.screen_preferences)
    public static class PreferencesScreen extends Screen implements HasParent<HomeScreen> {
        public PreferencesScreen() {
        }
        public HomeScreen getParent() {return new HomeScreen();}
    }

    @Layout(R.layout.screen_help_videos)
    public static class HelpVideosScreen extends Screen implements HasParent<HomeScreen> {
        public HelpVideosScreen() {
        }
        public HomeScreen getParent() {return new HomeScreen();}
    }
}
