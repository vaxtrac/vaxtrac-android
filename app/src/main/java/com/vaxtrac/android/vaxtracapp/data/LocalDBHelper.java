package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;

public class LocalDBHelper extends SQLiteOpenHelper {

    public final String TAG = "LocalDBHelper";
    public static final String DATABASE_NAME = "localdb";
    public static final int DATABASE_VERSION = 7;

    //This helper is for an on disk DB
    //For inMemory you want MemoryDBHelper
    public LocalDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i(TAG, "Creating DB on device");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.i(TAG, "Upgrading LocalDB");
        switch (oldVersion) {
            case 3:
                if(tableExists(sqLiteDatabase, CommCareDataManager.DoseDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("ALTER TABLE DOSE ADD COLUMN userid text");
                }
                if(tableExists(sqLiteDatabase, CommCareDataManager.GuardianDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("ALTER TABLE GUARDIAN ADD COLUMN userid text");
                }
                if(tableExists(sqLiteDatabase, CommCareDataManager.PatientDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("ALTER TABLE PATIENT ADD COLUMN userid text");
                }
            case 4:
                if(tableExists(sqLiteDatabase, CommCareDataManager.PatientDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("create index if not exists vtnumber_index on PATIENT" +
                            "(vtnumber)");
                    sqLiteDatabase.execSQL("create index if not exists userid_index on " +
                            "PATIENT(userid)");
                    sqLiteDatabase.execSQL("create index if not exists callbackdate_index on " +
                            "PATIENT(callbackdate)");
                    sqLiteDatabase.execSQL("create index if not exists village_index on PATIENT" +
                            "(village)");
                    sqLiteDatabase.execSQL("create index if not exists name_index on PATIENT" +
                            "(name)");
                }
            case 5:
                if(tableExists(sqLiteDatabase, CommCareDataManager.PatientDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("ALTER TABLE PATIENT ADD COLUMN createdby");
                    sqLiteDatabase.execSQL("ALTER TABLE PATIENT ADD COLUMN clincvisits");
                }
            case 6:
                if(tableExists(sqLiteDatabase, CommCareDataManager.PatientDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("ALTER TABLE PATIENT ADD COLUMN dateopened text");
                    sqLiteDatabase.execSQL("ALTER TABLE PATIENT ADD COLUMN address text");
                }
                if(tableExists(sqLiteDatabase, CommCareDataManager.GuardianDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("ALTER TABLE GUARDIAN ADD COLUMN dateopened text");
                }
                if(tableExists(sqLiteDatabase, CommCareDataManager.DoseDBHandler.TABLE_NAME)) {
                    sqLiteDatabase.execSQL("ALTER TABLE DOSE ADD COLUMN sessiontype text");
                }
                break;
        }
    }

    private boolean tableExists(SQLiteDatabase database, String tableName) {
        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? " +
                "AND name = ?", new String[] {"table", tableName});
        if(cursor.moveToFirst()) {
            Log.d(TAG, "Table " + tableName + " exists");
            return true;
        } else {
            Log.d(TAG, "No table found named " + tableName);
            return false;
        }
    }
}