package com.vaxtrac.android.vaxtracapp.models;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.models.Dose.Status;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Schedule {

    private static final String TAG = "Schedule";
    public static int update_frequency_days = 7;

    private Map<Integer,String> ageSlots;

    private LinkedHashMap<String, List<Integer>> machineSchedule;
    private LinkedHashMap<String, List<String>> readableSchedule;
    private LinkedHashMap<String, Map<String,String>> boosterSchedule;
    private LinkedList<String> vaccineOrder;

    private static final int MS_IN_DAYS = 1000 * 60 * 60 * 24;

    private int version;

    private JSONObject machineJson;
    private JSONObject readableJson;
    private JSONObject boosterJson;
    private JSONArray orderJson;

    public Schedule(JSONObject machineJson, JSONObject readableJson, JSONObject boosterSchedule,
                    JSONArray orderJson, int version) {
        this.machineJson = machineJson;
        this.readableJson = readableJson;
        this.boosterJson = boosterSchedule;
        this.orderJson = orderJson;
        this.version = version;

        machineSchedule = digestJSONInt(machineJson);
        readableSchedule = digestJSON(readableJson);
        this.boosterSchedule = digestBoosterJSON(boosterSchedule);
        vaccineOrder = digestJSONArray(orderJson);

        determineAgeSlots();
    }

    public List<String> requiredVaccines(){
        LinkedHashSet<String> keys = new LinkedHashSet<>();

        LinkedHashSet<String> machineSet = new LinkedHashSet<>(machineSchedule.keySet());
        for(String key: vaccineOrder){
            keys.add(key);
        }
        LinkedHashSet<String> bKeys = new LinkedHashSet<>(boosterSchedule.keySet());
        for(String bKey: bKeys){
            keys.add(bKey);
        }
        List<String> keyList = new ArrayList<String>(keys);

        return keyList;
    }

    public List<Dose> compareDoses(VaccineHistory history, String antigen){
        int regularDoseCounter = 0;
        int boosterDoseCounter = 0;
        int boosterAvailable = 0;
        boolean hasBooster = false;

        Map<String, String> boosterInfo = boosterSchedule.get(antigen);
        if (boosterInfo != null){
            boosterAvailable = 1; // actually count the boosters when we get more than one
            hasBooster = true;
            Log.d(TAG, "booster info interval " + boosterInfo.get("mininterval"));
        }

        int requiredDoses = 0;
        List<Integer> antigenSchedule = machineSchedule.get(antigen);
        if(antigenSchedule != null ){
            requiredDoses = antigenSchedule.size();
        }

        Date dob = history.getDOB();
        List<Date> previousDoses = history.getAntigenHistory(antigen);

        //Ensure list is ordered
        Collections.sort(previousDoses);
        List<Dose> doses = new ArrayList<Dose>();
        List<Date> poppedDoses = new ArrayList<Date>();

        Log.d(TAG, "antigen " + antigen);

        while(regularDoseCounter < requiredDoses
                || previousDoses.size() > 0
                || boosterAvailable > 0) {
            Log.d(TAG, "Reg count " + regularDoseCounter + " prev dose size " + previousDoses
                    .size() + " booster available " + boosterAvailable);
            if(doses.size() > 0) {
                //Log.d(TAG, antigen + " | last dose status: " + doses.get(doses.size()-1).status.toString());
            }
            /* Has History */
            if(previousDoses.size() > 0) {
                Date current = previousDoses.get(0);
                Log.d(TAG, "Previous Dose Date " + current.toString());
                if (regularDoseCounter <= requiredDoses){
                    int interval = 0;
                    if (poppedDoses.size() > 0){
                        interval = daysBetween(lastFromList(poppedDoses), current);
                    }else{
                        interval = daysBetween(dob, current);
                    }

                    int minInterval = 0;
                    if (regularDoseCounter > 0) {
                        //interval between current dose and previous on schedule
                        try {
                            minInterval = (antigenSchedule.get(regularDoseCounter) - antigenSchedule.get(regularDoseCounter-1));
                        } catch(Exception e){
                            previousDoses.remove(0);
                            doses.add(new Dose(antigen, regularDoseCounter+boosterDoseCounter, current, Status.received_not_valid));
                            continue;
                        }

                    } else {
                        //minimum age
                        if (hasBooster) {
                            if(boosterAvailable > 0) {
                                    int start = Integer.parseInt(boosterInfo.get("start"));
                                    int end = Integer.parseInt(boosterInfo.get("end"));
                                    if (interval >= start && interval <= end) {
                                        Log.i(TAG, "Booster Valid");
                                        Date outDose = previousDoses.remove(0);
                                        poppedDoses.add(outDose);
                                        //adding a valid booster to the schedule
                                        doses.add(new Dose(antigen, boosterDoseCounter, current, Status.received_valid, Dose.Type.birth_booster));
                                        boosterDoseCounter++;
                                        boosterAvailable--;
                                        continue;
                                    } else {
                                        doses.add(new Dose(antigen, boosterDoseCounter, dob, Status
                                                .missed_booster, Dose.Type.birth_booster));
                                        boosterAvailable--;
                                        boosterDoseCounter++;
                                        continue;
                                    }
                            } else {
                                if(boosterDoseCounter > 0) {
                                    if(doses.get(0).getStatus() == Status.received_valid) {
                                        int boosterInterval;
                                        if (boosterInfo.containsKey("mininterval")) {
                                            boosterInterval = Integer.parseInt(boosterInfo.get
                                                    ("mininterval"));
                                            Log.d(TAG, "booster interval " + boosterInterval);
                                            minInterval = boosterInterval;
                                        }
                                    }
                                }
                            }
                        }
                        try {
                            minInterval = antigenSchedule.get(regularDoseCounter);
                            if(regularDoseCounter == 0){
                                interval = daysBetween(dob, current);
                            }
                        } catch (NullPointerException e) {
                            previousDoses.remove(0);
                            doses.add(new Dose(antigen, regularDoseCounter + boosterDoseCounter, current, Status.received_not_valid));
                            continue;
                        }
                    }
                    Dose d = new Dose(antigen, regularDoseCounter + boosterDoseCounter);
                    Date outDose = previousDoses.remove(0);
                    if (interval >= minInterval){
                        //valid dose
                        d.setDate(current);
                        d.setStatus(Status.received_valid);
                        poppedDoses.add(outDose);
                        //remove requirement
                        regularDoseCounter++;
                    }else{
                        //invalid dose
                        d.setDate(current);
                        d.setStatus(Status.received_not_valid);
                    }
                    doses.add(d);
                    continue;
                }else{
                    //already had enough doses.. this was extra
                    previousDoses.remove(0);
                    doses.add(new Dose(antigen, regularDoseCounter + boosterDoseCounter, current, Status.received_not_valid));
                    continue;
                    //no more scheduled doses...
                }
            }
            else {
                //out of previous doses, not out of schedule
                //today
                Log.d(TAG, "non historic regular count " + regularDoseCounter);
                Date today = new Date();
                int minInterval;
                if (boosterDoseCounter == 0) {
                    if (hasBooster) {

                            Log.i(TAG, "Booster Exists");
                            int start = Integer.parseInt(boosterInfo.get("start"));
                            int end = Integer.parseInt(boosterInfo.get("end"));
                            int age = daysBetween(dob, today);
                            if ((age >= start) && (age <= end)) {
                                doses.add(new Dose(antigen, boosterDoseCounter, today, Status.eligible, Dose.Type.birth_booster));
                                boosterDoseCounter++;
                                boosterAvailable--;
                                continue;
                            }else{
                                doses.add(new Dose(antigen, boosterDoseCounter, dob, Status.missed_booster, Dose.Type.birth_booster));
                                boosterDoseCounter++;
                                boosterAvailable--;
                                continue;
                            }

                    }
                }
                if(regularDoseCounter == requiredDoses){
                    break;
                }

                if (regularDoseCounter > 0) {
                    //interval between current dose and previous on schedule
                    minInterval = (antigenSchedule.get(regularDoseCounter) - antigenSchedule.get
                            (regularDoseCounter - 1));
                } else {
                    Log.d(TAG, "regular dose counter is zero");
                    minInterval = antigenSchedule.get(regularDoseCounter);
                    if(hasBooster) {
                        if(!doses.isEmpty()) {
                            Dose boosterDose = doses.get(0);
                            if (boosterDose.getStatus() == Status.received_valid) {
                                int boosterInterval;
                                if(boosterInfo.containsKey("mininterval")) {
                                    boosterInterval = Integer.parseInt(boosterInfo.get
                                            ("mininterval"));
                                    Log.d(TAG, "regular dose 0 booster min interval " +
                                            minInterval);
                                    int age = daysBetween(dob, today);
                                    if((age + boosterInterval) > minInterval) {
                                        minInterval = boosterInterval;
                                    } else {
                                        minInterval = minInterval - age;
                                    }
                                }
                                else {
                                    //added booster Type enum to Dose, so we can accurately
                                    // disregard the 'sinceLastGood field later'
                                    Log.d(TAG, String.format("booster has no overriding mininterval so " +
                                                    "we're going to treat the next dose normally"));
                                }
                            }
                        }
                    }
                }
                //find last good dose;
                Dose lastGoodDose = null;
                Date lastGoodDate = null;
                for(Dose dose : doses){
                    if (dose.getStatus() == Status.received_valid){
                        lastGoodDate = dose.getDate();
                        lastGoodDose = dose;
                    }
                }
                int sinceLastGood = 0;
                if (lastGoodDate != null && lastGoodDose.type != Dose.Type.birth_booster) {
                    //we have the last good dose date
                    sinceLastGood = daysBetween(lastGoodDate, today);
                }else{
                    //no good doses, we'll use their dob
                    sinceLastGood = daysBetween(dob, today);
                }
                Log.d(TAG, "since last good " + sinceLastGood);
                //see if we've already made a recommendation (Status.next)
                Status lastStatus = Status.received_valid;
                if (!doses.isEmpty()){
                    lastStatus = doses.get(doses.size()-1).getStatus();
                }
                if(sinceLastGood >= minInterval && lastStatus != Status.next_valid && lastStatus != Status.future && lastStatus != Status.eligible){
                    //elegible!
                    doses.add(new Dose(antigen, regularDoseCounter + boosterDoseCounter, today, Status.eligible));
                    regularDoseCounter++;
                    continue;
                }

                /*
                TODO This never happens...?
                 */
                else if(lastStatus != Status.next_valid && lastStatus != Status.future && lastStatus != Status.eligible){
                    //if not, we need to calculate the callback
                    int daysUntil = minInterval - sinceLastGood;
                    today.setDate(today.getDate() + daysUntil);
                    doses.add(new Dose(antigen, regularDoseCounter + boosterDoseCounter, today, Status.next_valid));
                    regularDoseCounter++;
                } else {
                    //future doses
                    //if we have, we can stick a label on it
                    Dose d = new Dose(antigen, regularDoseCounter + boosterDoseCounter);
                    try {
                        Calendar c = Calendar.getInstance();
                        if (regularDoseCounter > 0) {
                            Dose lastDose = doses.get(doses.size()-1);
                            Date lastDoseDate = lastDose.getDate();
                            c.setTime(lastDoseDate);
                            c.add(Calendar.DATE, intervalBetweenDoses(antigen, regularDoseCounter - 1, regularDoseCounter));
                        }
                        else {
                            c.setTime(dob);
                            c.add(Calendar.DATE, antigenSchedule.get(0));
                        }
                        d.setDate(c.getTime());
                    }catch (Exception e){
                        Log.e(TAG, "Future Last Dose Exception?", e);
                    }
                    d.setStatus(Status.future);
                    //TODO stick a label from Human Readable on this.
                    doses.add(d);
                    regularDoseCounter++;
                }

            }
        }
        return doses;

    }

    public Map<Integer,String> getAgeSlots(){
        return ageSlots;
    }

    public String nameOfDoseNumber(String antigen, int doseNumber){
        try{
            List<String> antigenSchedule = readableSchedule.get(antigen);
            if(hasBooster(antigen)){
                return antigenSchedule.get(doseNumber-1);
            }
            return antigenSchedule.get(doseNumber);
        }catch (Exception e){
            return getBoosterName(antigen);
        }
    }

    public List<String> requiredVaccinationsForAntigen(String antigen){
        List<String> names = new ArrayList<>();
        List<String> fromReadable = readableSchedule.get(antigen);
        if(fromReadable != null){
            names.addAll(fromReadable);
        }
        Map<String, String> antigenBooster = boosterSchedule.get(antigen);
        if(antigenBooster != null){
                names.add(0, antigenBooster.get("slot"));

        }
        return names;
    }

    public int intervalBetweenDoses(String antigen, int doseOne, int doseTwo){
        List<Integer> antigenSchedule = machineSchedule.get(antigen);
        int firstDay = antigenSchedule.get(doseOne);
        int secondDay = antigenSchedule.get(doseTwo);
        return secondDay - firstDay;
    }

    private void determineAgeSlots(){
        ageSlots = new LinkedHashMap<>();
        Map<Integer, String> sorter = new HashMap<Integer,String>();
        List<String> readables;
        List<Integer> ages;

        for(String antigen: machineSchedule.keySet()){
            readables = readableSchedule.get(antigen);
            ages = machineSchedule.get(antigen);
            for (int i = 0, len = readables.size(); i<len; i++){
                sorter.put(ages.get(i), readables.get(i));
            }
        }

        for(String antigen: boosterSchedule.keySet()){
            Map<String,String> fields = boosterSchedule.get(antigen);
            int age = Integer.parseInt(fields.get("start"));
            String name = fields.get("slot");
            sorter.put(age, name);
        }
        List<Integer> orderedAges = new ArrayList<Integer>(sorter.keySet());
        java.util.Collections.sort(orderedAges);
        for (int age: orderedAges){
            ageSlots.put(age, sorter.get(age));
        }
    }

    //for building a standardized vaccine card
    public Integer cardColumnForDoseNumber(String antigen, int doseNumber){
        List<String> readableAntigenSchedule = readableSchedule.get(antigen);
        String currentDoseName = "";
        try{
            if(hasBooster(antigen)){
                currentDoseName = readableAntigenSchedule.get(doseNumber-1);
            }else{
                currentDoseName = readableAntigenSchedule.get(doseNumber);
            }
        }catch(Exception e){
            Map<String, String> boosterInfo = boosterSchedule.get(antigen);
            if (boosterInfo != null){
                currentDoseName = boosterInfo.get("slot");
            }
        }

        int c = 0;
        /* Do we need the counter or do we just want the Index ?*/
        for(Map.Entry<Integer, String> entry: ageSlots.entrySet()){
            if (currentDoseName.equals(entry.getValue())){
                return c;
            }
            c+=1;
        }
        return null;

    }

    private Date lastFromList(List<Date> list){
        return list.get(list.size()-1);
    }

    public static int daysBetween(Date early, Date late){
        int days = (int)( (late.getTime() - early.getTime()) / (MS_IN_DAYS));
        return days;
    }

    private LinkedList<String> digestJSONArray(JSONArray order) {
        LinkedList<String> orderedVaccines = new LinkedList<>();
        for(int i = 0, length = order.length(); i < length; i++) {
            try {
                orderedVaccines.add((String) order.get(i));
            } catch (JSONException exception) {
                //Log.e(TAG, "Error while getting vaccine order json " + exception);
            }
        }
        return orderedVaccines;
    }

    private LinkedHashMap<String,List<Integer>> digestJSONInt(JSONObject in){
        LinkedHashMap<String,List<Integer>> map = new LinkedHashMap<>();
        Iterator<?> keys = in.keys();
        while(keys.hasNext()){
            String k = (String) keys.next();
            try {
                JSONArray a = in.getJSONArray(k);
                List<Integer> items = new ArrayList<Integer>();
                if (a.length() > 0){
                    for (int i=0; i<a.length();i++){
                        items.add(a.getInt(i));
                    }
                }
                map.put(k, items);
            } catch (JSONException e) {
                //Log.i(TAG, "Couldn't get JSONArray for Antigen with key: " + k);
            }
        }
        return map;
    }

    private LinkedHashMap<String, Map<String,String>> digestBoosterJSON(JSONObject in){
        LinkedHashMap<String, Map<String, String>> booster = new LinkedHashMap<String, Map<String, String>>();
        if(in != null) {
            Iterator<?> keys = in.keys();
            if(keys != null) {
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    try {
                        JSONObject dict = in.getJSONObject(key);
                        Map<String, String> map = new HashMap<>();
                        Iterator<?> localKeys = dict.keys();
                        while (localKeys.hasNext()) {
                            try {
                                String localKey = (String) localKeys.next();
                                map.put(localKey, dict.getString(localKey));
                            } catch (JSONException e2) {
                                //Log.d(TAG, "Couldn't get JSON attributes");
                            }
                        }
                        booster.put(key, map);
                    } catch (JSONException e) {
                        //Log.i(TAG, "Couldn't get JSONArray for Antigen with key: " + key);
                    }
                }
            }
            return booster;
        }
        return booster;
    }

    private LinkedHashMap<String,List<String>> digestJSON(JSONObject in){
        LinkedHashMap<String,List<String>> map = new LinkedHashMap<>();
        Iterator<?> keys = in.keys();
        while(keys.hasNext()){
            String k = (String) keys.next();
            try {
                JSONArray a = in.getJSONArray(k);
                List<String> items = new ArrayList<String>();
                if (a.length() > 0){
                    for (int i=0; i<a.length();i++){
                        items.add(a.getString(i));
                    }
                }
                map.put(k, items);
            } catch (JSONException e) {
                //Log.i(TAG, "Couldn't get JSONArray for Antigen with key: " + k);
            }
        }
        return map;
    }


    public String getBoosterName(String antigen) {
        Map<String, String> info = boosterSchedule.get(antigen);
        return info != null ? info.get("slot") : null;
    }

    public boolean hasBirthBooster(String antigen){
        Map<String,String> info = boosterSchedule.get(antigen);
        return info != null && info.get("type").equals("birth");
    }

     public boolean hasBooster(String antigen){
        return boosterSchedule.get(antigen) != null;
    }

    public int getVersion() {
        return version;
    }

    public void persist() {
        if(version > Schedule.savedScheduleVersion()) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences
                    (AppController
                            .getAppContext()).edit();
            editor.putInt(Preferences.PREF_SCHEDULE_VERSION, version);
            editor.putString(Preferences.PREF_VACCINE_SCHEDULE,
                    serializeSchedules(machineJson, readableJson, boosterJson, orderJson));
            editor.apply();
        }
    }

    private int getCachedVersion() {
        return PreferenceManager.getDefaultSharedPreferences(AppController
                .getAppContext()).getInt(Preferences.PREF_SCHEDULE_VERSION, -1);
    }

    private String serializeSchedules(JSONObject machine, JSONObject readable, JSONObject booster,
                                      JSONArray order) {
        JSONObject combinedSchedules = new JSONObject();

        try {
            combinedSchedules.put("machine_schedule", machine);
            combinedSchedules.put("readable_schedule", readable);
            combinedSchedules.put("booster_schedule", booster);
            combinedSchedules.put("vaccine_order", order);
            combinedSchedules.put("version", version);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return combinedSchedules.toString();
    }

    public static int savedScheduleVersion() {
        return Preferences.getScheduleVersion();
    }

    public static Schedule deserializeSchedules() {
        String schedules = PreferenceManager.getDefaultSharedPreferences(AppController
                .getAppContext()).getString(Preferences.PREF_VACCINE_SCHEDULE, null);
        if(schedules != null) {
            try {
                JSONObject jsonSchedules = new JSONObject(schedules);
                return new Schedule(
                        (JSONObject) jsonSchedules.get("machine_schedule"),
                        (JSONObject) jsonSchedules.get("readable_schedule"),
                        (JSONObject) jsonSchedules.get("booster_schedule"),
                        (JSONArray) jsonSchedules.get("vaccine_order"),
                        jsonSchedules.getInt("version"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //Log.d(TAG, "Failed to deserialize Schedule JSON from Shared Preferences");
        }
        return null;
    }

    public static Schedule loadSchedule(String mac_str, String read_str, String booster_str,
                                        String order_str, int version) {
        JSONObject machine_schedule= null;
        JSONObject readable_schedule = null;
        JSONObject booster_schedule = null;
        JSONArray vaccine_order = null;

        if(mac_str != null){
            try {
                machine_schedule = new JSONObject(mac_str);
            } catch (JSONException e) {
                //Log.i(TAG,"Failed to parse machine schedule");
            }
        }else{
            machine_schedule = new JSONObject();
        }

        if(read_str != null){
            try {
                readable_schedule = new JSONObject(read_str);
            } catch (JSONException e) {
                //Log.i(TAG,"Failed to parse readable schedule");
            }
        }else{
            readable_schedule= new JSONObject();
        }

        if(booster_str != null){
            try {
                booster_schedule = new JSONObject(booster_str);
            } catch (JSONException e) {
                //Log.i(TAG, "Failed to parse booster schedule");
            }
        }else{
            booster_schedule = new JSONObject();
        }

        if(order_str != null) {
            try {
                vaccine_order = new JSONArray(order_str);
            } catch (JSONException e) {
                //Log.i(TAG,"Failed to parse order schedule");
            }
        }

        return new Schedule(machine_schedule, readable_schedule, booster_schedule, vaccine_order,
                version);
    }
}