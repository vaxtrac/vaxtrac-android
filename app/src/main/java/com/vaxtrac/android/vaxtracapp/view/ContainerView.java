package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.vaxtrac.android.vaxtracapp.Screen;

import flow.Layout;

public class ContainerView extends FrameLayout {

    public ContainerView(Context context, AttributeSet attrs) {
      super(context, attrs);
    }

    public void showScreen(Screen screen) {
        Layout layout = screen.getClass().getAnnotation(Layout.class);
        View view = LayoutInflater.from(getContext()).inflate(layout.value(), this, false);

        screen.onViewCreated(view);

        removeAllViews();
        addView(view);
    }

}
