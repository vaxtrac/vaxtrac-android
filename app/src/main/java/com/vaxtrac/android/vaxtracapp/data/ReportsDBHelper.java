package com.vaxtrac.android.vaxtracapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.AsyncTask;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.models.CommCareCase;
import com.vaxtrac.android.vaxtracapp.models.Dose;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ReportsDBHelper extends SQLiteOpenHelper {

    private static final String TAG = ReportsDBHelper.class.toString();

    private SQLiteDatabase database;

    public static final String DATABASE_NAME = "reports.db";
    public static final int DATABASE_VERSION = 4;
    public static final String TABLE_NAME = "reports";

    /* CommCare Standard Case Fields */
    public static final String CASE_ID_COLUMN = "case_id"; // YYYY-mm-userid
    public static final String USER_ID_COLUMN = "userid";
    public static final String LAST_MODIFIED_COULMN = "last_modified";
    public static final String DATE_OPENED_COLUMN = "date_opened";
    public static final String CASE_TYPE_COLUMN =  "case_type";
    public static final String CASE_NAME_COLUMN = "case_name";
    public static final String OWNER_ID_COLUMN = "owner_id"; // userid not shared
    public static final String STATUS_COLUMN = "status";

    /* Report Case Specific */
    public static final String DATE_KEY_COLUMN = "date_key"; // YYYY-mm
    public static final String YEAR_COLUMN = "year";
    public static final String MONTH_COLUMN = "month";
    public static final String MONTH_START_COLUMN = "start_date"; // YYYY-mm-dd
    public static final String MONTH_END_COLUMN = "end_date"; // YYYY-mm-dd
    public static final String CATEGORIES_COLUMN = "categories";
    public static final String REPORT_STATUS_COLUMN = "report_status";
    public static final String LAST_SENT_COLUMN = "last_sent";
    public static final String LAST_DOSE_TIMESTAMP_COLUMN = "last_dose_timestamp";
    public static final String PERIOD_COLUMN = "period";
    public static final String ARCHIVED_COLUMN = "archived";
    public static final String REVISIONS_COLUMN = "revisions";
    public static final String REPORT_DATA_COLUMN = "report_data";

    public static final String TEXT_TYPE = "TEXT";
    public static final String INTEGER_TYPE = "INTEGER";

    String[] columns = {
        CASE_ID_COLUMN,
        USER_ID_COLUMN,
        LAST_MODIFIED_COULMN,
        DATE_OPENED_COLUMN,
        CASE_TYPE_COLUMN,
        CASE_NAME_COLUMN,
        OWNER_ID_COLUMN,
        STATUS_COLUMN,
        DATE_KEY_COLUMN,
        YEAR_COLUMN,
        MONTH_COLUMN,
        MONTH_START_COLUMN,
        MONTH_END_COLUMN,
        CATEGORIES_COLUMN,
        REPORT_STATUS_COLUMN,
        LAST_SENT_COLUMN,
        LAST_DOSE_TIMESTAMP_COLUMN,
        PERIOD_COLUMN,
        ARCHIVED_COLUMN,
        REVISIONS_COLUMN,
        REPORT_DATA_COLUMN
    };

    String[] columnTypes = {
        TEXT_TYPE,  // case id
        TEXT_TYPE,  // user id
        TEXT_TYPE, // last modified
        TEXT_TYPE, // date opened
        TEXT_TYPE, // case type
        TEXT_TYPE, // case name
        TEXT_TYPE, // owner id
        TEXT_TYPE, // status

        TEXT_TYPE, // date key
        INTEGER_TYPE, // year
        INTEGER_TYPE, // month
        TEXT_TYPE, // start date
        TEXT_TYPE, // end date
        TEXT_TYPE, // categories
        INTEGER_TYPE, // report status
        TEXT_TYPE, // last sent
        TEXT_TYPE, // last dose timestamp
        TEXT_TYPE, // period
        INTEGER_TYPE, // archived
        INTEGER_TYPE, // revisions
        TEXT_TYPE, // report data
    };

    String[] indexColumns = {
        DATE_KEY_COLUMN,
        CASE_ID_COLUMN,
        CASE_TYPE_COLUMN,
        YEAR_COLUMN,
        MONTH_COLUMN,
        PERIOD_COLUMN
    };

    public ReportsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "ReportsDBHelper constructor");
        /* new OpenDatabaseTask().execute(); */
        database = getWritableDatabase();
    }

    @Override
    public String getDatabaseName() {
        return super.getDatabaseName();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate()");
        db.execSQL(buildCreateStatement());
        createIndexes(db);
        // insertTestData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    private void insertTestData(SQLiteDatabase db) {

        String testData = "{\"bcg\":{\"0\": [4,4,4]}," +
                "           \"opv\":{\"0\": [10,30,6]," +
                "                    \"1\": [8,9,16]}}";

        ContentValues contentValues = new ContentValues();
        contentValues.put(DATE_KEY_COLUMN, "2015-10");
        contentValues.put(CASE_ID_COLUMN, "");
        contentValues.put(REPORT_STATUS_COLUMN, 0);
        contentValues.put(PERIOD_COLUMN, "monthly");
        contentValues.put(ARCHIVED_COLUMN, 1);
        contentValues.put(REVISIONS_COLUMN, 0);
        contentValues.put(REPORT_DATA_COLUMN, testData);

        db.insert(TABLE_NAME, null, contentValues);
    }

    public void closeDatabase() {
        if(database != null && database.isOpen()) {
            database.close();
        }
    }

    public void allReports(QueryTaskListener listener) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_NAME);
        new QueryTask(listener).execute(queryBuilder.buildQuery(null, null, null, null, null,
                null));
    }

    public boolean hasReport(int month, int year) {
        StringBuilder selectClause = new StringBuilder("WHERE ")
                .append(DATE_KEY_COLUMN)
                .append("= ?");

        return database.query(TABLE_NAME, new String[]{DATE_KEY_COLUMN}, selectClause.toString
                        (), new String[]{month + "-" + year}, null, null, null, "LIMIT 1")
                .getCount() > 0;
    }

    private String buildCreateStatement() {
        StringBuilder createStatement = new StringBuilder("CREATE TABLE ");
        createStatement.append(TABLE_NAME)
                .append(" (");
        for(int i = 0, columnCount = columns.length; i < columnCount; i++) {
            createStatement.append(columns[i])
                    .append(" ")
                    .append(columnTypes[i]);
            if(i != (columnCount - 1)) {
                createStatement.append(", ");
            }
        }
        return createStatement.append(")").toString();
    }

    private void createIndexes(SQLiteDatabase db) {
        StringBuilder indexStatement = new StringBuilder("CREATE INDEX IF NOT EXISTS ");
        int startLength = indexStatement.length();
        for(String column : indexColumns) {
             indexStatement.setLength(startLength);
             indexStatement.append(column)
                     .append("_index on ")
                     .append(TABLE_NAME)
                     .append("(")
                     .append(column)
                     .append(")");
             db.execSQL(indexStatement.toString());
        }
    }

    private void incrementReport(int month, int year, List<Dose> doses) {

    }

    public void saveReport(CommCareCase reportCase) {
        try {
            database.insert(TABLE_NAME, null, reportCase.getContents());
        } catch (Exception exception) {
            Log.d(TAG, "Exception Saving Report Case to database", exception);
        }
    }

    public Cursor allItems() {
        try {
            return database.query(TABLE_NAME, null, null, null, null, null, null);
        } catch (Exception exception) {
            Log.d(TAG, "Exceptiong fetching all rows from database", exception);
            return null;
        }
    }

    private class OpenDatabaseTask extends AsyncTask<Void, Void, SQLiteDatabase> {

        @Override
        protected SQLiteDatabase doInBackground(Void... params) {
            return getWritableDatabase();
        }

        @Override
        protected void onPostExecute(SQLiteDatabase db) {
            super.onPostExecute(db);
            ReportsDBHelper.this.database = db;
            Log.d(TAG, "OpenDatabaseTask onPostExecute");
        }
    }

    public interface QueryTaskListener {
        void onTaskCompleted(Cursor cursor);
    }

    private class QueryTask extends AsyncTask<String, Void, Cursor> {

        private final QueryTaskListener listener;

        public QueryTask(QueryTaskListener listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(database == null || !database.isOpen()) {
                Log.d(TAG, "database is not open");
                try {
                    new OpenDatabaseTask().execute().get();
                } catch (InterruptedException e) {
                    Log.e(TAG, e.getMessage());
                } catch (ExecutionException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }


        @Override
        protected Cursor doInBackground(String... params) {
            Log.d(TAG, "raw query string " + params[0]);
            return database.rawQuery(params[0], null);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            listener.onTaskCompleted(cursor);
        }
    }
}
