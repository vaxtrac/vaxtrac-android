package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.AppVersion;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.Screens;
import com.vaxtrac.android.vaxtracapp.preferences.Preferences;
import com.vaxtrac.android.vaxtracapp.presenter.SearchPresenter;

public class SearchView extends RelativeLayout {

    Context context;
    SearchPresenter presenter;

    ImageView backButton;
    LinearLayout qrSearchButton;
    LinearLayout fingerprintSearchButton;
    LinearLayout phoneSearchButton;
    LinearLayout demographicSearchButton;
    LinearLayout cardNumberSearchButton;
    LinearLayout nameSearchButton;

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        presenter = new SearchPresenter(context, this);
    }

    @Override
    protected void onFinishInflate() {

        backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goUp();
            }
        });

        qrSearchButton = (LinearLayout) findViewById(R.id.qr_search);
        qrSearchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.launchQRSearchForm();
            }
        });


        fingerprintSearchButton = (LinearLayout) findViewById(R.id.fingerprint_search);
        fingerprintSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.launchFingerprintSearchForm();
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.context);

        if (!Preferences.isFingerprintingEnabled()){
            fingerprintSearchButton.setVisibility(View.GONE);
        }

        phoneSearchButton = (LinearLayout) findViewById(R.id.phone_search);
        if(Preferences.isPhoneSearch()) {
            phoneSearchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.launchPhoneNumberSearchForm();
                }
            });
        } else {
            phoneSearchButton.setVisibility(View.GONE);
        }

        demographicSearchButton = (LinearLayout) findViewById(R.id.demographic_search);
        demographicSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    presenter.launchDemographicSearchForm();
            }
        });

        cardNumberSearchButton = (LinearLayout) findViewById(R.id.card_number_search);
        if(AppVersion.isEnabled("card-number-search")) {
            cardNumberSearchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.launchVaccineCardSearchForm();
                }
            });
        }else{
            cardNumberSearchButton.setVisibility(View.GONE);
        }

        nameSearchButton = (LinearLayout) findViewById(R.id.name_search);
        if(Preferences.isNameSearch()) {
            nameSearchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.launchNameSearchForm();
                }
            });
        } else {
            nameSearchButton.setVisibility(View.GONE);
        }

        super.onFinishInflate();
    }
}
