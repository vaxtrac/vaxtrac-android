package com.vaxtrac.android.vaxtracapp.ocr;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class OCRDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "ocr.db";
    public static final int DATABASE_VERSION = 15;
    public static final String TABLE_NAME = "ocr";
    public static final String PRIMARY_KEY_COLUMN = "id";
    public static final String COMMCARE_UUID_COLUMN = "commcare_uuid";
    public static final String CASE_EXISTS_COLUMN = "case_exists";
    public static final String IMAGE_DATA_COLUMN = "image_data";
    public static final String VACCINE_TYPE_COLUMN = "vaccine_type";
    public static final String DATE_CREATED = "creation_date";
    public static final String RAW_OCR_TEXT = "raw_ocr";
    public static final String MANUFACTURE_DATE = "manufacture_date";
    public static final String EXPIRATION_DATE = "expiration_date";
    public static final String LOT_BATCH_NUMBER = "lot_batch_number";
    public static final String LOT_NUMBER = "lot_number";
    public static final String BATCH_NUMBER = "batch_number";
    public static final String MAXIMUM_DOSES = "max_doses";
    public static final String USED_DOSES = "used_doses";
    public static final String MULTIUSE = "multiuse";
    public static final String MULTIUSE_DATE = "multiuse_date";
    public static final String HIDDEN = "hidden";
    public static final String DISABLED = "disabled";

    String[] columns = {PRIMARY_KEY_COLUMN,
            COMMCARE_UUID_COLUMN,
            CASE_EXISTS_COLUMN,
            IMAGE_DATA_COLUMN,
            VACCINE_TYPE_COLUMN,
            DATE_CREATED,
            RAW_OCR_TEXT,
            MANUFACTURE_DATE,
            EXPIRATION_DATE,
            LOT_BATCH_NUMBER,
            LOT_NUMBER,
            BATCH_NUMBER,
            MAXIMUM_DOSES,
            USED_DOSES,
            MULTIUSE_DATE,
            MULTIUSE,
            HIDDEN,
            DISABLED};

    public static Set<String> columnSet;

    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
            PRIMARY_KEY_COLUMN + " INTEGER primary key autoincrement, " +
            COMMCARE_UUID_COLUMN + " TEXT, " + CASE_EXISTS_COLUMN + " INTEGER, " +
            IMAGE_DATA_COLUMN + " BLOB, " + VACCINE_TYPE_COLUMN + " TEXT, " +
            DATE_CREATED + " TEXT, " + RAW_OCR_TEXT + " TEXT, " +
            MANUFACTURE_DATE + " TEXT, " + EXPIRATION_DATE + " TEXT, "
            + LOT_BATCH_NUMBER + " TEXT, " + LOT_NUMBER + " TEXT, " +
            BATCH_NUMBER + " TEXT, " +
            MAXIMUM_DOSES + " INTEGER, " + MULTIUSE_DATE + " TEXT, "+  MULTIUSE + " INTEGER, " +
    USED_DOSES + " INTEGER, " + HIDDEN + " INTEGER, " + DISABLED + " INTEGER)";

    public OCRDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        columnSet = new LinkedHashSet<>(Arrays.asList(columns));
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(OCRDBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion);
        switch(oldVersion) {
            case 13:
                database.execSQL("ALTER TABLE ocr ADD COLUMN multiuse integer");
            case 14:
                database.execSQL("ALTER TABLE ocr ADD COLUMN multiuse_date text");
                break;
            default:
                database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                onCreate(database);
        }
    }

    public String getAllColumns() {
        return Arrays.toString(columns);
    }
}
