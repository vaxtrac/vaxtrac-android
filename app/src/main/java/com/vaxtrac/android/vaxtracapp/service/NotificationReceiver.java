package com.vaxtrac.android.vaxtracapp.service;

/**
 * Created by sarwar on 12/22/14.
 */

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;


public class NotificationReceiver extends Service {

    public static final String TAG = "NotificationReceiver";

    public NotificationReceiver() {}

    @Override
    public void onCreate() {
        kill_all();
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {return null;}

    private void kill_all(){
        Log.i(TAG, "Got Kill Switch");
        AppController.killAll();
        stopSelf();
        return;
    }
}
