package com.vaxtrac.android.vaxtracapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareConsistencyManager;

public class CommCareSyncService extends Service {

    private static boolean syncInProgress = false;

    private static final String TAG = "CommCareSyncService";
    private static CommCareConsistencyManager mCommCareConsistencyManager = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mCommCareConsistencyManager = new CommCareConsistencyManager(getApplicationContext(), AppController.mCommCareDataManager);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //true if sync starts || false if it fails to start/ already running
    public static boolean sync(){
        if (syncInProgress()){
            Log.i(TAG, "Sync already running...");
            return false;
        }
        Thread thread = new Thread(syncThread());
        thread.start();
        return true;

    }

    public static boolean syncInProgress(){
        return syncInProgress;
    }

    private static Runnable syncThread(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    syncInProgress = true;
                    mCommCareConsistencyManager.update();
                    syncInProgress = false;
                }catch (Exception e){
                    e.printStackTrace();
                    Log.i(TAG, "FatalError in CCCManager!");
                    syncInProgress = false;
                }
            }
        };
       return runnable;

    }
}
