package com.vaxtrac.android.vaxtracapp.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.MainActivity;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.presenter.CallbackCardPresenter;

public class CallbackCardView extends LinearLayout {

    TableLayout table;
    LayoutInflater inflater;
    ImageView back;
    Context context;
    TextView name;
    TextView nextVisit;
    TextView village;
    TextView mobile;
    CallbackCardPresenter presenter;
    MainActivity mainActivity;

    public CallbackCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        mainActivity = (MainActivity) getContext();
        presenter = new CallbackCardPresenter(context, this);
        AppController.mScreen.inject(presenter);
        presenter.setPatient();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        back = (ImageView) findViewById(R.id.back_button);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goBack();
            }
        });
        name = (TextView) findViewById(R.id.name);
        name.setText(presenter.getField("name"));
        nextVisit = (TextView) findViewById(R.id.next_visit);
        nextVisit.setText(presenter.getField("nextVisit"));
        village = (TextView) findViewById(R.id.village);
        village.setText(presenter.getField("village"));
        mobile = (TextView) findViewById(R.id.mobile);
        mobile.setText(presenter.getField("mobile"));

        inflater = LayoutInflater.from(getContext());
        table  = (TableLayout) findViewById(R.id.vaccination_table);
        reDrawTable();
    }

    public void reDrawTable(){
        table.removeAllViews();
        if (presenter.mPatient!= null) {
            presenter.fill_table(table, inflater);
            table.invalidate();
        }
        else{
            Toast.makeText(context, "No patient Loaded!", Toast.LENGTH_SHORT).show();
        }
    }
}
