package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareDataManager;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.data.TranslationCursor;
import com.vaxtrac.android.vaxtracapp.data.UnionCursor;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;
import com.vaxtrac.android.vaxtracapp.view.DemographicSearchView;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class DemographicSearchPresenter extends Presenter{
    DemographicSearchView view;
    Context context;
    Cursor sexCursor;
    Cursor dateCursor;
    public Cursor baseCursor = null;
    public Cursor parseCursor = null;
    CommCareDataManager ccDM;
    public ColoredCursorAdapter adapter;
    private static Bundle searchTerms = null;

    String sexString;
    String dobString;

    public static final String TAG = "DemographicSearchPresen";

    public DemographicSearchPresenter(Context context){
        this.context = context;
        ccDM = AppController.mCommCareDataManager;
    }

    public void setView(View v){
        this.view = (DemographicSearchView) v;
    }

    public static void setSearchTerms(Bundle bundle){
        searchTerms = bundle;
    }

    public void readBundle(){
        if(searchTerms!= null){
            dobString = searchTerms.getString("child_dob");
            view.dobView.setText(GlobalTranslations.translate(dobString, context));
            sexString = searchTerms.getString("child_sex");
            view.sexView.setText(GlobalTranslations.translate(sexString));
        }else{
            Log.d(TAG, "Null searchterms received.");
        }

    }

    public void readyList(){

        DateFormat df = new UnlocalizedDateFormat("yyyy-MM-dd");
        DateFormat endFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
        Date dob = null;
        try {
            dob = df.parse(dobString);
        }catch(ParseException e){}
        Calendar c = Calendar.getInstance();
        c.setTime(dob);
        c.add(Calendar.DATE, -30);
        Date startDate = c.getTime();
        c.add(Calendar.DATE, 60);
        Date endDate = c.getTime();

        String start = df.format(startDate);
        String finish = endFormat.format(endDate);

        sexCursor = new TranslationCursor(ccDM.guardianDB.allRowsColumnEquals("childsex",
                sexString), new String[]{"relation"}, context);
        dateCursor = new TranslationCursor(ccDM.guardianDB.allRowsItemsBetween("childdob", start,
                finish), new String[]{"relation", "childdob"}, context);
        Log.d(TAG, String.format("Found %s potential matches on DOB.", dateCursor.getCount()));
        Log.d(TAG, String.format("Found %s potential matches on Sex.", sexCursor.getCount()));

        baseCursor = mapVillages(new UnionCursor(ccDM.guardianDB.getColumnNamesWithRowID(),sexCursor,dateCursor));
        parseCursor = mapVillages(new UnionCursor(ccDM.guardianDB.getColumnNamesWithRowID(),sexCursor,dateCursor));

    }

    protected Cursor mapVillages(Cursor c) {
        return c;
    }
}


