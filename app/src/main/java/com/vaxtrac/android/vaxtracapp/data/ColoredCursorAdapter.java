package com.vaxtrac.android.vaxtracapp.data;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;

public class ColoredCursorAdapter extends SimpleCursorAdapter {

    int color1;
    int color2;

    public ColoredCursorAdapter(Context context, int layout, Cursor c,
                String[] from, int[] to) {
            super(context, layout, c, from, to);
        color1 = Color.argb(100, 235, 235, 235);
        color2 = Color.argb(150, 255, 255, 255);
    }



    public void setColors(int color1, int color2){
        this.color1 = color1;
        this.color2 = color2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        if(position % 2 == 0){
            view.setBackgroundColor(color1);
        }
        else {
            view.setBackgroundColor(color2);
        }
        return view;
    }
}

