package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.ColoredCursorAdapter;
import com.vaxtrac.android.vaxtracapp.data.FilteredCursor;
import com.vaxtrac.android.vaxtracapp.data.FilteredCursorFactory;
import com.vaxtrac.android.vaxtracapp.presenter.CallbacksListPresenter;
import com.vaxtrac.android.vaxtracapp.presenter.CustomCallbacksListPresenter;

import java.lang.ref.WeakReference;

public class CallbacksListView extends LinearLayout {

    WeakReference<Context> context;
    ImageView back;
    SimpleCursorAdapter adapter;
    HorizontalScrollView scrollView;
    ResizingListView list;
    EditText name;
    String nameFilter = "";

    CallbacksListPresenter presenter;

    private static final String TAG = "CallbacksListView";

    public CallbacksListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = new WeakReference<Context>(context);
        presenter = new CustomCallbacksListPresenter(context, this);
        AppController.mScreen.inject(presenter);
    }

    public String getNameFilter(){return nameFilter;}

    @Override
    protected void onDetachedFromWindow() {
        presenter.closeCursor();
        super.onDetachedFromWindow();
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        back = (ImageView) findViewById(R.id.back_button);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getFlow().goBack();
            }
        });

        list = (ResizingListView) findViewById(R.id.callback_table);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                presenter.callbackItemSelected((Cursor)parent.getItemAtPosition(i));
            }
        });

        scrollView = (HorizontalScrollView) findViewById(R.id.callbacks_scroll);
        scrollView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                list.setSmoothScrollbarEnabled(true);
                list.dispatchTouchEvent(event);
                return false;
            }
        });

        name = (EditText) findViewById(R.id.name_entry);
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null){
                    try {
                        Log.i(TAG, "ApplyFilter onTextChange");
                           updateAdapter(s.toString());
                    }catch (NullPointerException e){/*Rotation Change triggers...*/
                        e.printStackTrace();
                    }

                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });

        TextView nameHeader = (TextView) findViewById(R.id.list_item_text11);
        nameHeader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                 sortCallbackRows(CallbacksListPresenter.NAME_COLUMN);
            }
        });

        TextView dobHeader = (TextView) findViewById(R.id.list_item_text12);
        dobHeader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sortCallbackRows(CallbacksListPresenter.DOB_COLUMN);
            }
        });

        TextView sexHeader = (TextView) findViewById(R.id.list_item_text13);
        sexHeader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sortCallbackRows(CallbacksListPresenter.SEX_COLUMN);
            }
        });

        TextView callbackDateHeader = (TextView) findViewById(R.id.list_item_text14);
        callbackDateHeader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sortCallbackRows(CallbacksListPresenter.CALLBACK_DATE_COLUMN);
            }
        });

        TextView villageHeader = (TextView) findViewById(R.id.list_item_text16);
        villageHeader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sortCallbackRows(CallbacksListPresenter.VILLAGE_COLUMN);
            }
        });

        if (presenter.isOverdue()) {
            TextView title = (TextView) findViewById(R.id.callback_title);
            title.setText(getResources().getString(R.string.overdue));
            callbackDateHeader.setText(getResources().getString(R.string.expected_visit));
            LinearLayout tableRow = (LinearLayout) findViewById(R.id.callbacks_header);
            tableRow.setBackgroundColor(getResources().getColor(R.color.fbutton_color_alizarin));
        }

        ListLoader loader = new ListLoader();
        loader.execute();
    }

    void sortCallbackRows(String field) {
        Cursor c = presenter.updateCursor(field);
        adapter.swapCursor(c);
        adapter.notifyDataSetChanged();
    }

    public void updateAdapter(String name) {
        nameFilter = name;
        Cursor nameCursor = adapter.getFilterQueryProvider().runQuery(name);
        adapter.swapCursor(nameCursor);
        adapter.notifyDataSetChanged();
    }

    public void setFilter(final String column) {
        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence str) {
                if (str != null) {
                    final String parse_string = str.toString().toLowerCase();
                    Log.i(TAG, "parse " + parse_string);
                    FilteredCursor filtered = FilteredCursorFactory.createUsingSelector(presenter.parseCursor, new FilteredCursorFactory.Selector() {
                        int selectedIndex = -1;

                        @Override
                        public boolean select(Cursor cursor) {
                            if (selectedIndex == -1) {
                                selectedIndex = cursor.getColumnIndex(column);
                            }
                            if (cursor.getString(selectedIndex).toLowerCase().contains(parse_string)) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    });

                    return filtered;
                }
                return presenter.cursor;
            }
        });
        list.setAdapter(adapter);
    }

    private class ListLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            //presenter.closeCursor();
            presenter.prepareCursor();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            adapter = new ColoredCursorAdapter(context.get(),
                    R.layout.row_callbacklist,
                    presenter.cursor,
                    new String[]{CallbacksListPresenter.NAME_COLUMN,
                            CallbacksListPresenter.DOB_COLUMN,
                            CallbacksListPresenter.SEX_COLUMN,
                            CallbacksListPresenter.CALLBACK_DATE_COLUMN,
                            CallbacksListPresenter.MOBILE_COLUMN,
                            CallbacksListPresenter.VILLAGE_COLUMN,
                            CallbacksListPresenter.ADDRESS_COLUMN,
                            "guardianname"},
                    new int[]{R.id.cblist_item_text1, R.id.cblist_item_text2, R.id
                            .cblist_item_text3, R.id.cblist_item_text4, R.id.cblist_item_text5, R
                            .id.cblist_item_text6, R.id.cblist_item_text7, R.id.cblist_item_text8});
            list.setAdapter(adapter);
            setFilter(CallbacksListPresenter.NAME_COLUMN);
            Log.d(TAG, String.format("%s items in adapter", adapter.getCount()));
            super.onPostExecute(aVoid);

        }
    }
}
