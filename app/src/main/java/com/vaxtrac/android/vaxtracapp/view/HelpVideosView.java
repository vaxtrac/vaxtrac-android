package com.vaxtrac.android.vaxtracapp.view;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.VideoAdapter;
import com.vaxtrac.android.vaxtracapp.presenter.HelpVideosPresenter;

public class HelpVideosView extends FrameLayout implements MediaPlayer.OnPreparedListener, MediaPlayer
        .OnCompletionListener{

    private static final String TAG = "HelpVideosView";
    private static final String VIDEO_FOLDER_PATH = "videos";

    HelpVideosPresenter presenter;
    TextView textView;
    GridView gridView;
    VideoView videoView;
    VideoAdapter videoAdapter;
    MediaController mediaController;
    Context context;

    public HelpVideosView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        presenter = new HelpVideosPresenter(context, this);
        mediaController = new MediaController(context) {
            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                Log.d(TAG, "mediacontroller dispatchKeyEvent " + event.toString());
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    videoView.stopPlayback();
                    videoView.setVisibility(GONE);
                    gridView.bringToFront();
                    gridView.requestFocus();
                    return true;
                }
                return super.dispatchKeyEvent(event);
            }
        };
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        Log.d(TAG, "dispatchKeyEvent " + event.toString());
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && videoView.isPlaying()) {
            videoView.stopPlayback();
            videoView.setVisibility(GONE);
            gridView.bringToFront();
            gridView.requestFocus();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onFinishInflate() {
        textView = (TextView) findViewById(R.id.no_videos_message);
        gridView = (GridView) findViewById(R.id.video_grid);
        videoView = (VideoView) findViewById(R.id.video_view);
        setVideoViewFullScreen();
        videoView.setOnPreparedListener(this);
        videoView.setOnCompletionListener(this);
        gridView.setOnItemClickListener(new GridView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("HelpVideosView", "Item clicked " + gridView.getItemAtPosition(position));
                videoView.setVisibility(VISIBLE);
                videoView.requestFocus();
                Log.d(TAG, "videoview width " + videoView.getWidth() + " height " + videoView
                        .getHeight());
                videoView.bringToFront();
                videoView.setVideoPath(context.getExternalFilesDir(null) + "/" +
                        VIDEO_FOLDER_PATH + "/" + gridView.getItemAtPosition(position));
                videoView.setMediaController(mediaController);
                videoView.start();
            }
        });
        videoAdapter = new VideoAdapter(context);
        gridView.setAdapter(videoAdapter);
        if(videoAdapter.getCount() == 0) {
            showMessage();
        }
        super.onFinishInflate();
    }

    private void showMessage() {
        gridView.setVisibility(GONE);
        textView.setVisibility(VISIBLE);
        textView.setText(R.string.no_videos_found);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d("HelpVideosView", "video prepared");
        mp.setVolume(1,1);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    public void notifyVideoCopied(String filename) {
        videoAdapter.addVideo(filename);
    }

    public void setVideoViewFullScreen() {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        LayoutParams params = (android.widget.FrameLayout.LayoutParams) videoView.getLayoutParams();
        params.width =  metrics.widthPixels;
        params.height = metrics.heightPixels;
        videoView.setLayoutParams(params);
    }
}
