package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.presenter.LocalReportPresenter;

public class CustomReportPresenter extends LocalReportPresenter {

    public CustomReportPresenter(Context context, View view) {
        super(context, view);
    }
}
