package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by sam on 23/09/2016.
 */
public class CustomCallbacksListView extends CallbacksListView {
    public CustomCallbacksListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
