package com.vaxtrac.android.vaxtracapp.data;

import java.util.Map;

/**
 * Created by sam on 23/09/2016.
 */
public class CustomVillageFixtureParser extends VillageFixtureParser{
    public CustomVillageFixtureParser() {
        super();
    }

    public CustomVillageFixtureParser(String locale) {
        super(locale);
    }
}
