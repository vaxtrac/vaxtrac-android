package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;

import com.vaxtrac.android.vaxtracapp.presenter.CustomStatisticsPresenter;

/**
 * Created by sam on 10/20/16.
 */

public class CustomStatisticsView extends StatisticsReportView {
    public CustomStatisticsView(Context context) {
        super(context);
    }
    public CustomStatisticsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
