package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.presenter.LocalReportPresenter;
import com.vaxtrac.android.vaxtracapp.view.CustomReportView;

public class CustomReportPresenter extends LocalReportPresenter {

    private static final String TAG = CustomReportPresenter.class.getName();

    Context context;
    CustomReportView view;

    public CustomReportPresenter(Context context, View view) {
        super(context, view);
        this.context = context;
        this.view = (CustomReportView) view;
    }


}
