package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;

import com.vaxtrac.android.vaxtracapp.presenter.CustomReportPresenter;

public class CustomReportView extends LocalReportView {

    private static final String TAG = CustomReportView.class.getName();
    CustomReportPresenter presenter;

    public CustomReportView(Context context, AttributeSet attrs) {
        super(context, attrs);
        presenter = new CustomReportPresenter(context, this);
    }

}
