package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;

public class CustomStatisticsView extends StatisticsReportView {
    public CustomStatisticsView(Context context) {
        super(context);
    }

    public CustomStatisticsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
