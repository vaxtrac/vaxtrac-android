package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by sam on 10/18/16.
 */

public class CustomDemographicSearchView extends DemographicSearchView {
    public CustomDemographicSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
