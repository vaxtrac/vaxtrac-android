package com.vaxtrac.android.vaxtracapp.data;

import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.commcare.CommCareAPI;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

public class CustomVillageFixtureParser extends VillageFixtureParser{
    private static final String TAG = "CustomVilFixtureParser";
    private static final String CUSTOM_VILLAGE_ID_EXPRESSION = "//section_id/text()";
    private static final String CUSTOM_VILLAGE_NAME_EXPRESSION = "//name/text()";
    private static final String CUSTOM_USER_ID_EXPRESSION = "//user_id/text()";

    public CustomVillageFixtureParser() {
        super();
    }

    public CustomVillageFixtureParser(String locale) {
        super(locale);
    }

    @Override
    public void parse(String fixtureXml) {
        Log.d(TAG, "fixtureXml " + fixtureXml + " locale " + localeAttribute);
        try {
            XPath villagePath = XPathFactory.newInstance().newXPath();
            NodeList villageIds = (NodeList) villagePath.evaluate(
                    CUSTOM_VILLAGE_ID_EXPRESSION, new InputSource(new
                    StringReader(fixtureXml)), XPathConstants.NODESET);

            NodeList villageNames = (NodeList) villagePath.evaluate(CUSTOM_VILLAGE_NAME_EXPRESSION,
                    new InputSource(new StringReader(fixtureXml)), XPathConstants.NODESET);

            NodeList userIds = (NodeList) villagePath.evaluate(CUSTOM_USER_ID_EXPRESSION,
                    new InputSource(new StringReader(fixtureXml)), XPathConstants.NODESET);

            String currentUser = AppController.commCareAPI.getUserId();
            for (int i = 0, villageLength = villageNames.getLength(); i < villageLength; i++) {
                String villageId = villageIds.item(i).getNodeValue();
                getAllVillages().put(villageId, villageNames.item(i).getNodeValue());
                if (userIds.item(i).getNodeValue().equals(currentUser))
                    getCurrentUserVillages().add(villageId);
            }
        } catch (Exception exception) {
            Log.e(TAG, "Exception parsing village fixture", exception);
        }
    }


}
