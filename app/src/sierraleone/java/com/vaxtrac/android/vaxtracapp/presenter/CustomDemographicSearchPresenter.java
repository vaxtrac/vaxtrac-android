package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.database.Cursor;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.data.VillageMappingCursor;

/**
 * Created by sam on 10/18/16.
 */

public class CustomDemographicSearchPresenter extends DemographicSearchPresenter {
    private final String TAG = CustomDemographicSearchPresenter.class.getName();

    public CustomDemographicSearchPresenter(Context context) {
        super(context);
    }

    @Override
    protected Cursor mapVillages(Cursor c) {
        if (AppController.mCommCareDataManager.getAllVillages() != null)
            return new VillageMappingCursor(c, new String[]{"village"},
                    AppController.mCommCareDataManager.getAllVillages(), context);
        else return c;
    }
}

