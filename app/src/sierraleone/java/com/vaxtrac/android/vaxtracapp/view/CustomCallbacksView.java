package com.vaxtrac.android.vaxtracapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CustomCallbacksView extends CallbacksView {
    private static String TAG = "CustomCallbacksView";

    public CustomCallbacksView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected List<String> getVillageLabels(List<String> villages) {
        List<String> villageLabels = new ArrayList<>();
        Iterator<String> it = villages.iterator();
        while (it.hasNext()) {
            villageLabels.add(AppController.mCommCareDataManager.getAllVillages().get(it.next()));
        }
        villageLabels.add(0, AppController.getStringResource(R.string.all_villages));
        return villageLabels;
    }

    @Override
    protected String getVillage() {
        int selectedPos = super.villageSelect.getSelectedItemPosition();
        if (selectedPos > 0) {
            //Assumes there are no duplicate village names for a given user
            return getVillageIdFromName(villageAdapter.getItem(selectedPos));
        }
        else
            return null;
    }


    String getVillageIdFromName(String villageName) {
        if (villages == null)
            Log.d(TAG, "null villages");
        for (Map.Entry<String, String> entry : AppController.mCommCareDataManager.getAllVillages().entrySet()) {
            if (entry.getValue().equals(villageName) && villages.contains(entry.getKey())) {
                return entry.getKey();
            }
        }
        return null;
    }

}
