package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.AppController;
import com.vaxtrac.android.vaxtracapp.data.SortCursor;
import com.vaxtrac.android.vaxtracapp.data.VillageMappingCursor;

import java.util.LinkedList;
import java.util.List;

public class CustomCallbacksListPresenter  extends CallbacksListPresenter{
    String TAG = "CustomCBListPresenter";

    private boolean sortByVillage;
    private String sortOrder;

    public CustomCallbacksListPresenter(Context context, View view) {
        super(context, view);
        sortByVillage = false;
        sortOrder = ASC;
    }

    @Override
    public Cursor getOrderedCursor(String villageArg, String start, String finish,
                                   String field, String order){
        return getOrderedCursor(AppController.mCommCareDataManager.getCurrentUserVillages(), villageArg, start, finish, field, order);
    }

    @Override
    protected Cursor processCursor(Cursor c, String[] translationColumns) {
        if (sortByVillage)
            return new SortCursor(new VillageMappingCursor(super.processCursor(c, translationColumns),
                new String[]{VILLAGE_COLUMN}, villageList, context.get()), VILLAGE_COLUMN, sortOrder);
        return new VillageMappingCursor(super.processCursor(c, translationColumns),
                new String[]{VILLAGE_COLUMN}, villageList, context.get());
    }

    @Override
    public Cursor updateCursor(String field){
        String order;
        switch(field) {
            case NAME_COLUMN:
                sortOrder = super.isChildNameAsc() ? ASC : DESC;
                super.setChildNameAsc(!super.isChildNameAsc());
                super.setChildDobAsc(true);
                super.setChildSexAsc(true);
                super.setCallbackdateAsc(true);
                super.setVillageNameAsc(true);
                sortByVillage = false;
                break;
            case DOB_COLUMN:
                sortOrder = super.isChildDobAsc() ? ASC : DESC;
                super.setChildNameAsc(true);
                super.setChildDobAsc(!super.isChildDobAsc());
                super.setChildSexAsc(true);
                super.setCallbackdateAsc(true);
                super.setVillageNameAsc(true);
                sortByVillage = false;
                break;
            case SEX_COLUMN:
                sortOrder = super.isChildSexAsc() ? ASC : DESC;
                super.setChildNameAsc(true);
                super.setChildDobAsc(true);
                super.setChildSexAsc(!isChildSexAsc());
                super.setCallbackdateAsc(true);
                super.setVillageNameAsc(true);
                sortByVillage = false;
                break;
            case CALLBACK_DATE_COLUMN:
                sortOrder = isCallbackdateAsc() ? ASC : DESC;
                super.setChildNameAsc(true);
                super.setChildDobAsc(true);
                super.setChildSexAsc(true);
                super.setCallbackdateAsc(!super.isCallbackdateAsc());
                super.setVillageNameAsc(true);
                sortByVillage = false;
                break;
            case VILLAGE_COLUMN:
                sortOrder = super.isVillageNameAsc() ? ASC : DESC;
                super.setChildNameAsc(true);
                super.setChildDobAsc(true);
                super.setChildSexAsc(true);
                super.setCallbackdateAsc(true);
                super.setVillageNameAsc(!super.isVillageNameAsc());
                sortByVillage = true;
                break;
            default:
                sortOrder = ASC;
                sortByVillage = false;
                break;
        }
        cursor = getOrderedCursor(village, start, end, field, sortOrder);
        return cursor;
    }
}
