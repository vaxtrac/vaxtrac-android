package com.vaxtrac.android.vaxtracapp.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.presenter.LocalReportPresenter.ReportType;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;
import com.vaxtrac.nepalidatepickerlib.NepaliDateAlertBuilder;

import java.text.DateFormat;
import java.util.Calendar;

import dateconverter.Date;
import dateconverter.NepaliDateConverter;

public class CustomReportView extends LocalReportView {

    private static final String TAG = CustomReportView.class.getName();

    private NepaliDateAlertBuilder nepaliAlertBuilder;
    private AlertDialog nepaliDatePickerDialog;

    private static NepaliDateConverter converter = new NepaliDateConverter();
    private dateconverter.Date nepaliDate;

    private static DateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");

    public CustomReportView(Context context, AttributeSet attrs) {
        super(context, attrs);
        nepaliDate = new Date(presenter.getCalendarMonth() + 1, presenter.getCalendarDay(),
                presenter.getCalendarYear());
        nepaliAlertBuilder = new NepaliDateAlertBuilder(context, nepaliDate,
                getDialogSetListener());
        nepaliDatePickerDialog = nepaliAlertBuilder.create();
    }

    public DialogInterface.OnClickListener getDialogSetListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Date gregorianDate = converter.toGregorianDate(nepaliDate.toString());
                presenter.setCalendarYear(gregorianDate.getYear());
                presenter.setCalendarMonth(gregorianDate.getMonth() - 1);
                presenter.setCalendarDay(gregorianDate.getDay());
                if(isDailyReport()) {
                    selectedDate.setText(presenter.getDailyDateLabel());
                } else {
                    selectedDate.setText(presenter.getWeeklyDateLabel());
                }
                getReport(presenter.reportForPosition(period.getSelectedItemPosition()),
                        presenter.getCalendar());
                nepaliDatePickerDialog.dismiss();
            }
        };
    }

    public java.util.Date convertToJavaDate(dateconverter.Date date) {
        Calendar javaDate = Calendar.getInstance();
        javaDate.set(Calendar.YEAR, date.getYear());
        javaDate.set(Calendar.MONTH, date.getMonth() - 1);
        javaDate.set(Calendar.DAY_OF_MONTH, date.getDay());
        return javaDate.getTime();
    }

    @Override
    public View.OnClickListener getDateDialogListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                nepaliDatePickerDialog.show();
            }
        };
    }
}
