package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import dateconverter.NepaliDateConverter;
import dateconverter.NepaliMonthList;

public class CustomReportPresenter extends LocalReportPresenter {

    private static final String TAG = "CustomReportPresenter";
    private Context context;

    private dateconverter.Date today;

    private Locale currentLocale;

    private static NepaliDateConverter converter = new NepaliDateConverter();
    private static NepaliMonthList monthList = new NepaliMonthList();

    private static DateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");
    private static DateFormat timestampFormat = new UnlocalizedDateFormat("yyyy-MM-dd");
    private static DateFormat defaultDateFormat;

    private static NumberFormat localeFormat;

    private Map<Integer, Set<Integer>> uniqueDates;

    public CustomReportPresenter(Context context, View view) {
        super(context, view);
        this.context = context;
        currentLocale = context.getResources().getConfiguration().locale;
        localeFormat = NumberFormat.getIntegerInstance(currentLocale);
        /* Leave out commas */
        localeFormat.setGroupingUsed(false);
        uniqueDates = new HashMap<>();
        getDoseTimestampRange();
        today = converter.fromGregorianDate(nepaliDateFormat.format(calendar.getTime()));
        defaultDateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT,
                currentLocale);
    }

    @Override
    public int getCurrentYear() {
        return today.getYear();
    }

    @Override
    public int getCurrentMonth() {
        return today.getMonth();
    }

    @Override
    public Integer[] getYears() {
        Set<Integer> years = new TreeSet<>(getReportYears());
        years.addAll(uniqueDates.keySet());
        return years.toArray(new Integer[0]);
    }

    @Override
    public String[] getAdapterYears() {
        Integer[] years = getYears();
        String[] adapterYears = new String[years.length];
        for(int i = 0; i < years.length; i++) {
            adapterYears[i] = localeFormat.format(years[i]);
        }
        return adapterYears;
    }

    @Override
    public String[] getAdapterMonths(String year) {
        Integer[] months = getAvailableMonths(year);
        String[] adapterMonths = new String[months.length];
        for(int i = 0; i < months.length; i++) {
            if(months[i] < 10) {
                adapterMonths[i] = localeFormat.format(0) + localeFormat.format(months[i]);
            } else {
                adapterMonths[i] = localeFormat.format(months[i]);
            }
        }
        return adapterMonths;
    }

    public List<Integer> getReportYears() {
        return reportsManager.getReportYears();
    }

    @Override
    public String getDailyDateLabel() {
        dateconverter.Date convertedDate = converter.fromGregorianDate(nepaliDateFormat.format(calendar.getTime()));
        if(isNepaliLocale()) {
            return converter.toNepaliString(convertedDate);
        } else {
            return convertedDate.toString();
        }
    }

    @Override
    public String getWeeklyDateLabel() {
        if(isNepaliLocale()) {
            return converter.toNepaliString(getNepaliDate(getWeeklyStartDate(calendar))) + " - "
                    + converter.toNepaliString(getNepaliDate(getWeeklyEndDate(calendar)));
        } else {
            return getNepaliDate(getWeeklyStartDate(calendar)) + " - " + getNepaliDate
                    (getWeeklyEndDate(calendar));
        }

    }

    @Override
    public String getReportDateRange() {
        dateconverter.Date localizedStartDate = converter.fromGregorianDate(nepaliDateFormat
                .format(currentStartDate));

        if(isNepaliLocale()) {
            if(currentEndDate != null) {
                return converter.toNepaliString(localizedStartDate) + " - " + converter
                        .toNepaliString(converter.fromGregorianDate(nepaliDateFormat
                                .format(currentEndDate)));
            } else {
                return converter.toNepaliString(localizedStartDate);
            }
        } else {
            if(currentEndDate != null) {
                return getFormattedDate(localizedStartDate) + " - " + getFormattedDate
                        (converter.fromGregorianDate(nepaliDateFormat
                                .format(currentEndDate)));
            } else {
                return getFormattedDate(localizedStartDate);
            }
        }

    }

    public boolean isNepaliLocale() {
        return currentLocale.getISO3Language().equals("nep");
    }

    @Override
    public Integer[] getAvailableMonths(String year) {
        int parsedYear = -1;
        try {
            parsedYear = localeFormat.parse(year).intValue();
        } catch (ParseException e) {
            Log.e(TAG, "Parse Exception ", e);
        }
        Set<Integer> months = new TreeSet<>();
        Integer[] savedMonths = reportsManager.getReportMonths(parsedYear).toArray(new
                Integer[0]);
        Log.d(TAG, "Saved Report Months " + Arrays.toString(savedMonths));
        List<Integer> monthList = new ArrayList<>(Arrays.asList(savedMonths));
        months.addAll(monthList);

        Set<Integer> doseMonths = uniqueDates.get(parsedYear);
        if(doseMonths != null && doseMonths.size() > 0) {
            months.addAll(doseMonths);
        }
        Log.d(TAG, "months array " + Arrays.toString(months.toArray()));
        return months.toArray(new Integer[0]);
    }

    private void getDoseTimestampRange() {
        Date nextMonthTimestamp;
        Cursor sortedTimestamps = doseDB.allRowsUniqueColumnSorted("timestamp", "asc");
        if(sortedTimestamps != null && sortedTimestamps.moveToFirst()) {
            Log.d(TAG, "Unique dose count " + sortedTimestamps.getCount());
            int timestampColumn = sortedTimestamps.getColumnIndex("timestamp");

            String timestamp = sortedTimestamps.getString(timestampColumn);
            nextMonthTimestamp = getNepaliMonthEnd(timestamp);

            Set<Integer> months = new HashSet<>();
            Calendar nepaliMonth = getNepaliDate(timestamp);
            months.add(nepaliMonth.get(Calendar.MONTH) + 1);
            uniqueDates.put(nepaliMonth.get(Calendar.YEAR), months);

            Log.d(TAG, "Timestamp " + timestamp + " month end " + nextMonthTimestamp + " " +
                    nepaliMonth.getTime().toString());
            while(sortedTimestamps.moveToNext()) {
                timestamp = sortedTimestamps.getString(timestampColumn);
                Log.d(TAG, "timestamp " + timestamp);
                Date timestampDate;
                try {
                    timestampDate = timestampFormat.parse(timestamp);

                    if(timestampDate.after(nextMonthTimestamp)) {
                        Log.d(TAG, "After month end");
                        Calendar nepaliCal = getNepaliDate(timestamp);
                        Set<Integer> monthSet;
                        Integer nepYear = nepaliCal.get(Calendar.YEAR);
                        Integer nepMonth = nepaliCal.get(Calendar.MONTH) + 1;
                        if(uniqueDates.containsKey(nepYear)) {
                            monthSet = uniqueDates.get(nepYear);
                        } else {
                            monthSet = new HashSet<>();
                        }
                        monthSet.add(nepaliCal.get(Calendar.MONTH) + 1);
                        uniqueDates.put(nepYear, monthSet);

                        nextMonthTimestamp = getNepaliMonthEnd(nepYear, nepMonth).getTime();
                    } else {
                        Log.d(TAG, "Before month end");
                        continue;
                    }
                } catch (ParseException e) {
                    Log.e(TAG, "Couldn't parse dose timestamp");
                    continue;
                }
            }
            printUniqueDates();
        }
    }

    @Override
    public Date getMonthlyStartDate(Calendar selectedDate) {
        Calendar monthStart = Calendar.getInstance();
        monthStart.setTime(selectedDate.getTime());
        monthStart.set(Calendar.DAY_OF_MONTH, 1);
        Calendar monthStartAD = getGregorianDate(monthStart);
        return monthStartAD.getTime();
    }

    @Override
    public Date getMonthlyEndDate(Calendar selectedDate) {
        Calendar monthEnd = Calendar.getInstance();
        int days = monthList.get(selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar
                .MONTH) + 1);
        String lastDayOfMonth = String.format(Locale.US, "%02d", (selectedDate.get(Calendar
                .MONTH) + 1)) + "-" + String.format(Locale.US, "%02d", days) +
                "-" + selectedDate.get(Calendar.YEAR);
        dateconverter.Date monthEndAD = converter.toGregorianDate(lastDayOfMonth);
        monthEnd.set(Calendar.YEAR, monthEndAD.getYear());
        monthEnd.set(Calendar.MONTH, monthEndAD.getMonth() - 1);
        monthEnd.set(Calendar.DAY_OF_MONTH, monthEndAD.getDay());
        return monthEnd.getTime();
    }

    private void printUniqueDates() {
        for(Map.Entry<Integer, Set<Integer>> date : uniqueDates.entrySet()) {
            Log.d(TAG, "Year : " + date.getKey() + " " +
                    Arrays.toString(date.getValue().toArray()));
        }
    }

    public Date getNepaliMonthEnd(String gregorianTimestamp) {
        Log.d(TAG, "Nepali Month End Timestamp " + gregorianTimestamp);
        Calendar nepaliDate = getNepaliDate(gregorianTimestamp);
        Log.d(TAG, "Nepali Month End Nep Date " + nepaliDate.getTime().toString());
        Calendar nepaliMonthEnd = getNepaliMonthEnd(nepaliDate.get(Calendar.YEAR), nepaliDate.get
                (Calendar.MONTH) + 1);
        Log.d(TAG, "Nepali Month End Nep Date " + nepaliDate.getTime().toString());
        Log.d(TAG, "Gregorian Month End " + getGregorianDate(nepaliMonthEnd).getTime().toString());
        return getGregorianDate(nepaliMonthEnd).getTime();
    }

    public Calendar getNepaliDate(String gregorianDate) {
        Date gregorian;
        dateconverter.Date nepali;
        try {
            gregorian = timestampFormat.parse(gregorianDate);
            nepali = converter.fromGregorianDate(nepaliDateFormat.format
                    (gregorian));
        } catch (ParseException e) {
            Log.e(TAG, "Failed to parse database timestamp", e);
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, nepali.getYear());
        calendar.set(Calendar.MONTH, nepali.getMonth() - 1);
        calendar.set(Calendar.DAY_OF_MONTH, nepali.getDay());
        return calendar;
    }

    public Calendar getNepaliMonthEnd(int year, int month) {
        int days = monthList.get(year, month);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, days);
        return calendar;
    }

    public Calendar getGregorianDate(Calendar nepaliDate) {
        Log.d(TAG, "nepali date " + nepaliDate.getTime().toString());
        Log.d(TAG, "nepali date format " + nepaliDateFormat.format(nepaliDate.getTime()));
        dateconverter.Date gregorianDate = converter.toGregorianDate(nepaliDateFormat.format
                (nepaliDate.getTime()));
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, gregorianDate.getYear());
        cal.set(Calendar.MONTH, gregorianDate.getMonth() - 1);
        cal.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
        return cal;
    }

    public dateconverter.Date getNepaliDate(Date gregorianDate) {
        return converter.fromGregorianDate(nepaliDateFormat.format(gregorianDate));
    }

    public String getFormattedDate(dateconverter.Date nepaliDate) {
        return nepaliDate.toString();
    }

    public Locale getLocale() {
        return currentLocale;
    }
}
