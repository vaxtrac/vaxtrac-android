package com.vaxtrac.android.vaxtracapp.presenter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.BuildConfig;
import com.vaxtrac.android.vaxtracapp.data.GlobalTranslations;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;
import com.vaxtrac.android.vaxtracapp.view.CustomCallbacksView;
import com.vaxtrac.nepalidatepickerlib.NepaliDateAlertBuilder;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import dateconverter.NepaliDateConverter;


public class CustomCallbacksPresenter extends CallbacksPresenter {

    private static final String TAG = CustomCallbacksPresenter.class.getName();

    Locale locale;
    boolean isNepaliLocale;
    boolean isNepaliFlavor;

    private static NepaliDateConverter converter = new NepaliDateConverter();
    DialogInterface.OnClickListener listener;
    dateconverter.Date nepaliDate;

    DateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");

    CustomCallbacksView view;

    public CustomCallbacksPresenter(Context context, View view) {
        super(context, view);
        this.view = (CustomCallbacksView) view;
        locale = context.getResources().getConfiguration().locale;
        isNepaliLocale = locale.getISO3Language().equals("nep");
        isNepaliFlavor = BuildConfig.FLAVOR.equals("nepal");
        nepaliDate = new dateconverter.Date(0,0,0);
    }

    public DialogInterface.OnClickListener getStartingDateListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dateconverter.Date gregorianDate = converter.toGregorianDate(nepaliDate.toString());
                calendar.set(Calendar.YEAR, gregorianDate.getYear());
                calendar.set(Calendar.MONTH, gregorianDate.getMonth()-1);
                calendar.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
                setDate(calendar.getTime());
                dialog.dismiss();
            }
        };
    }

    public View.OnClickListener getCalendarListener() {
        NepaliDateAlertBuilder builder = new NepaliDateAlertBuilder(context, nepaliDate, getStartingDateListener());
        final AlertDialog dialog = builder.create();

        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.show();
            }
        };
    }

    public void setDate(Date date) {
        final String dateString;
        selectedDate = date;
        if(isNepaliLocale) {
           dateString = GlobalTranslations.dateToPrettyString(date, true);
        } else {
           dateString = converter.fromGregorianDate(nepaliDateFormat.format(date)).toString();
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    view.updateText(dateString);
                }catch (NullPointerException e){
                    Log.e(TAG, "Couldn't set date!");
                    e.printStackTrace();
                }
            }
        });
    }
}
