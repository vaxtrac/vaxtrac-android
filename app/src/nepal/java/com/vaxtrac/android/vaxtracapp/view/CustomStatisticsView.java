package com.vaxtrac.android.vaxtracapp.view;

import android.app.AlertDialog;
import android.content.Context;

import android.content.DialogInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;
import com.vaxtrac.nepalidatepickerlib.NepaliDateAlertBuilder;

import java.text.DateFormat;
import java.util.Calendar;

import dateconverter.Date;
import dateconverter.NepaliDateConverter;

public class CustomStatisticsView extends StatisticsReportView {

    private static final String TAG = CustomStatisticsView.class.getName();

    private boolean isNepaliLocale;
    private dateconverter.Date nepaliStartDate;
    private dateconverter.Date nepaliEndDate;
    private AlertDialog nepaliStartDateDialog;
    private AlertDialog nepaliEndDateDialog;

    private static NepaliDateConverter converter = new NepaliDateConverter();

    private DateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");

    public CustomStatisticsView(Context context) {
        super(context);
        init();
    }

    public CustomStatisticsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init() {
        isNepaliLocale = locale.getISO3Language().equals("nep");
        String todayNepali = nepaliDateFormat.format(calendar.getTime());
        nepaliStartDate = converter.fromGregorianDate(todayNepali);
        nepaliEndDate = converter.fromGregorianDate(todayNepali);
        nepaliStartDateDialog = new NepaliDateAlertBuilder(context, nepaliStartDate,
                getDialogStartDateListener(), !isNepaliLocale).create();
        nepaliEndDateDialog = new NepaliDateAlertBuilder(context, nepaliEndDate,
                getDialogEndDateListener(), !isNepaliLocale).create();
    }

    public dateconverter.Date getNepaliStartDate() {
        return converter.fromGregorianDate(nepaliDateFormat.format(startCal.getTime()));
    }

    public dateconverter.Date getNepaliEndDate() {
        return converter.fromGregorianDate(nepaliDateFormat.format(endCal.getTime()));
    }

    @Override
    public String getStartDateLabel() {
        Log.d(TAG, "getStartDateLabel nepali start date" + nepaliStartDate.toString());
        return getNepaliDateString(nepaliStartDate);
    }

    @Override
    public String getEndDateLabel() {
        return getNepaliDateString(nepaliEndDate);
    }

    private String getNepaliDateString(dateconverter.Date date) {
        Log.d(TAG, "getNepaliDateString " + date.toString());
        if(isNepaliLocale) {
            return converter.toNepaliString(date, true);
        } else {
            return date.toString();
        }
    }

    @Override
    public void updateStartDate(Calendar cal) {
        super.updateStartDate(cal);
        Log.d(TAG, "Updating start date " + cal.getTime());
        setNepaliStartDate(cal);
    }

    @Override
    public void updateEndDate(Calendar cal) {
        super.updateEndDate(cal);
        setNepaliEndDate(cal);
    }

    private void setNepaliStartDate(Calendar cal) {
        Log.d(TAG, "set nepali start date " + cal.getTime());
        nepaliStartDate = converter.fromGregorianDate(nepaliDateFormat.format(cal.getTime()));
    }


    private void setNepaliEndDate(Calendar cal) {
        nepaliEndDate = converter.fromGregorianDate(nepaliDateFormat.format(cal.getTime()));
    }

    @Override
    protected View.OnClickListener getStartDateListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                nepaliStartDate = getNepaliStartDate();
                nepaliStartDateDialog = new NepaliDateAlertBuilder(context, nepaliStartDate,
                        getDialogStartDateListener(), !isNepaliLocale).create();
                nepaliStartDateDialog.show();
            }
        };
    }

    @Override
    protected View.OnClickListener getEndDateListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                nepaliEndDate = getNepaliEndDate();
                nepaliEndDateDialog = new NepaliDateAlertBuilder(context, nepaliEndDate,
                        getDialogEndDateListener(), !isNepaliLocale).create();
                nepaliEndDateDialog.show();
            }
        };
    }

    private DialogInterface.OnClickListener getDialogStartDateListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "nepali start date " + nepaliStartDate.toString());
                Date gregorianDate = converter.toGregorianDate(nepaliStartDate.toString());
                /* Handle overflow to next month if date is greater than days in month */
                nepaliStartDate = converter.fromGregorianDate(gregorianDate.toString());
                Log.d(TAG, "gregorian start date " + gregorianDate);
                startCal.set(Calendar.YEAR, gregorianDate.getYear());
                startCal.set(Calendar.MONTH, gregorianDate.getMonth() - 1);
                startCal.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
                dialog.dismiss();
                startDate.setText(getStartDateLabel());
                presenter.reporPeriodChanged(reportPosition, getStartDate(), getEndDate());
            }
        };
    }

    private DialogInterface.OnClickListener getDialogEndDateListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "nepali end date " + nepaliEndDate.toString());
                Date gregorianDate = converter.toGregorianDate(nepaliEndDate.toString());
                /* Handle overflow to next month if date is greater than days in month */
                nepaliEndDate = converter.fromGregorianDate(gregorianDate.toString());
                Log.d(TAG, "gregorian end date " + gregorianDate);
                endCal.set(Calendar.YEAR, gregorianDate.getYear());
                endCal.set(Calendar.MONTH, gregorianDate.getMonth() - 1);
                endCal.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
                dialog.dismiss();
                endDate.setText(getEndDateLabel());
                presenter.reporPeriodChanged(reportPosition, getStartDate(), getEndDate());
            }
        };
    }
}