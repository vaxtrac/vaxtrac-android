package com.vaxtrac.android.vaxtracapp.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vaxtrac.android.vaxtracapp.R;
import com.vaxtrac.android.vaxtracapp.data.ChildVisitsTask;
import com.vaxtrac.android.vaxtracapp.data.ChildrenRegisteredTask;
import com.vaxtrac.android.vaxtracapp.data.VialDosesUsedTask;
import com.vaxtrac.android.vaxtracapp.data.VialsCreatedTask;
import com.vaxtrac.android.vaxtracapp.utils.UnlocalizedDateFormat;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Map;

import dateconverter.Date;
import dateconverter.NepaliDateConverter;
import dateconverter.NepaliMonthList;

public class CustomStatisticsPresenter extends StatisticsReportPresenter {

    private NepaliDateConverter converter = new NepaliDateConverter();
    private NepaliMonthList monthList = new NepaliMonthList();
    private DateFormat nepaliDateFormat = new UnlocalizedDateFormat("MM-dd-yyyy");
    private boolean isNepaliLocale;

    public CustomStatisticsPresenter(Context context, View view) {
        super(context, view);
        isNepaliLocale = context.getResources().getConfiguration().locale.getISO3Language()
                .equals("nep");
    }

    @Override
    protected Calendar getPriorMonth(Calendar currentDay) {
        Calendar priorMonth = Calendar.getInstance();
        dateconverter.Date nepaliDate = converter.fromGregorianDate(nepaliDateFormat.format
                (currentDay.getTime()));
        dateconverter.Date priorNepaliDate;
        int nepaliMonth = nepaliDate.getMonth();
        int nepaliYear = nepaliDate.getYear();
        int nepaliDay = nepaliDate.getDay();
        int pastDaysInMonth;
        /* Previous month would have a different year */
        if(nepaliMonth != 1) {
            int lastMonth = nepaliMonth - 1;
            pastDaysInMonth = monthList.get(nepaliYear, lastMonth);
            if(pastDaysInMonth < nepaliDay) {
                priorNepaliDate = new Date(lastMonth, pastDaysInMonth, nepaliYear);
            } else {
                priorNepaliDate = new Date(lastMonth, nepaliDay, nepaliYear);
            }
        } else {
            int lastYear = nepaliYear - 1;
            pastDaysInMonth = monthList.get(lastYear, 12);
            if(pastDaysInMonth < nepaliDay) {
                priorNepaliDate = new Date(12, pastDaysInMonth, lastYear);
            } else {
                priorNepaliDate = new Date(12, nepaliDay, lastYear);
            }
        }
        dateconverter.Date gregorianDate = converter.toGregorianDate(priorNepaliDate.toString());
        priorMonth.set(gregorianDate.getYear(), gregorianDate.getMonth() - 1,
                gregorianDate.getDay());
        return priorMonth;
    }

    @Override
    protected Calendar getNextMonth(Calendar currentDay) {
        Calendar nextMonth = Calendar.getInstance();
        dateconverter.Date nepaliDate = converter.fromGregorianDate(nepaliDateFormat.format
                (currentDay.getTime()));
        dateconverter.Date nextNepaliDate;
        int nepaliMonth = nepaliDate.getMonth();
        int nepaliYear = nepaliDate.getYear();
        int nepaliDay = nepaliDate.getDay();
        int futureDaysInMonth;
        /* Previous month would have a different year */
        if(nepaliMonth != 12) {
            int nextMonthNum = nepaliMonth + 1;
            futureDaysInMonth = monthList.get(nepaliYear, nextMonthNum);
            if(futureDaysInMonth < nepaliDay) {
                nextNepaliDate = new Date(nextMonthNum, futureDaysInMonth, nepaliYear);
            } else {
                nextNepaliDate = new Date(nextMonthNum, nepaliDay, nepaliYear);
            }
        } else {
            int nextYear = nepaliYear + 1;
            futureDaysInMonth = monthList.get(nextYear, 1);
            if(futureDaysInMonth < nepaliDay) {
                nextNepaliDate = new Date(1, futureDaysInMonth, nextYear);
            } else {
                nextNepaliDate = new Date(1, nepaliDay, nextYear);
            }
        }
        dateconverter.Date gregorianDate = converter.toGregorianDate(nextNepaliDate.toString());
        nextMonth.set(gregorianDate.getYear(), gregorianDate.getMonth() - 1,
                gregorianDate.getDay());
        return nextMonth;
    }

    @Override
    protected Calendar getFirstDayOfMonth(Calendar date) {
        dateconverter.Date nepaliDate = converter.fromGregorianDate(nepaliDateFormat.format(date
                .getTime()));
        nepaliDate.setDay(1);
        dateconverter.Date gregorianDate = converter.toGregorianDate(nepaliDate.toString());
        Calendar monthStart = Calendar.getInstance();
        monthStart.set(Calendar.YEAR, gregorianDate.getYear());
        monthStart.set(Calendar.MONTH, gregorianDate.getMonth() - 1);
        monthStart.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
        return setTimeStart(monthStart);
    }

    @Override
    protected Calendar getLastDayOfMonth(Calendar date) {
        dateconverter.Date nepaliDate = converter.fromGregorianDate(nepaliDateFormat.format(date
                .getTime()));
        int lastDayOfMonth = monthList.get(nepaliDate.getYear(), nepaliDate.getMonth());
        nepaliDate.setDay(lastDayOfMonth);
        dateconverter.Date gregorianDate = converter.toGregorianDate(nepaliDate.toString());
        Calendar monthEnd = Calendar.getInstance();
        monthEnd.set(Calendar.YEAR, gregorianDate.getYear());
        monthEnd.set(Calendar.MONTH, gregorianDate.getMonth() - 1);
        monthEnd.set(Calendar.DAY_OF_MONTH, gregorianDate.getDay());
        return setTimeEnd(monthEnd);
    }

    private String getNepaliDateText(dateconverter.Date date) {
        if(isNepaliLocale) {
            return converter.toNepaliString(date);
        } else {
            return date.toString();
        }
    }

    private String formatNepaliDate(Calendar cal) {
        return nepaliDateFormat.format(cal.getTime());
    }

    private dateconverter.Date convertNepaliDate(String gregorianDate) {
        return converter.fromGregorianDate(gregorianDate);
    }

    @Override
    protected void updateDateRange(Calendar startDate, Calendar endDate) {
        String range = getNepaliDateText(convertNepaliDate(formatNepaliDate(startDate)));

        if(!(view.getReportPosition() == DAILY)) {
            range += " - " + getNepaliDateText(convertNepaliDate(formatNepaliDate(endDate)));
        }

        view.updateDateRange(range);
    }
}