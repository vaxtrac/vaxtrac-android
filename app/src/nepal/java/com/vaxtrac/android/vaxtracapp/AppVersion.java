package com.vaxtrac.android.vaxtracapp;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sarwar on 3/18/15.
 *
 * Implementation specific feature controls.
 * Overrides defaults set in DefaultAppFlags
 */
public class AppVersion extends DefaultAppFlags {

    private static final Map<String, Boolean> versionOptions = new HashMap<String, Boolean>(){{
        put("ocr", true);
        put("card-number-search", true);
        put("mobile-search", false);
        put("name-search", false);
    }};

    protected static boolean getVersionOption(String option){
        return versionOptions.get(option);
    }

    protected static boolean versionOptionExists(String option){
        return versionOptions.containsKey(option);
    }

    public static boolean isEnabled(String option){
        try{
            if (versionOptionExists(option)){
                return getVersionOption(option);
            }else if(defaultOptions.containsKey(option)){
                return defaultOptions.get(option);
            }else{
                return false;
            }
        }catch (Exception e){
            Log.e(TAG, "Error finding option " + option);
            return false;
        }
    };

}
