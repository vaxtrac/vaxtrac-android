**VaxTrac Android**

Offline first vaccination registry with country specific scheduling logic, aggregate reporting, fingerprint verification/matching, quick barcode search, and vaccine vial capture.

**Requirements**
*  [CommCareHQ](https://commcarehq.org) with advanced subscription plan
*  [CommCare Android](https://github.com/dimagi/commcare-android) version 2.19+ needs to be installed on the device
*  Biometrac application for scanning fingerprints
*  [Barcode Scanner](https://play.google.com/store/apps/details?id=com.google.zxing.client.android) application 
*  File Manager (for offline installation)
*  [More Locale 2](https://play.google.com/store/apps/details?id=jp.co.c_lis.ccl.morelocale) (for full Nepali language support)
*  [ACRA](https://github.com/ACRA/acra) Offline crash reports

**Organization**

Product flavors are by country Benin, Nepal, and Sierra Leone.

The Nepal flavor supports vaccine vial management and optical character recognition of the lot/batch number and expiration/manufacture dates.  Credit to the [Tesseract](https://github.com/tesseract-ocr) and Robert Theis for the [original code](https://github.com/rmtheis/android-ocr).

Data is persisted in the encrypted [CommCare Content Provider](https://github.com/dimagi/commcare-android/wiki/APIs) and cached in the VaxTrac app's Sqlite database.

App screen navigation relies on an older version of Square's [Flow](https://github.com/square/flow) library